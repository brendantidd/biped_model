import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
from pathlib import Path
home = str(Path.home())

class TF_PCA():
  def __init__(self, data, X=None):
    # self.X = X
    self.data = data
    self.target = X
    self.dtype = tf.float32

  def fit(self):
    # self.graph = tf.Graph()
    # with self.graph.as_default():
    self.X = tf.placeholder(self.dtype, shape=self.data.shape)
    # Perform SVD
    singular_values, u, _ = tf.svd(self.X)
    # Create sigma matrix
    sigma = tf.diag(singular_values)
    # with tf.Session(graph=self.graph) as session:
    session =  tf.get_default_session()
    self.u, self.singular_values, self.sigma = session.run([u, singular_values, sigma], feed_dict={self.X: self.data})

  def reduce(self, n_dimensions=None, keep_info=None):
    if keep_info:
      # Normalize singular values
      normalized_singular_values = self.singular_values / sum(self.singular_values)
      # Create the aggregated ladder of kept information per dimension
      ladder = np.cumsum(normalized_singular_values)
      # Get the first index which is above the given information threshold
      index = next(idx for idx, value in enumerate(ladder) if value >= keep_info) + 1
      n_dimensions = index
    # with self.graph.as_default():
    # Cut out the relevant part from sigma
    sigma = tf.slice(self.sigma, [0, 0], [self.data.shape[1], n_dimensions])
    # PCA
    pca = tf.matmul(self.u, sigma)
    session = tf.get_default_session()
    # with tf.Session(graph=self.graph) as session:
    return session.run(pca, feed_dict={self.X: self.data})

def pca_plot():  
  '''
  single pca
  '''
  import gc
  # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.80)
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.5)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                        intra_op_parallelism_threads=1,             
                                        gpu_options=gpu_options), graph=None)
  # os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  PATH = home + '/results/biped_model/latest/s/com_data/'             
  flat_obs = np.load(PATH + 'flat_obs.npy')
  hj_obs = np.load(PATH + 'hj_obs.npy')
  setup_obs = np.load(PATH + 'setup_obs.npy')
  flat_com = np.load(PATH + 'flat_com.npy')
  hj_com = np.load(PATH + 'hj_com.npy')
  setup_com = np.load(PATH + 'setup_com.npy')

  # print(flat_obs.shape, flat_com.shape)
  flat_obs = np.concatenate([flat_obs, flat_com], axis=1)
  setup_obs = np.concatenate([setup_obs, setup_com], axis=1)
  hj_obs = np.concatenate([hj_obs, hj_com], axis=1)

  print(flat_obs.shape, hj_obs.shape, setup_obs.shape)

  # self.motor_names = ["right_hip_z", "right_hip_x", "right_hip_y", "right_knee"]
  # self.motor_names += ["right_ankle_x", "right_ankle_y"]
  # self.motor_names += ["left_hip_z", "left_hip_x", "left_hip_y", "left_knee"]
  # self.motor_names += ["left_ankle_x", "left_ankle_y"]

  # idx = [i for i in range(12)] + [j for j in range(24,33)]
  # idx = [i for i in range(12)] + [j for j in range(32,50)]
  # idx = [i for i in range(12)] + [53,54,55]
  # idx = [i for i in range(12)] + [i for i in range(24,56)]
  # idx = [i for i in range(24,56)]
  # idx = [i for i in range(12)] + [53]
  # idx = [2,3,53]
  idx = [2,3,56]
  # idx = [i for i in range(56)] 
  # data = np.concatenate([flat_obs, hj_obs, setup_obs])[:,:12]
  # print(flat_obs[:,32])
  # print(hj_obs[:,32])
  min_length = min(flat_obs.shape[0], hj_obs.shape[0], setup_obs.shape[0])
  # min_length = 100
  # data = np.concatenate([flat_obs, hj_obs, setup_obs])[:,idx]
  # print(min_length)
  # data = np.concatenate([flat_obs[:min_length,:], setup_obs[:min_length,:], hj_obs[:min_length,:]])[:,idx]
  # data = np.concatenate([flat_obs[:min_length,:], setup_obs[:min_length,:], hj_obs[:min_length,:]])[:,idx]
  data = np.concatenate([flat_obs[:,:], setup_obs[:,:], hj_obs[:,:]])[:,idx]
  print(flat_obs[:min_length,:].shape, setup_obs[:min_length,:].shape, hj_obs[:min_length,:].shape) 
  print(data.shape, flat_obs.shape, hj_obs.shape, setup_obs.shape)
  first = flat_obs.shape[0]
  second = hj_obs.shape[0]
  tf_pca = TF_PCA(data)
  tf_pca.fit()
  # pca = tf_pca.reduce(keep_info=0.5)
  pca = tf_pca.reduce(keep_info=0.9)
  fig = plt.figure(1, figsize=(8, 6))       
  plt.scatter(pca[second:, 0], pca[second:,1], s=10,  c='g')
  plt.scatter(pca[first:second, 0], pca[first:second,1], s=10,  c='b')
  plt.scatter(pca[:first, 0], pca[:first,1], s=10,  c='r')
  # print(pca.shapepca[first:second, 0], pca[first:second,1])
  # plt.legend(['Flat', 'Setup', 'Jump'])
  # plt.ylim([-1, 1])
  # plt.xlim([-1.5, 1])
  # plt.ylim([-0.5,1.5])
  # plt.xlim([-3.5, -2])
  plt.legend(['Jump', 'Setup', 'Flat'])
  
  plt.savefig('./data/data/pca_com.png', bbox_inches='tight')
  plt.close(fig) 
  

  fig = plt.figure(1, figsize=(8, 6))       
  plt.scatter(pca[second:, 0], pca[second:,1], s=10,  c='g')
  plt.ylim([-1, 1])
  plt.xlim([-1, 1])
  plt.savefig('./data/data/pca_jump.png', bbox_inches='tight')
  plt.close(fig) 

  fig = plt.figure(1, figsize=(8, 6))       
  plt.scatter(pca[first:second, 0], pca[first:second,1], s=10,  c='b')
  plt.ylim([-1, 1])
  plt.xlim([-1, 1])
  plt.savefig('./data/data/pca_setup.png', bbox_inches='tight')
  plt.close(fig) 

  fig = plt.figure(1, figsize=(8, 6))       
  plt.scatter(pca[:first, 0], pca[:first,1], s=10,  c='r')
  plt.ylim([-1, 1])
  plt.xlim([-1, 1])
  plt.savefig('./data/data/pca_flat.png', bbox_inches='tight')
  plt.close(fig) 
  
  # labels1 = [i for i in range(4) for _ in range(not_select.shape[1])]
  # data1 = np.concatenate([not_select_data, select[0]])
  # # print(data1.shape)
  # labels1.extend(4 for _ in range(select.shape[1]))
  # pca = []
  # colors = []
  # for i in range(5):
  #     if i == 4:
  #         tf_pca = TF_PCA(np.array(select[0]))
  #         data_len = select.shape[1]
  #     else:
  #         tf_pca = TF_PCA(np.array(not_select[i]))
  #         data_len = not_select.shape[1]
  #     tf_pca.fit()
  #     pca.append(tf_pca.reduce(keep_info=0.9))
  #     colors.extend([tableau20[i*2] for _ in range(data_len)])

  # # pca = np.array(pca)
  # # print(pca.shape)
  # # color_mapping = {0: tableau20[0], 1: tableau20[2], 2: tableau20[4], 3: tableau20[6], 4: tableau20[8]}
  # # color_mapping = {2: tableau20[0], 4: tableau20[2]}
  # # colors = list(map(lambda x: color_mapping[x], tf_pca.target))
  # fig = plt.figure(1, figsize=(8, 6))       
  # plt.scatter(pca[0][:, 0], pca[0][:,1], s=10,  c=tableau20[0])
  # plt.scatter(pca[1][:, 0], pca[1][:,1], s=10,  c=tableau20[2])
  # plt.scatter(pca[2][:, 0], pca[2][:,1], s=10,  c=tableau20[4])
  # plt.scatter(pca[3][:, 0], pca[3][:,1], s=10,  c=tableau20[6])
  # plt.scatter(pca[4][:, 0], pca[4][:,1], s=10,  c=tableau20[8])
  # # plt.scatter(pca[g, 0], pca[g,1], s=10, c=tableau20[14])
  # # plt.legend(['Down', 'Up', 'Flat', 'Balance'])
  # plt.savefig('/home/brendan/results/elements/select_doa/' + 'sep.png', bbox_inches='tight')
  # plt.close(fig) 

if __name__=="__main__":
  pca_plot()