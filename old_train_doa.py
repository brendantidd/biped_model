import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt

def run(args):

  PATH = home + '/results/biped_model/latest/d/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from assets.env import Env
  # env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, doa=True)
  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False)

  if args.lstm_pol:
    from models.ppo_lstm import Model
    # from models.ppo_lstm_old import Model
  else:
    from models.ppo import Model

  pol = Model(args.exp, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  # from models.doa import DoANet
  # doa_net = DoANet(name="doa_" + args.obstacle_type, args=args, ob_size=env.ob_size, im_size=env.im_size)

  initialize()
  # sync_from_root(sess, doa_net.vars, comm=comm)
  # doa_net.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)
  # pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if args.hpc:
    # pol.load(home + '/hpc-home/results/biped_model/weights/' + args.folder + '/' + args.sub_folder + '/' + args.obstacle_type + '/')      
    # pol.load(home + '/hpc-home/results/biped_model/latest/' + args.exp + '/')      
    pol.load(home + '/hpc-home/results/biped_model/weights/w6/base_speed/')      
  else:
    # pol.load(home + '/results/biped_model/weights/' + args.folder + '/' + args.sub_folder + '/' + args.obstacle_type + '/')
    pol.load(home + '/results/biped_model/weights/b2/' + args.exp + '/')

  # if args.test_pol:
  #   doa_net.load(PATH)

  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()

  prev_done = True

  # env.obstacles.train_world()

  # print(env.ob_size)
  ob = env.reset()
  im = env.get_im()
  ep_steps = 0
  iters_so_far = 0
  timesteps_so_far = 0
  stochastic = False
  t1 = time.time()
  plot_data = []
  vpred_data = []
  all_vpred_data = []
  slope_data = []
  prev_vpred = 0
  prev_prob = 0
  death_countdown = None
  slope = deque(maxlen=5)
  ep_count = 0
  while True:
    if timesteps_so_far > args.max_ts:
      break 
    if args.lstm_pol:
      act, vpred, pol_states, nlogp = pol.step(ob, im, stochastic=stochastic, states=pol_states)
    else:
      act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)

    torques = act
    if args.test_pol:
      vpred_data.append(vpred)
      # if ep_count > 0:
      # dpred = doa_net.step(ob, im)
      # plot_data.append(dpred)
      prob = calc_probs(vpred, all_vpred_data)
      if env.steps == 0:
        prev_vpred = vpred
        prev_prob = prob
      # print(prob)
      death = prob < -4 and prob < prev_prob
      plot_data.append(death)
      # slope.append(vpred - prev_vpred)
      # slope_data.append(max(slope))
      slope_data.append(vpred - prev_vpred)
      # all_vpred_data.append(vpred)
      # next_ob, rew, done, _ = env.step(torques, dpred)
      next_ob, rew, done, _ = env.step(torques)
    else:
      next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_im()

    # print(vpred, prev_vpred, env.steps)
    if death and death_countdown is None and env.steps > 10:
      death_countdown = env.steps
      print("gonna die!", env.steps)
      env.speed = 0.0

    prev_vpred = vpred
    prev_prob = prob
    if not args.test_pol:
      if args.gae:
        doa_net.buffer.add(ob, im, done)
      else:
        dpred = doa_net.step(ob, im)
        doa_net.buffer.add(ob, im, done, dpred)

    prev_done = done
    ob = next_ob
    im = next_im
    
    ep_steps += 1
    
    # if not args.test_pol and doa_net.buffer.size == doa_net.buffer.buffer_size and ep_steps % horizon == 0:
    if not args.test_pol and ep_steps % horizon == 0:
      if args.gae:
        last_dpred = doa_net.step(ob, im)
        loss = doa_net.train(last_dpred, done)
      else:
        loss = doa_net.train()
      if rank == 0:
        print("Iterations {0:d} Loss {1:.6f} Time {2:.4f}".format(iters_so_far, loss, time.time() - t1))
        t1 = time.time()
        doa_net.save(PATH)
        writer.add_scalar("loss", loss, iters_so_far)   
      iters_so_far += 1

    if done:    
      slope = deque(maxlen=5)
      if death_countdown is None:
        print("didn't predict death")
      else:
        print("predicted death", env.steps - death_countdown, "steps ago")
      if args.test_pol:
        if len(vpred_data) > 150:
          all_vpred_data.extend(vpred_data[:-150])
        print(len(all_vpred_data))
        probs = calc_probs(vpred_data, all_vpred_data)
        if args.plot:
          # fig = plt.figure()
          # ax=fig.add_subplot(121)
          num_axes = 4
          fig, ax = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
          # slope_data = reject_outliers(np.array(slope_data))
          ax[0].plot([_ for _ in range(len(plot_data))], plot_data, c='b', alpha=1.0)  
          ax[0].set_ylim([0,1])
          ax[1].plot([_ for _ in range(len(vpred_data))], vpred_data, c='b', alpha=1.0)  
          ax[2].plot([_ for _ in range(len(slope_data))], slope_data, c='b', alpha=1.0)  
          
          ax[3].plot([_ for _ in range(len(probs))], probs, c='b', alpha=1.0)  
          plt.show()
        plot_data = []
        vpred_data = []
        slope_data = []
      ep_count += 1
      death_countdown = None
      prev_vpred = 0
      ob = env.reset()
      im = env.get_im()
      if args.lstm_pol: 
        pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]    
   
def calc_probs(data, all_data):
  data = np.array(data)
  all_data = np.array(all_data)
  # print(all_data.mean(), all_data.std())
  # print(all_data.shape)
  # return (data - all_data.mean())/all_data.std()
  return (data - 87.97)/1.97

# def reject_outliers(data, m=2.):
#   d = np.abs(data - np.median(data))
#   mdev = np.median(d)
#   s = d / (mdev if mdev else 1.)
#   return data[s < m]

if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--sub_folder', default='b')
  parser.add_argument('--folder', default='w5')

  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')
  parser.add_argument('--gae', default=False, action='store_true')
  parser.add_argument('--doa', default=True, action='store_false')

  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--vis', default=False, action='store_true')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="base", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=1e5, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)