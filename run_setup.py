import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2
import json
import git
np.set_printoptions(precision=3, suppress=True)

def run(args):

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  num_workers = comm.Get_size()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)

  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    with open(PATH + 'commandline_args.txt', 'w') as f:
      f.write('Hash:')
      f.write(str(sha) + "\n")
      json.dump(args.__dict__, f, indent=2)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  # horizon = 2048
  # horizon = 1024
  # horizon = 763
  horizon = 1440
  # horizon = 512

  from assets.env import Env

  env = Env(render=args.render, PATH=PATH, args=args, obstacle_type=args.obstacle_type, vis=True, disturbances=False, vis_type=args.vis_type)

  from models.ppo_setup import Model

  obstacle_types = ['flat', args.obstacle_type]
  start_obstacle_types = ['flat']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
  # start_obstacle_types = ['gaps', 'base', 'jumps','stairs']
  # if args.obstacle_type != 'zero':
  #   start_obstacle_types.remove(args.obstacle_type)

  pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis, const_std=args.const_std) for name in obstacle_types if name != 'zero'}  

  # if args.term:
  #   setup_pol = Model(args.obstacle_type + "_setup", env=env, ob_size=env.ob_size, ac_size=env.ac_size+1, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis, const_std=args.const_std)
  # else:
  setup_pol = Model(args.obstacle_type + "_setup", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis, const_std=args.const_std)
  
  # initialize()

  # if args.hpc:
  #   # WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/'+ 'b10_vis' 
  #   WEIGHTS_PATH = home + '/hpc-home/results/biped_model/latest/b12' 
  # else:
  #   # WEIGHTS_PATH = home + '/results/biped_model/weights/'+ 'b10_vis' 
  #   WEIGHTS_PATH = home + '/results/biped_model/latest/' + 'b' 
  
  # if not args.blank_pol:
    # setup_pol.load_base(base_name=args.obstacle_type, WEIGHT_PATH=WEIGHTS_PATH + '/' + args.obstacle_type + '/')
  # if args.test_pol:
  #   setup_pol.load(WEIGHT_PATH=home + '/results/biped_model/latest/' + 's' + '/gaps_fixed/')
  # else:
  #   if not args.term:
  #     # setup_pol.load_base(base_name='flat', WEIGHT_PATH=WEIGHTS_PATH + '/flat_base2/')
  #     setup_pol.load_base(base_name='flat', WEIGHT_PATH=home + '/hpc-home/results/biped_model/latest/b12/flat_base2/')

  # for name in obstacle_types:
    # if name == 'zero': continue

  # pol['flat'].load(home + '/hpc-home/results/biped_model/latest/b12/flat_base2/')

  # setup_pol.load_base(base_name='flat', WEIGHT_PATH=home + '/results/biped_model/latest/setup/flat/')
  # pol['flat'].load(home + '/results/biped_model/latest/setup/flat/')
  # pol['high_jumps'].load( home + '/results/biped_model/latest/setup/high_jumps/')
  if args.test_pol:
    # setup_pol.load(home + '/hpc-home/results/biped_model/latest/setup/s3/high_jumps_adv/')
    setup_pol.load('./weights/high_jumps_adv/')
  elif args.dqn:
    setup_pol.load('./weights/high_jumps_adv/')
  else:
    setup_pol.load_base(base_name='flat', WEIGHT_PATH='./weights/flat/')
  pol['flat'].load('./weights/flat/')
  pol['high_jumps'].load('./weights/high_jumps/')

  if args.term or args.dqn:
    setup_pol.setup_term()

  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)

  setup_pol.setup_training()

  initialize_uninitialized()
  # initialize()
  sync_from_root(sess, setup_pol.vars, comm=comm)
  setup_pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  

  env.difficulty = 10
  env.height_coeff = 0.07

  prev_done = True
  prev_setup_done = True

  # Load data
  # data_pointer = 0
  # data = np.load(home + "/results/biped_setup/latest/d/samples/" + args.obstacle_type + "/goal.npy")
  
  current_pol = np.random.choice(start_obstacle_types) 
  initial_pol = start_obstacle_types.index(current_pol)

  ob = env.reset()
  im = env.get_im()
  ep_ret = 0
  ep_len = 0
  term_rew = []
  term_not_rew = []
  ep_rets = []
  ep_lens = []
  ep_steps = 0

  detection_dist = 0.9
  artifact_x = 0
  dist_to_artifact = 0
  prev_detected = detected = None
  
  current_pol_data = []
  
  feet_data = {'left':[],'right':[]}
  com_data = []
  switch_data = []
  terrain_data = []
  max_z_height = 0
  params = {"Success":[], "Distance to target":[]}
  episode_count = 0
  setup_length = 60
  max_disturbances = 1500
  prev_next_pol = current_pol
  box_cross_x = 0
  if args.dqn:
    term = setup_pol.get_term(ob, im, stochastic=True)
  else:
    next_term = term = False
  act, vpred, _, nlogp = pol[current_pol].step(ob, im, stochastic=False)
  exp_next_vpred = exp_vpred = 0
  in_local_roa = False
  setup_done = False
  if args.render and args.debug:
    replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)
  vpred_buffer = []
  rew_buffer = []
  max_vpred = 1
  scaled_vpred_buffer = []
  
  flat_obs_buffer = []
  flat_com_buffer = []
  flat_imgs_buffer = []
  setup_obs_buffer = []
  setup_com_buffer = []
  setup_imgs_buffer = []
  hj_obs_buffer = []
  hj_com_buffer = []
  hj_imgs_buffer = []

  terms = []
  box_cross_steps = None
  ep_min_length = 45
  ep_max_length = 90
  # use_term = False
  use_term = True
  # success_buf = deque(maxlen=100)
  success_buf = []
  rew_buf = []
  exp_vpred0 = None

  if args.rew == 'gae':
    rews = []
    exp_vpreds = []
    dones = []

  if args.spike:
    spike = deque(maxlen=5)

  try:
    while True:    
      if env.body_xyz[2] > max_z_height:
        max_z_height = env.body_xyz[2]
      if setup_pol.timesteps_so_far > setup_pol.max_timesteps:
        break 

      # if env.order[env.box_num] == 'flat':
      #   # if args.old:
      #   #   next_pol = obstacle_types.index('base')
      #   # else:
      #   next_pol = obstacle_types.index('flat')
      # else:

      # if prev_detected is None and detected:
      #   point = np.array(list(env.body_xyz) + [env.qx,env.qy,env.qz,env.qw] + env.joints)
      #   # error = (data[:,0,:-12] - point)**2
      #   # print(error.shape)
      #   dists = np.sum((data[:,0,:-12] - point)**2, axis=1)
      #   # print(dists.shape)
      #   # print(dists)
      #   data_pointer = np.argmin(dists)
      #   # print(data_pointer)
      #   data_pointer = 0
      # prev_detected = detected

      # if detected and env.steps - detection >= setup_length:
      #   current_pol = args.obstacle_type
      # if detected and env.steps - detection < setup_length:

      
      # if current_pol != args.obstacle_type and random.random() < 0.02:
      #   # print("adding dist")
      #   env.add_disturbance(max_disturbances)

      # if args.term:
      #   # if env.order[env.box_num] == args.obstacle_type and current_pol != args.obstacle_type:
      #   if act.shape[0] > env.ac_size:
      #   # if env.order[env.box_num] == args.obstacle_type and not setup_done:
      #     torques = act[:-1]
      #   else:
          # print(env.order[env.box_num] == args.obstacle_type, current_pol, args.obstacle_type)
      #   torques = act 
      # else:
      torques = act 
      next_ob, orig_rew, done, _ = env.step(torques)
      next_im = env.get_im()

      next_pol = env.order[env.box_num]
      # print(next_pol)
      if args.on_detection:
        current_pol = next_pol
      # print(prev_next_pol, next_pol)
      if prev_next_pol != next_pol and next_pol != 'zero':
        box_cross_x = env.body_xyz[0]
        box_cross_steps = env.steps
        # print("crosed box cross", prev_next_pol, next_pol)

      prev_next_pol = next_pol
    
      dist_to_artifact = box_cross_x  + 0.9 - env.body_xyz[0]   

      if args.term or args.dqn:
        if args.dqn:
          if args.test_pol:
            next_term = setup_pol.get_term(next_ob, next_im, stochastic=False)
          else: 
            next_term = setup_pol.get_term(next_ob, next_im, stochastic=True)
            if np.random.random() < setup_pol.train_model.eps:
              if not box_cross_steps or env.body_xyz[0] > env.box_info[1][7][0]:
                next_term = 2
              elif box_cross_steps and box_cross_steps + ep_max_length < env.steps:
                next_term = 0
              elif box_cross_steps and env.body_xyz[0] <= env.box_info[1][7][0]:
                next_term = 1

          # if not box_cross_steps:
          #   next_term = 2 

        # if use_term:
        #   if setup_pol.iters_so_far > 100:
        #     next_term = setup_pol.get_term(next_ob, next_im, stochastic=True)
        #   else:
        #     text_term = False
        # else:
          # term = setup_pol.get_term(next_ob, next_im, stochastic=False)
      if args.dqn:
        # if not box_cross_steps:
        #   next_act, next_vpred, _, next_nlogp = pol['flat'].step(next_ob, next_im, stochastic=False)
        # else:
        if next_term == 0:
          next_act, next_vpred, _, next_nlogp = setup_pol.step(next_ob, next_im, stochastic=False)
        elif next_term == 1:
          next_act, next_vpred, _, next_nlogp = pol[args.obstacle_type].step(next_ob, next_im, stochastic=False)
        elif next_term == 2:
          # print("this shouldnt happen")
          next_act, next_vpred, _, next_nlogp = pol['flat'].step(next_ob, next_im, stochastic=False)
      else:
        if env.order[env.box_num] == args.obstacle_type and current_pol != args.obstacle_type:
          if args.test_pol or args.dqn:
            next_act, next_vpred, _, next_nlogp = setup_pol.step(next_ob, next_im, stochastic=False)
          else:
            next_act, next_vpred, _, next_nlogp = setup_pol.step(next_ob, next_im, stochastic=True)
        else:
          next_act, next_vpred, _, next_nlogp = pol[current_pol].step(next_ob, next_im, stochastic=False)
        
      
      if not args.test_pol and current_pol != args.obstacle_type and random.random() < 0.02:
        env.add_disturbance(500)

      if args.dqn:
        if not box_cross_steps or env.body_xyz[0] > (env.box_info[1][6][0] + env.box_info[1][6][1]):
          next_rew = np.exp(-2.0*np.sum((env.vx - 1.0)**2))
        else:
          next_rew = orig_rew
        
        if (not box_cross_steps or env.body_xyz[0] > env.box_info[1][7][0]) and next_term == 2:
          rew = next_rew
        elif box_cross_steps and box_cross_steps + ep_max_length < env.steps and next_term == 0:
          rew = next_rew
        elif box_cross_steps and env.body_xyz[0] <= env.box_info[1][7][0] and next_term == 1:
          rew = next_rew
  
                
        # rew = 1
        # if not box_cross_steps and next_term != 2:
        #   rew -= 2
        # elif box_cross_steps and env.body_xyz[0] < (env.box_info[1][6][0] + env.box_info[1][6][1]) and next_term == 2:
        #   rew -= 2
        # if box_cross_steps and box_cross_steps + ep_max_length < env.steps and next_term != 0:
          # rew -= 1

        # if box_cross_steps and abs(env.vx) > 0.3 not switched and next_term == 1:
          # rew -= 2
        # elif box_cross_steps and box_cross_steps + ep_max_length > env.steps and env.body_xyz[0] < (env.box_info[1][6][0] + env.box_info[1][6][1]) and next_term != 1:
        #   rew -= 0.9
        # elif env.body_xyz[0] > (env.box_info[1][7][0]) and next_term != 2:
        #   rew -= 0.9
        adv = 0
      elif not args.dqn and box_cross_steps:
        exp_act, exp_next_vpred, _, _ = pol[args.obstacle_type].step(next_ob, next_im, stochastic=False)  
        adv = orig_rew + (1-done)*0.99*exp_next_vpred - exp_vpred
        # adv2 = rew + (1-done)*0.99*exp_next_vpred - exp_vpred
        if not exp_vpred0:
          exp_vpred0 = exp_vpred
        rew_buf.append(orig_rew)
        adv2 = sum(rew_buf) + (1-done)*0.99*exp_next_vpred - exp_vpred0

        # adv = rew + 0.99*exp_next_vpred - exp_vpred
        # # print(adv)
        # if args.new_reward:
        #   rew = (1 - min(adv**2, 1))*vpred/max_vpred

        # # print(rew )
        # # if not setup_done and env.order[env.box_num] == args.obstacle_type and current_pol != args.obstacle_type:
        # if setup_done:  
        #   # adv = rew + 0.99*next_vpred - vpred
        # rew = (1 - min(adv**2, 1))*(vpred/200)
        # rew = (1 - min(adv**2, 1))*(vpred)
      # else:
      #   exp_act, _, _, _ = pol[args.obstacle_type].step(next_ob, next_im, stochastic=False)
        if args.rew == 'tor':
          rew = np.exp(-2.0*np.sum((act - exp_act)**2))
        elif args.rew == 'tor10': 
          rew = np.exp(-2.0*np.sum((act - exp_act)**2))*10
        elif args.rew == 'adv':
          # rew = (1 - min(adv**2, 1))*vpred/100
          rew = (1 - min(adv**2, 1))*exp_vpred/100
        elif args.rew == 'adv2':
          rew = (1 - min(adv2**2, 1))*exp_vpred/100
        elif args.rew in ['rew', 'gae']:
          rew = orig_rew
        elif args.rew == 'const':
          rew = 2 

      # print(prev_next_pol != next_pol)
      # if box_cross_steps and box_cross_steps + ep_max_length == env.steps and env.order[env.box_num] == args.obstacle_type:
      #   setup_done = 2
      # if box_cross_steps:
        # print(env.steps - box_cross_steps, ep_max_length, box_cross_steps + ep_max_length > env.steps , env.order[env.box_num] == args.obstacle_type:
      if (box_cross_steps and ((not use_term and box_cross_steps + ep_max_length > env.steps) or (use_term and not setup_done)) and env.order[env.box_num] == args.obstacle_type) or (box_cross_steps and args.dqn):
      # if not setup_done and env.order[env.box_num] == args.obstacle_type:
        # print(env.steps, act[-1])
        # if in_local_roa:
        
        # print(rew)
          # rew = (1 - min(adv**2, 1))
          # print(rew)
          # print(rew, abs(adv), vpred)
        
        setup_done = done
        # if in_local_roa and abs(adv) < 0.05:
        # if in_local_roa:
        # print(env.steps)

        if args.term:
          # print(term)
          terms.append(term)
          if term and box_cross_steps + ep_min_length < env.steps:
            # terms.append(term)
            # if act[-1] > 3:
            # print(env.steps - box_cross_steps)
            setup_done = 2
            # print(ep_steps)

          # print(setup_done, box_cross_steps + ep_max_length, env.steps)

        # if args.obstacle_type in ['high_jumps']:
        #   if env.box_info[1][5][0] - 0.19 > env.body_xyz[0] > env.box_info[1][5][0] - 0.21 and abs(env.body_xyz[1]) < 0.3 and abs(env.yaw) < 0.3:
        #   # if env.box_info[1][5][0]  > env.body_xyz[0] > env.box_info[1][5][0] - 0.15 and abs(env.body_xyz[1]) < 0.15 and abs(env.yaw) < 0.3:
        #     in_local_roa = True
        #     # print("in local roa")
        # else:
        #   if abs(env.body_xyz[1]) < 0.3 and abs(env.yaw) < 0.3:
        #     in_local_roa = True


        # if (not args.test_pol and detected and env.steps - detection < setup_length):
          # point = np.array(list(env.body_xyz) + [env.qx,env.qy,env.qz,env.qw] + env.joints + env.joint_vel)
          # print(point.shape)
          # data = list(env.body_xyz) + [env.qx,env.qy,env.qz,env.qw] + env.joints + env.joint_vel
          # rew = 0.25*np.exp(-10.0*(np.sum((data[data_pointer,env.steps - detection,:7] -  np.array(list(env.body_xyz) + [env.qx,env.qy,env.qz,env.qw]))**2)))
          # rew += 0.65*np.exp(-2.0*(np.sum((data[data_pointer,env.steps - detection,7:19] -  np.array(env.joints))**2)))
          # rew += 0.1*np.exp(-0.1*(np.sum((data[data_pointer,env.steps - detection,19:] -  np.array(env.joint_vel))**2)))
        # print(rew)
        # setup_pol.add_to_buffer([ob, im, act, rew, prev_setup_done, vpred, nlogp])
        if not args.test_pol:
          if args.rew == 'gae':
            rews.append(orig_rew)
            exp_vpreds.append(exp_vpred)
            dones.append(prev_setup_done)
          if args.dqn:
            setup_pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp, next_vpred, term])
          elif args.term:
            setup_pol.add_to_buffer([ob, im, act, rew, prev_setup_done, vpred, nlogp, next_vpred, term, use_term])
          else:
            setup_pol.add_to_buffer([ob, im, act, rew, prev_setup_done, vpred, nlogp, next_vpred])
        # print(ep_steps)
        ep_steps += 1
        ep_ret += rew
        ep_len += 1
        prev_setup_done = setup_done

        # print(env.steps - box_cross_steps, ep_steps)
        # if (not args.test_pol and ep_steps % horizon == 0 and ep_steps != 0 and detected and env.steps - detection < setup_length):  
        if not args.test_pol and ep_steps % horizon == 0 and ep_steps != 0:  
          # print("rank", rank, " ready to train")
          # _, vpred, _, _ = setup_pol.step(next_ob, next_im, stochastic=True)
          # params["Success"].append(label)
          # print(rank, params["Success"])
          
          successes = MPI.COMM_WORLD.allgather(success_buf)
          successes = np.array([d for h in successes for d in h], dtype=np.float32)
          # if rank == 0:
          #   # print(successes)
          #   print(successes.dtype, np.mean(successes), np.sum(successes), len(successes))
          # params["Success"] = np.mean(successes)
          # print(params["Success"])
          # if rank == 0:
            # print(params["Success"])
          # print(rank)
          things = {"Avg_success":np.mean(successes)}

          # if rank == 0:
          #   print(things)

          # things = {p:np.sum(params[p]) for p in params}
          if args.term:
            things['terms'] = np.mean(terms)
          # things['term_rew'] = np.mean(term_rew)
          # things['term_not_rew'] = np.mean(term_not_rew)
          things['eps'] = setup_pol.train_model.eps
          setup_pol.log_stuff(things)
          # if setup_done == 2:
          #   _, next_vpred, _, _ = pol[args.obstacle_type].step(next_ob, next_im, stochastic=False)
          # if rank == 0 :
          #   print(sum(setup_pol.data['rew']))
          if args.rew == 'gae':
            exp_adv = setup_pol.get_reward(np.array(rews), np.array(exp_vpreds), np.array(dones), exp_next_vpred, setup_done)
            # print(1 - np.clip(np.square(exp_adv), 0, 1))
            # print(np.min(np.square(exp_adv), 1))
            setup_pol.data['rew'] = list((1 - np.clip(np.square(exp_adv), 0, 1))*np.array(exp_vpreds)/100)
            
          setup_pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=next_vpred, last_done=setup_done)  
          setup_pol.run_train(ep_rets,ep_lens)

          if args.rew == 'gae':
            rews = []
            exp_vpreds = []
            dones = []

          params = {"Success":[], "Distance to target":[]}
          success_buf = []
          term_rew = []
          term_not_rew = []
          ep_rets = []
          ep_lens = []
          ep_steps = 0
          if args.term:
            terms = []
      # if box_cross_steps:
        # print( box_cross_steps + ep_max_length == env.step
      if box_cross_steps and ((box_cross_steps + ep_max_length == env.steps) or (use_term and setup_done)) and env.order[env.box_num] == args.obstacle_type:
      # if box_cross_steps and box_cross_steps + ep_max_length == env.steps and env.order[env.box_num] == args.obstacle_type:
      # if current_pol != args.obstacle_type and env.order[env.box_num] == args.obstacle_type and setup_done:
        current_pol = args.obstacle_type
        setup_done = 2
      # print(type(setup_pol.data['rew']))

      if setup_done and not args.dqn:
        # print("before", setup_pol.data['rew'][-1])
        if isinstance(setup_pol.data['rew'], list) and len(setup_pol.data['rew']) > 0:
          if args.rew != 'gae':
            setup_pol.data['rew'][-1] += rew
          else:
            rews[-1] += rew
            exp_vpreds[-1] = exp_vpred
            dones[-1] = setup_done
          
          ep_ret += rew
          # print(rew)
        # print("after", setup_pol.data['rew'][-1])
      prev_done = done
      ob = next_ob
      im = next_im
      act = next_act
      vpred = next_vpred
      exp_vpred = exp_next_vpred
      nlogp = next_nlogp
      term = next_term

      # if detected and env.steps - detection <= setup_length:
      if args.dqn:
        current_pol_data.append(term)
      elif env.order[env.box_num] == args.obstacle_type:
        _, ob_vpred, _, _ = pol[current_pol].step(ob, im, stochastic=False)
        vpred_buffer.append(ob_vpred)
        rew_buffer.append(rew)
        # switch_data.append(rew)
        if len(vpred_buffer) > 1:
          # rews = 0
          # print(len(rew_buffer), len(vpred_buffer))
          # for r in rew_buffer:
            # rews += r
          # print(rew_buffer)
          # print(vpred_buffer)
          # print(rew)
          # adv = sum(rew_buffer[:-1]) + 0.99*vpred_buffer[-1] - vpred_buffer[0]
          # print(adv)
          # adv = rew_buffer[-2] + 0.99*vpred_buffer[-1] - vpred_buffer[-2]
          # print(abs(adv))
          # print(1 - min(abs(adv), 1))
          # switch_data.append(1 - min(abs(adv), 1))
          # print(adv)
          scaled_vpred_buffer.append((1 - min(adv**2, 1))*vpred_buffer[-2])
          switch_data.append(((1 - min(adv**2, 1))*vpred_buffer[-2])/max_vpred)
        else:
          switch_data.append(0)
        current_pol_data.append(len(obstacle_types) + 1)
      else:
        switch_data.append(0)
        current_pol_data.append(obstacle_types.index(current_pol))

      terrain_data.append(env.z_offset)
      if abs(env.foot_pos['left'][0] - env.foot_pos['right'][0]) < 0.05:
        feet_data['left'].append(env.foot_pos['left'][2])
        feet_data['right'].append(env.foot_pos['right'][2])
      else:
        feet_data['left'].append(np.nan)
        feet_data['right'].append(np.nan)
      if not env.ob_dict['left_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.05:
        com_data.append(env.body_xyz[2])
      elif not env.ob_dict['right_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.05:
        com_data.append(env.body_xyz[2])
      else:
        com_data.append(np.nan)

      if args.render and args.debug:
        if env.order[env.box_num] == args.obstacle_type and not setup_done:
          replace_Id2 = p.addUserDebugText("setup",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,1,0],textSize=3,replaceItemUniqueId=replace_Id2)
        else:
          replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,1,0],textSize=3,replaceItemUniqueId=replace_Id2)

      # print(box_cross_steps, env.box_num)

      if not box_cross_steps and env.box_num > 2:
      # if not box_cross_steps:
        flat_obs_buffer.append(ob)
        flat_com_buffer.append(env.body_xyz)
        flat_imgs_buffer.append(im)
      elif current_pol == args.obstacle_type and env.box_num < 7:
      # elif current_pol == args.obstacle_type:
        hj_obs_buffer.append(ob)
        hj_com_buffer.append(env.body_xyz)
        hj_imgs_buffer.append(im)
      elif box_cross_steps and current_pol != args.obstacle_type:
        # print("setup", box_cross_steps, current_pol != args.obstacle_type,  env.steps)
        setup_obs_buffer.append(ob)
        setup_com_buffer.append(env.body_xyz)
        setup_imgs_buffer.append(im)

      # ==================================================================================================
      # ==================================================================================================
      if done:  
        # use_term = not use_term
        if args.get_data:
          print(len(setup_obs_buffer))
          if len(setup_obs_buffer) > 2000:
            np.save(PATH + 'flat_obs.npy',  np.array(flat_obs_buffer))
            np.save(PATH + 'flat_com.npy',  np.array(flat_com_buffer))
            np.save(PATH + 'flat_imgs.npy', np.array(flat_imgs_buffer))
            np.save(PATH + 'setup_obs.npy',  np.array(setup_obs_buffer))
            np.save(PATH + 'setup_com.npy',  np.array(setup_com_buffer))
            np.save(PATH + 'setup_imgs.npy', np.array(setup_imgs_buffer))
            np.save(PATH + 'hj_obs.npy',  np.array(hj_obs_buffer))
            np.save(PATH + 'hj_com.npy',  np.array(hj_com_buffer))
            np.save(PATH + 'hj_imgs.npy', np.array(hj_imgs_buffer))
            exit()

        vel = np.sqrt(env.vx**2 + env.vy**2 + env.vz**2)
        params["Distance to target"].append(ep_ret)
        # if (args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') and (env.body_xyz[2] - env.z_offset) > 0.88 and vel < 1.25 and env.steps - detection > setup_length:
        # if (env.box_info[1][env.box_num][0] < env.body_xyz[0]) and (done == 2):
        # if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.body_xyz[0]) and (done == 2):
        if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.body_xyz[0]) and env.steps >= env.max_steps - 2:
          label = True  
          colour = [0.1,0.9,0.1,0.5]
          rew = 0
        else:
          label = False
          colour = [0.9,0.1,0.1,0.5]
          # print("fell over!")
        success_buf.append(label)
        if args.dqn and isinstance(setup_pol.data['rew'], list) and len(setup_pol.data['rew']) > 0:
          setup_pol.data['rew'][-1] += 300*label
          

        if rank == 0 and episode_count % 10 == 0 and episode_count != 0 and not args.test_pol:
          colours = [np.array([51, 204, 51])/255.0, np.array([51, 51, 204])/255.0, np.array([204, 51, 51])/255.0, np.array([204, 102, 204])/255.0, np.array([204,102,51])/255.0,]
          num_axes = 3
          fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
          axes[0].plot([s for s in range(len(switch_data))], switch_data, c=np.array([204, 102, 0])/255.0, alpha=1.0)  
          
          axes[num_axes-2].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
          # axes[num_axes-2].plot([_ for _ in range(len(next_pol_data))], next_pol_data, c=np.array([0, 204, 102])/255.0, alpha=1.0)  
          axes[num_axes-2].set_ylim([-0.9,len(obstacle_types) + 1 + 0.1])
          if args.dqn:
            for s, n in enumerate(['setup', 'policy', 'flat']):
              axes[num_axes-2].text(0,s*0.75+0.5,str(s) + ". " + n)
          else:
            for s, n in enumerate(obstacle_types + ["setup"]):
              axes[num_axes-2].text(0,s*0.75+0.5,str(s) + ". " + n)
          axes[num_axes-2].set_title("Current policy", loc='left')
          axes[num_axes-2].legend(["current_policy"])  

          axes[num_axes-1].plot([_ for _ in range(len(feet_data['left']))], feet_data['left'], c=colours[0], alpha=1.0)  
          axes[num_axes-1].plot([_ for _ in range(len(feet_data['right']))], feet_data['right'], c=colours[1], alpha=1.0)  
          axes[num_axes-1].plot([_ for _ in range(len(com_data))], com_data, c=colours[2], alpha=1.0)  
          axes[num_axes-1].plot([t for t in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
          axes[num_axes-1].set_title("terrain height", loc='left')
          axes[num_axes-1].set_ylim([-0.1, max_z_height + 0.1])

          fig.tight_layout(pad=4.0)
          # plt.savefig(PATH + 'switch.png', bbox_inches='tight')
                      
          plt.savefig(PATH + args.obstacle_type + '_plot.png', bbox_inches='tight')
          plt.close()

        current_pol_data = []
        feet_data = {'left':[],'right':[]}
        com_data = []
        switch_data = []
        terrain_data = []
        max_z_height = 0

        episode_count += 1
        prev_next_pol = current_pol = np.random.choice(start_obstacle_types) 
        initial_pol = start_obstacle_types.index(current_pol)

        myseed += 1
        np.random.seed(myseed)

        ob = env.reset()
        im = env.get_im()
        if use_term:
          term_rew.append(ep_ret)
        else:
          term_not_rew.append(ep_ret)
        ep_rets.append(ep_ret)  
        ep_lens.append(ep_len)    
        ep_ret = 0
        ep_len = 0

        detection_dist = 0.9
        artifact_x = 0
        dist_to_artifact = 0
        prev_detected = detected = None
        in_local_roa = False
        box_cross_x = 0
        prev_setup_done = True
        setup_done = False
        box_cross_steps = None
        if scaled_vpred_buffer:
          max_vpred = max(max(scaled_vpred_buffer), max_vpred)
        vpred_buffer = []
        rew_buffer = []
        act, vpred, _, nlogp = pol[current_pol].step(ob, im, stochastic=False)
        exp_next_vpred = exp_vpred = 0
        exp_vpred0 = None
        rew_buf = []

        
  except KeyboardInterrupt:
    print()
    print("exiting nicely")
    MPI.COMM_WORLD.Abort(1)


if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--cur_decay', default='exp')
  parser.add_argument('--rew', default='rew')
  parser.add_argument('--term_stop_gradient', default=False, action='store_true')
  parser.add_argument('--get_data', default=False, action='store_true')
  parser.add_argument('--show_detection', default=False, action='store_true')
  parser.add_argument('--term', default=False, action='store_true')
  parser.add_argument('--new_reward', default=False, action='store_true')
  parser.add_argument('--on_detection', default=False, action='store_true')
  parser.add_argument('--debug', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--blank_pol', default=False, action='store_true')
  parser.add_argument('--two', default=False, action='store_true')
  # parser.add_argument('--single_pol', default=False, action='store_true')
  parser.add_argument('--terrain_first', default=True, action='store_false')
  parser.add_argument('--single_pol', default=True, action='store_false')
  parser.add_argument('--dist_off_ground', default=True, action='store_false')
  parser.add_argument('--min_eps', default=0.01, type=float)
  parser.add_argument('--more_power', default=1.0, type=float)
  parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--const_lr', default=False, action='store_true')
  parser.add_argument('--cur_len', default=1, type=int)
  parser.add_argument('--cur_num', default=5, type=int)
  parser.add_argument('--camera_rate', default=6, type=int)
  parser.add_argument('--num_artifacts', default=1, type=int)

  # parser.add_argument('--eps_decay', default=1000, type=int)
  parser.add_argument('--eps_decay', default=3000, type=int)
  parser.add_argument('--dqn', default=False, action='store_true')
  parser.add_argument('--spike', default=False, action='store_true')
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--const_std', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--advantage2', default=True, action='store_false')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--vis_type', default="depth")
  parser.add_argument('--exp', default="test")
  parser.add_argument('--folder', default="s")
  # parser.add_argument('--obstacle_type', default="jumps", help="flat, stairs, path, jump")
  parser.add_argument('--obstacle_type', default="high_jumps", help="flat, stairs, path, jump")
  parser.add_argument('--seed', default=84, type=int)
  parser.add_argument('--max_ts', default=10e6, type=int)
  # parser.add_argument('--max_ts', default=5e6, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)