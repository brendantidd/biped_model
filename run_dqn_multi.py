import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2
import json
import git
np.set_printoptions(precision=3, suppress=True)

def run(args):

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  # myseed = int(time.time() + 10000 * rank)
  myseed = int(args.seed + 10000 * rank)
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    with open(PATH + 'commandline_args.txt', 'w') as f:
      f.write('Hash:')
      f.write(str(sha) + "\n")
      json.dump(args.__dict__, f, indent=2)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from assets.env import Env
  from models.ppo import Model
  if args.use_classifier:
    from models.classifier import Classifier
    classifier = Classifier(args, sess=sess)
    classifier.load('./weights/classifier/')

  if args.adv:
    from models.dqn_adv import DQN
  else:
    from models.dqn import DQN

  # obstacle_types = ['gaps','base','jumps','stairs','zero']
  # obstacle_types = ['flat','high_jumps', 'gaps']
  # obstacle_types = ['flat','high_jumps', 'gaps', 'jumps', 'stairs', 'steps']
  # obstacle_types = ['flat','high_jumps', 'gaps', 'jumps', 'stairs']
  # obstacle_types = ['flat','high_jumps', 'gaps', 'jumps', 'stairs', 'steps']
  obstacle_types = ['flat','high_jumps', 'gaps', 'jumps', 'stairs']

  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False, multi=args.multi, dqn=True, obstacle_types=obstacle_types)
  # print(env.im_size)

  pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis) for name in obstacle_types if name != 'zero'}  

  for name in obstacle_types:
    if name == 'flat': continue
    pol[name + '_setup'] = Model(name + '_setup', env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis) 
  

  dqn = {name + "_dqn":DQN(name + "_dqn", env=env, ob_size=env.ob_size, ac_size=3, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=args.lr, eps_decay=args.eps_decay) for name in obstacle_types if name != 'flat'}
  dqn['dqn'] = DQN("dqn", env=env, ob_size=env.ob_size, ac_size=3, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=args.lr, eps_decay=args.eps_decay) 


  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
  else:
    # WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
    WEIGHTS_PATH = './weights'
  
  for name in obstacle_types:
    pol[name].load_pol(base_name=name, WEIGHTS_PATH=WEIGHTS_PATH + '/' + name + '/')
    # if name != 'flat':
    if name == 'high_jumps':
      pol[name + '_setup'].load_pol(base_name=name + '_setup', WEIGHTS_PATH=WEIGHTS_PATH + '/' + name + '_setup/')
      dqn[name + '_dqn'].load_dqn(base_name='dqn', WEIGHTS_PATH=WEIGHTS_PATH + '/' + name + '_dqn/')


  # if args.test_pol:
  #   dqn.load(home + '/hpc-home/results/biped_model/latest/dqn/dqn_adv_baseline_reward/')
  #   # dqn.load(home + '/results/biped_model/latest/dqn/longer_exp2/')

  initialize_uninitialized()


  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()

  prev_done = True

  # env.obstacle_type = 'mix'
  # env.obstacle_type = 'high_jumps'
  env.difficulty = args.difficulty
  env.height_coeff = args.height_coeff
  prev_done = True
  ob = env.reset()
  im = env.get_im()
  desired_pol = obstacle_types.index(env.order[env.box_num])
  if args.no_state:
    # dqn_ob = np.array([desired_pol])
    dqn_ob = np.array([0])
    # dqn_ob[-1] = desired_pol 
    desired = np.zeros(len(obstacle_types))
    desired[desired_pol] = 1
    dqn_ob = np.concatenate([dqn_ob, desired])
    print(dqn_ob.shape)
    # dqn_im = np.array([0])
    if args.use_boxes:
      boxes = []
      for box in env.box_nums:
        info = []
        if (env.box_num + box < 1) or (env.box_num + box > len(env.box_info[0]) - 1):
          info.append(0)
        else:
          info.append(env.body_xyz[2] - (env.box_info[1][env.box_num+box][2] + env.box_info[2][env.box_num+box][2]))
        info.append(env.body_xyz[0] - (env.box_info[1][env.box_num+box][0] + env.box_info[2][env.box_num+box][0]))
        info.append(env.body_xyz[1])
        info.append(env.yaw)
        boxes.extend(info)
      dqn_im = np.array(boxes)
    else:
      dqn_im = env.get_im()
  else:

    dqn_ob = ob
    # desired = np.zeros(len(obstacle_types))
    # desired[desired_pol] = 1
    # dqn_ob = np.concatenate([dqn_ob, desired])
    # print(dqn_ob.shape)
    # dqn_im = im
    dqn_im = env.get_im()

  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  ep_steps = 0
  ep_count = 0
  # timesteps_so_far = 0
  stochastic = False

  vpred_data = []
  vpred_orig_data = []
  probs_data = []
  stable_vpred = []
  terrain_data = []
  current_pol_data = []
  baseline_data = []
  success = []

  stable_count = 120

  eval_length = 20

  # print(dqn_acts)
  # current_pol = np.argmax(vpreds)
  # vpred = vpreds[current_pol]

  

  # pols = [i for i in range(len(obstacle_types))]             
  # box_cross_steps = None

  # next_pol = current_pol
  current_pol = obstacle_types[0]
  active = None

  pol_change = False  

  if args.render and args.debug:
    replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)
  

  while True:
    # if dqn.timesteps_so_far > args.max_ts:
      # break 
    # print(env.order)
    # next_pol = env.order[env.box_num]
    # if not pol_change and current_pol != 'flat':
    #   current_pol = next_pol
    #   pol_change = True
    # print(current_pol)
    if args.use_classifier:
      classifier_current_pol = classifier.obstacle_types[classifier.step(im)]
      print(classifier_current_pol, env.order[env.box_num])
    else:
      classifier_current_pol = env.order[env.box_num]

    if args.use_classifier:
      if classifier_current_pol != 'high_jumps':
        current_pol = classifier_current_pol
      else:
        dqn_acts = dqn[classifier_current_pol + '_dqn'].step(ob, im)    
        if dqn_acts == 2:
          current_pol = classifier_current_pol + '_setup'
        elif dqn_acts == 0:
          current_pol = 'flat'
        elif dqn_acts == 1:
          current_pol = classifier_current_pol
    else:
      current_pol = classifier_current_pol

    # # dqn_acts = [dqn[name + '_dqn'].step(ob, im) for name in obstacle_types if name != 'flat']
    # # dqn_acts = [dqn[current_classifier_pol + '_dqn'].step(ob, im) for name in obstacle_types if name != 'flat']

    # # print(dqn_acts)
    # active = dqn_acts[obstacle_types.index('high_jumps') - 1]

    # # if active in [1, 2]:
    # #   current_pol = 'high_jumps'
    # # else:
    # #   current_pol = 'flat'

    # if not active and (np.array(dqn_acts) == 2).any():
    #   # current_pol = 'high_jumps'
    #   current_pol = obstacle_types[dqn_acts.index(2) + 1]
    #   active = 2 
    # # elif active == 2 and dqn_acts[obstacle_types.index(current_pol) - 1] == 1:
    # elif dqn_acts[obstacle_types.index(current_pol) - 1] == 0:
    #   active = None
    #   current_pol = 'flat'
    # elif active: 
    #   # current_pol = 'high_jumps'
    #   active = dqn_acts[obstacle_types.index(current_pol) - 1]
     

    # # print(dqn_acts, current_pol, active)
    # # time.sleep(0.01)
    # active = 0

    # print(current_pol)
    # if active == 2 and current_pol != 'flat':
    #   act, _, _, _ = pol[current_pol + '_setup'].step(ob, im, stochastic=False)
    # else:
      # act, _, _, _ = pol[current_pol].step(ob, im, stochastic=False)

    act, _, _, _ = pol[current_pol].step(ob, im, stochastic=False)

    if args.render and args.debug:
      # if active == 2:
      #   replace_Id2 = p.addUserDebugText("Setting up for " + current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
      # else:
      replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
    
    terrain_data.append(env.z_offset)

    torques = act
    
    next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_im()

    # # if env.box_num > len(env.order) - 2:
    # #   desired_pol = obstacle_types.index('zero')
    # # else:********
    # next_pol = env.order[env.box_num]
    # if not box_cross_steps and next_pol == args.obstacle_type:
    #   box_cross_steps = env.steps
    # prev_next_pol = next_pol
  
    # if not box_cross_steps or env.body_xyz[0] > env.box_info[1][7][0]:
    #   desired_pol = 0
    # elif box_cross_steps and box_cross_steps + 90 > env.steps:
    #   desired_pol = 2
    # elif box_cross_steps and env.body_xyz[0] <= env.box_info[1][7][0]:
    #   desired_pol = 1
    #   # desired_pol = obstacle_types.index(env.order[env.box_num])
    
    # if args.baseline_reward:
    #   # if env.steps > 600 and env.body_xyz[0] < env.box_info[1][7][0]:
    #   #   rew -= 0.5
    #   if current_pol == desired_pol:
    #     rew += 1.0
    #   elif current_pol != desired_pol:
    #     rew -= 1.0
    # elif args.exp_rew:
    #   # rew = 1
    #   # rew = env.body_xyz[0]/19.8
    #   if current_pol == desired_pol:
    #     rew = 1 + 0.2
    #   elif current_pol != desired_pol:
    #     rew = 1 - 0.2

    # if args.no_state:
    #   # dqn_ob = np.array([0])
    #   dqn_next_ob = np.array([0])
    #   desired = np.zeros(len(obstacle_types))
    #   desired[desired_pol] = 1
    #   dqn_next_ob = np.concatenate([dqn_next_ob, desired])
    #   # dqn_ob = np.array([desired_pol])
    #   # dqn_next_ob = np.array([desired_pol])
    #   # dqn_im = np.array([0])
    #   # dqn_im = np.array([0])
    #   # dqn_im = im
    #   # dqn_im = dqn_next_im
    #   if args.use_boxes:
    #     boxes = []
    #     for box in env.box_nums:
    #       info = []
    #       if (env.box_num + box < 1) or (env.box_num + box > len(env.box_info[0]) - 1):
    #         info.append(0)
    #       else:
    #         info.append(env.body_xyz[2] - (env.box_info[1][env.box_num+box][2] + env.box_info[2][env.box_num+box][2]))
    #       info.append(env.body_xyz[0] - (env.box_info[1][env.box_num+box][0] + env.box_info[2][env.box_num+box][0]))
    #       info.append(env.body_xyz[1])
    #       info.append(env.yaw)
    #       boxes.extend(info)
    #     dqn_next_im = np.array(boxes)
    #   else:
    #     dqn_next_im = env.get_im()

    #   # dqn_next_im = np.array([0])
    #   # dqn_next_im = next_im
    #   # dqn_ob = np.array([0])
    #   # dqn_next_ob = np.array([0])
    # else:
    #   # dqn_ob = ob
    #   dqn_next_ob = next_ob
    #   # desired = np.zeros(len(obstacle_types))
    #   # desired[desired_pol] = 1
    #   # dqn_next_ob = np.concatenate([dqn_next_ob, desired])
    #   # dqn_im = im
    #   # dqn_next_im = next_im
    #   dqn_next_im = env.get_im()

    # # # print(dqn_next_im.dtype)
    # # image = (dqn_next_im*255.0).astype(np.uint8)
    # # # norm_hm = np.array(self.hm*255.0, dtype=np.uint8)
    # # cv2.imshow("thing", image)
    # # cv2.waitKey(1)
    # # env.display()

    # if not args.test_pol:
    #   if args.adv:
    #     dqn.add_to_buffer([dqn_ob, dqn_im, current_pol, rew, prev_done, vpred])
    #   else:
    #     dqn.buffer.push(dqn_ob, dqn_im, current_pol, dqn_next_ob, dqn_next_im, rew, done)

    # prev_done = done
    # ob = next_ob
    # im = next_im
    # ep_ret += rew
    # ep_len += 1
    # ep_steps += 1
    # if not args.test_pol:
    #   if args.adv:
    #     if ep_steps % 2048 == 0:
    #       vpred = np.max(dqn.get_vpreds(dqn_next_ob, dqn_next_im))
    #       dqn.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  

    #       things =  {'Success':np.mean(success)}
    #       dqn.log_stuff(things)

    #       dqn.run_train(ep_rets,ep_lens)
    #       ep_rets = []
    #       ep_lens = []
    #       success = []

    #   elif ep_steps > 128:
    #     if ep_steps % 10 == 0:
    #       dqn.run_train(ep_rets,ep_lens)

      
    #     if ep_steps % 2048 == 0:
    #       dqn.evaluate(ep_rets, ep_lens)
    #       ep_rets = []
    #       ep_lens = []

    #     if ep_steps % 2048 == 0:
    #       dqn.update_target()

    # current_pol_data.append(current_pol)
    # baseline_data.append(desired_pol)

    # dqn.train_model.get_eps()
    # # if env.steps % 5 == 0:
    # # if env.steps % 1 == 0:
    # vpreds = dqn.get_vpreds(dqn_next_ob, dqn_next_im)
    # # if args.adv:
    # #   current_pol = np.random.choice(pols, p=abs(vpreds)/np.sum(abs(vpreds)))
    # # else:      
    # prev_pol = current_pol
    # if args.test_pol:
    #   current_pol = np.argmax(vpreds)
    # else:
    #   if args.prob:
    #     current_pol = np.random.choice(pols, p=abs(vpreds)/np.sum(abs(vpreds)))
    #   else:
    #     if np.random.random() < dqn.train_model.eps:
    #       current_pol = np.random.randint(len(obstacle_types))
    #     else:
    #       current_pol = np.argmax(vpreds)
    #     if args.force_pol:
    #       if current_pol != desired_pol:
    #         current_pol = prev_pol
    #     if args.use_expert:
    #       current_pol = desired_pol

    # vpred = vpreds[current_pol]

    # dqn_ob = dqn_next_ob
    # dqn_im = dqn_next_im

    ob = next_ob
    im = next_im
    ep_ret += rew
    ep_len += 1

    # env.display()

    # if np.random.random() > 0.5:
    #   current_pol = obstacle_types.index(env.order[env.box_num])

    if done:    
      # "Success", np.mean(np.array(self.success)), self.iters_so_far)
      # logger.record_tabular("Success", np.mean(np.array(self.success)))
      if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.body_xyz[0]) and env.steps >= env.max_steps - 2:
        label = True  
      else:
        label = False
      success.append(label)

      if args.plot or (rank == 0 and ep_count % 100 == 0):
        num_axes = 3
        fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
        axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)  
        
        colours = [np.array([0, 102, 204])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([51, 204, 51])/255.0, np.array([100, 204, 0])/255.0, np.array([200, 100, 51])/255.0]
        
        print(np.array(current_pol_data).shape, len(current_pol_data))
        axes[0].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[0].set_ylim([-0.9,len(obstacle_types) + 0.1])
        for s, n in enumerate(obstacle_types):
          axes[0].text(0,s*0.5+0.5,str(s) + ". " + n)
        axes[0].set_title("Current policy", loc='left')


        axes[1].plot([_ for _ in range(len(baseline_data))], baseline_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[1].set_ylim([-0.9,len(obstacle_types) + 0.1])
        for s, n in enumerate(obstacle_types):
          axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
        axes[1].set_title("baseline data", loc='left')

        axes[2].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[2].set_title("terrain height", loc='left')
        fig.tight_layout(pad=4.0)
        print(PATH + 'dqn.png')
        plt.savefig(PATH + 'dqn.png', bbox_inches='tight')

      vpred_data = []
      vpred_orig_data = []
      probs_data = []
      terrain_data = []
      current_pol_data = []
      baseline_data = []
      switch_data = []
      
      # if rank == 0:
      #   print(ep_len, env.steps, env.max_steps)
      
      ob = env.reset()
      im = env.get_im()
      # desired_pol = obstacle_types.index(env.order[env.box_num])
      # if args.no_state:
      #   # dqn_ob = np.array([desired_pol])
      #   dqn_ob = np.array([0])
      #   desired = np.zeros(len(obstacle_types))
      #   desired[desired_pol] = 1
      #   dqn_ob = np.concatenate([dqn_ob, desired])
      #   # dqn_im = np.array([0])
      #   if args.use_boxes:
      #     boxes = []
      #     for box in env.box_nums:
      #       info = []
      #       if (env.box_num + box < 1) or (env.box_num + box > len(env.box_info[0]) - 1):
      #         info.append(0)
      #       else:
      #         info.append(env.body_xyz[2] - (env.box_info[1][env.box_num+box][2] + env.box_info[2][env.box_num+box][2]))
      #       info.append(env.body_xyz[0] - (env.box_info[1][env.box_num+box][0] + env.box_info[2][env.box_num+box][0]))
      #       info.append(env.body_xyz[1])
      #       info.append(env.yaw)
      #       boxes.extend(info)
      #     dqn_im =np.array(boxes)
      #   else:
      #     dqn_im = env.get_im()
      # else:
      #   dqn_ob = ob
      #   # desired = np.zeros(len(obstacle_types))
      #   # desired[desired_pol] = 1
      #   # dqn_ob = np.concatenate([dqn_ob, desired])

      #   # dqn_im = im
      #   dqn_im = env.get_im()

      ep_rets.append(ep_ret)  
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
      ep_count += 1  
      

      # box_cross_steps = None
      # next_pol = current_pol
      
      # vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
      # current_pol = np.argmax(vpreds)
      # vpred = vpreds[current_pol]
  
      current_pol = obstacle_types[0]
      active = None

if __name__ == '__main__':
  
  # My hack for loading defaults, but still accepting run specific defaults
  import defaults
  from dotmap import DotMap
  args1 = defaults.get_defaults() 
  parser = argparse.ArgumentParser()
  # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
  parser.add_argument('--folder', default='multi')
  parser.add_argument('--obstacle_type', default='mix')
  parser.add_argument('--difficulty', default=10, type=int)
  args2, _ = parser.parse_known_args()
  args2 = vars(args2)
  for key in args2:
    args1[key] = args2[key]
  args = DotMap(args1)
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)