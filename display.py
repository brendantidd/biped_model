'''
Script used to display robot stuff while training (loads in save robot state)
'''
import pybullet as p
import numpy as np
import argparse
from pathlib import Path
import time
home = str(Path.home())
from assets.obstacles import Obstacles

obstacles = Obstacles()
import defaults
from dotmap import DotMap
args1 = defaults.get_defaults() 
parser = argparse.ArgumentParser()
# Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
parser.add_argument('--render', default=True, action='store_false')
parser.add_argument('--eval', default=False, action='store_true')
args2, _ = parser.parse_known_args()
args2 = vars(args2)
for key in args2:
  args1[key] = args2[key]
args = DotMap(args1)

# parser = argparse.ArgumentParser()
# parser.add_argument('--hpc', default=False,action='store_true')
# parser.add_argument('--exp', default='test')
# parser.add_argument('--mocap', default=False, action='store_true')
# parser.add_argument('--nicks', default=False, action='store_true')
# parser.add_argument('--single_pol', default=False, action='store_true')
# parser.add_argument('--test_pol', default=False, action='store_true')
# parser.add_argument('--baseline', default=False, action='store_true')
# parser.add_argument('--dqn', default=False, action='store_true')
# parser.add_argument('--terrain_first', default=False, action='store_true')
# parser.add_argument('--new', default=False, action='store_true')
# parser.add_argument('--expert', default=False, action='store_true')
# parser.add_argument('--eval', default=False, action='store_true')
# parser.add_argument('--long', default=False, action='store_true')
# parser.add_argument('--show_detection', default=False, action='store_true')
# parser.add_argument('--render', default=True, action='store_false')
# parser.add_argument('--cur_len', default=1, type=int)
# parser.add_argument('--cur_num', default=1, type=int)
# parser.add_argument('--dist_inc', default=0, type=int)
# parser.add_argument('--num_artifacts', default=1, type=int)
# args = parser.parse_args()

# if 'mocap' in args.exp:
#   args.mocap = True

if args.hpc:
  hpc = 'hpc-home/'
    # MODEL_PATH = home + '/' + hpc + 'results/biped_model/weights/w6/' + args.exp + '/'
    # MODEL_PATH = home + '/' + hpc + 'results/biped_model/a11/' + args.exp + '/'
else:
  hpc = ''
  # MODEL_PATH = home + '/' + hpc + 'results/biped_model/weights/b10/base/'
MODEL_PATH = home + '/' + hpc + 'results/biped_model/latest/' + args.exp + '/'

# from assets.env_mocap import Env
from assets.env import Env
env = Env(render=args.render, record_step=False, args=args)

def data_gen(path, box_path = None):
  print(path)
  env.reset()
  while True:
    try:
      try:
        box_info = np.load(box_path, allow_pickle=True)
      except:
        pass
      sim_data = np.load(path, allow_pickle=True)
      try:
        obstacles.remove_obstacles()
        # box_info = np.load(box_path, allow_pickle=True)
        for pos, size, colour in zip(box_info[1], box_info[2], box_info[3]):
          # print(pos[2], size[2])
          # print(pos[3:])
          obstacles.add_box(pos=pos[:3], orn=pos[3:], size=size, colour=colour)
      except Exception as e:
        print("box stuff ", e)
      if len(sim_data) == 0:
        yield None
      count = 0
      for seg in sim_data:
        yield seg, count
        count += 1
      print(count)
    except Exception as e:
        print("exception")
        print(e)
if args.eval:
  data = data_gen(path = MODEL_PATH + 'sim_data_eval.npy', box_path = MODEL_PATH + 'box_info_eval.npy')
elif args.long:
  data = data_gen(path = MODEL_PATH + 'sim_data_eval_9000.npy', box_path = MODEL_PATH + 'box_info_eval_9000.npy')
else:
  data = data_gen(path = MODEL_PATH + 'sim_data.npy', box_path = MODEL_PATH + 'box_info.npy')
if args.mocap:
  exp_data = data_gen(path = MODEL_PATH + 'exp_sim_data.npy')

while True:
  seg, count = data.__next__()
  if seg is not None:
    env.set_position(seg[0], seg[1], seg[2])
    env.get_observation()
    # env.actions = np.zeros(env.ac_size)
    # env.order = 
    # env.get_reward()
    # print(count)
  else:
    env.set_position([0, 0, -0.045], [0,0,0,1], np.zeros(12))
  if args.mocap:
    exp_seg, exp_count = exp_data.__next__()
    if exp_seg is not None:
      env.set_position(exp_seg[0], exp_seg[1], exp_seg[2], robot_id=env.exp_Id)
    else:
      env.set_position([0, 1, -0.045], [0,0,0,1], np.zeros(12), robot_id=env.exp_Id)
  p.stepSimulation()
  if args.render:
    time.sleep(0.02)
