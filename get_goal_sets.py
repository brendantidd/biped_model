import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt

def run(args):

  PATH = home + '/results/biped_model/latest/b/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from assets.env import Env

  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False)

  from models.ppo_vf import Model
  
  # obstacle_types = ['gaps', 'jumps', 'base']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs','turn','zero']
  obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
  # obstacle_types = ['zero', 'base']

  if args.multi:
    pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}  
    if args.use_vf:
      vf = {name:Model(name + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types}
    else:
      vf = {name:Model(name + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}
  
  else:
    pol = Model(args.exp, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
    vf = Model(args.exp + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  
  initialize()

  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
  else:
    WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
  
  if args.multi:
    for name in obstacle_types:
      if args.use_vf:
        if rank == 0:
          print("loading vf from ", home + '/results/biped_model/latest/vf5/' + name + '/')
        vf[name].load(home + '/results/biped_model/latest/vf5/' + name + '/') 
        if name == 'zero': continue
        pol[name].load(WEIGHTS_PATH + '/' + name + '/')
      else:
        if name == 'zero': continue
        pol[name].load(WEIGHTS_PATH + '/' + name + '/')
        vf[name].load_base(pol[name].name, WEIGHTS_PATH + '/' + name + '/') 
  else:
    pol.load(WEIGHTS_PATH + '/' + args.exp + '/')
    vf.load_base(pol.name, WEIGHTS_PATH + '/' + args.exp + '/') 


  env.difficulty = args.difficulty
  env.height_coeff = args.height_coeff

  means = {}
  stds = {}
  if args.render:
    eval_length = 100
  else:
    eval_length = 10000
  # eval_length = 1000

  for current_pol in obstacle_types:  
    if current_pol == 'zero':
      env.obstacle_type = 'base'
    else:
      env.obstacle_type = current_pol
    prev_done = True
    ob = env.reset()
    im = env.get_im()
    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    ep_steps = 0
    timesteps_so_far = 0
    stochastic = False

    vpred_data = []
    stable_vpred = []
    terrain_data = []
    stable_count = 120
    episode_count = 0
    print("Getting goal set for ", current_pol, " in terrain type for ", current_pol, ", recording values for stable regions in velocity range. Collecting ", eval_length, " samples for each")
    while True:
  
      if current_pol == 'zero':
        zero_ob = copy.copy(ob)
        zero_ob[-2] = 0
        act, vpred, _, nlogp = pol['base'].step(zero_ob, im, stochastic=stochastic)
      else:
        act, vpred, _, nlogp = pol[current_pol].step(ob, im, stochastic=stochastic)
      
      if args.use_vf:
        vpred = vf[name].get_value(ob, im)

      vpred_data.append(vpred)

      # Stability criteria: alive for longer than stable count (2 steps), and value not decreasing
      if env.steps > stable_count and vpred_data[-1] > vpred_data[env.steps - stable_count]:
        if current_pol == 'zero':
          stable_vpred.append(vpred_data[env.steps - stable_count])
        elif env.vx > 0.1:
          stable_vpred.append(vpred_data[env.steps - stable_count])
      
      terrain_data.append(env.z_offset)

      torques = act

      
      next_ob, rew, done, _ = env.step(torques)
      next_im = env.get_im()

      prev_done = done
      ob = next_ob
      im = next_im
      ep_ret += rew
      ep_len += 1
      ep_steps += 1

      if not args.render and len(stable_vpred) > eval_length:
        means[current_pol] = np.mean(stable_vpred)
        stds[current_pol] = np.std(stable_vpred)
        break

      if done:    
        print(current_pol, " terrain type ", env.obstacle_type, " ", len(stable_vpred), " of ", eval_length)
        if args.plot:
          if not args.multi:
            num_axes = 3
          else:    
            num_axes = 6    
          fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
          # slope_data = reject_outliers(np.array(slope_data))
          axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)  
          if not args.multi:
            axes[1].plot([_ for _ in range(len(stable_vpred))], stable_vpred, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
            if stable_vpred:
              print("min stable value", min(stable_vpred), np.mean(stable_vpred), np.std(stable_vpred))
            axes[2].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  

          else:
          #   axes[0].plot([_ for _ in range(len(vpred_data))], [msv[0]]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='--', alpha=1.0)  
          #   axes[0].plot([_ for _ in range(len(vpred_data))], [msv[1]]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='--', alpha=1.0)  
          #   axes[0].plot([_ for _ in range(len(vpred_data))], [msv[2]]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='--', alpha=1.0)  
            # axes[0].plot([_ for _ in range(len(vpred_data))], [means[0] - stds[0]]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='--', alpha=1.0)  
            # axes[0].plot([_ for _ in range(len(vpred_data))], [means[1] - stds[1]]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='--', alpha=1.0)  
            # axes[0].plot([_ for _ in range(len(vpred_data))], [means[2] - stds[2]]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='--', alpha=1.0)  
            axes[0].plot([_ for _ in range(len(vpred_data))], [means[0]]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
            axes[0].plot([_ for _ in range(len(vpred_data))], [means[1]]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
            axes[0].plot([_ for _ in range(len(vpred_data))], [means[2]]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
            axes[0].plot([_ for _ in range(len(vpred_data))], [means[0] - 2*stds[0]]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='--', linewidth=1.0, alpha=0.2)  
            axes[0].set_title("Values (cost to go)", loc='left')

            axes[0].plot([_ for _ in range(len(vpred_data))], [means[1] - 2*stds[1]]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='--', linewidth=1.0, alpha=0.2)  
            axes[0].plot([_ for _ in range(len(vpred_data))], [means[2] - 2*stds[2]]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='--', linewidth=1.0, alpha=0.2)  
            axes[0].legend(obstacle_types)  
            print(np.array(current_pol_data).shape, len(current_pol_data))
            axes[1].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
            axes[1].set_ylim([-0.9,2.1])
            for s, n in enumerate(obstacle_types):
              axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
            axes[1].set_title("Current policy", loc='left')
            
            # probs_data = -1*np.abs(probs_data)
            # print(probs_data)
            axes[2].plot([_ for _ in range(len(probs_data))], probs_data, alpha=1.0)  
            axes[2].legend(obstacle_types)  
            axes[2].set_title("Likelihood in goal set (higher is better)", loc='left')

            axes[3].plot([_ for _ in range(len(probs_data))], logp(np.array(vpred_data), np.array(means), np.array(stds)), alpha=1.0)  
            axes[3].legend(obstacle_types)  
            axes[3].set_title("Log Likelihood in goal set (higher is better)", loc='left')
            
            axes[4].plot([_ for _ in range(len(probs_data))], mahalanobois_distance(np.array(vpred_data), np.array(means), np.array(stds)), alpha=1.0)  
            axes[4].legend(obstacle_types)  
            axes[4].set_title("Mahalanobois distance from goal set (lower is better)", loc='left')

            # axes[2].plot([_ for _ in range(len(vpred_data))], [normpdf(msv[0], means[0], stds[0])]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
            # axes[2].plot([_ for _ in range(len(vpred_data))], [normpdf(msv[1], means[1], stds[1])]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
            # axes[2].plot([_ for _ in range(len(vpred_data))], [normpdf(msv[2], means[2], stds[2])]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
            axes[5].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
            axes[5].set_title("terrain height", loc='left')

          fig.tight_layout(pad=4.0)


          # axes[0].plot([_ for _ in range(len(plot_data))], plot_data, c='b', alpha=1.0)  
          # axes[0].set_ylim([0,1])
          # axes[1].plot([_ for _ in range(len(vpred_data))], vpred_data, c='b', alpha=1.0)  
          # axes[2].plot([_ for _ in range(len(slope_data))], slope_data, c='b', alpha=1.0)  
          
          # axes[3].plot([_ for _ in range(len(probs))], probs, c='b', alpha=1.0)  
          # plt.show(block=False)
          plt.show()
          if args.render:
            break
        #   plot_data = []
        
        episode_count += 1
        # print("Current policy: ", current_pol, " episode ", episode_count, " of ", eval_length)

        vpred_data = []
        terrain_data = []

        ob = env.reset()
        im = env.get_im()
        ep_rets.append(ep_ret)  
        ep_lens.append(ep_len)     
        ep_ret = 0
        ep_len = 0

      
  print("============================================================================================================================")
  print("obstacles: ", [o for o in obstacle_types])
  print("means = ", [means[o] for o in obstacle_types])
  print("stds = ", [stds[o] for o in obstacle_types])
  print("============================================================================================================================")


if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--sub_folder', default='b')
  # parser.add_argument('--folder', default='b4')
  parser.add_argument('--folder', default='b9')

  parser.add_argument('--difficulty', default=6, type=int)
  parser.add_argument('--height_coeff', default=0.07)

  parser.add_argument('--cur_len', default=1000, type=int)
  parser.add_argument('--inc', default=2, type=int)

  parser.add_argument('--use_vf', default=False, action='store_true')
  parser.add_argument('--multi', default=True, action='store_false')
  parser.add_argument('--goal_set', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')
  parser.add_argument('--gae', default=False, action='store_true')
  parser.add_argument('--doa', default=False, action='store_true')

  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--vis', default=False, action='store_true')
  # parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="domain_train_no_stairs", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=1e5, type=int)
  parser.add_argument('--lr', default=1e-5, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)