import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
from scripts.plots import Plots

def run(args):

  PATH = home + '/results/biped_model/latest/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from assets.env_mocap import Env
  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, doa=True)

  from models.ppo import Model
  
  # names = ['up_stairs', 'down_stairs', 'stairs', 'jumps', 'gaps', 'stop_turn'] 
  # names = ['up_stairs', 'down_stairs', 'stairs', 'jumps', 'gaps'] 
  names = ['up_stairs', 'down_stairs', 'jumps', 'gaps'] 
  # names = ['jumps', 'gaps'] 

  policies = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in names}
  
  from models.doa import DoANet
  doa_nets = {name:DoANet(name="doa_" + name, ob_size=env.ob_size, im_size=env.im_size) for name in names}

  initialize()
  # sync_from_root(sess, pol.vars, comm=comm)
  # pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder + '/'
  else:
    WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder + '/'
    
  for name in names:
    policies[name].load(WEIGHTS_PATH + 'b/' + name + '/')
    # doa_nets[name].load(WEIGHTS_PATH + 'doa_' + name + '/')
    # doa_nets[name].load(home + '/hpc-home/results/biped_model/latest/d/' + name + '/')
    doa_nets[name].load(WEIGHTS_PATH + 'd/' + name + '/')

  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()

  prev_done = True
  ob = env.reset()
  im = env.get_im()
  current_selection = None
  plots = Plots(names=names, PATH=PATH)  
  best_length = 0
  ep_len = 0
  while True:
    # if pol.timesteps_so_far > pol.max_timesteps:
    #   break 

    doa_preds = []
    for name in names:
      doa_preds.append(doa_nets[name].step(ob,im))    
      plots.data['doa'][name].append(doa_preds[-1])
    # if current_selection == None or doa_preds[current_selection] > 0.6:
    current_selection = np.argmin(doa_preds)
    plots.data['selection'].append(current_selection)
    
    plots.data['terrain']['z'].append(env.z_offset)

    act, _, _, _ = policies[names[current_selection]].step(ob, im, stochastic=False)

    torques = act
    next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_im()


    env.episode_buffer.append([env.body_xyz[0], env.body_xyz[1], env.yaw, doa_preds[current_selection], current_selection])
    #   else:
    #     self.episode_buffer.append([self.body_xyz[0], self.body_xyz[1], self.yaw, 0])

    ob = next_ob
    im = next_im
    ep_len += 1
    if done:    
      if args.plot and ep_len > best_length:
        plots.do_plot()
        best_length = ep_len
        env.save_sim_data()
    
      ob = env.reset()
      im = env.get_im()        
      
      ep_len = 0
if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--folder', default='w5')

  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')

  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="flat", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=1e8, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)