import numpy as np
import matplotlib.pyplot as plt

# e2e
e1, e2, e3, e4, e5 = np.array([0.9667, 0.9356, 0.9280]),np.array([0.9592, 0.9284, 0.8940]),np.array([0.9592, 0.9277, 0.8980]), np.array([0.9535, 0.9238, 0.8900]), np.array([0.9531, 0.9233, 0.9000])


e_gaps = 26+22+25+17+16
e_jumps = 13+6  +9  +14 +4  
e_stairs = 17 +21+17+ 23+  16

es = np.concatenate([[e1], [e2], [e3], [e4], [e5]], axis=0)
e_mean = es.mean(axis=0)
e_std = es.std(axis=0)

print(e_mean)
print( e_std)
print(e_gaps, e_jumps, e_stairs, e_gaps + e_jumps + e_stairs)
exit()



# Thres 0.00 time alive: 0.9667 distance travelled: 0.9356  success: 0.9280
# ['gaps : 16', 'jumps : 4', 'stairs : 16', 'base : 0']
# Thres 0.00 time alive: 0.9592 distance travelled: 0.9284  success: 0.8940
# ['gaps : 17', 'jumps : 14', 'stairs : 23', 'base : 0']
# Thres 0.00 time alive: 0.9592 distance travelled: 0.9277  success: 0.8980
# ['gaps : 25', 'jumps : 9', 'stairs : 17', 'base : 0']
# Thres 0.00 time alive: 0.9535 distance travelled: 0.9238  success: 0.8900
# ['gaps : 26', 'jumps : 13', 'stairs : 17', 'base : 0']
# Thres 0.00 time alive: 0.9531 distance travelled: 0.9233  success: 0.9000
# ['gaps : 22', 'jumps : 6', 'stairs : 21', 'base : 0']



# rand	    0.477	0.4447	0.112	339	64	40	      0.4529	0.4188	0.086	363	54	40		0.4702	0.4364	0.112	361	57	26		0.4521	0.4177	0.084	355	62	40		0.453	0.4178	0.112	341	52	44
# on detect	0.7693	0.7442	0.576	115	18	79  		0.7914	0.7665	0.63	98	13	73		0.8037	0.7772	0.642	100	17	63		0.7793	0.7515	0.582	108	18	82		0.7668	0.7404	0.576	114	16	83
# q_table	  0.7918	0.7602	0.594	114	26	63		  0.8039	0.7729	0.6	99	24	77		  0.8016	0.7726	0.606	88	26	82		0.7779	0.7452	0.58	104	25	80		0.7978	0.7669	0.574	124	17	70
# feet	    0.8065	0.776	0.634	104	17	62		    0.8275	0.7968	0.678	82	13	66		0.8174	0.7872	0.682	89	10	60		0.8433	0.8136	0.69	77	18	60		0.8145	0.7836	0.656	89	9	75
																																		
# feet + roa	0.8593	0.8593	0.74	72	10	48		0.8453	0.8188	0.702	95	10	44		0.8465	0.8205	0.706	89	9	48		0.818	0.79	0.658	96	12	61		0.8379	0.8114	0.69	91	15	50

# dqn:
dq1, dq2, dq3, dq4, dq5 = np.array([0.8649, 0.8347, 0.7340]),np.array([0.8602, 0.8272, 0.7160]),np.array([0.8684, 0.8365, 0.7260]),np.array([0.8614, 0.8278, 0.7100]),np.array([0.8665, 0.8347, 0.7340])


dqs = np.concatenate([[dq1], [dq2], [dq3], [dq4], [dq5]], axis=0)
dq_mean = dqs.mean(axis=0)
dq_std = dqs.std(axis=0)

print(dq_mean, dq_std)
dqn_gaps = 74 +68 +79 +75 +81 
dqn_jumps =16+18+19+22+17
dqn_stairs = 42+56+39+49+35
print(dqn_gaps, dqn_jumps, dqn_stairs)


exit()


# ['gaps : 74', 'jumps : 16', 'stairs : 42', 'base : 0']
# ['gaps : 68', 'jumps : 18', 'stairs : 56', 'base : 0']
# ['gaps : 79', 'jumps : 19', 'stairs : 39', 'base : 0']
# ['gaps : 75', 'jumps : 22', 'stairs : 49', 'base : 0']
# ['gaps : 81', 'jumps : 17', 'stairs : 35', 'base : 0']




r1, r2, r3, r4, r5 = np.array([0.477	,  0.4447,	0.112]), np.array([  0.4529,	0.4188,	0.086]), np.array([	0.4702,	0.4364,	0.112]), np.array([	0.4521,	0.4177,	0.084]), 			np.array([0.453	,  0.4178,	0.112	])
d1, d2, d3, d4, d5 = np.array([0.7693,	0.7442	,0.576 ]), np.array([0.7914,	0.7665,	0.63	]), np.array([0.8037,	0.7772,	0.642	]), np.array([0.7793,	0.7515,	0.582	]), 		  np.array([0.7668,	0.7404	,0.576	])
q1, q2, q3, q4,  q5 = np.array([0.7918,	0.7602	,0.594	]), np.array([0.8039,	0.7729,	0.6	  ]), np.array([0.8016,	0.7726,	0.606	 ]), np.array([0.7779,	0.7452,	0.58		]), 	np.array([0.7978,	0.7669	,0.574	])
f1, f2, f3, f4, f5 = np.array([0.8065,	0.776	 , 0.634]), np.array([  0.8275,	0.7968,	0.678	]), np.array([0.8174,	0.7872,	0.682	 ]), np.array([0.8433,	0.8136,	0.69		]),   np.array([0.8145,	0.7836	,0.656	])


r_gaps = 339+	363+	361	+ 355	+ 341
r_jumps = 64	+ 54	+	57+		62	+52	
r_stairs = 40 +	40+	26	+	40+	44

d_gaps = 115+98+100+108+114
d_jumps = 	18	+	13+	17+18+	16
d_stairs = 79+73+63+82+83

q_gaps = 114+	99+	88+104+124
q_jumps = 26+24+26+25+	17
q_stairs = 63+	77+	82+	80+	70

f_gaps = 104+82+89+77+89
f_jumps = 17+13+10+18+9	
f_stairs = 62	+66+60+60+75

m_gaps = 82+65+80+94+95
m_jumps = 11+12+14+14+9
m_stairs = 48+42+45+52+48

print(r_gaps, r_jumps, r_stairs, r_gaps+r_jumps+r_stairs)
print(d_gaps, d_jumps, d_stairs, d_gaps+d_jumps+d_stairs)
print(q_gaps, q_jumps, q_stairs, q_gaps+q_jumps+q_stairs)
print(f_gaps, f_jumps, f_stairs, f_gaps+f_jumps+f_stairs)
print(m_gaps, m_jumps, m_stairs, m_gaps+m_jumps+m_stairs)
exit()
# ['gaps : 82', 'jumps : 11', 'stairs : 48', 'base : 0']
# ['gaps : 65', 'jumps : 12', 'stairs : 42', 'base : 0']
# ['gaps : 80', 'jumps : 14', 'stairs : 45', 'base : 0']
# ['gaps : 94', 'jumps : 14', 'stairs : 52', 'base : 1']\
# ['gaps : 95', 'jumps : 9', 'stairs : 48', 'base : 0']



# m1, m2, m3, m4, m5 = np.array([0.8544, 0.8284, 0.7160]),np.array([0.8151, 0.7898, 0.6500]),np.array([0.8380, 0.8103, 0.7020]),np.array([0.8349, 0.8081, 0.6940]), np.array([0.8443, 0.8173, 0.7060])

m1, m2, m3, m4, m5 = np.array([0.8548, 0.8260, 0.7160]),np.array([0.8738, 0.8448, 0.7600]),np.array([0.8559, 0.8286, 0.7220]),np.array([0.8329, 0.8044, 0.6780]),np.array([0.8477, 0.8195, 0.6980])

# Thres 0.85 time alive: 0.8548 distance travelled: 0.8260  success: 0.7160
# ['gaps : 82', 'jumps : 11', 'stairs : 48', 'base : 0']
# Thres 0.85 time alive: 0.8738 distance travelled: 0.8448  success: 0.7600
# ['gaps : 65', 'jumps : 12', 'stairs : 42', 'base : 0']
# Thres 0.85 time alive: 0.8559 distance travelled: 0.8286  success: 0.7220
# ['gaps : 80', 'jumps : 14', 'stairs : 45', 'base : 0']
# Thres 0.85 time alive: 0.8329 distance travelled: 0.8044  success: 0.6780
# ['gaps : 94', 'jumps : 14', 'stairs : 52', 'base : 1']
# Thres 0.85 time alive: 0.8477 distance travelled: 0.8195  success: 0.6980
# ['gaps : 95', 'jumps : 9', 'stairs : 48', 'base : 0']





# y1, y2, y3, y4, y5 = np.array([0.8219, 0.7949, 0.6620]),np.array([0.8264, 0.7990, 0.6800]),np.array([0.8392, 0.8104, 0.7140]),np.array([0.8362, 0.8095, 0.6740]),np.array([0.8151, 0.7898, 0.6500])


# Thres 0.85 time alive: 0.8548 distance travelled: 0.8260  success: 0.7160
# ['gaps : 82', 'jumps : 11', 'stairs : 48', 'base : 0']

# Thres 0.87 time alive: 0.8626 distance travelled: 0.8353  success: 0.7100
# ['gaps : 85', 'jumps : 15', 'stairs : 45', 'base : 0']

# Thres 0.84 time alive: 0.8365 distance travelled: 0.8099  success: 0.6760
# ['gaps : 94', 'jumps : 15', 'stairs : 52', 'base : 1']


# Thres 0.86 time alive: 0.8493 distance travelled: 0.8213  success: 0.7120
# ['gaps : 82', 'jumps : 13', 'stairs : 47', 'base : 1']

# Thres 0.85 time alive: 0.8544 distance travelled: 0.8284  success: 0.7160
# ['gaps : 88', 'jumps : 13', 'stairs : 41', 'base : 0']
# Thres 0.80 time alive: 0.8151 distance travelled: 0.7898  success: 0.6500
# ['gaps : 110', 'jumps : 10', 'stairs : 55', 'base : 1']
# Thres 0.80 time alive: 0.8380 distance travelled: 0.8103  success: 0.7020
# ['gaps : 87', 'jumps : 13', 'stairs : 48', 'base : 1']
# Thres 0.80 time alive: 0.8349 distance travelled: 0.8081  success: 0.6940
# ['gaps : 90', 'jumps : 7', 'stairs : 57', 'base : 0']
# Thres 0.80 time alive: 0.8443 distance travelled: 0.8173  success: 0.7060
# ['gaps : 88', 'jumps : 12', 'stairs : 46', 'base : 1']


# Thres 0.80 time alive: 0.8219 distance travelled: 0.7949  success: 0.6620
# ['gaps : 91', 'jumps : 19', 'stairs : 59', 'base : 0']
# Thres 0.80 time alive: 0.8264 distance travelled: 0.7990  success: 0.6800
# ['gaps : 93', 'jumps : 16', 'stairs : 49', 'base : 3']
# Thres 0.80 time alive: 0.8392 distance travelled: 0.8104  success: 0.7140
# ['gaps : 76', 'jumps : 11', 'stairs : 56', 'base : 0']
# Thres 0.80 time alive: 0.8362 distance travelled: 0.8095  success: 0.6740
# ['gaps : 94', 'jumps : 11', 'stairs : 55', 'base : 2']
# Thres 0.80 time alive: 0.8151 distance travelled: 0.7898  success: 0.6500
# ['gaps : 110', 'jumps : 10', 'stairs : 55', 'base : 1']



rs = np.concatenate([[r1], [r2], [r3], [r4], [r5]], axis=0)
r_mean = rs.mean(axis=0)
r_std = rs.std(axis=0)

ds = np.concatenate([[d1], [d2], [d3], [d4], [d5]], axis=0)
d_mean = ds.mean(axis=0)
d_std = ds.std(axis=0)

qs = np.concatenate([[q1], [q2], [q3], [q4], [q5]], axis=0)
q_mean = qs.mean(axis=0)
q_std = qs.std(axis=0)

fs = np.concatenate([[f1], [f2], [f3], [f4], [f5]], axis=0)
f_mean = fs.mean(axis=0)
f_std = fs.std(axis=0)

ms = np.concatenate([[m1], [m2], [m3], [m4], [m5]], axis=0)
m_mean = ms.mean(axis=0)
m_std = ms.std(axis=0)

# ys = np.concatenate([[y1], [y2], [y3], [y4], [y5]], axis=0)
# y_mean = ys.mean(axis=0)
# y_std = ys.std(axis=0)


# means = np.array([r_mean, d_mean, q_mean, f_mean, m_mean, y_mean])
# errors = np.array([r_std, d_std, q_std, f_std, m_std, y_std])
means = np.array([r_mean, d_mean, q_mean, f_mean, m_mean])
errors = np.array([r_std, d_std, q_std, f_std, m_std])

print(means)
print(errors)
print(means.shape, errors.shape)

# experiments = ['Random', 'Copper', 'Steel']
experiments = ['Random', 'On detection', 'Q table', 'CoM over feet', 'Our Estimator']
# experiments = ['Random', 'On detection', 'Q table', 'CoM over feet', 'RoA', 'RoA feet']
num_experiments = np.arange(len(experiments))
for i,(s,e) in enumerate(zip(['alive','distance','success'], ["Time Alive", "Distance", "Success"])):
  fig, ax = plt.subplots()
  print(means[:,i], errors[:,i])
  ax.bar(num_experiments, means[:,i], yerr=errors[:,i], align='center', alpha=0.5, ecolor='black', capsize=10)
  # ax.set_ylabel(i)
  ax.set_xticks(num_experiments)
  ax.set_xticklabels(experiments)
  ax.set_title(e)
  ax.yaxis.grid(True)
  # ax.set(ylim=[0.40, 1.0])
  # Save the figure and show
  plt.tight_layout()
  plt.savefig('plot_' + s  + '.png')
  plt.show()
  