#!/bin/bash
Terrains="hard_steps hard_high_jumps one_leg_hop"

declare -a Experiments=("base")
 
declare -a Arguments=("--folder b --initial_disturbance 500 --final_disturbance 500 --cur_decay exp --cur_num 3 --cur")

  
for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
        sbatch ./base.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
        # echo $terrain ${Experiments[$i]} ${Arguments[$i]}
    done
done


