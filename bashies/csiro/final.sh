Terrains="flat jumps gaps hard_stairs hard_steps"

declare -a Experiments=("stage1_stage2"
                        "no_stage1_stage2"
                        "no_stage1_no_stage2"
                        "stage1_no_stage2"
                        "continuous"
                        "no_exp"
                        "no_link"
                        "stage3"
                        "2000")
 

declare -a Arguments=("--folder final --cur"
                      "--folder final --cur --difficulty 10" 
                      "--folder final --difficulty 10" 
                      "--folder final --cur --cur_decay continuous_exp 0.9" 
                      "--folder final --cur --comparison no_exp" 
                      "--folder final --cur --comparison no_link" 
                      "--folder final --cur --initial_disturbance 500 --final_disturbance 2000" 
                      "--folder final --cur --initial_disturbance 2000 --final_disturbance 2000")

  
for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
        sbatch ./base.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
        # echo $terrain ${Experiments[$i]} ${Arguments[$i]}
    done
done

