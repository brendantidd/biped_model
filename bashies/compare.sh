#!/bin/bash
Terrains="flat jumps gaps stairs steps high_jumps"

declare -a Experiments=("no_cur_dif10"
                        "no_link"
                        "no_exp"
                        "no_stage1")
 
declare -a Arguments=("--folder b --initial_disturbance 500 --final_disturbance 500 --difficulty 10"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur_decay exp --cur_num 3 --cur --comparison no_link"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur_decay exp --cur_num 3 --cur --comparison no_exp"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur_decay exp --cur_num 3 --cur --difficulty 10")

  
for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
        sbatch ./base.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
        # echo $terrain ${Experiments[$i]} ${Arguments[$i]}
    done
done


