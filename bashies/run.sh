# Terrains="flat jumps gaps stairs steps high_jumps"
Terrains="jumps"

declare -a Experiments=("exp_1"
                        "exp_2"
                        "exp_3"
                        "exp_4"
                        "exp_5"
                        "exp_6"
                        "exp_7"
                        "adaptive")
 
 
declare -a Arguments=("--folder b --initial_disturbance 500 --final_disturbance 500 --cur --cur_decay exp --cur_num 1"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur --cur_decay exp --cur_num 2"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur --cur_decay exp --cur_num 3"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur --cur_decay exp --cur_num 4"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur --cur_decay exp --cur_num 5"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur --cur_decay exp --cur_num 6"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur --cur_decay exp --cur_num 7"
                      "--folder b --initial_disturbance 500 --final_disturbance 500 --cur --cur_decay adaptive")

  
for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
        sbatch ./base.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
        # echo $terrain ${Experiments[$i]} ${Arguments[$i]}
    done
done

bash compare.sh

bash hard.sh

