import os   
import sys
import psutil
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2
import json
import git
np.set_printoptions(precision=3, suppress=True)
import gc

def all_gather(data, name, rank, num_workers):
  all_d = MPI.COMM_WORLD.allgather(data)
  temp_d = np.concatenate(all_d)
  length = temp_d.shape[0]//num_workers
  start = rank*length
  end = start + length
  if name == 'done' and rank == 0:
    print("data shape", data.shape, [len(ad) for ad in all_d])
  if len(data.shape) == 1:
    return temp_d[start:end]
  else:
    return temp_d[start:end, ::]

def run(args):

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  num_workers = comm.Get_size()
  myseed = int(args.seed + 10000 * rank)
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    with open(PATH + 'commandline_args.txt', 'w') as f:
      f.write('Hash:')
      f.write(str(sha) + "\n")
      json.dump(args.__dict__, f, indent=2)
  else: 
    writer = None 

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from assets.env import Env
  from models.ppo import Model
  from models.dqn_adv import DQN
  if args.use_classifier:
    from models.classifier import Classifier
    classifier = Classifier(args, im_size=[48,48,1], dense_size=128, sess=sess) 
    classifier.load('./weights/classifier/')

  # obstacle_types = ['gaps','base','jumps','stairs','zero']
  # obstacle_types = ['flat','high_jumps', 'high_jumps_setup']
  obstacle_types = ['flat', args.obstacle_type, args.obstacle_type + '_setup']

  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False, multi=args.multi, dqn=True, difficulty=args.difficulty)

  pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for name in obstacle_types if name != 'zero'}  

  dqn = DQN(args.obstacle_type + "_dqn", env=env, ob_size=env.ob_size, ac_size=len(obstacle_types), im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=args.lr, eps_decay=args.eps_decay)


  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
  else:
    # WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
    WEIGHTS_PATH = './weights'
  
  for name in obstacle_types:
    if name == 'zero': continue
    # pol[name].load(WEIGHTS_PATH + '/' + name + '/')
    if 'setup' in name:
      pol[name].load_pol(base_name='flat', WEIGHTS_PATH='./weights/' + 'flat' + '/')
    else:
      pol[name].load_pol(base_name=name, WEIGHTS_PATH='./weights/' + name + '/')

  if args.test_pol:
    dqn.load(home + '/hpc-home/results/biped_model/latest/dqn/dqn_adv_baseline_reward/')
    
  initialize_uninitialized()
  sync_from_root(sess, dqn.vars, comm=comm)
  sync_from_root(sess, pol[args.obstacle_type + '_setup'].vars, comm=comm)

  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  tf.get_default_graph().finalize()

  prev_done = True
  prev_setup_done = True
  ob = env.reset()
  im = env.get_im()
  dqn_ob = ob
  dqn_im = im
  
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  setup_ep_ret = 0
  setup_ep_len = 0
  setup_ep_rets = []
  setup_ep_lens = []
  ep_steps = 0
  ep_count = 0
  # timesteps_so_far = 0
  stochastic = False

  vpred_data = []
  vpred_orig_data = []
  probs_data = []
  stable_vpred = []
  terrain_data = []
  current_pol_data = []
  baseline_data = []
  success = []

  vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
  # next_pol = prev_pol = current_pol = np.argmax(vpreds)
  desired_pol = prev_pol = current_pol = 0

  vpred = vpreds[current_pol]

  if args.render:
    replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)

  pols = [i for i in range(len(obstacle_types))]             
  box_cross_steps = None

  max_setup_length = 90
  _, exp_vpred, _, _ = pol[args.obstacle_type].step(ob, im, stochastic=False)  
  setup_done = False

  while True:
    if (args.just_setup and pol[args.obstacle_type + '_setup'].timesteps_so_far > args.max_ts) or (not args.just_setup and dqn.timesteps_so_far > args.max_ts):
      break 

    # ===================================================================================
    # Step env ==========================================================================
    # ===================================================================================
    
    if obstacle_types[current_pol] == args.obstacle_type + '_setup':
      act, setup_vpred, _, nlogp = pol[obstacle_types[current_pol]].step(ob, im, stochastic=True)
    else:
      act, _, _, _ = pol[obstacle_types[current_pol]].step(ob, im, stochastic=False)

    torques = act
    
    next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_im()
    dqn_next_ob = next_ob
    dqn_next_im = next_im
    # ===================================================================================

    terrain_data.append(env.z_offset)

    if args.render:
      replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
    
    # ===================================================================================
    # Setup stuff =======================================================================
    # ===================================================================================
    if obstacle_types[prev_pol] == args.obstacle_type + '_setup' and obstacle_types[current_pol] != args.obstacle_type + '_setup': 
      last_next_ob = next_ob
      last_next_im = next_im
      setup_done = True
    elif obstacle_types[current_pol] == args.obstacle_type + '_setup':
      last_next_ob = next_ob
      last_next_im = next_im
      setup_done = done

    _, exp_next_vpred, _, _ = pol[args.obstacle_type].step(next_ob, next_im, stochastic=False)  
    adv = rew + (1-done)*0.99*exp_next_vpred - exp_vpred
    setup_rew = (1 - min(adv**2, 1))*exp_vpred/100
    
    if setup_done:        
      if len(pol[args.obstacle_type + '_setup'].data['rew']) > 0:
        pol[args.obstacle_type + '_setup'].data['rew'][-1] += setup_rew
        setup_ep_ret += setup_rew
        # if args.debug and rank == 0: print(pol[args.obstacle_type + '_setup'].data['rew'][-1])
    elif obstacle_types[prev_pol] == args.obstacle_type + '_setup':
      setup_ep_ret += setup_rew
      setup_ep_len += 1
    # ===================================================================================

    # ===================================================================================
    # Terrain oracle ====================================================================
    # ===================================================================================
    if args.use_classifier:
      classifier_pol = classifier.step(im)
      # classifier_pol = classifier_desired_pol if classifier.obstacle_types[classifier_pol] in ['flat', args.obstacle_type] else 0
      if not box_cross_steps and args.obstacle_type == classifier.obstacle_types[classifier_pol]: 
        box_cross_steps = env.steps
    else:
      if not box_cross_steps and env.order[env.box_num] == args.obstacle_type:
        box_cross_steps = env.steps
    
    if args.use_classifier:
      # if box_cross_steps:
        # print(box_cross_steps, box_cross_steps + max_setup_length > env.steps)
      if box_cross_steps and box_cross_steps + max_setup_length > env.steps:
        desired_pol = 2
      else:
        # print(classifier_pol, classifier.obstacle_types[classifier_pol])
        desired_pol = classifier_pol if classifier.obstacle_types[classifier_pol] in ['flat', args.obstacle_type] else classifier.obstacle_types.index('flat')
    else:
      if not box_cross_steps or env.body_xyz[0] > env.box_info[1][7][0]: #TODO check other artifacts are at the 6th box
        desired_pol = 0
      elif box_cross_steps and box_cross_steps + max_setup_length > env.steps:
        desired_pol = 2
      elif box_cross_steps and env.body_xyz[0] <= env.box_info[1][7][0]:
        desired_pol = 1
    # ===================================================================================

    if args.baseline_reward:
      if current_pol == desired_pol:
        rew += 1.0
      elif current_pol != desired_pol:
        rew -= 1.0

    # ===================================================================================
    # Training stuff ====================================================================
    # ===================================================================================
    if not args.test_pol:
      if not args.just_dqn and obstacle_types[current_pol] == args.obstacle_type + '_setup':
        pol[args.obstacle_type + '_setup'].add_to_buffer([ob, im, act, setup_rew, prev_setup_done, setup_vpred, nlogp])
      if not args.just_setup:
        dqn.add_to_buffer([dqn_ob, dqn_im, current_pol, rew, prev_done, vpred])

    if not args.test_pol: 
      # pritn()
      if ep_steps % 2048 == 0 and ep_steps != 0:  

        # if rank == 0: 
          # print("data size bytes before", sys.getsizeof(pol[args.obstacle_type + '_setup'].data))
        successes = MPI.COMM_WORLD.allgather(success)
        successes = np.array([d for h in successes for d in h], dtype=np.float32)
        things = {'Avg_success':np.mean(successes)}
        if rank == 0:
          process = psutil.Process(os.getpid())
          # print("Total RAM usage Gb", num_workers*process.memory_info().rss/(1024.0 ** 3))  # in bytes 
          things["RAM"] = num_workers*process.memory_info().rss/(1024.0 ** 3)
        
        if not args.just_dqn:
          setup_data_lengths = MPI.COMM_WORLD.allgather(len(pol[args.obstacle_type + '_setup'].data['rew']))
          if (np.array(setup_data_lengths) > 1044).all():
            if args.debug and rank == 0: print(rank, "training setup, total steps", len(pol[args.obstacle_type + '_setup'].data['rew']), ep_steps)
            _, setup_next_vpred, _, _ = pol[args.obstacle_type + '_setup'].step(last_next_ob, last_next_im, stochastic=True)
            pol[args.obstacle_type + '_setup'].finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=setup_next_vpred, last_done=setup_done)  

            # if args.mem_test == 'smallest':
              # Take the smallest (throw away data)
            # min_length = np.min(setup_data_lengths)
            # for d in pol[args.obstacle_type + '_setup'].training_input:
            #   if len(pol[args.obstacle_type + '_setup'].data[d].shape) == 1:
            #     pol[args.obstacle_type + '_setup'].data[d] = pol[args.obstacle_type + '_setup'].data[d][:min_length, ]
            #   else:
            #     pol[args.obstacle_type + '_setup'].data[d] = pol[args.obstacle_type + '_setup'].data[d][:min_length, ::]
            #   if d == 'done' and rank == 0:
            #     print("data shape", pol[args.obstacle_type + '_setup'].data[d].shape, setup_data_lengths)
            # elif args.mem_test == 'large':
            # One call to allgather
            big_list = [pol[args.obstacle_type   + '_setup'].data[d] for d in pol[args.obstacle_type + '_setup'].training_input]
            all_big_lists = MPI.COMM_WORLD.allgather(big_list)          
            for i,d in enumerate(pol[args.obstacle_type   + '_setup'].training_input):
              all_d = []
              for n in range(num_workers):
                all_d.append(all_big_lists[n][i])
              temp_d = np.concatenate(all_d)
              length = temp_d.shape[0]//num_workers
              start = rank*length
              end = start + length
              if len(pol[args.obstacle_type + '_setup'].data[d].shape) == 1:
                pol[args.obstacle_type + '_setup'].data[d] = temp_d[start:end]
              else:
                pol[args.obstacle_type + '_setup'].data[d] = temp_d[start:end, ::]
              if d == 'done' and rank == 0:
                print("data shape", pol[args.obstacle_type + '_setup'].data[d].shape, [len(ad) for ad in all_d])
            big_list = []
            all_big_lists = []
            all_d = []
            temp_d = None
            # elif args.mem_test == 'small':
            #   # Lots of smaller calls to allgather
            #   for d in pol[args.obstacle_type + '_setup'].training_input:
            #     all_d = MPI.COMM_WORLD.allgather(pol[args.obstacle_type   + '_setup'].data[d])
            #     temp_d = np.concatenate(all_d)
            #     length = temp_d.shape[0]//num_workers
            #     start = rank*length
            #     end = start + length
            #     if len(pol[args.obstacle_type + '_setup'].data[d].shape) == 1:
            #       pol[args.obstacle_type + '_setup'].data[d] = temp_d[start:end]
            #     else:
            #       pol[args.obstacle_type + '_setup'].data[d] = temp_d[start:end, ::]
            #     if d == 'done' and rank == 0:
            #       print("data shape", pol[args.obstacle_type + '_setup'].data[d].shape, [len(ad) for ad in all_d])
            #   temp_d = None
            #   all_d = None
            
            if args.just_setup:
              pol[args.obstacle_type + '_setup'].log_stuff(things)

            setup_things = pol[args.obstacle_type + '_setup'].run_train(setup_ep_rets, setup_ep_lens, evaluate=args.just_setup)

            if args.debug and rank == 0: print(rank, "finished setup training")
            
            if not args.just_setup:
              setup_lrlocal = (setup_ep_lens, setup_ep_rets) # local values
              setup_listoflrpairs = MPI.COMM_WORLD.allgather(setup_lrlocal) # list of tuples
              setup_lens, setup_rews = map(flatten_lists, zip(*setup_listoflrpairs))
              setup_things['Setup Rews'] = np.mean(setup_rews)
              setup_things['Setup Lens'] = np.mean(setup_lens)
              for key in setup_things:
                things[key] = setup_things[key]
            else:
              success = []

            setup_ep_rets = []
            setup_ep_lens = []

        if not args.just_setup:
          if args.debug and rank == 0: print(rank, "training dqn, total steps", len(dqn.data['rew']),  ep_steps)
          vpred = np.max(dqn.get_vpreds(dqn_next_ob, dqn_next_im))
          dqn.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
          dqn.log_stuff(things)
          dqn.run_train(ep_rets,ep_lens)
          success = []
          if args.debug and rank == 0: print(rank, "finished dqn")
        ep_rets = []
        ep_lens = []

        # collected = gc.collect() 
        # if rank == 0: print("garbage collected", collected)
    # ===================================================================================

    current_pol_data.append(current_pol)
    baseline_data.append(desired_pol)

    dqn.train_model.get_eps()
    vpreds = dqn.get_vpreds(dqn_next_ob, dqn_next_im)   
    prev_pol = current_pol
    if args.test_pol:
      current_pol = np.argmax(vpreds)
    else:
      if args.prob:
        current_pol = np.random.choice(pols, p=abs(vpreds)/np.sum(abs(vpreds)))
      else:
        if np.random.random() < dqn.train_model.eps:
          current_pol = np.random.randint(len(obstacle_types))
        else:
          current_pol = np.argmax(vpreds)
        if args.force_pol:
          if current_pol != desired_pol:
            current_pol = prev_pol
        if args.use_expert or args.just_setup:
          current_pol = desired_pol

    vpred = vpreds[current_pol]
    exp_vpred = exp_next_vpred

    if obstacle_types[current_pol] == args.obstacle_type + '_setup':
      prev_setup_done = setup_done
    prev_done = done
    ob = next_ob
    im = next_im
    dqn_ob = dqn_next_ob
    dqn_im = dqn_next_im
    ep_ret += rew
    ep_len += 1
    ep_steps += 1

    # ====================================================================================================================
    # Done
    # ====================================================================================================================
    if done:    
      if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.x_min):
      # if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.body_xyz[0]) and env.steps >= env.max_steps - 2:
        label = True  
      else:
        label = False
      success.append(label)

      if args.plot or (rank == 0 and ep_count % 100 == 0):
        num_axes = 3
        fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
        axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)  
        
        colours = [np.array([0, 102, 204])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([51, 204, 51])/255.0, np.array([100, 204, 0])/255.0, np.array([200, 100, 51])/255.0]
        
        print(np.array(current_pol_data).shape, len(current_pol_data))
        axes[0].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[0].set_ylim([-0.9,len(obstacle_types) + 0.1])
        for s, n in enumerate(obstacle_types):
          axes[0].text(0,s*0.5+0.5,str(s) + ". " + n)
        axes[0].set_title("Current policy", loc='left')


        axes[1].plot([_ for _ in range(len(baseline_data))], baseline_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[1].set_ylim([-0.9,len(obstacle_types) + 0.1])
        for s, n in enumerate(obstacle_types):
          axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
        axes[1].set_title("baseline data", loc='left')

        axes[2].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[2].set_title("terrain height", loc='left')
        fig.tight_layout(pad=4.0)
        print(PATH + 'dqn.png')
        plt.savefig(PATH + 'dqn.png', bbox_inches='tight')

      vpred_data = []
      vpred_orig_data = []
      probs_data = []
      terrain_data = []
      current_pol_data = []
      baseline_data = []
      switch_data = []
      
      # if rank == 0:
      #   print(ep_len, env.steps, env.max_steps)
      
      ob = env.reset()
      im = env.get_im()
      dqn_ob = ob
      dqn_im = env.get_im()

      ep_rets.append(ep_ret)  
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
      setup_ep_rets.append(setup_ep_ret)  
      setup_ep_lens.append(setup_ep_len)     
      setup_ep_ret = 0
      setup_ep_len = 0
      ep_count += 1  

      box_cross_steps = None
      vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
      desired_pol = prev_pol = current_pol = 0
      vpred = vpreds[current_pol]
      _, exp_vpred, _, _ = pol[args.obstacle_type].step(ob, im, stochastic=False)  
      setup_done = False

if __name__ == '__main__':
  import defaults
  from dotmap import DotMap
  args1 = defaults.get_defaults() 
  parser = argparse.ArgumentParser()
  # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
  parser.add_argument('--folder', default='dqn_with_setup')
  parser.add_argument('--difficulty', default=10, type=int)
  parser.add_argument('--single_pol', default=True, action='store_false')
  parser.add_argument('--obstacle_type', default='high_jumps')
  parser.add_argument('--adv', default=True, action='store_false')
  parser.add_argument('--dqn', default=True, action='store_false')
  # parser.add_argument('--dual_dqn', default=True, action='store_false')
  parser.add_argument('--dual_dqn', default=False, action='store_true')
  parser.add_argument('--baseline_reward', default=True, action='store_false')
  parser.add_argument('--advantage2', default=False, action='store_true')
  parser.add_argument('--just_setup', default=False, action='store_true')
  parser.add_argument('--just_dqn', default=False, action='store_true')
  parser.add_argument('--use_classifier', default=False, action='store_true')
  parser.add_argument('--old_rew', default=True, action='store_false')
  parser.add_argument('--mem_test', default='smallest')

  args2, _ = parser.parse_known_args()
  args2 = vars(args2)
  for key in args2:
    args1[key] = args2[key]
  # print([isinstance(args1[key], DotMap()) for key in args1])
  # print([type(args1[key]) for key in args1])
  # exit()

  args = DotMap(args1)
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)