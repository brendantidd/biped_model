import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import json
import git
np.set_printoptions(precision=3, suppress=True)

def run(args):

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'
  
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)

  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    with open(PATH + 'commandline_args.txt', 'w') as f:
      f.write('Hash:')
      f.write(str(sha) + "\n")
      json.dump(args.__dict__, f, indent=2)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  if args.mocap:
    from assets.env_mocap import Env
  else:
    if args.new:
      from assets.env_new import Env
    # elif args.obstacle_type == 'high_jumps':
    #   from assets.env_hj import Env
    else:
      from assets.env import Env
  env = Env(render=args.render, PATH=PATH, args=args, cur=args.cur, obstacle_type=args.obstacle_type, control_type=args.control_type, display_im=args.display_im, vis=args.vis, speed_cur=args.speed_cur, comparison=args.comparison, initial_disturbance=args.initial_disturbance, final_disturbance=args.final_disturbance, vis_type=args.vis_type, difficulty=args.difficulty, dist_difficulty=args.dist_difficulty, num_artifacts=args.num_artifacts, early_stop=args.early_stop, dqn=args.dqn)
  
  eval_env = Env(render=args.render, PATH=PATH, args=args, cur=False, obstacle_type=args.obstacle_type, control_type=args.control_type, display_im=args.display_im, vis=args.vis, disturbances=False, vis_type=args.vis_type, difficulty=10, dist_difficulty=0, num_artifacts=7, MASTER=False)
  eval_env.eval = True
  # if args.eval_dist:
  #   eval_env.disturbances = True
  #   eval_env.max_disturbance = 2000
  # else:
  eval_env.disturbances = False
  eval_env.max_disturbance = 0

  if args.lstm_pol:
    from models.ppo_lstm import Model
    # from models.ppo_lstm_old import Model
  else:
    # if args.separate_vf:
    #   from models.ppo_vf import Model
    # else:
    #   from models.ppo import Model
    from models.ppo import Model


  pol = Model(args.obstacle_type, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
  
  initialize()
  sync_from_root(sess, pol.vars, comm=comm)
  if args.separate_vf:
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, vf_learning_rate=args.vf_lr, horizon=horizon)
  else:
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if args.stage3:
    pol.load_base(base_name=args.obstacle_type, WEIGHT_PATH=home + '/results/biped_model/latest/final_65/' + args.obstacle_type + '_stage1_stage2/')      
  elif args.test_pol:
    if args.hpc:
      # pol.load(home + '/hpc-home/results/biped_model/latest/' + args.exp + '/')      
      # pol.load(home + '/hpc-home/results/biped_model/latest/final_submitted2/' + args.obstacle_type + '_stage1_stage2/')      
      pol.load(home + '/hpc-home/results/biped_model/latest/final_submitted/' + args.exp + '/')      
      # pol.load_base(base_name=args.obstacle_type, WEIGHT_PATH=home + '/hpc-home/results/biped_model/latest/final_65/' + args.obstacle_type + '_stage1_stage2/')      
      # pol.load(home + '/hpc-home/results/biped_model/weights/w6/base_speed/')      
      # pol.load(home + '/hpc-home/results/biped_model/weights/w5/b/' + args.exp + '/')      
    else:
      # pol.load_base(base_name=args.obstacle_type, WEIGHT_PATH=home + '/results/biped_model/latest/final_65/' + args.obstacle_type + '_stage1_stage2/')      
      
      pol.load(home + '/results/biped_model/latest/' + args.exp + '/')      
      # pol.load('./weights/high_jumps/')      

      # pol.load(home + '/results/biped_model/latest/b/hj2/')      
      # pol.load(home + '/results/biped_model/weights/b10_vis/' + args.obstacle_type + '/')      

      # pol.load(PATH)

  if args.use_base:
    base = Model("base", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, vis=args.vis)
    if args.hpc:
      # pol.load_base(base, home + '/hpc-home/results/biped_model/weights/w6/base_speed/')      
      pol.load_base(base, home + '/hpc-home/results/biped_model/latest/' +  args.exp)      
    else:
      # pol.load_base(base, home + '/results/biped_model/weights/w5/b2/base_speed/')
      pol.load_base(base, home + '/results/biped_model/latest/b2/base/')      


  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()
  # print(env.ob_size)

  if args.test_pol:
    stochastic = False
  else:
    stochastic = True
  
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  ep_steps = 0
  total_steps = 0
  evaluate = False
  eval_next = False
  if not args.test_pol:
    prev_done = True
    # Evaluate the first run
    if args.eval_first:
      eval_next = True
      dists = []
      rews = []
      success = []
      t_eval = time.time()
      evaluate = True
      stochastic = False
      env.obstacles.remove_obstacles()
      ob = eval_env.reset()
      im = eval_env.get_im()
    else:
      ob = env.reset()
      im = env.get_im()
      if rank == 0:
        pol.writer.add_scalar("Eval_dists", 0, 0)
        pol.writer.add_scalar("Eval_succes", 0, 0)
        pol.writer.add_scalar("Eval_rews", 0, 0)
    if args.advantage2:
      act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
    
    while True:

      if eval_next and total_steps % 1000 == 0:
        all_dists = MPI.COMM_WORLD.allgather(dists)
        all_rews = MPI.COMM_WORLD.allgather(rews)
        all_success = MPI.COMM_WORLD.allgather(success)
        all_dists = [h for d in all_dists for h in d]
        all_rews = [h for d in all_rews for h in d]
        all_success = [h for d in all_success for h in d]
        # print(rank, len(all_dists))
        if len(all_dists) > 500:
          eval_next = False
          done = True
          if rank == 0:
            pol.writer.add_scalar("Eval_dists", np.mean(all_dists), pol.iters_so_far)
            pol.writer.add_scalar("Eval_succes", np.mean(all_success), pol.iters_so_far)
            pol.writer.add_scalar("Eval_rews", np.mean(all_rews), pol.iters_so_far)
            print("resuming training", len(all_dists), "evaluation took ", time.time() - t_eval, "eval dist", np.mean(all_dists))

      # if (pol.timesteps_so_far > pol.max_timesteps) or (args.early_stop and env.all_ready_to_stop):
      # if (pol.timesteps_so_far > pol.max_timesteps) or (args.stage3 and pol.iters_so_far > 500):
      if (pol.timesteps_so_far > pol.max_timesteps) or (args.stage3 and pol.iters_so_far > 200):
        if rank == 0:
          pol.writer.add_scalar("Total time", pol.timesteps_so_far / pol.max_timesteps, 0)
        break 
      if not args.advantage2:
        act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)

      torques = act

      if evaluate:
        next_ob, rew, done, _ = eval_env.step(torques)
        next_im = eval_env.get_im()
        act = eval_env.actions
      else:
        next_ob, rew, done, _ = env.step(torques)
        next_im = env.get_im()
        act = env.actions

      if args.advantage2:
        next_act, next_vpred, _, next_nlogp = pol.step(next_ob, next_im, stochastic=stochastic)

      if not args.test_pol and not evaluate:
        if args.advantage2:
          # _, next_vpred, _, _ = pol.step(ob, im, stochastic=stochastic)
          pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp, next_vpred])
        else:
          pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])
      
      prev_done = done
      ob = next_ob
      im = next_im
      ep_ret += rew
      ep_len += 1

      if not evaluate:
        ep_steps += 1
      
      total_steps += 1

      if args.advantage2:
        vpred = next_vpred
        act = next_act
        nlogp = next_nlogp

      if not args.test_pol and not args.expert and not evaluate and ep_steps % horizon == 0:
        if not args.advantage2:
          _, next_vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
        pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=next_vpred, last_done=done)  
        pol.run_train(ep_rets,ep_lens)
        ep_rets = []
        ep_lens = []
        if args.vf_only and np.random.random() < 0.01:
          env.obstacle_type = 'mix'
        else:
          env.obstacle_type = args.obstacle_type
        
        if args.eval and not eval_next and ep_steps % (horizon*50) == 0: 
        # if args.eval and not eval_next and ep_steps % (horizon*100) == 0: 
          # if args.eval and total_steps % 20000 == 0 and total_steps != 0: 
          eval_next = True
          dists = []
          rews = []
          success = []
          t_eval = time.time()
          # if rank == 0:
          #   print(rank, "evaluating")

      if evaluate and eval_env.steps > 3000:
        done = True

      if done:      
        if not evaluate:
          ep_rets.append(ep_ret)  
          ep_lens.append(ep_len)  
        else:
          if eval_env.obstacle_type in ['high_jumps', 'hard_high_jumps'] and eval_env.x_min > (eval_env.box_info[1][-1][0] +  eval_env.box_info[2][-1][0]) and eval_env.steps >= eval_env.max_steps - 2:
            success.append(True)
          elif eval_env.obstacle_type in ['hard_high_jumps'] and eval_env.x_min > (eval_env.box_info[1][-1][0] +  eval_env.box_info[2][-1][0]) and eval_env.steps >= eval_env.max_steps - 2:
            success.append(True)
          elif args.obstacle_type == 'flat':
            success.append(eval_env.box_num == (len(eval_env.box_info[1]) - 1))
          else:
            success.append(eval_env.x_min > (eval_env.box_info[1][-1][0] -  eval_env.box_info[2][-1][0]))
          if args.obstacle_type == 'flat':
            dists.append(eval_env.box_num / (len(eval_env.box_info[1]) - 1))
          else:
            max_x = (eval_env.box_info[1][-1][0] +  eval_env.box_info[2][-1][0])
            min_x = (eval_env.box_info[1][0][0] -  eval_env.box_info[2][0][0])
            dists.append((eval_env.x_min - min_x)/(max_x - min_x))
          rews.append(ep_ret)

        if eval_next:
          evaluate = True
          stochastic = False
          env.obstacles.remove_obstacles()
          ob = eval_env.reset()
          im = eval_env.get_im()
        else:
          evaluate = False
          stochastic = True
          eval_env.obstacles.remove_obstacles()
          ob = env.reset()
          im = env.get_im()
        ep_ret = 0
        ep_len = 0   

        if args.advantage2:
          act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)

  if not args.stage3:
    # if args.eval:
    # pol.load(PATH + 'best/')      
    if args.eval_dist:
    # env.disturbances = False
      env.disturbances = True
      # env.max_disturbance = 1500
      env.max_disturbance = 2000
    else:
      env.disturbances = False
      env.max_disturbance = 0
    
    # env.max_disturbance = 2000
    eval_length = 500
    # eval_length = 200
    env.cur = False

    env.difficulty = args.difficulty
    # env.difficulty = 10
    # env.height_coeff = 0.07
    
    # if args.obstacle_type in ['high_jumps', 'hard_high_jumps', 'one_leg_hop']:
    #   env.num_artifacts = 1
    # else:
    env.num_artifacts = 7
      # env.args.num_artifacts = 2
    env.max_steps = 10000
    env.eval = True
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)
    ob = env.reset()
    im = env.get_im()
    ep_len = 0
    dists = []
    rews = []
    success = []
    all_dists = []
    # all_success = []
    av_ep_vel = []
    t1 = time.time()
    t2 = time.time()
    while True:

      torques, vpred, _, nlogp = pol.step(ob, im, stochastic=False)
      next_ob, rew, done, _ = env.step(torques)
      next_im = env.get_im()
      ob = next_ob
      im = next_im
      ep_ret += rew
      ep_len += 1       
      if ep_len % 2000 == 0:
        all_dists = MPI.COMM_WORLD.allgather(dists)
        all_rews = MPI.COMM_WORLD.allgather(rews)
        all_success = MPI.COMM_WORLD.allgather(success)
        all_dists = [h for d in all_dists for h in d]
        all_rews = [h for d in all_rews for h in d]
        all_success = [h for d in all_success for h in d]
        if rank == 0:
          # print(all_dists)
          # print(all_rews)
          print(len(dists), len(all_dists), len(rews), len(all_rews), len(success), len(all_success), env.max_disturbance)
          # print("Dists {0:.4f} Rews {1:.4f} Success {2:.4f} Av Vel {3:0.4f} Time {4:.4f} ".format(np.mean(all_dists), np.mean(all_rews), np.mean(all_success), np.mean(av_ep_vel), time.time  () - t1))
          
          print("Dists {0:.4f} Rews {1:.4f} Success {2:.4f} Time {3:.4f} ".format(np.mean(all_dists), np.mean(all_rews), np.mean(all_success), time.time  () - t1))
          t1 = time.time()
        if len(all_dists) > eval_length:
          break 
      if env.steps > 100:
        av_ep_vel.append(env.vx)
      # if env.box_info[1][-1][0] < env.body_xyz[0]:
      #   done = True
      if done:
        if env.steps > 9000:
          print()
          print("!!!!!!!!!!!!!!!!OVER 9000!!!!!!!!!!!!!!!!!!!!!!!1")
          print()
          env.save_sim_data(tail='_9000')
          print("saving sim data, succes: ", env.x_min > (env.box_info[1][-1][0] -  env.box_info[2][-1][0]), env.x_min/(env.box_info[1][-1][0]))

          # break
        if env.obstacle_type in ['high_jumps', 'hard_high_jumps'] and env.x_min > (env.box_info[1][6][0] +  env.box_info[2][6][0]) and env.steps >= env.max_steps - 2:
          success.append(True)
        elif env.obstacle_type in ['hard_high_jumps'] and env.x_min > (env.box_info[1][8][0] +  env.box_info[2][8][0]) and env.steps >= env.max_steps - 2:
          success.append(True)
        elif args.obstacle_type == 'flat':
          success.append(env.box_num == (len(env.box_info[1]) - 1))
        else:
          success.append(env.x_min > (env.box_info[1][-1][0] -  env.box_info[2][-1][0]))
        # dists.append(env.x_min/(env.box_info[1][-1][0] -  env.box_info[2][-1][0]))
        if args.obstacle_type == 'flat':
          dists.append(env.box_num / (len(env.box_info[1]) - 1))
        else:
          max_x = (env.box_info[1][-1][0] +  env.box_info[2][-1][0])
          min_x = (env.box_info[1][0][0] -  env.box_info[2][0][0])
          dists.append((env.x_min - min_x)/(max_x - min_x))
        rews.append(ep_ret)
        myseed += 1
        np.random.seed(myseed)
        random.seed(myseed)
        tf.set_random_seed(myseed)
        ob = env.reset()
        im = env.get_im()
        ep_rets.append(ep_ret)  
        ep_lens.append(ep_len)     
        ep_ret = 0
        av_ep_vel = []
    all_dists = MPI.COMM_WORLD.allgather(dists)
    all_rews = MPI.COMM_WORLD.allgather(rews)
    all_success = MPI.COMM_WORLD.allgather(success)
    if rank == 0:
      all_dists = [h for d in all_dists for h in d]
      all_rews = [h for d in all_rews for h in d]
      all_success = [h for d in all_success for h in d]
      print("-------- FINAL: ----------- ", args.obstacle_type)
      print("Dists {0:.4f} Rews {1:.4f} Success {2:.4f} Time {3:.4f} ".format(np.mean(all_dists), np.mean(all_rews), np.mean(all_success), time.time() - t2))
      print("-------------------------------------- ")
      print("experiment: ", args.obstacle_type)
      # print(args.__dict__)
      print(PATH)
      print(len(all_dists), len(all_rews), len(all_success))
      results = {'dists': np.mean(all_dists), 'rewards': np.mean(all_rews), 'success': np.mean(all_success)}
      with open(PATH + 'results.txt', 'w') as f:
        json.dump(results, f, indent=2)
      writer.add_scalar("results_dists", np.mean(all_dists), 1)
      writer.add_scalar("results_rews", np.mean(all_rews), 1)
      writer.add_scalar("results_success", np.mean(all_success), 1)
      writer.close()

if __name__ == '__main__':
  # My hack for loading defaults, but still accepting run specific defaults
  import defaults
  from dotmap import DotMap
  args1 = defaults.get_defaults() 
  parser = argparse.ArgumentParser()
  # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
  parser.add_argument('--folder', default='b')
  parser.add_argument('--difficulty', default=1, type=int)
  parser.add_argument('--obstacle_type', default='high_jumps')
  args2, _ = parser.parse_known_args()
  args2 = vars(args2)
  for key in args2:
    args1[key] = args2[key]
  args = DotMap(args1)
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)
  # parser = argparse.ArgumentParser()

  # # parser.add_argument('--cur_len', default=1000, type=int)
  # parser.add_argument('--cur_decay', default='exp')
  # parser.add_argument('--decay_rate', default=1, type=int)
  # # parser.add_argument('--decay', default=0.85, type=float)
  # parser.add_argument('--decay', default=0.65, type=float)
  # # parser.add_argument('--cur_local', default=False, action='store_true')
  # # parser.add_argument('--yu', default=True, action='store_false')
  # parser.add_argument('--dual_value', default=False, action='store_true')
  # parser.add_argument('--dqn', default=False, action='store_true')
  # parser.add_argument('--eval_first', default=False, action='store_true')
  # parser.add_argument('--std_clip', default=False, action='store_true')
  # parser.add_argument('--yu', default=False, action='store_true')
  # parser.add_argument('--more_power', default=1, type=float)
  # # parser.add_argument('--more_power', default=1.5, type=float)
  # # parser.add_argument('--dist_off_ground', default=False, action='store_true')
  # parser.add_argument('--dist_off_ground', default=True, action='store_true')
  # parser.add_argument('--rand_Kp', default=False, action='store_true')
  # # parser.add_argument('--stair_thing', default=False, action='store_true')
  # parser.add_argument('--stair_thing', default=True, action='store_false')
  # # parser.add_argument('--early_stop', default=False, action='store_true')
  # parser.add_argument('--early_stop', default=True, action='store_false')
  # parser.add_argument('--cur_local', default=True, action='store_false')
  # parser.add_argument('--cur_len', default=1200, type=int)
  # parser.add_argument('--cur_num', default=3, type=int)
  # parser.add_argument('--show_detection', default=False, action='store_true')
  # parser.add_argument('--num_artifacts', default=2, type=int)
  
  # parser.add_argument('--inc', default=1, type=int)
  # parser.add_argument('--camera_rate', default=6, type=int)
  #   # parser.add_argument('--terrain_first', default=False, action='store_true')
  # parser.add_argument('--terrain_first', default=True, action='store_false')
  # parser.add_argument('--advantage2', default=True, action='store_false')
  # # parser.add_argument('--advantage2', default=False, action='store_true')
  # parser.add_argument('--include_actions', default=False, action='store_true')
  # parser.add_argument('--const_std', default=False, action='store_true')
  # # parser.add_argument('--const_std', default=True, action='store_false')
  # parser.add_argument('--const_lr', default=False, action='store_true')
  # # parser.add_argument('--const_lr', default=True, action='store_false')
  # parser.add_argument('--single_pol', default=False, action='store_true')
  # parser.add_argument('--comparison', default=None)
  # parser.add_argument('--use_roa', default=False, action='store_true')
  # parser.add_argument('--baseline', default=False, action='store_true')
  # parser.add_argument('--rand_flat', default=False, action='store_true')
  # parser.add_argument('--new', default=False, action='store_true')
  # # parser.add_argument('--box_pen', default=True, action='store_false')
  # parser.add_argument('--box_pen', default=False, action='store_true')
  # parser.add_argument('--eval', default=True, action='store_false')
  # # parser.add_argument('--eval', default=False, action='store_true')
  # parser.add_argument('--eval_dist', default=False, action='store_true')
  # parser.add_argument('--dist_inc', default=0, type=int)
  # parser.add_argument('--initial_disturbance', default=500, type=int)
  # # parser.add_argument('--initial_disturbance', default=200, type=int)
  # parser.add_argument('--final_disturbance', default=500, type=int)
  # parser.add_argument('--difficulty', default=1, type=int)
  # parser.add_argument('--dist_difficulty', default=0, type=int)

  # parser.add_argument('--folder', default='b')

  # parser.add_argument('--nicks', default=False, action='store_true')
  # parser.add_argument('--vf_only', default=False, action='store_true')
  # parser.add_argument('--speed_cur', default=False, action='store_true')
  # parser.add_argument('--expert', default=False, action='store_true')
  # parser.add_argument('--use_base', default=False, action='store_true')
  # parser.add_argument('--separate_vf', default=False, action='store_true')
  # parser.add_argument('--display_doa', default=False, action='store_true')
  # parser.add_argument('--act', default=False, action='store_true')
  # parser.add_argument('--forces', default=False, action='store_true')
  # parser.add_argument('--render', default=False, action='store_true')
  # parser.add_argument('--hpc', default=False, action='store_true')
  # parser.add_argument('--lstm_pol', default=False, action='store_true')
  # parser.add_argument('--test_pol', default=False, action='store_true')
  # # parser.add_argument('--vis', default=False, action='store_true')
  # parser.add_argument('--vis', default=True, action='store_false')
  # parser.add_argument('--vis_type', default="depth")
  # parser.add_argument('--cur', default=False, action='store_true')
  # parser.add_argument('--mocap', default=False, action='store_true')
  # parser.add_argument('--display_im', default=False, action='store_true')
  # parser.add_argument('--stage3', default=False, action='store_true')

  # parser.add_argument('--exp', default="test")
  # # parser.add_argument('--obstacle_type', default="base", help="flat, stairs, path, jump")
  # parser.add_argument('--obstacle_type', default="None", help="flat, stairs, path, jump")
  # parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  # parser.add_argument('--seed', default=42, type=int)
  # parser.add_argument('--max_ts', default=5e7, type=int)
  # # parser.add_argument('--max_ts', default=10e7, type=int)
  # # parser.add_argument('--max_ts', default=20e7, type=int)
  # # parser.add_argument('--max_ts', default=30e7, type=int)
  # parser.add_argument('--lr', default=3e-4, type=float)
  # # parser.add_argument('--vf_lr', default=1e-2, type=float)
  # parser.add_argument('--vf_lr', default=3e-4, type=float)
  # args = parser.parse_args()

  # os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  # run(args)


  # parser = ArgumentParser()
  # args = parser.parse_args()
  # with open('commandline_args.txt', 'r') as f:
  #     args.__dict__ = json.load(f)


  # HYPERS = collections.OrderedDict()
  # def arg(tag, default):
  #   HYPERS[tag] = type(default)((sys.argv[sys.argv.index(tag)+1])) if tag in sys.argv else default
  #   return HYPERS[tag]