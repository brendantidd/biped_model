#!/bin/bash
Terrains="flat jumps gaps stairs steps high_jumps"

declare -a Experiments=("base2")
 
declare -a Arguments=("--folder b --cur --initial_disturbance 500 --final_disturbance 2500 --max_ts 40000000")

  
# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         sbatch ./base.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#         # echo $terrain ${Experiments[$i]} ${Arguments[$i]}
#     done
# done

Terrains="hard_steps hard_high_jumps one_leg_hop"

declare -a Experiments=("base2")
 
declare -a Arguments=("--folder b --cur --initial_disturbance 500 --final_disturbance 2500 --max_ts 60000000")
  
# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         sbatch ./base.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#         # echo $terrain ${Experiments[$i]} ${Arguments[$i]}
#     done
# done
