import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2

def run(args):

  PATH = home + '/results/biped_model/latest/dqn/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  # myseed = int(time.time() + 10000 * rank)
  myseed = int(args.seed + 10000 * rank)
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from assets.env import Env
  from models.ppo_vf import Model
  if args.adv:
    from models.dqn_adv import DQN
  else:
    from models.dqn import DQN

  obstacle_types = ['gaps','base','jumps','stairs','zero']

  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False, multi=args.multi, dqn=True)


  pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis) for name in obstacle_types if name != 
  'zero'}  

  if args.no_state:
    ob_size = 1
  else:
    ob_size = env.ob_size
  if args.use_boxes:
    im_size = [20]
  else:
    im_size = [60,40,1]
  dqn = DQN("dqn", env=env, ob_size=ob_size+len(obstacle_types), ac_size=len(obstacle_types), im_size=im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=args.lr, eps_decay=args.eps_decay)

  initialize()
  sync_from_root(sess, dqn.vars, comm=comm)
  if not args.adv:
    dqn.update_target()

  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
  else:
    WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
  
  for name in obstacle_types:
    if name == 'zero': continue
    pol[name].load(WEIGHTS_PATH + '/' + name + '/')

  if args.test_pol:
    dqn.load(home + '/hpc-home/results/biped_model/latest/dqn/dqn_adv_baseline_reward/')
    # dqn.load(home + '/results/biped_model/latest/dqn/longer_exp2/')


  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()

  prev_done = True

  env.obstacle_type = 'mix'
  env.difficulty = args.difficulty
  env.height_coeff = args.height_coeff
  prev_done = True
  ob = env.reset()
  im = env.get_im()
  desired_pol = obstacle_types.index(env.order[env.box_num])
  if args.no_state:
    # dqn_ob = np.array([desired_pol])
    dqn_ob = np.array([0])
    # dqn_ob[-1] = desired_pol 
    desired = np.zeros(len(obstacle_types))
    desired[desired_pol] = 1
    dqn_ob = np.concatenate([dqn_ob, desired])
    print(dqn_ob.shape)
    # dqn_im = np.array([0])
    if args.use_boxes:
      boxes = []
      for box in env.box_nums:
        info = []
        if (env.box_num + box < 1) or (env.box_num + box > len(env.box_info[0]) - 1):
          info.append(0)
        else:
          info.append(env.body_xyz[2] - (env.box_info[1][env.box_num+box][2] + env.box_info[2][env.box_num+box][2]))
        info.append(env.body_xyz[0] - (env.box_info[1][env.box_num+box][0] + env.box_info[2][env.box_num+box][0]))
        info.append(env.body_xyz[1])
        info.append(env.yaw)
        boxes.extend(info)
      dqn_im = np.array(boxes)
    else:
      dqn_im = env.get_im()
  else:

    dqn_ob = ob
    desired = np.zeros(len(obstacle_types))
    desired[desired_pol] = 1
    dqn_ob = np.concatenate([dqn_ob, desired])
    # print(dqn_ob.shape)
    # dqn_im = im
    dqn_im = env.get_im()

  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  ep_steps = 0
  ep_count = 0
  # timesteps_so_far = 0
  stochastic = False

  vpred_data = []
  vpred_orig_data = []
  probs_data = []
  stable_vpred = []
  terrain_data = []
  current_pol_data = []
  baseline_data = []
  stable_count = 120

  eval_length = 20

  vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
  current_pol = np.argmax(vpreds)
  vpred = vpreds[current_pol]

  if args.render:
    replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)

  pols = [i for i in range(len(obstacle_types))]             

  while True:
    if dqn.timesteps_so_far > args.max_ts:
      break 
      
    if obstacle_types[current_pol] == 'zero':
      zero_ob = copy.copy(ob)
      zero_ob[-2] = 0.0
      act, _, _, _ = pol['base'].step(zero_ob, im, stochastic=False)
    else:
      act, _, _, _ = pol[obstacle_types[current_pol]].step(ob, im, stochastic=False)
  
    if args.render:
      replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
    
    terrain_data.append(env.z_offset)

    torques = act
    
    next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_im()

    # if env.box_num > len(env.order) - 2:
    #   desired_pol = obstacle_types.index('zero')
    # else:
    desired_pol = obstacle_types.index(env.order[env.box_num])
    if args.baseline_reward:
      if current_pol == desired_pol:
        rew += 1.0
      elif current_pol != desired_pol:
        rew -= 1.0
    elif args.exp_rew:
      # rew = 1
      # rew = env.body_xyz[0]/19.8
      if current_pol == desired_pol:
        rew = 1 + 0.2
      elif current_pol != desired_pol:
        rew = 1 - 0.2

    if args.no_state:
      # dqn_ob = np.array([0])
      dqn_next_ob = np.array([0])
      desired = np.zeros(len(obstacle_types))
      desired[desired_pol] = 1
      dqn_next_ob = np.concatenate([dqn_next_ob, desired])
      # dqn_ob = np.array([desired_pol])
      # dqn_next_ob = np.array([desired_pol])
      # dqn_im = np.array([0])
      # dqn_im = np.array([0])
      # dqn_im = im
      # dqn_im = dqn_next_im
      if args.use_boxes:
        boxes = []
        for box in env.box_nums:
          info = []
          if (env.box_num + box < 1) or (env.box_num + box > len(env.box_info[0]) - 1):
            info.append(0)
          else:
            info.append(env.body_xyz[2] - (env.box_info[1][env.box_num+box][2] + env.box_info[2][env.box_num+box][2]))
          info.append(env.body_xyz[0] - (env.box_info[1][env.box_num+box][0] + env.box_info[2][env.box_num+box][0]))
          info.append(env.body_xyz[1])
          info.append(env.yaw)
          boxes.extend(info)
        dqn_next_im = np.array(boxes)
      else:
        dqn_next_im = env.get_im()

      # dqn_next_im = np.array([0])
      # dqn_next_im = next_im
      # dqn_ob = np.array([0])
      # dqn_next_ob = np.array([0])
    else:
      # dqn_ob = ob
      dqn_next_ob = next_ob
      desired = np.zeros(len(obstacle_types))
      desired[desired_pol] = 1
      dqn_next_ob = np.concatenate([dqn_next_ob, desired])
      # dqn_im = im
      # dqn_next_im = next_im
      dqn_next_im = env.get_im()

    # # print(dqn_next_im.dtype)
    # image = (dqn_next_im*255.0).astype(np.uint8)
    # # norm_hm = np.array(self.hm*255.0, dtype=np.uint8)
    # cv2.imshow("thing", image)
    # cv2.waitKey(1)
    # env.display()

    if not args.test_pol:
      if args.adv:
        dqn.add_to_buffer([dqn_ob, dqn_im, current_pol, rew, prev_done, vpred])
      else:
        dqn.buffer.push(dqn_ob, dqn_im, current_pol, dqn_next_ob, dqn_next_im, rew, done)

    prev_done = done
    ob = next_ob
    im = next_im
    ep_ret += rew
    ep_len += 1
    ep_steps += 1
    if not args.test_pol:
      if args.adv:
        if ep_steps % 2048 == 0:
          vpred = np.max(dqn.get_vpreds(dqn_next_ob, dqn_next_im))
          dqn.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
          dqn.run_train(ep_rets,ep_lens)
          ep_rets = []
          ep_lens = []

      elif ep_steps > 128:
        if ep_steps % 10 == 0:
          dqn.run_train(ep_rets,ep_lens)

      
        if ep_steps % 2048 == 0:
          dqn.evaluate(ep_rets, ep_lens)
          ep_rets = []
          ep_lens = []

        if ep_steps % 2048 == 0:
          dqn.update_target()

    current_pol_data.append(current_pol)
    baseline_data.append(desired_pol)

    dqn.train_model.get_eps()
    # if env.steps % 5 == 0:
    # if env.steps % 1 == 0:
    vpreds = dqn.get_vpreds(dqn_next_ob, dqn_next_im)
    # if args.adv:
    #   current_pol = np.random.choice(pols, p=abs(vpreds)/np.sum(abs(vpreds)))
    # else:      
    prev_pol = current_pol
    if args.test_pol:
      current_pol = np.argmax(vpreds)
    else:
      if args.prob:
        current_pol = np.random.choice(pols, p=abs(vpreds)/np.sum(abs(vpreds)))
      else:
        if np.random.random() < dqn.train_model.eps:
          current_pol = np.random.randint(len(obstacle_types))
        else:
          current_pol = np.argmax(vpreds)
        if args.force_pol:
          if current_pol != desired_pol:
            current_pol = prev_pol
    vpred = vpreds[current_pol]

    dqn_ob = dqn_next_ob
    dqn_im = dqn_next_im

    # env.display()

    # if np.random.random() > 0.5:
    #   current_pol = obstacle_types.index(env.order[env.box_num])

    if done:    

      if args.plot or (rank == 0 and ep_count % 100 == 0):
        num_axes = 3
        fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
        axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)  
        
        colours = [np.array([0, 102, 204])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([51, 204, 51])/255.0, np.array([100, 204, 0])/255.0, np.array([200, 100, 51])/255.0]
        
        print(np.array(current_pol_data).shape, len(current_pol_data))
        axes[0].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[0].set_ylim([-0.9,len(obstacle_types) + 0.1])
        for s, n in enumerate(obstacle_types):
          axes[0].text(0,s*0.5+0.5,str(s) + ". " + n)
        axes[0].set_title("Current policy", loc='left')


        axes[1].plot([_ for _ in range(len(baseline_data))], baseline_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[1].set_ylim([-0.9,len(obstacle_types) + 0.1])
        for s, n in enumerate(obstacle_types):
          axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
        axes[1].set_title("baseline data", loc='left')

        axes[2].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[2].set_title("terrain height", loc='left')
        fig.tight_layout(pad=4.0)
        print(PATH + 'dqn.png')
        plt.savefig(PATH + 'dqn.png', bbox_inches='tight')

      vpred_data = []
      vpred_orig_data = []
      probs_data = []
      terrain_data = []
      current_pol_data = []
      baseline_data = []
      switch_data = []
      
      ob = env.reset()
      im = env.get_im()
      desired_pol = obstacle_types.index(env.order[env.box_num])
      if args.no_state:
        # dqn_ob = np.array([desired_pol])
        dqn_ob = np.array([0])
        desired = np.zeros(len(obstacle_types))
        desired[desired_pol] = 1
        dqn_ob = np.concatenate([dqn_ob, desired])
        # dqn_im = np.array([0])
        if args.use_boxes:
          boxes = []
          for box in env.box_nums:
            info = []
            if (env.box_num + box < 1) or (env.box_num + box > len(env.box_info[0]) - 1):
              info.append(0)
            else:
              info.append(env.body_xyz[2] - (env.box_info[1][env.box_num+box][2] + env.box_info[2][env.box_num+box][2]))
            info.append(env.body_xyz[0] - (env.box_info[1][env.box_num+box][0] + env.box_info[2][env.box_num+box][0]))
            info.append(env.body_xyz[1])
            info.append(env.yaw)
            boxes.extend(info)
          dqn_im =np.array(boxes)
        else:
          dqn_im = env.get_im()
      else:
        dqn_ob = ob
        desired = np.zeros(len(obstacle_types))
        desired[desired_pol] = 1
        dqn_ob = np.concatenate([dqn_ob, desired])

        # dqn_im = im
        dqn_im = env.get_im()

      ep_rets.append(ep_ret)  
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
      ep_count += 1  
      
      vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
      current_pol = np.argmax(vpreds)
      vpred = vpreds[current_pol]


if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--sub_folder', default='b')
  parser.add_argument('--folder', default='b10_vis')

  parser.add_argument('--difficulty', default=10, type=int)
  parser.add_argument('--height_coeff', default=0.07)

  parser.add_argument('--cur_len', default=1000, type=int)
  parser.add_argument('--inc', default=2, type=int)

  parser.add_argument('--force_pol', default=False, action='store_true')
  parser.add_argument('--use_boxes', default=False, action='store_true')
  parser.add_argument('--no_state', default=False, action='store_true')
  parser.add_argument('--exp_rew', default=False, action='store_true')
  parser.add_argument('--base_before', default=False, action='store_true')
  parser.add_argument('--single_pol', default=False, action='store_true')
  parser.add_argument('--no_reg', default=False, action='store_true')
  parser.add_argument('--adv', default=False, action='store_true')

  parser.add_argument('--baseline_reward', default=False, action='store_true')
  parser.add_argument('--prob', default=False, action='store_true')

  # parser.add_argument('--baseline', default=False, action='store_true')
  parser.add_argument('--baseline', default=True, action='store_false')
  parser.add_argument('--switch', default=False, action='store_true')
  parser.add_argument('--exp_switch', default=False, action='store_true')
  parser.add_argument('--use_roa', default=False, action='store_true')

  parser.add_argument('--use_vf', default=False, action='store_true')
  parser.add_argument('--multi', default=True, action='store_false')
  parser.add_argument('--goal_set', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')
  parser.add_argument('--gae', default=False, action='store_true')
  parser.add_argument('--doa', default=True, action='store_false')
  # parser.add_argument('--doa', default=False, action='store_true')

  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--vis', default=False, action='store_true')
  # parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="mix", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=60e6, type=int)
  parser.add_argument('--eps_decay', default=2000, type=int)
  # parser.add_argument('--max_ts', default=20e6, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  # parser.add_argument('--lr', default=3e-5, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)