import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
import pybullet as p
import cv2

def run(args):

  if args.vis:
    args.folder += "_vis"

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from assets.env import Env

  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False)

  from models.ppo_vf import Model
  from models.doa import RoANet
  
  # obstacle_types = ['jumps', 'base']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs','turn','zero']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
  obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
  # obstacle_types = ['zero', 'base']
  start_obstacle_types = ['gaps', 'base', 'jumps','stairs']
  if args.obstacle_type != 'zero':
    start_obstacle_types.remove(args.obstacle_type)

  if args.multi:
    pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}  
    if args.use_vf:
      vf = {name:Model(name + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types}
    else:
      vf = {name:Model(name + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}
    if args.use_roa:
      # roa = {name:RoANet(name, args=args, ob_size=env.ob_size, im_size=env.im_size[0]) for name in ['gaps','jumps','stairs']}  
      roa = {name:RoANet(name, args=args, ob_size=env.ob_size, im_size=env.im_size[0]) for name in ['jumps','gaps','stairs']}  
    if args.use_goal_set:
      gs = {name:RoANet(name, args=args, ob_size=env.ob_size, im_size=env.im_size[0]) for name in ['jumps','gaps','stairs']}  
  else:
    pol = Model(args.exp, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
    vf = Model(args.exp + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  
  initialize()

  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
  else:
    WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
  
  if args.multi:
    for name in obstacle_types:
      if args.use_vf:
        if rank == 0:
          print("loading vf from ", home + '/results/biped_model/latest/vf5/' + name + '/')
        vf[name].load(home + '/results/biped_model/latest/vf5/' + name + '/') 
        if name == 'zero': continue
        pol[name].load(WEIGHTS_PATH + '/' + name + '/')
      else:
        if name == 'zero': continue
        pol[name].load(WEIGHTS_PATH + '/' + name + '/')
        vf[name].load_base(pol[name].name, WEIGHTS_PATH + '/' + name + '/') 
      if args.use_roa and name in ['jumps','stairs','gaps']:
        # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '/best/')
        # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_small/best/')
        # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_small_no_reg/best/')
        # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_goal_set2/best/')
        roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_new_no_reg/best/')
      if args.use_goal_set and name in ['jumps','stairs','gaps']:
        gs[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_goal_set/best/')
  else:
    pol.load(WEIGHTS_PATH + '/' + args.exp + '/')
    vf.load_base(pol.name, WEIGHTS_PATH + '/' + args.exp + '/') 


  env.difficulty = args.difficulty
  env.height_coeff = args.height_coeff

  means = {}
  stds = {}
  if args.render:
    eval_length = 100
  else:
    eval_length = 10000
  # eval_length = 1000

  # for current_pol in obstacle_types:  
  # if current_pol == 'zero':
  #   env.obstacle_type = 'base'
  # else:
  #   env.obstacle_type = current_pol
  # data_length = 100000
  if args.goal_set or args.full:
    data_length = 100000
  else:
    # data_length = 5000
    data_length = 10000

  prev_done = True
  ob = env.reset()
  im = env.get_im()
  input_ob = np.zeros([data_length, env.ob_size])
  input_im = np.zeros([data_length] + env.im_size)
  labels = np.zeros([data_length, 1])
  data_pointer = 0

  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  ep_steps = 0
  timesteps_so_far = 0
  stochastic = False

  vpred_data = []
  stable_vpred = []
  terrain_data = []
  stable_count = 120
  episode_count = 0
  switch_step = None
  if args.baseline or args.goal_set:
    current_pol = args.obstacle_type
  else:
    current_pol = np.random.choice(start_obstacle_types) 

  if args.render:

    replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)

  if args.use_goal_set:
    prev_prev_prev_gs_estimate = prev_prev_gs_estimate = prev_gs_estimate = gs_estimate = gs[args.obstacle_type].step(ob,im)
    gs_estimate_data = [gs_estimate]
    delta_gs_estimate_data = [0]

  obs = []
  imgs = []
  num_fell = 0

  current_pol_data = []

  t1 = time.time()
  t2 = time.time()
  count = 0
  initial_pos = None
  failed_to_switch = False
  num_switches = []

  if args.push:
    max_disturbance = 500
  print("Getting goal set for ", current_pol, " in terrain type for ", current_pol, ", recording values for stable regions in velocity range. Collecting ", eval_length, " samples for each")
  while True:

    if args.test_pol:
      if len(ep_lens)%2 == 0 and ((args.obstacle_type == 'base' and env.box_num > 4) or (args.obstacle_type in ['gaps','jumps','stairs','zero'] and args.obstacle_type == env.order[env.box_num])):
        if roa[args.obstacle_type].step(ob,im) > args.roa_thres:
          current_pol = args.obstacle_type
      if len(ep_lens) + 1 % 2 == 0 and ((args.obstacle_type == 'base' and env.box_num > 4) or (args.obstacle_type in ['gaps','jumps','stairs','zero'] and args.obstacle_type == env.order[env.box_num])) and switch_step is None:
        current_pol = args.obstacle_type
    elif args.use_roa:

      # if ((args.obstacle_type == 'base' and env.box_num > 4) or (args.obstacle_type in ['gaps','jumps','stairs','zero'] and args.obstacle_type == env.order[env.box_num])):
      # if env.env.box_num == 5 and current_pol != args.obstacle_type:
      if current_pol != args.obstacle_type:
        switch_estimate = roa[args.obstacle_type].step(ob,im)
        # print(switch_estimate)
        # print(switch_estimate)
        if switch_estimate > args.roa_thres:
        # if switch_estimate > 0.95:
          current_pol = args.obstacle_type
          obs.append(ob)
          imgs.append(im)
      elif env.box_num >= 8 and current_pol != args.obstacle_type and not failed_to_switch:
        failed_to_switch = True
        # print(failed_to_switch)
        # switch_estimate = roa[args.obstacle_type].step(ob,im)
        # print("didn't switch", switch_estimate)
    elif args.use_goal_set:
      # if ((args.obstacle_type in ['gaps','jumps'] and env.box_num >= 5) or (args.obstacle_type in ['stairs'] and env.box_num >= 5)) and switch_step is None:
      # if (args.obstacle_type in ['gaps','jumps'] and env.box_num >= 5 and env.box_num < 8) or (args.obstacle_type in ['stairs'] and env.box_num >= 5 and env.box_num < 7):
      if (args.obstacle_type in ['gaps','jumps','base','zero'] and env.box_num >= 5 and env.box_num < 8) or (args.obstacle_type in ['stairs'] and env.box_num >= 5 and env.box_num < 7):

        gs_estimate = gs[args.obstacle_type].step(ob,im)
        if current_pol != args.obstacle_type and prev_gs_estimate > gs_estimate and prev_prev_gs_estimate > prev_gs_estimate:
        # if current_pol != args.obstacle_type and prev_gs_estimate < gs_estimate and prev_prev_gs_estimate < prev_gs_estimate and prev_prev_prev_gs_estimate < prev_prev_gs_estimate:
        # num = 5
        # temp_gs = []

        # if current_pol != args.obstacle_type and len(gs_estimate_data) > num:
          
          # for t in reversed(range(1,num)):
          #   # print(t)
          #   temp_gs.append(gs_estimate_data[-(t-1)] < gs_estimate_data[-(t)])
          # # print(temp_gs)
          # temp_gs = np.array(temp_gs).all()
          # print(temp_gs)
        # if current_pol != args.obstacle_type and prev_gs_estimate < gs_estimate:
        # if current_pol != args.obstacle_type:
        #   if args.obstacle_type in ['gaps','jumps']:
        #     switch_step = np.random.randint(0,150) + env.steps
        #   elif args.obstacle_type in ['stairs']:
        #     switch_step = np.random.randint(0,100) + env.steps
        # if temp_gs:
          current_pol = args.obstacle_type
          # print("changing pols", prev_gs_estimate, gs_estimate)
          obs.append(ob)
          imgs.append(im)
        # print(gs_estimate)
        gs_estimate_data.append(gs_estimate)
        delta_gs_estimate_data.append(gs_estimate - prev_gs_estimate)

        prev_prev_prev_gs_estimate = prev_prev_gs_estimate
        prev_prev_gs_estimate = prev_gs_estimate
        prev_gs_estimate = gs_estimate
      else:
        gs_estimate_data.append(0)
        delta_gs_estimate_data.append(0)

    else:
      # if ((args.obstacle_type in ['gaps','jumps','base','zero'] and env.box_num == 5) or (args.obstacle_type in ['stairs'] and env.box_num == 5)) and switch_step is None:
      if ((args.obstacle_type == 'base' and env.box_num > 4) or (args.obstacle_type in ['gaps','jumps','stairs','zero'] and args.obstacle_type == env.order[env.box_num])) and switch_step is None:
        # print(env.box_info[1][5][0], env.body_xyz[0])
        if args.no_rand:
          if args.centre:
            if env.box_info[1][5][0] <= env.body_xyz[0]:
              # print("switching cause over line")
              switch_step = env.steps
          else:
            switch_step = env.steps
          # print(switch_step, env.steps)
        else:
          if args.obstacle_type in ['gaps','jumps','base','zero']:
            # switch_step = np.random.randint(0,150) + env.steps
            switch_step = np.random.randint(0,100) + env.steps
          elif args.obstacle_type in ['stairs']:
            switch_step = np.random.randint(0,60) + env.steps
            # print(switch_step)
        # switch_step = 150 + env.steps
      if args.goal_set:
        if (args.obstacle_type in ['gaps','jumps','base','zero'] and env.box_num >= 5 and env.box_num < 8) or (args.obstacle_type in ['stairs'] and env.box_num >= 5 and env.box_num < 7):
          obs.append(ob)
          imgs.append(im)
      elif ((args.obstacle_type == 'base' and env.box_num > 4) or (args.obstacle_type in ['gaps','jumps','stairs','zero'] and args.obstacle_type == env.order[env.box_num])) and switch_step == env.steps:
      # elif args.obstacle_type == env.order[env.box_num] and switch_step == env.steps:
      # elif ((args.obstacle_type in ['gaps','jumps','base','zero'] and env.box_num >= 5) or (args.obstacle_type in ['stairs'] and env.box_num >= 5)) and switch_step == env.steps:
        initial_pos = [env.body_xyz[0], env.body_xyz[1], 1.0]
        initial_heading = env.yaw
        current_pol = args.obstacle_type
        obs.append(ob)
        imgs.append(im)
      if args.full and ((args.obstacle_type in ['gaps','jumps','base','zero'] and env.box_num >= 5 and env.box_num < 8) or (args.obstacle_type in ['stairs'] and env.box_num >= 5 and env.box_num < 7)) and switch_step < env.steps:
        obs.append(ob)
        imgs.append(im)
        # print(env.box_num, switch_step, env.steps)
        # print(switch_step, env.steps, "switching")
  
    if args.push and np.random.random() < 0.2 and env.vx > 0.5 and ((args.obstacle_type in ['gaps','jumps','base','zero'] and env.box_num < 4) or (args.obstacle_type in ['stairs'] and env.box_num < 4)):
      env.add_disturbance(max_disturbance)


    # if current_pol == args.obstacle_type and env.box_num < 8:
    #   obs.append(ob)
    #   imgs.append(im)

    if current_pol == 'zero':
      zero_ob = copy.copy(ob)
      zero_ob[-2] = 0
      act, vpred, _, nlogp = pol['base'].step(zero_ob, im, stochastic=stochastic)
    else:
      act, vpred, _, nlogp = pol[current_pol].step(ob, im, stochastic=stochastic)
    
    if args.use_vf:
      vpred = vf[name].get_value(ob, im)

    vpred_data.append(vpred)

    # Stability criteria: alive for longer than stable count (2 steps), and value not decreasing
    if env.steps > stable_count and vpred_data[-1] > vpred_data[env.steps - stable_count]:
      if current_pol == 'zero':
        stable_vpred.append(vpred_data[env.steps - stable_count])
      elif env.vx > 0.1:
        stable_vpred.append(vpred_data[env.steps - stable_count])
    
    terrain_data.append(env.z_offset)

    torques = act

    
    next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_im()

    if env.box_num > 13 or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300):
      # print(env.steps - switch_step)
      done = True


    prev_done = done
    ob = next_ob
    im = next_im
    ep_ret += rew
    ep_len += 1
    ep_steps += 1

    # if count % 100 == 0:
    #   print("Data: ", data_pointer, "of ", data_length, "fell: " , num_fell,  "breakdown: ", round(np.mean(labels[:data_pointer,0]),2), "time: ", round(time.time() - t1,2), "total: ", round(time.time() - t2,2))
    #   # print(labels[:data_pointer,0])
    #   t1 = time.time()

    #   np.save(PATH + 'input_ob.npy', input_ob[:data_pointer,:])
    #   np.save(PATH + 'input_im.npy', input_im[:data_pointer,:])
    #   np.save(PATH + 'labels.npy', labels[:data_pointer,:])
    # count += 1
    if args.render:
      replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)


    # if not args.render and len(stable_vpred) > eval_length:
    #   means[current_pol] = np.mean(stable_vpred)
    #   stds[current_pol] = np.std(stable_vpred)
    #   break
    
    if args.test_pol and data_pointer > 300:
      print("Data: ", data_pointer, "of ", data_length, "fell: " , num_fell,  "breakdown: ", round(np.mean(labels[:data_pointer,0]),2), "time: ", round(time.time() - t1,2), "total: ", round(time.time() - t2,2))
      # print("============================================================================================================================")
      # print("number of episodes {0:d}, time: {1:0.4f} ".format(metrics["episodes"], time.time() - t1))
      # print("Thres {0:0.2f} time alive: {1:0.4f} distance travelled: {2:0.4f}  success: {3:0.4f} switch: {4:0.4f}".format(roa_thres, np.mean(metrics['time_alive'])/2700, np.mean(metrics['dist_travelled'])/19.8, np.mean(metrics['success']), np.mean(metrics['switch'])))
      # print("============================================================================================================================")

      break

    current_pol_data.append(obstacle_types.index(current_pol))


    if done:    
      # if (env.box_num > 12 and abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.5)  or (args.obstacle_type == 'zero' and abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.5 and env.steps > switch_step and env.vx < 0.2):
      if switch_step is not None and ((env.box_num > 12 and abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.5)  or (args.obstacle_type == 'zero' and abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.5 and env.steps - switch_step > 300)):

        label = True  
        colour = [0.1,0.9,0.1,0.5]
      else:
        label = False
        colour = [0.9,0.1,0.1,0.5]

      switch_step = None
      # print(current_pol, " terrain type ", env.obstacle_type, " ", len(stable_vpred), " of ", eval_length)
      if args.plot:
        if not args.multi:
          num_axes = 3
        else:    
          num_axes = 6    
        fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
        # slope_data = reject_outliers(np.array(slope_data))
        if args.use_goal_set:
          axes[0].plot([_ for _ in range(len(gs_estimate_data))], gs_estimate_data, alpha=1.0)  
          axes[1].plot([_ for _ in range(len(delta_gs_estimate_data))], delta_gs_estimate_data, alpha=1.0)  
        else:
          axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)  
        if not args.multi:
          axes[1].plot([_ for _ in range(len(stable_vpred))], stable_vpred, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
          if stable_vpred:
            print("min stable value", min(stable_vpred), np.mean(stable_vpred), np.std(stable_vpred))
          axes[2].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  

        else:
          axes[0].legend(obstacle_types)  
          # print(np.array(current_pol_data).shape, len(current_pol_data))
          axes[2].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
          axes[2].set_ylim([-0.9,2.1])
          for s, n in enumerate(obstacle_types):
            axes[2].text(0,s*0.5+0.5,str(s) + ". " + n)
          axes[2].set_title("Current policy", loc='left')
          
          # probs_data = -1*np.abs(probs_data)
          # # print(probs_data)
          # axes[2].plot([_ for _ in range(len(probs_data))], probs_data, alpha=1.0)  
          # axes[2].legend(obstacle_types)  
          # axes[2].set_title("Likelihood in goal set (higher is better)", loc='left')

          # axes[3].plot([_ for _ in range(len(probs_data))], logp(np.array(vpred_data), np.array(means), np.array(stds)), alpha=1.0)  
          # axes[3].legend(obstacle_types)  
          # axes[3].set_title("Log Likelihood in goal set (higher is better)", loc='left')
          
          # axes[4].plot([_ for _ in range(len(probs_data))], mahalanobois_distance(np.array(vpred_data), np.array(means), np.array(stds)), alpha=1.0)  
          # axes[4].legend(obstacle_types)  
          # axes[4].set_title("Mahalanobois distance from goal set (lower is better)", loc='left')

          # axes[2].plot([_ for _ in range(len(vpred_data))], [normpdf(msv[0], means[0], stds[0])]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
          # axes[2].plot([_ for _ in range(len(vpred_data))], [normpdf(msv[1], means[1], stds[1])]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
          # axes[2].plot([_ for _ in range(len(vpred_data))], [normpdf(msv[2], means[2], stds[2])]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
          axes[5].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
          axes[5].set_title("terrain height", loc='left')

        fig.tight_layout(pad=4.0)


        # axes[0].plot([_ for _ in range(len(plot_data))], plot_data, c='b', alpha=1.0)  
        # axes[0].set_ylim([0,1])
        # axes[1].plot([_ for _ in range(len(vpred_data))], vpred_data, c='b', alpha=1.0)  
        # axes[2].plot([_ for _ in range(len(slope_data))], slope_data, c='b', alpha=1.0)  
        
        # axes[3].plot([_ for _ in range(len(probs))], probs, c='b', alpha=1.0)  
        # plt.show(block=False)
        plt.show()
        if args.render:
          break
      #   plot_data = []
      if args.render and initial_pos is not None:
        visBoxId = p.createVisualShape(p.GEOM_SPHERE, radius=0.1, rgbaColor=colour)
        box_id = p.createMultiBody(baseMass=0, baseVisualShapeIndex=visBoxId, basePosition=initial_pos, baseOrientation= p.getQuaternionFromEuler([0.0,1.5709,initial_heading]))

      idx = min(data_pointer+len(obs), data_length) - data_pointer
      if idx > 0:
        if not failed_to_switch:
          # print(idx, len(obs[:idx]), len(obs), data_pointer)
          input_ob[data_pointer:data_pointer+idx] = obs[:idx]
          input_im[data_pointer:data_pointer+idx] = imgs[:idx]

          # for i in range(idx):
          #   print(input_im[i+data_pointer].shape)
          #   cv2.imshow('frame', input_im[i+data_pointer])
          #   cv2.waitKey(1)

          labels[data_pointer:data_pointer+idx] = label
          data_pointer += idx
          obs = []
          imgs = []
          if data_pointer == data_length:
            break
          # print(failed_to_switch)
        # else:
          # print("failed to switch")
      # print(failed_to_switch)
      # if failed_to_switch:
        # print("failed to switch")
      # else:
      #   print("switched")
      num_switches.append(not failed_to_switch)
      failed_to_switch = False


      if count % 10 == 0:
        # print("Data: ", data_pointer, "of ", data_length, "fell: " , num_fell,  "breakdown: ", round(np.mean(labels[:data_pointer,0]),2), "time: ", round(time.time() - t1,2), "total: ", round(time.time() - t2,2))
        print("Data: {0:4d} of {1:4d} breakdown: {2:0.4f} time: {3:4.2f} total: {4:5.2f}".format(data_pointer,  data_length, round(np.mean(labels[:data_pointer,0]),2), round(time.time() - t1,2), round(time.time() - t2,2)))
        # print("Thres {0:0.2f} time alive: {1:0.4f} distance travelled: {2:0.4f}  success: {3:0.4f} switch: {4:0.4f}".format(roa_thres, np.mean(metrics['time_alive'])/2700, np.mean(metrics['dist_travelled'])/19.8, np.mean(metrics['success']), np.mean(metrics['switch'])))

        # print(np.mean(num_switches))
        # print(labels[:data_pointer,0])
        t1 = time.time()
        np.save(PATH + 'input_ob.npy', input_ob[:data_pointer,:])
        np.save(PATH + 'input_im.npy', input_im[:data_pointer,:])
        np.save(PATH + 'labels.npy', labels[:data_pointer,:])
      count += 1
      episode_count += 1
      # print("Current policy: ", current_pol, " episode ", episode_count, " of ", eval_length)
      
      if args.baseline or args.goal_set:
        current_pol = args.obstacle_type
      elif args.base_only:
        current_pol = 'base'  
      else:
        current_pol = np.random.choice(start_obstacle_types) 

      vpred_data = []
      terrain_data = []

      ob = env.reset()
      im = env.get_im()
      ep_rets.append(ep_ret)  
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
      initial_pos = None
      
      current_pol_data = []

      if args.use_goal_set:
        prev_prev_prev_gs_estimate = prev_prev_gs_estimate = prev_gs_estimate = gs_estimate = gs[args.obstacle_type].step(ob,im)
        gs_estimate_data = [gs_estimate]
        delta_gs_estimate_data = [0]

  print("-------------------------------------------------------------------------------------------------------")
  print("Data: {0:4d} of {1:4d} time: {2:4.2f} total: {3:5.2f}".format(data_pointer,  data_length, round(time.time() - t1,2), round(time.time() - t2,2)))
  print("{0:s} thres: {1:0.2f} breakdown: {2:0.2f}".format(args.obstacle_type, args.roa_thres, round(np.mean(labels[:data_pointer,0]),2)))
  print("-------------------------------------------------------------------------------------------------------")


if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--sub_folder', default='b')
  # parser.add_argument('--folder', default='b4')
  # parser.add_argument('--folder', default='b9')
  parser.add_argument('--folder', default='b10')
  # parser.add_argument('--folder', default='b10_vis')

  parser.add_argument('--difficulty', default=10, type=int)
  parser.add_argument('--height_coeff', default=0.07)
  parser.add_argument('--roa_thres', default=0.8, type=float)

  parser.add_argument('--cur_len', default=1000, type=int)
  parser.add_argument('--inc', default=2, type=int)

  parser.add_argument('--single_pol', default=True, action='store_false')
  parser.add_argument('--full', default=False, action='store_true')
  parser.add_argument('--push', default=False, action='store_true')
  parser.add_argument('--no_rand', default=False, action='store_true')
  parser.add_argument('--centre', default=False, action='store_true')
  parser.add_argument('--use_roa', default=False, action='store_true')
  parser.add_argument('--no_reg', default=False, action='store_true')
  parser.add_argument('--base_only', default=False, action='store_true')
  parser.add_argument('--baseline', default=False, action='store_true')
  parser.add_argument('--use_goal_set', default=False, action='store_true')

  parser.add_argument('--use_vf', default=False, action='store_true')
  parser.add_argument('--multi', default=True, action='store_false')
  parser.add_argument('--goal_set', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')
  parser.add_argument('--gae', default=False, action='store_true')
  parser.add_argument('--doa', default=False, action='store_true')

  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--vis', default=False, action='store_true')
  # parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="domain_train_no_stairs", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=1e5, type=int)
  parser.add_argument('--lr', default=1e-5, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)