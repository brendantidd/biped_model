import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow.keras import regularizers
from tensorflow import nn
import tensorboardX
import numpy as np
import time
import scripts.utils as U
from baselines.common.mpi_running_mean_std import RunningMeanStd
import argparse
from pathlib import Path
home = str(Path.home())
from scripts.utils import subplot
import random

class RoANet():
  # def __init__(self, name, args, ob_size, im_size, label_size=1, mean=None, std=None, max_val=None, min_val=None, dense_size=64, sess=None, train_pol=True, no_vis=False):
  def __init__(self, name, args, ob_size, im_size, label_size=1, mean=None, std=None, max_val=None, min_val=None, dense_size=64, sess=None, train_pol=True, no_vis=True):
    self.name = name + "_roa"
    self.args = args
    self.no_vis = no_vis
    self.train_pol = train_pol
    self.ob_size = ob_size
    self.im_size = im_size
    self.label_size = label_size
    self.dense_size = dense_size
    self.sess = tf.get_default_session() if sess is None else sess
    self.inputs = tf.placeholder(tf.float32, [None, self.ob_size])
    if self.args.vis:
      self.imgs = tf.placeholder(tf.float32, [None] + self.im_size)
    else:
      self.imgs = tf.placeholder(tf.float32, [None, self.im_size])
    self.labels = tf.placeholder(tf.float32, [None, self.label_size])
    with tf.variable_scope(self.name):
      if mean is not None:
        self.mean = tf.get_variable('mean', initializer=mean, dtype=tf.float32)
      else:
        self.mean = tf.get_variable('mean', initializer=np.zeros([1,self.ob_size], dtype=np.float32), dtype=tf.float32)
      if std is not None:
        self.std = tf.get_variable('std', initializer=std, dtype=tf.float32)
      else:
        self.std = tf.get_variable('std', initializer=np.ones([1,self.ob_size], dtype=np.float32), dtype=tf.float32)
      if min_val is not None:
        self.max_val = tf.get_variable('max_val', initializer=max_val, dtype=tf.float32)
      else:
        self.max_val = tf.get_variable('max_val', initializer=np.ones([1,self.ob_size], dtype=np.float32), dtype=tf.float32)
      if min_val is not None:
        self.min_val = tf.get_variable('min_val', initializer=min_val, dtype=tf.float32)
      else:
        self.min_val = tf.get_variable('min_val', initializer=np.ones([1,self.ob_size], dtype=np.float32), dtype=tf.float32)
      self.model()
    var = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in var if 'RMSProp' not in v.name or 'Adam' not in v.name]
    # print(self.vars)
    self.saver = tf.train.Saver(var_list=self.vars)

  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'eth_model.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = 'eth_model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for eth module")
    
  def model(self):
    activation = tf.nn.softsign
    # activation = tf.nn.relu
    if False:
      last_out = tf.layers.dense(self.imgs, self.dense_size, activation=activation,kernel_regularizer=regularizers.l2(0.0001), name="l1")
      last_out = tf.layers.dense(last_out, self.dense_size, activation=activation,kernel_regularizer=regularizers.l2(0.0001), name="l2")
      x = (self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std + 1e-8)
      last_out = tf.concat(axis=1,values=[x, last_out])
      last_out = tf.layers.dense(last_out, self.dense_size, activation=activation,kernel_regularizer=regularizers.l2(0.0001), name="l3")
      last_out = tf.layers.dense(last_out, self.dense_size, activation=activation,kernel_regularizer=regularizers.l2(0.0001), name="l4")
      self.outputs = tf.layers.dense(last_out, self.label_size, activation=tf.nn.sigmoid,kernel_regularizer=regularizers.l2(0.0001), name="l5")
      
    else:
      # x = (self.inputs - tf.stop_gradient(self.mean))/tf.stop_gradient(self.std + 1e-8)
      x = tf.stop_gradient((self.inputs - self.min_val)/(self.max_val-self.min_val + 1e-8))
      if self.no_vis:
        last_out = x
      else:
        if self.args.vis:
          last_out = tf.nn.relu(U.conv2d(self.imgs, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
          last_out = tf.nn.relu(U.conv2d(last_out, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
          last_out = U.flattenallbut0(last_out)
          last_out = tf.nn.tanh(tf.layers.dense(last_out, self.dense_size, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
        else:
          last_out = tf.layers.dense(self.imgs, self.dense_size, activation=activation, name="l1")
          last_out = tf.nn.dropout(last_out, rate=0.5)
          last_out = tf.layers.dense(last_out, self.dense_size, activation=activation, name="l2")
      

        last_out = tf.concat(axis=1,values=[x, last_out])

      # x = tf.stop_gradient((self.inputs - self.mean)/(self.std + 1e-8))
      # x = tf.stop_gradient((self.inputs - self.min_val)/(self.max_val-self.min_val + 1e-8))


      # last_out = x
      # last_out = tf.layers.dense(last_out, self.dense_size, activation=.activation, name="l3")
      # if self.train_pol:
      #   last_out = tf.nn.dropout(last_out, rate=0.5)
        # last_out = tf.nn.dropout(last_out, rate=0.25)
      # else:
        # last_out = tf.nn.dropout(last_out, rate=1)

      # last_out = tf.nn.dropout(last_out, rate=0.25)
      # last_out = tf.nn.dropout(last_out, rate=0.5)
      # last_out = tf.nn.dropout(last_out, rate=0.8)
      
      # ----------------------      
      # last_out = tf.layers.dense(last_out, self.dense_size, activation=activation, name="l4")
      # if self.train_pol:
      #   # last_out = tf.nn.dropout(last_out, rate=0.5)
      #   last_out = tf.nn.dropout(last_out, rate=0.5)
      #   # last_out = tf.nn.dropout(last_out, rate=0.25)
      # ----------------------
      last_out = tf.layers.dense(last_out, self.dense_size, activation=activation, name="l5")
      # if self.train_pol:
      #   # last_out = tf.nn.dropout(last_out, rate=0.25)
      #   # last_out = tf.nn.dropout(last_out, rate=0.3)
      #   last_out = tf.nn.dropout(last_out, rate=0.6)
        # last_out = tf.nn.dropout(last_out, rate=0.5)
        # last_out = tf.nn.dropout(last_out, rate=0.1)
        # last_out = tf.nn.dropout(last_out, rate=0.2)


      last_out = tf.layers.dense(last_out, self.dense_size, activation=activation, name="l6")

      # if self.train_pol:
      #   last_out = tf.nn.dropout(last_out, rate=0.4)
      last_out = tf.layers.dense(last_out, self.dense_size, activation=activation, name="l8")
      # if self.train_pol:
      #   last_out = tf.nn.dropout(last_out, rate=0.2)

      # if self.train_pol:
        # last_out = tf.nn.dropout(last_out, rate=0.25)
        # last_out = tf.nn.dropout(last_out, rate=0.15)
        # last_out = tf.nn.dropout(last_out, rate=0.2)
        # last_out = tf.nn.dropout(last_out, rate=0.1)

      # if self.train_pol:
      #   last_out = tf.nn.dropout(last_out, rate=0.25)
        # last_out = tf.nn.dropout(last_out, rate=0.1)
      # else:
        # last_out = tf.nn.dropout(last_out, rate=1)

      # last_out = tf.nn.dropout(last_out, rate=0.25)
      # last_out = tf.nn.dropout(last_out, rate=0.5)
      # last_out = tf.nn.dropout(last_out, rate=0.6)
      self.outputs = tf.layers.dense(last_out, self.label_size, activation=tf.nn.sigmoid, name="l7")

    # x = tf.dense(...)
    # vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    # print(vars)
    # self.args.no_reg = False
    self.args.no_reg = True
    if not self.args.no_reg:
      weights = []
      # for name in ["l1","l2","l3","l4","l5","l6","l7"]:
      for name in ["l4","l5","l6","l7"]:
      # for name in ["l4","l5"]:
        weights.append(tf.get_default_graph().get_tensor_by_name(self.name + "/" + name + '/kernel:0'))
    # # weights = tf.get_default_graph().get_tensor_by_name(os.path.split(self.outputs.name)[0] + '/kernel:0')
    # # weights = tf.get_default_graph().get_tensor_by_name(os.path.split(x.name)[0] + '/kernel:0')
    # # weights = tf.get_default_graph().get_tensor_by_name("l1")
    # print(weights)
    # exit()
    # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) + tf.reduce_sum([0.005*tf.nn.l2_loss(w) for w in weights])
    # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) + tf.reduce_sum([0.001*tf.nn.l2_loss(w) for w in weights])
    # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) + tf.reduce_sum([0.0001*tf.nn.l2_loss(w) for w in weights])
    if self.args.no_reg:
      self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) 
    else:
      # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) + tf.reduce_sum([0.0001*tf.nn.l2_loss(w) for w in weights])
      # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) + tf.reduce_sum([0.001*tf.nn.l2_loss(w) for w in weights])
      self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) + tf.reduce_sum([0.01*tf.nn.l2_loss(w) for w in weights])
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.000005)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.00001)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)

    # optimizer = tf.train.AdamOptimizer(learning_rate=0.00001)
    optimizer = tf.train.AdamOptimizer(learning_rate=0.0005)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.00002)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.0002)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.002)
    
    
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.00005)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.000025)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.00002)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.00003)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
    self.optimise = optimizer.minimize(self.loss)

  def step(self, X, im):
    if self.args.vis:
      outputs = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.imgs:np.array(im).reshape([-1] + self.im_size)})
    else:
      outputs = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.imgs:np.array(im).reshape([-1,self.im_size])})
    return outputs[0][0]

  def multi_step(self, X, im):
    if self.args.vis:
      outputs = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.imgs:np.array(im).reshape([-1] + self.im_size)})
    else:
      outputs = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.imgs:np.array(im).reshape([-1,self.im_size])})
    return outputs

  def train(self, X, im, y):
    if self.args.vis:
      _, loss = self.sess.run([self.optimise, self.loss],feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.imgs:np.array(im).reshape([-1] + self.im_size), self.labels:np.array(y).reshape(-1,self.label_size)})
    else:
      _, loss = self.sess.run([self.optimise, self.loss],feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.imgs:np.array(im).reshape([-1,self.im_size]), self.labels:np.array(y).reshape(-1,self.label_size)})
    return loss

  def initialise(self):
    self.sess.run(tf.variables_initializer(self.vars))


if __name__=="__main__":

  parser = argparse.ArgumentParser()
  # parser.add_argument('--folder', default="b9")
  # parser.add_argument('--folder', default="b10_vis")
  parser.add_argument('--folder', default="b10")
  parser.add_argument('--exp', default="jumps")
  parser.add_argument('--obstacle_type', default="jumps")
  parser.add_argument('--vis', default=True, action='store_false')
  # parser.add_argument('--vis', default=False, action='store_true')
  # parser.add_argument('--no_reg', default=False, action='store_true')
  parser.add_argument('--no_reg', default=True, action='store_false')
  # parser.add_argument('--goal_set', default=False, action='store_true')
  parser.add_argument('--no_vis', default=False, action='store_true')
  parser.add_argument('--render', default=False, action='store_true')
  args = parser.parse_args()

  # os.environ["CUDA_VISIBLE_DEVICES"]="-1"

  if args.vis:
    args.folder += '_vis'

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_small_rand/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_goal_set/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_new/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_goal_set/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_full/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_new_vis/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '3/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_new_criteria2/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_all2/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_single2/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_new/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_hard/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_no_vis/'
  WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_small/'
  writer = tensorboardX.SummaryWriter(log_dir=PATH)


  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.15)
  # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.2)
  # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 1.0)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  

  obs = np.load(WEIGHTS_PATH +  'input_ob.npy').astype(np.float32)
  imgs = np.load(WEIGHTS_PATH + 'input_im.npy').astype(np.float32)
  labels = np.load(WEIGHTS_PATH + 'labels.npy').astype(np.float32).reshape(-1,1)
  
  # np.save(WEIGHTS_PATH + 'input_ob.npy', obs[:obs.shape[0],:])
  # np.save(WEIGHTS_PATH + 'input_im.npy', imgs[:obs.shape[0],:])
  # exit()
  # np.save(WEIGHTS_PATH + 'labels.npy', labels[:data_pointer,:])

  mean = np.mean(obs,axis=0).reshape([1,obs.shape[1]])
  std = np.std(obs,axis=0).reshape([1,obs.shape[1]])
  norm_mean = (obs - mean)/(std + 1e-8)
  max_val = np.max(obs,axis=0).reshape([1,obs.shape[1]])
  min_val = np.min(obs,axis=0).reshape([1,obs.shape[1]])

  # print(norm_mean[:10,:10])
  # print(mean)
  # print(std + 1e-8)
  # print(mean.shape, std.shape)
  # print(imgs.shape)
  if args.vis:
    im_size = [60,40,1]
  else:
    im_size = imgs.shape[1]
  net = RoANet(name=args.obstacle_type, args=args, ob_size=obs.shape[1], im_size=im_size, label_size=1, mean=mean, std=std, max_val=max_val, min_val=min_val, sess=sess, no_vis=args.no_vis)
  net.initialise()
  
  print(obs.shape, imgs.shape,  labels.shape)

  train_obs, test_obs = obs[:int(obs.shape[0]*0.9),:], obs[int(obs.shape[0]*0.9):,:]
  train_imgs, test_imgs = imgs[:int(imgs.shape[0]*0.9),::], imgs[int(imgs.shape[0]*0.9):,::]
  train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:], labels[int(labels.shape[0]*0.9):,:]

  print("data loaded from ", WEIGHTS_PATH)
  print(train_obs.shape, train_imgs.shape, test_obs.shape, test_imgs.shape)
  print(train_labels.shape, test_labels.shape)

  test_idx = [i for i in range(test_obs.shape[0])]

  # batch_size = 16
  # batch_size = 256
  # batch_size = 512
  if args.vis:
    # batch_size = 512
    # batch_size = 1024
    batch_size = 64
    # batch_size = 512
    # batch_size = 256
  else:
    # batch_size = train_obs.shape[0]//1250
    # batch_size = 64
    batch_size = 8
  # if args.goal_set:
  #   batch_size = 512
  # print(batch_size)
  # else:
  #   batch_size = 4
  epochs = 10000
  best_val_loss = 1000
  best_count = 0
  for e in range(epochs):
    
    idx = [i for i in range(train_obs.shape[0])]
    np.random.shuffle(idx)
    
    t1 = time.time()
    epoch_loss = []
    for batch in range(train_obs.shape[0]//batch_size):
      b_idx = batch*batch_size
      batch_x, batch_im, batch_y = train_obs[idx[b_idx:b_idx+batch_size],:], train_imgs[idx[b_idx:b_idx+batch_size],:], train_labels[idx[b_idx:b_idx+batch_size],:]
      loss = net.train(batch_x, batch_im, batch_y)
      epoch_loss.append(loss)
   
    val_loss, predictions = sess.run([net.loss, net.outputs],feed_dict={net.inputs:test_obs, net.imgs:test_imgs, net.labels:test_labels})
    # print(predictions)
    # -----------------------------------------------------------
    # Display epoch losses. Save best weights, periodically plot things
    # -----------------------------------------------------------
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    if np.mean(val_loss) < best_val_loss:
      net.save(PATH + '/best/')
      best_val_loss = np.mean(val_loss)
      best_count += 1
      # if np.random.random() < 0.2:
      if e > 100 and best_count % 10 == 0:
        rand_idx = random.sample(test_idx, min(test_obs.shape[0], 128))
        y1 = test_labels[rand_idx,0]
        k1 = predictions[rand_idx,0]
        subplot([[y1,k1],[y1,k1]], legend=[['target','predictions']]*2,PATH=PATH + '/best/')
    if e % 10 == 0:
      net.save(PATH)      
    if e % 200 == 0:
      rand_idx = random.sample(test_idx, min(test_obs.shape[0], 128))
      y1 = test_labels[rand_idx,0]
      k1 = predictions[rand_idx,0]
      subplot([[y1,k1],[y1,k1]], legend=[['target','predictions']]*2,PATH=PATH)
  
