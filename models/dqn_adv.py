from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from baselines.common.distributions import make_pdtype
from baselines.common.mpi_running_mean_std import RunningMeanStd
from baselines.common.mpi_moments import mpi_moments
from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
import scripts.utils as U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from baselines import logger
import time

def get_vars(scope):
  return [x for x in tf.global_variables() if scope in x.name]

class Policy():
  def __init__(self, ob, im, ob_size, im_size, ac_size, sess, vis,  args, hid_size, eps_decay, normalize=True):
    self.ob = ob
    self.im = im
    self.ac_size = ac_size
    self.ob_size = ob_size
    self.im_size = im_size
    # print("dqn", self.im_size)
    self.sess = sess
    self.args = args
    self.vis = vis
    
    self.steps = 0
    # self.eps_start = 0.5
    self.eps_start = 0.9
    self.eps_end = 0.001
    self.eps = self.eps_start
    # self.eps_decay = 100000
    self.eps_decay = eps_decay

    sequence_length = None
    with tf.variable_scope("obfilter"):
      self.ob_rms = RunningMeanStd(shape=[self.ob_size,])

    with tf.variable_scope('dqn'):
      if len(self.im_size) > 2:
        x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
        x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
        x = U.flattenallbut0(x)
        x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
      else:
        x = tf.nn.tanh(tf.layers.dense(im, 32, name="vis_fc0", kernel_initializer=U.normc_initializer(1.0)))
        x = tf.nn.tanh(tf.layers.dense(x, 32, name="vis_fc1", kernel_initializer=U.normc_initializer(0.01)))
      obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
      last_out = obz

      if self.args.dual_dqn:
        last_out1 = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1_1", kernel_initializer=U.normc_initializer(1.0)))
        last_out1 = tf.nn.tanh(tf.layers.dense(last_out1, hid_size, name="fc2_1", kernel_initializer=U.normc_initializer(1.0)))
        self.vpred1 = tf.layers.dense(last_out1, self.ac_size, name='final_1', kernel_initializer=U.normc_initializer(1.0))
        last_out2 = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1_2", kernel_initializer=U.normc_initializer(1.0)))
        last_out2 = tf.nn.tanh(tf.layers.dense(last_out2, hid_size, name="fc2_2", kernel_initializer=U.normc_initializer(1.0)))
        self.vpred2 = tf.layers.dense(last_out2, self.ac_size, name='final_2', kernel_initializer=U.normc_initializer(1.0))
        self.vpred = tf.minimum(self.vpred1, self.vpred2)
      else:
        last_out = tf.concat(axis=1,values=[last_out, x])
        last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=U.normc_initializer(1.0)))
        last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=U.normc_initializer(1.0)))
        self.vpred = tf.layers.dense(last_out, self.ac_size, name='final', kernel_initializer=U.normc_initializer(1.0))
    
    
    self.stochastic = tf.placeholder(dtype=tf.bool, shape=())

  def get_eps(self, training_steps=0):
    # self.eps_decay = 100000

    # self.eps = self.eps_end + (self.eps_start - self.eps_end) * np.exp(-1. * self.steps / self.eps_decay)
    
    # self.eps_decay = 50000
    # self.eps = (self.eps_end/self.eps_start)**(self.eps_decay/self.steps)
    
    # self.eps_decay = 100000
    # self.eps = max(self.eps_start * (1 - self.steps/self.eps_decay), self.eps_end)
    # self.steps += 1
    
    # Best so far
    # self.eps =  max(0.5 - (float(self.steps) / (1000*100)), 0.001)
    # self.eps =  max(0.5 - (float(self.steps) / (2000*100)), 0.001)
    # self.eps =  max(0.5 - (float(self.steps) / (4000*100)), 0.001)


    # self.eps =  max(0.5 - (float(self.steps) / (self.eps_decay*100)), 0.01)

    # self.eps =  max(1.0 - (float(self.steps) / (self.eps_decay*100)), 0.01)
    # self.steps += 1
    pass
    # (1-1/(1+\exp(-\left(x\ -5\right))))
    # self.eps = max(1 - 1/(1+np.exp(-0.2*(training_steps - 100))), 0.01)

    
    # self.eps =  max(0.5 - (float(self.steps) / (self.eps_decay*100)), 0.001)
    # self.eps =  max(0.9 - (float(self.steps) / (self.eps_decay*100)), 0.001)
    
    # More explore
    # self.eps =  max(0.9 - (float(self.steps) / (2*2000*100)), 0.001)
    # self.eps =  max(0.9 - (float(self.steps) / (2*2000*100)), 0.05)

    # self.eps =  max(0.5 - (float(self.steps) / (2000*100)), 0.01)
    # self.eps =  max(0.5 - (float(self.steps) / (2000*100)), 0.02)


  def get_vpreds(self, ob, im):
    if self.args.dual_dqn:
      # vpreds = self.sess.run([self.vpred, self.vpred1, self.vpred2], feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})
      vpreds = self.sess.run(self.vpred , feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
    else:
      vpreds = self.sess.run(self.vpred , feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
    return vpreds

  def step(self, ob, im=None, stochastic=False):
    vpred =  self.sess.run(self.vpred, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})
    
    # scaled_vpred = (abs(vpred)/np.sum(abs(vpred)))[0]
    # return np.random.choice([i for i in range(self.ac_size)], p=scaled_vpred)
    
    self.get_eps()
    self.steps += 1
    if stochastic:
      if np.random.random() > self.eps: 
        return np.argmax(vpred)
      else:
        return np.random.randint(self.ac_size)
    else:
      return np.argmax(vpred) 

class DQN(Base):
  def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,4], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, eps_decay=2000):
    self.max_timesteps = max_timesteps
    self.horizon = horizon
    self.learning_rate = lr
    self.env = env

    self.sess = sess = U.get_session()
    comm = MPI.COMM_WORLD
    self.name = name
    high = np.inf*np.ones(ac_size)
    low = -high
    ac_space = spaces.Box(low, high, dtype=np.float32)
    high = np.inf*np.ones(ob_size)
    low = -high
    ob_space = spaces.Box(low, high, dtype=np.float32)
    self.args = args   
    im_size = im_size
    self.PATH = PATH
    self.hid_size = hid_size
    self.writer = writer
    ob = U.get_placeholder(name="sp_ob" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
    im = U.get_placeholder(name="sp_im" + name, dtype=tf.float32, shape=[None] + im_size)
    self.args = args
    if ('vel' in name or 'hex' in name) and not args.vis:
      self.vis = False
    else:
      self.vis = vis
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      # May need some more params when using recurrent
      # act_model = Policy(ob, im, ob_space, im_size, ac_space, sess, self.vis, normalize=normalize, hid_size=hid_size)
      train_model = Policy(ob, im, ob_size, im_size, ac_size, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, eps_decay=eps_decay)
    Base.__init__(self)

    # CREATE THE PLACEHOLDERS
    # self.A = A = train_model.pdtype.sample_placeholder([None])
    self.ADV = tf.placeholder(tf.float32, [None])
    self.R = tf.placeholder(tf.float32, [None])
    self.ACTION = tf.placeholder(tf.int32, [None])
    # Keep track of old actor
    # self.OLDNEGLOGPAC = OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
    # Keep track of old critic
    # self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
    self.LR = LR = tf.placeholder(tf.float32, [])

    # Get the predicted value
    if self.args.dual_dqn:
      one_hot1 = tf.one_hot(self.ACTION, ac_size)
      current_q_t1 = tf.reduce_sum(train_model.vpred1 * one_hot1, 1)
      vf_loss1 = 0.5*tf.reduce_mean(tf.square(current_q_t1 - self.R))      
      one_hot2 = tf.one_hot(self.ACTION, ac_size)
      current_q_t2 = tf.reduce_sum(train_model.vpred2 * one_hot2, 1)
      vf_loss2 = 0.5*tf.reduce_mean(tf.square(current_q_t2 - self.R))
      vf_loss = vf_loss1 + vf_loss2
    else:
      one_hot = tf.one_hot(self.ACTION, ac_size)
      current_q_t = tf.reduce_sum(train_model.vpred * one_hot, 1)
      vf_loss = 0.5*tf.reduce_mean(tf.square(current_q_t - self.R))

    loss = vf_loss

    # UPDATE THE PARAMETERS USING LOSS
    params = tf.trainable_variables(name)
    # 2. Build our trainer
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      self.trainer = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)

    # 3. Calculate the gradients
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      grads_and_var = self.trainer.compute_gradients(loss, params)
    grads, var = zip(*grads_and_var)
    
    grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
      
    grads_and_var = list(zip(grads, var))
    # zip aggregate each gradient with parameters associated
    # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

    self.grads = grads
    self.var = var
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      self._train_op = self.trainer.apply_gradients(grads_and_var)
    
    # std = tf.reduce_mean(train_model.pd.std)
    # mean_ratio = tf.reduce_mean(ratio)
    mean_adv = tf.reduce_mean(self.ADV)
   
    self.loss_names = ['value_loss', 'adv', 'learning_rate']
    self.stats_list = [vf_loss, mean_adv, LR]
  
    self.train_model = train_model
    # self.act_model = act_model
    # Not sure where weights for act_model and train_model are synced. For safety using train_model
    # self.step = train_model.step
    # self.value = train_model.value
    # self.get_value = train_model.get_value
    self.step = train_model.step
    self.get_vpreds = train_model.get_vpreds
    self.init_buffer()

  def train(self, lr, obs, imgs, actions, returns, masks, values):
    # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
    # Returns = R + yV(s')
    advs = returns - values
    # Normalize the advantages
    advs = (advs - advs.mean()) / (advs.std() + 1e-8)
    td_map = {
      self.train_model.ob : obs,
      self.train_model.im : imgs,
      self.ACTION : actions,
      self.ADV : advs,
      self.R : returns,
      self.LR : lr
    }
    return self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]

  def run_train(self, ep_rets, ep_lens, vf_only=False):
    max_grad_norm=0.5
    self.train_model.ob_rms.update(self.data['ob'])
    self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
    # lr = lambda f:self.learning_rate*f
    # lrnow = lr(self.cur_lrmult)
    lrnow = self.learning_rate*self.cur_lrmult
    # lrnow = self.learning_rate
    self.lr = lrnow
    #**PPO2 doesn't seem to anneal clipping, may need to try with and without: works better without**#
    # cr = lambda f:0.2*f
    # if self.args.const_clip:
    #   # cliprangenow = cr(1.0)
    cliprangenow = 0.2
    # else:
    # for key in self.training_input:
      # print(key, self.data[key].shape)
      # cliprangenow = cr(self.cur_lrmult)
    # cliprangenow = 0.2*self.cur_lrmult
    inds = np.arange(self.n)
    for epoch in range(self.epochs):
      if self.enable_shuffle:
        np.random.shuffle(inds)
      self.loss = [] 
      for start in range(0, self.n, self.batch_size):
        end = start + self.batch_size
        mbinds = inds[start:end]
        # print({key:self.data[key][mbinds].shape for key in self.training_input})
        slices = (self.data[key][mbinds] for key in self.training_input)
        outs = self.train(lrnow, *slices)

        self.loss.append(outs[:len(self.loss_names)])
    self.loss = np.mean(self.loss, axis=0)
    self.evaluate(ep_rets, ep_lens)
    self.init_buffer()

  def step(self, ob, im, stochastic=False, multi=False): 
    if not multi:
      actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
      return actions[0], values, self.states, neglogpacs
    else:
      actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
      return actions, values, self.states, neglogpacs        


  def get_advantage(self, last_value, last_done):    
    gamma = 0.99; lam = 0.95
    # gamma = 0.999; lam = 0.95
    advs = np.zeros_like(self.data['rew'])
    lastgaelam = 0
    for t in reversed(range(len(self.data['rew']))):
      if t == len(self.data['rew']) - 1:
        nextnonterminal = 1.0 - last_done
        nextvalues = last_value
      else:
        nextnonterminal = 1.0 - self.data['done'][t+1]
        nextvalues = self.data['value'][t+1]
      delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    self.data['return'] = advs + self.data['value']
  
  def init_buffer(self):
    # Make sure reward is 'rew' and value/vpred is 'value', for get_advantage function in base.py
    # self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', 'roa_pred', 'roa_done']
  
    self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value']
    self.training_input = ['ob', 'im', 'ac', 'return', 'done', 'value']
    self.data = {t:[] for t in self.data_input}

  # def log_stuff(self):
  #   logger.record_tabular("eps", self.train_model.eps)
  #   self.writer.add_scalar("eps", self.train_model.eps, self.iters_so_far)

  def log_stuff(self, things):
    things["eps"] = self.train_model.eps
    if self.rank == 0:
      for thing in things:
        self.writer.add_scalar(thing, things[thing], self.iters_so_far)
        logger.record_tabular(thing, things[thing])

  def add_to_buffer(self, data):
    ''' data needs to be a list of lists, same length as self.data'''
    for d,key in zip(data, self.data):
      self.data[key].append(d)

  def finalise_buffer(self, data, last_value, last_done, last_roa_done=None, last_roa=None):
    ''' data must be dict'''
    for key in self.data_input:
      if key == 'done':
        self.data[key] = np.asarray(self.data[key], dtype=np.bool)
      else:
        self.data[key] = np.asarray(self.data[key])
    self.n = next(iter(self.data.values())).shape[0]
    for key in data:
      self.data[key] = data[key]
    self.get_advantage(last_value, last_done)
    # if self.rank == 0:
    #   self.log_stuff()
