import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow.keras import regularizers
from tensorflow import nn
import tensorboardX
import numpy as np
import time
import scripts.utils as U
from baselines.common.mpi_running_mean_std import RunningMeanStd
import argparse
from pathlib import Path
home = str(Path.home())
from scripts.utils import subplot
import random

class RoANet():
  # def __init__(self, name, args, ob_size, im_size, label_size=1, mean=None, std=None, max_val=None, min_val=None, dense_size=64, sess=None, train_pol=True, no_vis=False):
  # def __init__(self, name, args, ob_size, label_size=1, dense_size=64, sess=None, train_pol=True):
  # def __init__(self, name, args, ob_size, im_size=[60,40,1], label_size=1, dense_size=64, sess=None, train_pol=True, vis=True):
  def __init__(self, name, args, ob_size, im_size=[60,40,1], label_size=1, dense_size=128, sess=None, train_pol=True, vis=True):

    self.name = name + "_roa"
    self.args = args
    self.train_pol = train_pol
    self.ob_size = ob_size
    self.vis = vis
    self.im_size = im_size
    self.label_size = label_size
    self.dense_size = dense_size
    self.sess = tf.get_default_session() if sess is None else sess
    self.inputs = tf.placeholder(tf.float32, [None, self.ob_size])
    self.im = tf.placeholder(tf.float32, [None] + self.im_size)
    self.labels = tf.placeholder(tf.float32, [None, self.label_size])
    with tf.variable_scope(self.name):
      self.model()
    var = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in var if 'RMSProp' not in v.name or 'Adam' not in v.name]
    # print(self.vars)
    self.saver = tf.train.Saver(var_list=self.vars)

  def save(self, SAVE_PATH):
      self.saver.save(self.sess, SAVE_PATH + 'eth_model.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = 'eth_model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for eth module")
    
  def model(self):
    # activation = tf.nn.softsign
    activation = tf.nn.relu
    
    x = tf.layers.dense(self.inputs, self.dense_size, activation=activation, name="l1")
    
    if self.vis:
      last_out = tf.nn.relu(U.conv2d(self.im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      # print("------------")
      # print("convolutional shape for estimator", last_out.shape)
      # print("------------")
      last_out = tf.nn.relu(U.conv2d(last_out, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      
      last_out = U.flattenallbut0(last_out)
      last_out = tf.nn.tanh(tf.layers.dense(last_out, self.dense_size, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
      last_out = tf.concat(axis=1,values=[x, last_out])
    else:
      last_out = x

    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation, name="l2")
    last_out = tf.layers.dense(last_out, self.dense_size, activation=activation, name="l3")  
    self.outputs = tf.layers.dense(last_out, self.label_size, activation=tf.nn.sigmoid, name="l4")

    # self.args.no_reg = False
    # self.args.no_reg = True
    # if not self.args.no_reg:
    #   weights = []
    #   for name in ["l1","l2","l3","l4","l5","l6","l7"]:
    #     weights.append(tf.get_default_graph().get_tensor_by_name(self.name + "/" + name + '/kernel:0'))
    # if self.args.no_reg:
    # else:
      # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) + tf.reduce_sum([0.0001*tf.nn.l2_loss(w) for w in weights])
      # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) + tf.reduce_sum([0.001*tf.nn.l2_loss(w) for w in weights])
      # self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) + tf.reduce_sum([0.01*tf.nn.l2_loss(w) for w in weights])
    self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) 

    optimizer = tf.train.AdamOptimizer(learning_rate=0.00001)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.00002)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.00005)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.0002)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.0005)
    self.optimise = optimizer.minimize(self.loss)

  def step(self, X, im=None):
    if self.vis:
      outputs = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.im:np.array(im).reshape([-1] + self.im_size)})
    else:
      outputs = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size)})
    return outputs[0][0]

  def multi_step(self, X, im=None):
    if self.vis:
      outputs = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.im:np.array(im).reshape([-1] + self.im_size)})
    else:
      outputs = self.sess.run(self.outputs,feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size)})
    return outputs

  def train(self, X, y, im=None):
    if self.vis:
      _, loss = self.sess.run([self.optimise, self.loss],feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.im:np.array(im).reshape([-1] + self.im_size), self.labels:np.array(y).reshape(-1,self.label_size)})
    else:
      _, loss = self.sess.run([self.optimise, self.loss],feed_dict={self.inputs:np.array(X).reshape(-1,self.ob_size), self.labels:np.array(y).reshape(-1,self.label_size)})
    return loss


  def initialise(self):
    self.sess.run(tf.variables_initializer(self.vars))


if __name__=="__main__":

  parser = argparse.ArgumentParser()
  # parser.add_argument('--folder', default="b9")
  # parser.add_argument('--folder', default="b10_vis")
  parser.add_argument('--folder', default="b10")
  parser.add_argument('--exp', default="jumps")
  parser.add_argument('--obstacle_type', default="jumps")
  parser.add_argument('--vis', default=True, action='store_false')
  # parser.add_argument('--vis', default=False, action='store_true')
  # parser.add_argument('--no_reg', default=False, action='store_true')
  # parser.add_argument('--no_reg', default=True, action='store_false')
  # parser.add_argument('--goal_set', default=False, action='store_true')
  # parser.add_argument('--no_vis', default=False, action='store_true')
  parser.add_argument('--render', default=False, action='store_true')
  args = parser.parse_args()

  # os.environ["CUDA_VISIBLE_DEVICES"]="-1"

  # if args.vis:
  args.folder += '_vis'

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_really_small/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_small/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_80000/'
  WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_large/'
  # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '_test/'
  writer = tensorboardX.SummaryWriter(log_dir=PATH)


  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.15)
  # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.2)
  # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 1.0)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  

  obs = np.load(WEIGHTS_PATH +  'input_ob.npy').astype(np.float32)
  imgs = np.load(WEIGHTS_PATH +  'input_im.npy').astype(np.float32)
  labels = np.load(WEIGHTS_PATH + 'labels.npy').astype(np.float32).reshape(-1,1)
  
  net = RoANet(name=args.obstacle_type, args=args, ob_size=obs.shape[1], label_size=1, sess=sess)
  # net = RoANet(name=args.obstacle_type, args=args, ob_size=obs.shape[1], im_size=im_size, label_size=1, mean=mean, std=std, max_val=max_val, min_val=min_val, sess=sess, no_vis=args.no_vis)
  net.initialise()
  
  print(obs.shape, imgs.shape, labels.shape)

  train_obs, test_obs = obs[:int(obs.shape[0]*0.9),:], obs[int(obs.shape[0]*0.9):,:]
  train_imgs, test_imgs = imgs[:int(imgs.shape[0]*0.9),::], imgs[int(imgs.shape[0]*0.9):,::]
  train_labels, test_labels = labels[:int(labels.shape[0]*0.9),:], labels[int(labels.shape[0]*0.9):,:]

  print("data loaded from ", WEIGHTS_PATH)
  print(train_obs.shape, test_obs.shape)
  print(train_imgs.shape, test_imgs.shape)

  print(train_labels.shape, test_labels.shape)

  # test_idx = [i for i in range(test_obs.shape[0])]

  # batch_size = 16
  # batch_size = 128
  # batch_size = 32
  batch_size = 256
  # batch_size = 1024
  epochs = 10000
  best_val_loss = 1000
  best_count = 0

  pos = np.where(train_labels==1)[0]
  neg = np.where(train_labels==0)[0]
  
  if pos.shape[0] > neg.shape[0]:
    bigger = pos
    smaller = neg
  else:
    smaller = pos
    bigger = neg


  test_pos = np.where(test_labels==1)[0]
  test_neg = np.where(test_labels==0)[0]
  
  if test_pos.shape[0] > test_neg.shape[0]:
    test_bigger = test_pos
    test_smaller = test_neg
  else:
    test_smaller = test_pos
    test_bigger = test_neg

  for e in range(epochs):
    equal_idx = random.sample([_ for _ in range(bigger.shape[0])], smaller.shape[0])
    idx = list(smaller) + list(bigger[equal_idx])
    # idx = [i for i in range(train_obs.shape[0])]
    np.random.shuffle(idx)
    # print(bigger.shape, smaller.shape, len(idx))
    # exit()
    t1 = time.time()
    epoch_loss = []
    for batch in range(len(idx)//batch_size):
      b_idx = batch*batch_size
      batch_x, batch_im, batch_y = train_obs[idx[b_idx:b_idx+batch_size],:], train_imgs[idx[b_idx:b_idx+batch_size],:], train_labels[idx[b_idx:b_idx+batch_size],:]
      # batch_x, batch_y = train_obs[idx[b_idx:b_idx+batch_size],:], train_labels[idx[b_idx:b_idx+batch_size],:]
      # loss = net.train(batch_x, batch_y)
      loss = net.train(batch_x, batch_y, batch_im)
      epoch_loss.append(loss)
   
    
    test_equal_idx = random.sample([_ for _ in range(test_bigger.shape[0])], test_smaller.shape[0])
    test_idx = list(test_smaller) + list(test_bigger[test_equal_idx])   
    # print(len(test_idx), len(test_smaller), len(test_bigger))
    np.random.shuffle(test_idx)
   
    val_loss, predictions = sess.run([net.loss, net.outputs],feed_dict={net.inputs:test_obs[test_idx,:], net.im:test_imgs[test_idx,:], net.labels:test_labels[test_idx,:]})
    # val_loss, predictions = sess.run([net.loss, net.outputs],feed_dict={net.inputs:test_obs, net.labels:test_labels})
    # print(predictions)
    # -----------------------------------------------------------
    # Display epoch losses. Save best weights, periodically plot things
    # -----------------------------------------------------------
    print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),  np.mean(val_loss), time.time() - t1, best_val_loss))
    writer.add_scalar("error", np.mean(epoch_loss), e)
    writer.add_scalar("val error", np.mean(val_loss), e)
    if np.mean(val_loss) < best_val_loss:
      net.save(PATH + '/best/')
      best_val_loss = np.mean(val_loss)
      best_count += 1
      # if np.random.random() < 0.2:
      if e > 100 and best_count % 10 == 0:
        rand_idx = test_idx[:128]
        y1 = test_labels[rand_idx,0]
        k1 = predictions[:128]
        subplot([[y1,k1],[y1,k1]], legend=[['target','predictions']]*2,PATH=PATH + '/best/')
    if e % 10 == 0:
      net.save(PATH)      
    if e % 200 == 0:
      rand_idx = test_idx[:128]
      y1 = test_labels[rand_idx,0]
      k1 = predictions[:128]  
      subplot([[y1,k1],[y1,k1]], legend=[['target','predictions']]*2,PATH=PATH)
  
