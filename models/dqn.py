from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from baselines.common.distributions import make_pdtype
from baselines.common.mpi_running_mean_std import RunningMeanStd
from baselines.common.mpi_moments import mpi_moments
from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
import scripts.utils as U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from baselines import logger
import time
import random

def get_vars(scope):
  return [x for x in tf.global_variables() if scope in x.name]

class Policy():
  def __init__(self, ob, im, ob_size, im_size, ac_size, sess, vis,  args, hid_size, normalize=True):
    self.ob = ob
    self.im = im
    self.ac_size = ac_size
    self.ob_size = ob_size
    self.im_size = im_size
    self.sess = sess
    self.args = args
    self.vis = vis
    
    self.steps = 0
    self.eps_start = 0.9
    # self.eps_end = 0.2
    # self.eps_end = 0.001
    self.eps_end = 0.01

    sequence_length = None
    with tf.variable_scope("obfilter"):
        self.ob_rms = RunningMeanStd(shape=[self.ob_size,])

    with tf.variable_scope('dqn'):

      if len(self.im_size) > 2:
        x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
        x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
        x = U.flattenallbut0(x)
        x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
      else:
        x = tf.nn.tanh(tf.layers.dense(im, 32, name="vis_fc0", kernel_initializer=U.normc_initializer(1.0)))
        x = tf.nn.tanh(tf.layers.dense(x, 32, name="vis_fc1", kernel_initializer=U.normc_initializer(0.01)))
      obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
      last_out = obz
      last_out = tf.concat(axis=1,values=[last_out, x])
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=U.normc_initializer(1.0)))
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=U.normc_initializer(1.0)))
      self.vpred = tf.layers.dense(last_out, self.ac_size, name='final', kernel_initializer=U.normc_initializer(1.0))
    self.stochastic = tf.placeholder(dtype=tf.bool, shape=())

  
  def get_eps(self):
    # self.eps_decay = 100000
    # self.eps = self.eps_end + (self.eps_start - self.eps_end) * np.exp(-1. * self.steps / self.eps_decay)
    # self.eps_decay = 50000
    # self.eps = (self.eps_end/self.eps_start)**(self.eps_decay/self.steps)
    
    self.eps_decay = 1000000
    self.eps = max(self.eps_start * (1 - self.steps/self.eps_decay), self.eps_end)
    self.steps += 1

  def get_vpreds(self, ob, im):
    vpreds = self.sess.run(self.vpred, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
    # print(vpreds)
    return vpreds

  def step(self, ob, im=None, stochastic=False):
    vpred =  self.sess.run(self.vpred, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})
    
    # scaled_vpred = (abs(vpred)/np.sum(abs(vpred)))[0]
    # return np.random.choice([i for i in range(self.ac_size)], p=scaled_vpred)
    
    self.get_eps()
    self.steps += 1
    if stochastic:
      if np.random.random() > self.eps: 
        return np.argmax(vpred)
      else:
        return np.random.randint(self.ac_size)
    else:
      return np.argmax(vpred)  

class DQN(Base):
  def __init__(self, name, env, ob_size, ac_size, im_size=[48,48,4], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, eps_decay=1000000):
    self.max_timesteps = max_timesteps
    self.horizon = horizon
    self.learning_rate = lr
    self.env = env
    self.ac_size = ac_size
    self.sess = sess = U.get_session()
    comm = MPI.COMM_WORLD
    self.name = name
    high = np.inf*np.ones(ac_size)
    low = -high
    ac_space = spaces.Box(low, high, dtype=np.float32)
    high = np.inf*np.ones(ob_size)
    low = -high
    ob_space = spaces.Box(low, high, dtype=np.float32)
    self.args = args   
    im_size = im_size
    self.PATH = PATH
    self.hid_size = hid_size
    self.writer = writer
    ob = U.get_placeholder(name="sp_ob" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
    im = U.get_placeholder(name="sp_im" + name, dtype=tf.float32, shape=[None] + im_size)
    next_ob = U.get_placeholder(name="next_ob" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
    next_im = U.get_placeholder(name="next_im" + name, dtype=tf.float32, shape=[None] + im_size)
    self.args = args
    if ('vel' in name or 'hex' in name) and not args.vis:
      self.vis = False
    else:
      self.vis = vis
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      self.train_model = Policy(ob, im, ob_size, im_size, ac_size, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size)
    with tf.variable_scope(name + "_target", reuse=tf.AUTO_REUSE):
      self.target_model = Policy(next_ob, next_im, ob_size, im_size, ac_size, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size)
    Base.__init__(self)

    self.GAMMA = 0.99
    self.ACTION = tf.placeholder(tf.int32, [None,1])
    self.DONE = tf.placeholder(tf.float32, [None,1])
    self.R = tf.placeholder(tf.float32, [None,1])
    self.REWARD = tf.placeholder(tf.float32, [None,1])
    self.LR = tf.placeholder(tf.float32, [])

    one_hot = tf.one_hot(self.ACTION, self.ac_size)
    current_q_t = tf.reduce_sum(self.train_model.vpred * one_hot, 1)

    if args.adv:
      target_q_t = self.REWARD + (1-self.DONE)*self.GAMMA*tf.reduce_max(self.target_model.vpred)    
      loss = tf.reduce_mean(tf.square(current_q_t - self.R))
    else:
      target_q_t = self.REWARD + (1-self.DONE)*self.GAMMA*tf.reduce_max(self.target_model.vpred)    
      loss = tf.reduce_mean(tf.square(current_q_t - target_q_t))

    self.params = tf.trainable_variables(name + "/")
    self.target_params = tf.trainable_variables(name + "_target/")

    # update_target_fn will be called periodically to copy Q network to target Q network
    update_target_expr = []
    for var, var_target in zip(sorted(self.params, key=lambda v: v.name),
                                sorted(self.target_params, key=lambda v: v.name)):
        update_target_expr.append(var_target.assign(var))
    update_target_expr = tf.group(*update_target_expr)
    self.update_target = U.function([], [], updates=[update_target_expr])
    
    # 2. Build our trainer
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      self.trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)

    # 3. Calculate the gradients
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      grads_and_var = self.trainer.compute_gradients(loss, self.params)
    grads, var = zip(*grads_and_var)
    
    grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
      
    grads_and_var = list(zip(grads, var))
    # zip aggregate each gradient with parameters associated
    # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

    self.grads = grads
    self.var = var
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      self._train_op = self.trainer.apply_gradients(grads_and_var)
    
    self.loss_names = ['loss','learning_rate']
    self.stats_list = [loss, self.LR]

    self.step = self.train_model.step
    self.get_vpreds = self.train_model.get_vpreds
    
    # self.buffer = ReplayBuffer(ob_size=ob_size, im_size=im_size, capacity=100000)
    self.buffer = ReplayBuffer(ob_size=ob_size, im_size=im_size, capacity=10000)


  def train(self, lr, obs, imgs, next_obs, next_imgs, actions, rewards, dones):
    
    td_map = {
      self.train_model.ob : obs,
      self.train_model.im : imgs,
      self.target_model.ob : next_obs,
      self.target_model.im : next_imgs,
      self.ACTION : actions,
      self.LR : lr,
      self.REWARD : rewards,
      self.DONE : dones
    }
    
    return self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]

  def run_train(self, ep_rets, ep_lens):
    self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
    lrnow = self.learning_rate*self.cur_lrmult
    self.lr = lrnow

    obs, imgs, actions, next_obs, next_imgs, rewards, dones = self.buffer.sample(batch_size=128)
    self.train_model.ob_rms.update(obs)

    self.loss = self.train(self.lr, obs, imgs, next_obs, next_imgs, actions, rewards, dones)
    # print(loss)
    # self.loss = np.mean(loss, axis=0)

  def evaluate(self, rews, lens):

    lrlocal = (lens, rews) # local values
    listoflrpairs = MPI.COMM_WORLD.allgather(lrlocal) # list of tuples
    lens, rews = map(flatten_lists, zip(*listoflrpairs))
    # self.lenbuffer.extend(lens)
    # self.rewbuffer.extend(rews)

    if self.rank == 0:
      logger.log("********** Iteration %i ************"%self.iters_so_far)
      for loss, name in zip(self.loss, self.loss_names):
        logger.record_tabular(name, loss)
        self.writer.add_scalar(name, loss, self.iters_so_far)
      logger.record_tabular("eps", self.train_model.eps)
      logger.record_tabular("LearningRate", self.lr)
      logger.record_tabular("EpRewMean", np.mean(rews))
      logger.record_tabular("EpLenMean", np.mean(lens))
      logger.record_tabular("EpThisIter", len(lens))
      logger.record_tabular("TimeThisIter", time.time() - self.t1)
      logger.record_tabular("TimeStepsSoFar", self.timesteps_so_far)
      logger.record_tabular("EnvTotalSteps", self.env.total_steps)
      self.t1 = time.time()
      self.writer.add_scalar("rewards", np.mean(rews), self.iters_so_far)
      self.writer.add_scalar("lengths", np.mean(lens), self.iters_so_far)
      self.writer.add_scalar("eps", self.train_model.eps, self.iters_so_far)
      self.all_rewards.append(np.mean(rews))
      np.save(self.PATH + 'rewards.npy',self.all_rewards)
      # print(np.mean(self.env.reward_breakdown['pos']),np.mean(self.env.reward_breakdown['vel']))
      self.env.log_stuff(logger, self.writer, self.iters_so_far)
      try:
          # if np.mean(rews) > self.best_reward :
          self.save()
      except:
          print("Couldn't save training model")
      logger.dump_tabular()
    self.timesteps_so_far += sum(lens)     
    self.iters_so_far += 1


  # def step(self, ob, im, stochastic=False, multi=False): 
  #   if not multi:
  #     actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
  #     return actions[0], values, self.states, neglogpacs
  #   else:
  #     actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
  #     return actions, values, self.states, neglogpacs        


  def get_advantage(self, last_value, last_done):    
    gamma = 0.99; lam = 0.95
    advs = np.zeros_like(self.data['rew'])
    lastgaelam = 0
    for t in reversed(range(len(self.data['rew']))):
      if t == len(self.data['rew']) - 1:
        nextnonterminal = 1.0 - last_done
        nextvalues = last_value
      else:
        nextnonterminal = 1.0 - self.data['done'][t+1]
        nextvalues = self.data['value'][t+1]
      delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    self.data['return'] = advs + self.data['value']
  
  def init_buffer(self):
    # Make sure reward is 'rew' and value/vpred is 'value', for get_advantage function in base.py
    # self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', 'roa_pred', 'roa_done']

    self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac']
    self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
    self.data = {t:[] for t in self.data_input}

  def log_stuff(self):
    # self.env.sample_delay
    pass

  def add_to_buffer(self, data):
    ''' data needs to be a list of lists, same length as self.data'''
    for d,key in zip(data, self.data):
      self.data[key].append(d)

  def finalise_buffer(self, data, last_value, last_done, last_roa_done=None, last_roa=None):
    ''' data must be dict'''
    for key in self.data_input:
      if key == 'done':
        self.data[key] = np.asarray(self.data[key], dtype=np.bool)
      else:
        self.data[key] = np.asarray(self.data[key])
    self.n = next(iter(self.data.values())).shape[0]
    for key in data:
      self.data[key] = data[key]
    self.get_advantage(last_value, last_done)
    if self.rank == 0:
      self.log_stuff()


class ReplayBuffer(object):
  def __init__(self, capacity, ob_size, im_size):
    self.capacity = capacity
    self.ob_size = ob_size
    self.im_size = im_size
    self.position = 0
    self.full = False
    self.obs = np.zeros([self.capacity, self.ob_size])
    if len(self.im_size) > 1:
      self.imgs = np.zeros([self.capacity] + self.im_size)
      self.next_imgs = np.zeros([self.capacity] + self.im_size)
    else:
      self.imgs = np.zeros([self.capacity, self.im_size[0]])
      self.next_imgs = np.zeros([self.capacity, self.im_size[0]])
    self.actions = np.zeros([self.capacity, 1])
    self.next_obs = np.zeros([self.capacity, self.ob_size])
    self.rewards = np.zeros([self.capacity, 1])
    self.dones = np.zeros([self.capacity, 1])

  def push(self, ob, im, action, next_ob, next_im, reward, done):
    """Saves a transition."""
    self.obs[self.position, :] = ob
    self.imgs[self.position, :] = im
    self.actions[self.position, :] = action
    self.next_obs[self.position, :] = next_ob
    self.next_imgs[self.position, :] = next_im
    self.rewards[self.position, :] = reward
    self.dones[self.position, :] = done
    self.position += 1
    if self.position == self.capacity:
      self.position = 0
      self.full = True

  def sample(self, batch_size):
    if self.full == True:
      idx = random.sample([i for i in range(self.capacity)], batch_size)
    else:
      idx = random.sample([i for i in range(self.position)], batch_size)
    return self.obs[idx, :], self.imgs[idx, :], self.actions[idx,:], self.next_obs[idx,:], self.next_imgs[idx,:], self.rewards[idx,:], self.dones[idx,:]

def flatten_lists(listoflists):
  return [el for list_ in listoflists for el in list_]