import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import tensorflow as tf
from tensorflow import nn
import numpy as np
import time
import scripts.utils as U
from baselines.common.mpi_running_mean_std import RunningMeanStd

class DoANet():
  gamma = 0.99
  def __init__(self, name='doa', args=None, ob_size=None, im_size=None, output_size=1, hid_size=128, epochs=10, batch_size=32):
    # self.use_mpi = False
    self.use_mpi = True
    if self.use_mpi:
      from mpi4py import MPI
      self.comm = MPI.COMM_WORLD
    self.args = args
    self.name = name
    self.ob_size = ob_size
    self.im_size = im_size
    self.output_size = output_size
    self.hid_size = hid_size
    self.epochs = epochs
    self.batch_size = batch_size
    self.sess = tf.get_default_session()
    self.ob = tf.placeholder(tf.float32, [None,self.ob_size])
    self.im = tf.placeholder(tf.float32, [None]+self.im_size)
    # self.next_ob = tf.placeholder(tf.float32, [None,self.ob_size])
    # self.next_im = tf.placeholder(tf.float32, [None]+self.im_size)
    # self.done = tf.placeholder(tf.float32, [None,1])
    self.labels = tf.placeholder(tf.float32, [None,1])
    # print("doa ob space", [self.ob_size,])
    with tf.variable_scope(self.name + '/'):
      self.ob_rms = RunningMeanStd(shape=self.ob_size)
      self.model()
    self.vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name + '/')
    self.saver = tf.train.Saver(var_list=self.vars)
    # self.labels = self.done + self.gamma*(1.0-self.done)*self.outputs
    self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels))
    
    if not self.use_mpi:
      optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
      self.optimise = optimizer.minimize(self.loss)
    else:
      from scripts.mpi_adam_optimizer import MpiAdamOptimizer
      # MPI Adam
      params = tf.trainable_variables(name)
      # 2. Build our trainer
      with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
        self.trainer = MpiAdamOptimizer(self.comm, learning_rate=0.001, mpi_rank_weight=1, epsilon=1e-5)

      # 3. Calculate the gradients
      with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
        grads_and_var = self.trainer.compute_gradients(self.loss, params)
      grads, var = zip(*grads_and_var)
      
      # grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
      grads_and_var = list(zip(grads, var))
      self.grads = grads
      self.var = var
      with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):
        self.optimise = self.trainer.apply_gradients(grads_and_var)
      self.buffer = Buffer(ob_size=ob_size, im_size=im_size)
  
  def model(self):
    x = tf.nn.relu(U.conv2d(self.im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
    x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
    x = U.flattenallbut0(x)
    self.vis_output = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))

    obz = tf.clip_by_value((self.ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)

    last_out = tf.concat(axis=1,values=[obz, self.vis_output])
    last_out = tf.nn.tanh(tf.layers.dense(last_out, self.hid_size, name='fc1', kernel_initializer=U.normc_initializer(1.0)))
    last_out = tf.nn.tanh(tf.layers.dense(last_out, self.hid_size, name='fc2', kernel_initializer=U.normc_initializer(1.0)))
    self.outputs = tf.nn.sigmoid(tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0)))
    
  def step(self, ob, im):
    output = self.sess.run(self.outputs,feed_dict={self.ob:ob.reshape(-1,self.ob_size),self.im:im.reshape([-1] + self.im_size)})
    # print("doa output", output[0][0])
    return output[0][0]
  
  def train(self, last_dpred=1.0, last_done=False):
    ob, im, done, next_ob, next_im, dpreds = self.buffer.get(2048, shuffle=False)
    self.ob_rms.update(ob.reshape(-1,self.ob_size))    
    losses = []
    if self.args.gae:
      labels = self.get_gae_labels(done, dpreds, last_dpred, last_done)
      # print(labels)
    else:
      labels = self.get_labels(done, ob, im, next_ob, next_im)
      # print(labels)
    # print(self.buffer.size)
    for _ in range(self.epochs):
      for b in range(0,ob.shape[0],ob.shape[0]//self.batch_size):
        start = b
        end = b+self.batch_size
        _, loss = self.sess.run([self.optimise, self.loss],feed_dict={self.ob:ob[start:end,:],self.im:im[start:end,:],self.labels:labels[start:end,:]})
        losses.append(loss)
    return np.mean(losses)

  # def evaluate(self):

  def initialise(self):
    self.sess.run(tf.variables_initializer(self.vars))

  def save(self, SAVE_PATH):
    self.saver.save(self.sess, SAVE_PATH + self.name + '/model.ckpt', write_meta_graph=False)
  
  def load(self, WEIGHT_PATH):
    model_name = self.name + '/model.ckpt'
    self.saver.restore(self.sess, WEIGHT_PATH + model_name)
    print("Loaded weights for forward module")

  def get_labels(self, done, ob, im, next_ob, next_im):    
    # print(len(done))
    labels = np.zeros_like(done)
    for i in range(ob.shape[0]):
      labels[i] = done[i] + (1.0-done[i])*self.gamma*self.step(next_ob[i],next_im[i])
      # print(labels[i])
    return labels

  def get_gae_labels(self, done, dpreds, last_value, last_done):    
    gamma = 0.99; lam = 0.95
    advs = np.zeros_like(done)
    # print(len(done))
    lastgaelam = 0
    for t in reversed(range(len(done))):
      if t == len(done) - 1:
        nextnonterminal = 1.0 - last_done
        nextvalues = last_value
      else:
        nextnonterminal = 1.0 - done[t+1]
        nextvalues = dpreds[t+1]
      delta = done[t] + gamma * nextvalues * nextnonterminal - dpreds[t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    return advs + dpreds

class Buffer():
  # def __init__(self, ob_size, im_size, buffer_size=50000):
  def __init__(self, ob_size, im_size, buffer_size=2048):
    self.buffer_size = buffer_size
    self.obs = np.zeros([buffer_size, ob_size])
    self.imgs = np.zeros([buffer_size] + im_size)
    self.dones = np.zeros([buffer_size,1])
    self.dpreds = np.zeros([buffer_size,1])
    self.pointer = 0
    self.size = 0
    self.full_buffer = False
  
  def add(self, ob, im, done, dpred=1.0):
    # print(self.obs.shape, self.imgs.shape, self.labels.shape)
    self.obs[self.pointer,:] = ob
    self.imgs[self.pointer,::] = im
    self.dones[self.pointer,:] = float(done)
    self.dpreds[self.pointer,:] = dpred
    self.pointer += 1
    if self.pointer >= self.buffer_size:
      self.pointer = 0
      if not self.full_buffer:
        self.full_buffer = True
    if self.size < self.buffer_size:
      self.size += 1

  def get(self, size, shuffle=False):
    if self.full_buffer:
      idx = [i for i in range(self.buffer_size-1)]
    else:
      idx = [i for i in range(self.pointer-1)]
    if shuffle:
      np.random.shuffle(idx)
    return self.obs[idx[:size],:], self.imgs[idx[:size],::], self.dones[idx[:size],:], self.obs[np.array(idx[:size])+1,:], self.imgs[np.array(idx[:size])+1,::], self.dpreds[idx[:size],:]

if __name__=="__main__":

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  net = DoANet(name="stairs", ob_size=54, im_size=[60,40,1])
  initialize()

  x = np.arange(9).reshape(1,9)
  print(x.shape)
  print(net.step(x))