from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from baselines.common.distributions import make_pdtype, DiagGaussianPdType
from baselines.common.mpi_running_mean_std import RunningMeanStd
from baselines.common.mpi_moments import mpi_moments
from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
import scripts.utils as U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from baselines import logger
import time

def get_vars(scope):
  return [x for x in tf.global_variables() if scope in x.name]

class Policy():
  def __init__(self, ob, im, ob_space, ob_size, im_size, ac_space, sess, vis,  args, hid_size, normalize=True, const_std=False):
    # with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
    #   self.model(name, ob, im, ob_space, im_size, ac_space, vis, hid_size)
    #   self.scope = tf.get_variable_scope().name

  # def model(self, name, ob, im, ob_space, im_size, ac_space, vis, hid_size):
    self.ob = ob
    self.im = im
    self.ob_size = ob_size
    self.im_size = im_size
    self.sess = sess
    self.args = args
    self.pdtype = pdtype = make_pdtype(ac_space)
    self.vis = vis
    self.const_std = const_std
    self.hid_size = hid_size
    self.steps = 0
    # self.eps_start = 0.9
    self.eps_start = 0.5
    # self.eps_end = 0.001
    # self.eps_end = 0.1
    # self.eps_end = 0.05
    self.eps_end = self.args.min_eps
    self.eps = self.eps_start
    # self.eps_decay = 100000
    # self.eps_decay = 2000
    # self.eps_decay = 500
    self.eps_decay = self.args.eps_decay
    self.dqn_size = 3
    # self.dqn_size = 2

    sequence_length = None
    if normalize:
      with tf.variable_scope("obfilter"):
        self.ob_rms = RunningMeanStd(shape=ob_space.shape)
    if self.vis:
      x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      # print("------------")
      # print("convolutional shape for policy", x.shape)
      # print("------------")

      x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      x = U.flattenallbut0(x)
      x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
      
      # x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      # x = tf.nn.relu(U.conv2d(x, 16, "vis_l2", [4, 4], [2, 2], pad="VALID"))

      # x = tf.nn.relu(U.conv2d(im, 8, "vis_l1", [4, 4], [2, 2], pad="VALID"))
      # x = tf.nn.relu(U.conv2d(x, 8, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      
      # x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [4, 4], [2, 2], pad="VALID"))
      # x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))

      # x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      # x = tf.nn.relu(U.conv2d(x, 32, "vis_l1", [4, 4], [2, 2], pad="VALID"))

      # x = U.flattenallbut0(x)
      # x = tf.nn.tanh(tf.layers.dense(x, 32, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
    else:
      x = tf.nn.tanh(tf.layers.dense(im, 32, name="vis_fc0", kernel_initializer=U.normc_initializer(1.0)))
      x = tf.nn.tanh(tf.layers.dense(x, 32, name="vis_fc1", kernel_initializer=U.normc_initializer(1.0)))

      # print("-----------------------------------------------------------------")
      # print()
      # print("there is a bug that needs fixing next run")
      # print()
      # print("-----------------------------------------------------------------")

      # x = tf.nn.tanh(tf.layers.dense(x, hid_size, name="vis_fc1", kernel_initializer=U.normc_initializer(1.0)))
      # x = tf.nn.tanh(tf.layers.dense(x, hid_size, name="vis_fc2", kernel_initializer=U.normc_initializer(1.0)))

    if normalize:
      obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
    else:
      obz = ob
    self.obz = obz
    with tf.variable_scope('vf'):
      last_out = obz
      # if self.vis:
      #   x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      #   x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      #   x = U.flattenallbut0(x)
      #   x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
      # else:
      #   x = tf.nn.tanh(tf.layers.dense(im, hid_size, name="fc0", kernel_initializer=U.normc_initializer(1.0)))
      last_out = tf.concat(axis=1,values=[last_out, x])
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=U.normc_initializer(1.0)))
      self.vf_last_layer = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=U.normc_initializer(1.0)))
      self.vpred = tf.layers.dense(self.vf_last_layer, 1, name='final', kernel_initializer=U.normc_initializer(1.0))[:,0]

    with tf.variable_scope('pol'):
      last_out = obz
      # if self.vis:
      #   x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      #   x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      #   x = U.flattenallbut0(x)
      #   x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
      # else:
      #   x = tf.nn.tanh(tf.layers.dense(im, hid_size, name="fc0", kernel_initializer=U.normc_initializer(1.0)))

      last_out = tf.concat(axis=1,values=[last_out, x])
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc1', kernel_initializer=U.normc_initializer(1.0)))
      self.pol_last_layer = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc2', kernel_initializer=U.normc_initializer(1.0)))
      self.mean = tf.layers.dense(self.pol_last_layer, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01))
      # self.mean = tf.clip_by_value(tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01)),-50,50)
      # self.mean = tf.nn.tanh(tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01)))
      # logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2], initializer=tf.zeros_initializer())
      # logstd = tf.clip_by_value(tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2], initializer=tf.zeros_initializer()), -5.0, 0.0)
      # init = tf.constant_initializer([np.log(0.7)]*int(pdtype.param_shape()[0]//2))
      if self.const_std:
        logstd = tf.constant([np.log(1.0)]*int(pdtype.param_shape()[0]//2))
      else:      
        init = tf.constant_initializer([np.log(1.0)]*int(pdtype.param_shape()[0]//2))
        logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2],initializer=init)
      # logstd = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=init)

    pdparam = tf.concat([self.mean, self.mean * 0.0 + logstd], axis=1)

    self.pd = pdtype.pdfromflat(pdparam)

    self.state_in = []
    self.state_out = []
    self.stochastic = tf.placeholder(dtype=tf.bool, shape=())
    self.action = U.switch(self.stochastic, self.pd.sample(), self.pd.mode())
    # self.action = self.pd.sample()
    self.neglogp = self.pd.neglogp(self.action)
    self.state = tf.constant([])
    self.initial_state = None

  def add_term(self):
    # with tf.variable_scope('term'):
    # pass
    use_vpred = False
    # use_vpred = True
    if not use_vpred:
      x = tf.nn.relu(U.conv2d(self.im, 16, "term_vis_l1", [8, 8], [4, 4], pad="VALID"))
      x = tf.nn.relu(U.conv2d(x, 32, "term_vis_l2", [4, 4], [2, 2], pad="VALID"))
      x = U.flattenallbut0(x)
      x = tf.nn.tanh(tf.layers.dense(x, 64, name='term_vis_lin', kernel_initializer=U.normc_initializer(1.0)))
      last_out = tf.concat(axis=1,values=[self.obz, x])
      last_out = tf.layers.dense(last_out, self.hid_size, name='term1', kernel_initializer=U.normc_initializer(1.0))
      last_out = tf.layers.dense(last_out, self.hid_size, name='term2', kernel_initializer=U.normc_initializer(1.0))

    # self.term = tf.nn.sigmoid(tf.layers.dense(last_out, 1, name='final_term', kernel_initializer=U.normc_initializer(0.01)))
    else:
      if self.args.term_stop_gradient or self.args.dqn:
        last_out = tf.layers.dense(tf.stop_gradient(self.vf_last_layer), self.hid_size, name='term1', kernel_initializer=U.normc_initializer(1.0))
        last_out = tf.layers.dense(last_out, self.hid_size, name='term2', kernel_initializer=U.normc_initializer(1.0))
      else:
        last_out = tf.layers.dense(self.vf_last_layer, self.hid_size, name='term1', kernel_initializer=U.normc_initializer(1.0))
        last_out = tf.layers.dense(last_out, self.hid_size, name='term2', kernel_initializer=U.normc_initializer(1.0))
    
    # print("adding term", self.args.supervised)
    if self.args.supervised:
      self.term = tf.nn.softmax(tf.layers.dense(last_out, self.dqn_size, name='term_final', kernel_initializer=U.normc_initializer(1.0)))    
    else:
      self.term = tf.layers.dense(last_out, self.dqn_size, name='term_final', kernel_initializer=U.normc_initializer(1.0))

    # self.term_mean = tf.layers.dense(last_out, 1, name='term_final', kernel_initializer=U.normc_initializer(0.01))
    # init = tf.constant_initializer([np.log(1.0)]*int(1))
    # logstd = tf.get_variable(name="term_logstd", shape=[1, 1],initializer=init)

    # self.term = self.term_mean
    # std = tf.constant(1.0)
    # logstd = tf.constant([np.log(1.0)])

    # pdparam = tf.concat([self.term_mean, self.term_mean * 0.0 + logstd], axis=1)
    # # pdtype = make_pdtype([1])
    # pdtype = DiagGaussianPdType(1)
    # self.term_pd = pdtype.pdfromflat(pdparam)
    # self.term = U.switch(self.stochastic, self.term_pd.sample(), self.term_pd.mode())
    
    # self.term_neglogp = self.pd.neglogp(self.term)

      # if self.const_std:
      # self.term = tf.random.normal(mean=self.term_mean, stddev=std, shape=[1])

  # def get_term(self, ob, im, stochastic):
  #   term = self.sess.run(self.term, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size), self.stochastic:stochastic})[0] > 3.0
  #   # print(term)
  #   return term
    # term = self.sess.run(self.term, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
    # print(term)
    # return np.random.choice([0,1], p=[1-term, term])

  def get_eps(self):
    # self.eps_decay = 100000

    # self.eps = self.eps_end + (self.eps_start - self.eps_end) * np.exp(-1. * self.steps / self.eps_decay)
    
    # self.eps_decay = 50000
    # self.eps = (self.eps_end/self.eps_start)**(self.eps_decay/self.steps)
    
    # self.eps_decay = 100000
    # self.eps = max(self.eps_start * (1 - self.steps/self.eps_decay), self.eps_end)
    # self.steps += 1
    
    # Best so far
    # self.eps =  max(0.5 - (float(self.steps) / (1000*100)), 0.001)
    # self.eps =  max(0.5 - (float(self.steps) / (2000*100)), 0.001)
    # self.eps =  max(0.5 - (float(self.steps) / (4000*100)), 0.001)
    # self.eps =  max(0.9 - (float(self.steps) / (self.eps_decay*100)), 0.001)
    # self.eps = max(0.9 - (float(self.steps) / (self.eps_decay*100)), 0.001)
    # print(self.args)
    self.eps = max(self.eps_start - (float(self.steps) / (self.eps_decay*100)), self.eps_end)
    
    # More explore
    # self.eps =  max(0.9 - (float(self.steps) / (2*2000*100)), 0.001)
    # self.eps =  max(0.9 - (float(self.steps) / (2*2000*100)), 0.05)

    # self.eps =  max(0.5 - (float(self.steps) / (2000*100)), 0.01)
    # self.eps =  max(0.5 - (float(self.steps) / (2000*100)), 0.02)
    self.steps += 1

  def get_term(self, ob, im=None, stochastic=False):
    vpred = self.sess.run(self.term, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
    if self.args.supervised:
      # print(vpred)
      return np.argmax(vpred), vpred
    if self.args.dqn_cur_decay:
      return np.argmax(vpred), vpred
    else:
      # # scaled_vpred = (abs(vpred)/np.sum(abs(vpred)))[0]
      # # return np.random.choice([i for i in range(self.ac_size)], p=scaled_vpred)
      self.get_eps()
      self.steps += 1

      # print("getting term")
      if stochastic:
        # # Boltzman exp
        # exp_vpred = np.exp(vpred)
        # sm_vpred = np.round(np.squeeze(exp_vpred/np.sum(exp_vpred)), 3)
        # sm_vpred[-1] = 1 - np.sum(sm_vpred[:-1])
        # try:
        #   return np.random.choice([0,1], p=sm_vpred)
        # except:
        #   print(sm_vpred, np.sum(sm_vpred))
        #   exit()

        # return np.argmax(sm_vpred)
        
        if np.random.random() > self.eps: 
          # print(self.eps, np.argmax(vpred))
          return np.argmax(vpred), vpred
        else:
          return np.random.randint(self.dqn_size), vpred
          # return np.random.randint(2)
          # return np.random.choice([0,1,2], p=[0.4, 0.3, 0.3])
      else:
        return np.argmax(vpred), vpred

  def _evaluate(self, variables, observation, stochastic, im=None, **extra_feed):
    sess = self.sess
    # if self.vis:
    feed_dict = {self.ob: U.adjust_shape(self.ob, observation),self.im: im, self.stochastic:stochastic}
    # else:
    #   feed_dict = {self.ob: U.adjust_shape(self.ob, observation), self.stochastic:stochastic}
    for inpt_name, data in extra_feed.items():
      if inpt_name in self.__dict__.keys():
        inpt = self.__dict__[inpt_name]
        if isinstance(inpt, tf.Tensor) and inpt._op.type == 'Placeholder':
          feed_dict[inpt] = U.adjust_shape(inpt, data)
    return sess.run(variables, feed_dict)

  def step(self, observation, image=None, stochastic=False, **extra_feed):
    # if self.vis:
    a, v, state, neglogp = self._evaluate([self.action, self.vpred, self.state, self.neglogp], observation, stochastic, image, **extra_feed)
    # else:
    #   a, v, state, neglogp = self._evaluate([self.action, self.vpred, self.state, self.neglogp], observation, stochastic, **extra_feed)
    if state.size == 0:
      state = None   
    return a, v[0], state, neglogp[0]

  def value(self, ob, *args, **kwargs):
    return self._evaluate(self.vpred, ob, *args, **kwargs)

  def get_value(self, ob, im):
    return self.sess.run(self.vpred, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]

class Model(Base):
  def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,4], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, batch_size=32, const_std=False):
    self.max_timesteps = max_timesteps
    self.horizon = horizon
    self.batch_size = batch_size
    self.learning_rate = lr
    self.env = env
    self.ac_size = ac_size
    self.name = name
    self.sess = sess = U.get_session()
    self.comm = MPI.COMM_WORLD
    self.name = name
    self.mpi_rank_weight = mpi_rank_weight
    self.max_grad_norm = max_grad_norm
    self.ent_coef = ent_coef
    high = np.inf*np.ones(ac_size)
    low = -high
    ac_space = spaces.Box(low, high, dtype=np.float32)
    high = np.inf*np.ones(ob_size)
    low = -high
    ob_space = spaces.Box(low, high, dtype=np.float32)
    self.args = args   
    im_size = im_size
    self.PATH = PATH
    self.hid_size = hid_size
    self.writer = writer
    ob = U.get_placeholder(name="sp_ob" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
    im = U.get_placeholder(name="sp_im" + name, dtype=tf.float32, shape=[None] + im_size)
    self.args = args
    if ('vel' in name or 'hex' in name) and not args.vis:
      self.vis = False
    else:
      self.vis = vis
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      # May need some more params when using recurrent
      # act_model = Policy(ob, im, ob_space, im_size, ac_space, sess, self.vis, normalize=normalize, hid_size=hid_size)
      self.train_model = Policy(ob, im, ob_space, ob_size, im_size, ac_space, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std)
    Base.__init__(self)

  def setup_term(self):

    # if self.args.term:

      # load weights
    with tf.variable_scope(self.name + '_term', reuse=tf.AUTO_REUSE):
      self.train_model.add_term()
    # Base.__init__(self)

    vars_with_adam = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"")
    self.vars = [v for v in vars_with_adam if 'Adam' not in v.name]
    self.policy_saver = tf.train.Saver(var_list=self.vars)
    # if self.rank == 0: print(self.vars)


  def setup_training(self):

    # CREATE THE PLACEHOLDERS
    self.A = A = self.train_model.pdtype.sample_placeholder([None])
    self.ADV = ADV = tf.placeholder(tf.float32, [None])
    self.R = R = tf.placeholder(tf.float32, [None])
    # Keep track of old actor
    self.OLDNEGLOGPAC = OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
    # Keep track of old critic
    self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
    self.LR = LR = tf.placeholder(tf.float32, [])
    # self.VF_LR = VF_LR = tf.placeholder(tf.float32, [])
    # Cliprange
    self.CLIPRANGE = CLIPRANGE = tf.placeholder(tf.float32, [])

    neglogpac = self.train_model.pd.neglogp(A)

    # Calculate the entropy
    # Entropy is used to improve exploration by limiting the premature convergence to suboptimal policy.
    entropy = tf.reduce_mean(self.train_model.pd.entropy())

    # Get the predicted value
    vf_loss = 0.5*tf.reduce_mean(tf.square(self.train_model.vpred - R))

    # Calculate ratio (pi current policy / pi old policy)
    ratio = tf.exp(OLDNEGLOGPAC - neglogpac)

    # Defining Loss = - J is equivalent to max J
    pg_losses = -ADV * ratio
    pg_losses2 = -ADV * tf.clip_by_value(ratio, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)

    # Final PG loss
    pg_loss = tf.reduce_mean(tf.maximum(pg_losses, pg_losses2))
    approxkl = .5 * tf.reduce_mean(tf.square(neglogpac - OLDNEGLOGPAC))
    clipfrac = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0), CLIPRANGE)))

    self.TERM = TERM = tf.placeholder(tf.int32, [None])

    if self.args.supervised:
      one_hot = tf.one_hot(self.TERM, self.train_model.dqn_size)
      loss = tf.reduce_mean(tf.square(one_hot - self.train_model.term))

    elif self.args.dqn:
      one_hot = tf.one_hot(self.TERM, self.train_model.dqn_size)
      current_q_t = tf.reduce_sum(self.train_model.term * one_hot, 1)
      # target_q_t = self.REWARD + (1-self.DONE)*self.GAMMA*tf.reduce_max(self.target_model.vpred)    
      # term_loss = 0.5*tf.reduce_mean(tf.square(current_q_t - self.R)*self.USE_TERM)

      loss = tf.reduce_mean(tf.square(current_q_t - self.R))

    elif self.args.term:
      self.USE_TERM = tf.placeholder(dtype=tf.float32, shape=[None])
      one_hot = tf.one_hot(self.TERM, 2)
      current_q_t = tf.reduce_sum(self.train_model.term * one_hot, 1)
      term_loss = 0.5*tf.reduce_mean(tf.square(current_q_t - self.R)*self.USE_TERM)

      # term_loss = -tf.reduce_mean(self.train_model.term * ADV)
      # term_loss = -tf.reduce_mean(self.train_model.term * -ADV)
      # term_loss = -tf.reduce_mean(self.train_model.term * -ADV)
      # term_loss = 0.5*tf.reduce_mean(tf.square(self.train_model.term - TERM))*ADV
      # term_loss = -tf.reduce_mean(self.train_model.term * -ADV)
      # neglogpac = self.train_model.pd.neglogp(A)

      # term_loss = tf.reduce_mean(self.train_model.term_pd.neglogp(self.TERM)*-ADV)
      # term_loss = tf.reduce_mean(tf.square(self.train_model.term - 1.0))

      # term_loss = tf.reduce_mean(self.train_model.term_pd.neglogp(self.TERM)*ADV)
      # self.term_neglogp
      # loss = pg_loss - entropy * self.ent_coef + vf_loss 
      # self.action = U.switch(self.stochastic, self.pd.sample(), self.pd.mode())
      
      # print(term_loss.shape, self.USE_TERM.shape); exit()
      # term_loss = term_loss * self.USE_TERM

      # print(term_loss.shape); exit()
      loss = pg_loss - entropy * self.ent_coef + vf_loss + term_loss
      # loss_no_term = pg_loss - entropy * self.ent_coef + vf_loss + term_loss

      # loss = U.switch(self.USE_TERM, loss_with_term, loss_no_term)
      # loss = pg_loss - entropy * self.ent_coef + vf_loss 
    else:
      loss = pg_loss - entropy * self.ent_coef + vf_loss
    # vf_loss = vf_loss
    # UPDATE THE PARAMETERS USING LOSS
    
    # Vf only train op
    # vf_params = tf.trainable_variables(self.name+"/vf/")
    # with tf.variable_scope(self.name + "_vf", reuse=tf.AUTO_REUSE):
    #   # self.trainer = MpiAdamOptimizer(comm, learning_rate=VF_LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
    #   self.trainer = MpiAdamOptimizer(self.comm, learning_rate=LR, mpi_rank_weight=self.mpi_rank_weight, epsilon=1e-5)
    # with tf.variable_scope(self.name + "_vf", reuse=tf.AUTO_REUSE):
    #   vf_grads_and_var = self.trainer.compute_gradients(vf_loss, vf_params)
    # vf_grads, vf_var = zip(*vf_grads_and_var)
    # vf_grads, vf_grad_norm = tf.clip_by_global_norm(vf_grads, self.max_grad_norm)
    # vf_grads_and_var = list(zip(vf_grads, vf_var))
    # self.vf_grads = vf_grads
    # self.vf_var = vf_var
    # with tf.variable_scope(self.name + "_vf", reuse=tf.AUTO_REUSE):
    #   self._vf_train_op = self.trainer.apply_gradients(vf_grads_and_var)

    # if not (self.args.dqn or self.args.supervised):
    #   self._train_op = tf.train.AdamOptimizer(LR).minimize(loss)
    #   print("non-synced loss")
    # else:
    params = tf.trainable_variables(self.name)
    # 2. Build our trainer
    with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):
      self.trainer = MpiAdamOptimizer(self.comm, learning_rate=LR, mpi_rank_weight=self.mpi_rank_weight, epsilon=1e-5)

    # 3. Calculate the gradients
    with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):
      grads_and_var = self.trainer.compute_gradients(loss, params)
    grads, var = zip(*grads_and_var)
    
    grads, _grad_norm = tf.clip_by_global_norm(grads, self.max_grad_norm)
      
    grads_and_var = list(zip(grads, var))
    # zip aggregate each gradient with parameters associated
    # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

    self.grads = grads
    self.var = var
    with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):
      self._train_op = self.trainer.apply_gradients(grads_and_var)
    
    std = tf.reduce_mean(self.train_model.pd.std)
    mean_ratio = tf.reduce_mean(ratio)
    mean_adv = tf.reduce_mean(self.ADV)
   
    if self.args.term:
      self.loss_names = ['policy_loss', 'value_loss', 'term_loss', 'policy_entropy', 'approxkl', 
      'clipfrac', 'std', 'ratio', 'adv', 'cliprange', 'learning_rate']
      self.stats_list = [pg_loss, vf_loss, term_loss, entropy, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR]
    elif self.args.dqn or self.args.supervised:
      self.loss_names = ['loss', 'std', 'adv', 'cliprange', 'learning_rate']
      self.stats_list = [ loss,  std, mean_adv, CLIPRANGE, LR]
    else:
      self.loss_names = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl', 
      'clipfrac', 'std', 'ratio', 'adv', 'cliprange', 'learning_rate']
      self.stats_list = [pg_loss, vf_loss, entropy, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR]
  
    # self.act_model = act_model
    # self.step = self.train_model.step
    # self.value = self.train_model.value
    self.get_value = self.train_model.get_value
    self.get_term = self.train_model.get_term
    self.init_buffer()

  def train(self, epoch, lr, cliprange, vf_only, obs, imgs, returns, masks, actions, values, neglogpacs, term=None, use_term=False, exp_actions=None, states=None):
  # def train(self, epoch, lr, cliprange, vf_only, obs, imgs, returns, masks, actions, values, neglogpacs, exp_actions=None, states=None):
    # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
    # Returns = R + yV(s')

    advs = returns - values
    # Normalize the advantages
    advs = (advs - advs.mean()) / (advs.std() + 1e-8)
    td_map = {
      self.train_model.ob : obs,
      self.A : actions,
      self.ADV : advs,
      self.R : returns,
      self.LR : lr,
      self.CLIPRANGE : cliprange,
      self.OLDNEGLOGPAC : neglogpacs,
      self.OLDVPRED : values

    }
    td_map[self.train_model.im] = imgs
    
    if self.args.dqn or self.args.supervised:
      td_map[self.TERM] = term
      
    if self.args.term:
      td_map[self.TERM] = term
      td_map[self.USE_TERM] = use_term
  
    if states is not None:
      td_map[self.train_model.S] = states
      td_map[self.train_model.M] = masks
    
    if vf_only:
      return self.sess.run(self.stats_list + [self._vf_train_op], td_map)[:-1]
    return self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]

  def run_train(self, ep_rets, ep_lens, vf_only=False):
    max_grad_norm=0.5
    # print(self.data['ob'].shape)
    # print(self.data['ob'])
    self.train_model.ob_rms.update(self.data['ob'])
    if self.args.const_lr:
      self.cur_lrmult =  1.0
    else:
      self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
    # lr = lambda f:self.learning_rate*f
    # lrnow = lr(self.cur_lrmult)
    lrnow = self.learning_rate*self.cur_lrmult
    # lrnow = self.learning_rate
    self.lr = lrnow
    #**PPO2 doesn't seem to anneal clipping, may need to try with and without: works better without**#
    # cr = lambda f:0.2*f
    # if self.args.const_clip:
    #   # cliprangenow = cr(1.0)
    cliprangenow = 0.2
    # else:
    # for key in self.training_input:
      # print(key, self.data[key].shape)
      # cliprangenow = cr(self.cur_lrmult)
    # cliprangenow = 0.2*self.cur_lrmult
    self.n = self.data['done'].shape[0]
    inds = np.arange(self.n)
    # inds = np.arange()
    for epoch in range(self.epochs):
      # print(self.rank, epoch)
      if self.enable_shuffle:
        np.random.shuffle(inds)
      self.loss = [] 
      for start in range(0, self.n, self.batch_size):
        # print(self.rank, start)
        end = start + self.batch_size
        mbinds = inds[start:end]
        # print([key for key in self.training_input])
        # for key in self.training_input:
        #   print(key)
        #   print(self.data[key].shape)
        #   print(len(mbinds))placeholders = 
        # print([ op for op in self.get_operations() if op.type == "Placeholder"])
        # print([key for key in self.training_input], [self.data[key].shape for key in self.training_input])
        # print(mbinds)
        # print([self.data[key][mbinds].shape for key in self.training_input])
        slices = (self.data[key][mbinds] for key in self.training_input)
        outs = self.train(epoch, lrnow, cliprangenow, vf_only, *slices)
        self.loss.append(outs[:len(self.loss_names)])
    # print(self.loss)
    # print(self.rank, "finished training")
    self.loss = np.mean(self.loss, axis=0)
    # if ( self.args.supervised or self.args.dqn ):
    self.evaluate(ep_rets, ep_lens)
    self.init_buffer()

  def step(self, ob, im, stochastic=False, multi=False): 
    if not multi:
      actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
      return actions[0], values, self.states, neglogpacs
    else:
      actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
      return actions, values, self.states, neglogpacs        

  def get_reward(self, rew, vpred, done, last_value, last_done):    
    gamma = 0.99; lam = 0.95
    advs = np.zeros_like(rew)
    lastgaelam = 0
    for t in reversed(range(len(rew))):
      if t == len(rew) - 1:
        nextnonterminal = 1.0 - last_done
        nextvalues = last_value
      else:
        nextnonterminal = 1.0 - done[t+1]
        nextvalues = vpred[t+1]
      delta = rew[t] + gamma * nextvalues * nextnonterminal - vpred[t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    advs = (advs - advs.mean()) / (advs.std() + 1e-8)
    return advs

  def get_advantage(self, last_value, last_done):    
    gamma = 0.99; lam = 0.95
    advs = np.zeros_like(self.data['rew'])
    lastgaelam = 0
    for t in reversed(range(len(self.data['rew']))):
      if t == len(self.data['rew']) - 1:
        nextnonterminal = 1.0 - last_done
        nextvalues = last_value
      else:
        nextnonterminal = 1.0 - self.data['done'][t+1]
        nextvalues = self.data['value'][t+1]
      delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    self.data['return'] = advs + self.data['value']
  
  def get_advantage2(self, last_value, last_done):    
    # print("running advantage2")
    gamma = 0.99; lam = 0.95
    advs = np.zeros_like(self.data['rew'])
    lastgaelam = 0
    for t in reversed(range(len(self.data['rew']))):
      reset_gae_lam = False
      if t == len(self.data['rew']) - 1:
        if last_done == 1:
          nextnonterminal = 0.0
        else:
          if last_done == 2:
            lastgaelam = 0
            print("reseting gae_lam")
          nextnonterminal = 1.0 
        nextvalues = last_value
      else:
        if self.data['done'][t+1] == 1:
          nextnonterminal = 0.0
        else:
          if self.data['done'][t+1] == 2:
            lastgaelam = 0
            print("reseting gae_lam")
          nextnonterminal = 1.0 
        # nextvalues = self.data['value'][t+1]
        nextvalues = self.data['next_value'][t]
      delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    self.data['return'] = advs + self.data['value']

  def init_buffer(self):
    # Make sure reward is 'rew' and value/vpred is 'value', for get_advantage function in base.py
    # self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', 'roa_pred', 'roa_done']
    if self.args.advantage2:
      if self.args.term:
        self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac','next_value', 'term', 'use_term']
        self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'term', 'use_term']
      elif self.args.dqn or self.args.supervised:
        self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac','next_value', 'term']
        self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'term']
      else:
        self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac','next_value']
        self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
    else:
      self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac']
      self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
    self.data = {t:[] for t in self.data_input}

  def log_stuff(self, things):
    if self.rank == 0:
      for thing in things:
        if thing == 'Success':
          print("inside log stuff", things[thing], self.iters_so_far)
        self.writer.add_scalar(thing, things[thing], self.iters_so_far)
        logger.record_tabular(thing, things[thing])
    
  def add_to_buffer(self, data):
    ''' data needs to be a list of lists, same length as self.data'''
    for d,key in zip(data, self.data):
      self.data[key].append(d)

  def finalise_buffer(self, data, last_value, last_done, last_roa_done=None, last_roa=None):
    ''' data must be dict'''
    for key in self.data_input:
      if key == 'done':
        self.data[key] = np.asarray(self.data[key], dtype=np.bool)
      else:
        self.data[key] = np.asarray(self.data[key])
    self.n = next(iter(self.data.values())).shape[0]
    for key in data:
      self.data[key] = data[key]
    # if not self.args.supervised:
    if self.args.advantage2:
      self.get_advantage2(last_value, last_done)
    else:
      self.get_advantage(last_value, last_done)
    # if self.rank == 0:
      # self.log_stuff()
