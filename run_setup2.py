import os   
import psutil
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2
import json
import git
np.set_printoptions(precision=3, suppress=True)

def smooth_data(data, window=50):
  slider = deque(maxlen=window)
  new_data = []
  for i in data:
    slider.append(i)
    new_data.append(np.mean(slider))
  return new_data

def all_gather(data, name, rank, num_workers):
  all_d = MPI.COMM_WORLD.allgather(data)
  temp_d = np.concatenate(all_d)
  length = temp_d.shape[0]//num_workers
  start = rank*length
  end = start + length
  if name == 'done' and rank == 0:
    print("data shape", data.shape, [len(ad) for ad in all_d])
  if len(data.shape) == 1:
    return temp_d[start:end]
  else:
    return temp_d[start:end, ::]

def run(args):

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  num_workers = comm.Get_size()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)

  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    with open(PATH + 'commandline_args.txt', 'w') as f:
      f.write('Hash:')
      f.write(str(sha) + "\n")
      json.dump(args.__dict__, f, indent=2)
  else: 
    writer = None 

  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  # horizon = 2048
  # horizon = 1024
  # horizon = 763
  if args.supervised:
    horizon = 4096
  elif args.dqn:
    horizon = 2048
  else:
    horizon = 1440
  # horizon = 512
  
  from assets.env import Env

  env = Env(render=args.render, PATH=PATH, args=args, obstacle_type=args.obstacle_type, vis=True, disturbances=False, vis_type=args.vis_type, multi_robots=args.multi_robots, dqn=True)

  from models.ppo_setup import Model

  obstacle_types = ['flat', args.obstacle_type]
  start_obstacle_types = ['flat']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
  # start_obstacle_types = ['gaps', 'base', 'jumps','stairs']
  # if args.obstacle_type != 'zero':
  #   start_obstacle_types.remove(args.obstacle_type)

  pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis, const_std=args.const_std) for name in obstacle_types if name != 'zero'}  

  setup_pol = Model(args.obstacle_type + "_setup", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis, const_std=args.const_std)
  
  if args.test_pol:
    setup_pol.load('./weights/high_jumps_setup/')
  elif args.dqn:
    setup_pol.load('./weights/high_jumps_setup/')
  else:
    setup_pol.load_base(base_name='flat', WEIGHTS_PATH='./weights/flat/')
  pol['flat'].load_base(base_name='flat', WEIGHTS_PATH='./weights/flat/')
  # pol[args.obstacle_type].load_base(base_name=args.obstacle_type, WEIGHTS_PATH='./weights/' + args.obstacle_type + '/')
  # pol[args.obstacle_type].load_pol(WEIGHTS_PATH='./weights/' + args.obstacle_type + '/')
  pol[args.obstacle_type].load_pol(args.obstacle_type, WEIGHTS_PATH='./weights/' + args.obstacle_type + '/')

  if args.term or args.dqn or args.supervised:
    setup_pol.setup_term()

  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)

  setup_pol.setup_training()

  initialize_uninitialized()
  # initialize()
  sync_from_root(sess, setup_pol.vars, comm=comm)
  setup_pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  

  env.difficulty = 10
  env.height_coeff = 0.07

  prev_done = True
  prev_setup_done = True

  # Load data
  # data_pointer = 0
  # data = np.load(home + "/results/biped_setup/latest/d/samples/" + args.obstacle_type + "/goal.npy")
  
  # current_pol = np.random.choice(start_obstacle_types) 
  current_pol = 'flat'
  initial_pol = start_obstacle_types.index(current_pol)

  ob = env.reset()
  im = env.get_im()
  evaluate = False
  eval_next = False
  stochastic = True
  success = []
  if args.eval and args.eval_first:
    eval_next = True
    t_eval = time.time()
    evaluate = True
    stochastic = False
  else:
    if rank == 0:
      setup_pol.writer.add_scalar("Eval_success", 0, 0)
  
  ep_ret = 0
  ep_len = 0
  term_rew = []
  term_not_rew = []
  ep_rets = []
  ep_lens = []
  ep_steps = 0
  total_steps = 0
  train_steps = 0

  detection_dist = 0.9
  artifact_x = 0
  dist_to_artifact = 0
  prev_detected = detected = None
  
  current_pol_data = []
  adv_data = []
  adv_term_data = []
  vpred_data = []
  
  feet_data = {'left':[],'right':[]}
  com_data = []
  switch_data = []
  terrain_data = []
  max_z_height = 0
  params = {"Success":[], "Distance to target":[]}
  episode_count = 0
  setup_length = 60
  max_disturbances = 1500
  prev_next_pol = current_pol
  box_cross_x = 0
  if args.dqn or args.supervised:
    term, dqn_vpred = setup_pol.get_term(ob, im, stochastic=stochastic)
  else:
    next_term = term = False
  # act, vpred, _, nlogp = pol[current_pol].step(ob, im, stochastic=False)
  act, vpred, _, nlogp = pol['flat'].step(ob, im, stochastic=False)
  exp_next_vpred = exp_vpred = 0
  in_local_roa = False
  setup_done = False
  if args.render and args.debug:
    replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)
  vpred_buffer = []
  rew_buffer = []
  max_vpred = 1
  scaled_vpred_buffer = []
  
  flat_obs_buffer = []
  flat_com_buffer = []
  flat_imgs_buffer = []
  setup_obs_buffer = []
  setup_com_buffer = []
  setup_imgs_buffer = []
  hj_obs_buffer = []
  hj_com_buffer = []
  hj_imgs_buffer = []

  terms = []
  box_cross_steps = None
  ep_min_length = 45
  ep_max_length = 90
  # use_term = False
  use_term = True
  # success_buf = deque(maxlen=100)
  success_buf = []
  rew_buf = []
  exp_vpred0 = None

  if args.rew == 'gae':
    rews = []
    exp_vpreds = []
    dones = []
  prev_frozen = env.body_xyz[0]
  prev_data_length = horizon
  if args.spike:
    spike = deque(maxlen=5)

  training_number = 4
  train_times = 0
  run_time = time.time()

  if args.dqn_cur_decay:
    setup_pol.train_model.eps = 1.0
    ep_success = deque(maxlen=args.cur_num)
  try:
    while True:    

      if eval_next and total_steps % 1000 == 0:
        all_success = MPI.COMM_WORLD.allgather(success)
        all_success = [h for d in all_success for h in d]
        # print(rank, len(all_success))
        if len(all_success) > 100:
          eval_next = False
          done = True
          if rank == 0:
            setup_pol.writer.add_scalar("Eval_success", np.mean(all_success), setup_pol.iters_so_far)
            print("resuming training", len(all_success), "evaluation took ", time.time() - t_eval, "eval success", np.mean(all_success))

      # if (pol.timesteps

      if env.body_xyz[2] > max_z_height:
        max_z_height = env.body_xyz[2]
      if setup_pol.timesteps_so_far > setup_pol.max_timesteps:
        break 

      torques = act 
      if args.multi_robots:
        # if temp_pol != current_pol:
        # if total_steps % 150 == 0:
        if env.body_xyz[0] - prev_frozen > 0.3:
          prev_frozen = env.body_xyz[0]
          next_ob, orig_rew, done, _ = env.step(torques, freeze_robot=True)
        else:
          next_ob, orig_rew, done, _ = env.step(torques, freeze_robot=False)
      else:
        next_ob, orig_rew, done, _ = env.step(torques)
      next_im = env.get_im()

      next_pol = env.order[env.box_num]
      if args.on_detection:
        current_pol = next_pol
      # print(prev_next_pol, next_pol)
      # if not box_cross_steps and prev_next_pol != next_pol and next_pol != 'zero':
      if not box_cross_steps and next_pol == args.obstacle_type:
        # print("crossed")
        box_cross_x = env.body_xyz[0]
        box_cross_steps = env.steps
        # print("crosed box cross", prev_next_pol, next_pol)
      prev_next_pol = next_pol
    
      dist_to_artifact = box_cross_x  + 0.9 - env.body_xyz[0]   

      if args.dqn or args.supervised:
        if args.test_pol:
          next_term, dqn_next_vpred = setup_pol.get_term(next_ob, next_im, stochastic=False)
        else: 
          next_term, dqn_next_vpred = setup_pol.get_term(next_ob, next_im, stochastic=stochastic)
          # print(len(ep_success)== args.cur_num ,  (np.array(ep_success) > 0))
          if args.dqn_cur_decay:
            if len(ep_success) == args.cur_num and (np.array(ep_success) > 0).all():
              setup_pol.train_model.eps = max(setup_pol.train_model.eps - 0.1, 0.001)
              ep_success = deque(maxlen=args.cur_num)
              # print("decreasing eps", setup_pol.train_model.eps)
          else:
            setup_pol.train_model.eps = max(setup_pol.train_model.eps - 0.00001*args.decay_rate, args.min_decay)
          # if np.random.random() < setup_pol.train_model.eps:
          #   if args.use_expert:
          #     if not box_cross_steps or env.body_xyz[0] > env.box_info[1][7][0]:
          #       next_term = 2
          #     elif box_cross_steps and box_cross_steps + ep_max_length > env.steps:
          #       next_term = 0
          #     elif box_cross_steps and env.body_xyz[0] <= env.box_info[1][7][0]:
          #       next_term = 1
          #   else:
          #     next_term = np.random.randint(3)

      if args.dqn:
        if next_term == 0:
          next_act, next_vpred, _, next_nlogp = setup_pol.step(next_ob, next_im, stochastic=False)
        elif next_term == 1:
          next_act, next_vpred, _, next_nlogp = pol[args.obstacle_type].step(next_ob, next_im, stochastic=False)
        elif next_term == 2:
          next_act, next_vpred, _, next_nlogp = pol['flat'].step(next_ob, next_im, stochastic=False)
      else:
        # print(env.order[env.box_num])
        if env.order[env.box_num] == args.obstacle_type and current_pol != args.obstacle_type:
          if args.test_pol or args.dqn:
            next_act, next_vpred, _, next_nlogp = setup_pol.step(next_ob, next_im, stochastic=False)
          else:
            next_act, next_vpred, _, next_nlogp = setup_pol.step(next_ob, next_im, stochastic=stochastic)
        else:
          next_act, next_vpred, _, next_nlogp = pol[current_pol].step(next_ob, next_im, stochastic=False)
        
      if args.supervised:
        if not box_cross_steps or env.body_xyz[0] > env.box_info[1][7][0]:
          next_term = 2
        elif box_cross_steps and box_cross_steps + ep_max_length > env.steps:
          next_term = 0
        elif box_cross_steps and env.body_xyz[0] <= env.box_info[1][7][0]:
          next_term = 1

      # if not args.test_pol and current_pol != args.obstacle_type and random.random() < 0.02:
      if not args.test_pol and not args.dqn and not evaluate and current_pol != args.obstacle_type and random.random() < 0.02:
      # if not args.test_pol and not args.dqn and not evaluate and random.random() < 0.02:
        env.add_disturbance(500)

      exp_act, exp_next_vpred, _, _ = pol[args.obstacle_type].step(next_ob, next_im, stochastic=False)  
      adv = orig_rew + (1-done)*0.99*exp_next_vpred - exp_vpred
      if args.dqn:
        # if not box_cross_steps or env.body_xyz[0] > (env.box_info[1][7][0]):
        #   next_rew = np.exp(-2.0*np.sum((env.vx - 1.0)**2))
        # elif box_cross_steps and box_cross_steps + ep_max_length > env.steps and next_term == 0:
        #   next_rew = (1 - min(adv**2, 1))*exp_vpred/100
        # else:
        #   next_rew = orig_rew
        # rew = next_rew = 1
        next_rew = 1
        if (not box_cross_steps or env.body_xyz[0] > env.box_info[1][7][0]):
          if next_term == 2:
            rew = next_rew
          else:
            rew = -2
        elif box_cross_steps and box_cross_steps + ep_max_length > env.steps:
          if next_term == 0:
            rew = next_rew
          else:
            rew = -2
        elif box_cross_steps and env.body_xyz[0] <= env.box_info[1][7][0]:
          if next_term == 1:
            rew = next_rew
          else:
            rew = -2
        else:
          print("this shouldn't happen")
          # rew = 0
          rew = -2
      elif args.supervised:
        rew = 1
      elif not args.dqn and box_cross_steps:

        # adv2 = rew + (1-done)*0.99*exp_next_vpred - exp_vpred
        if not exp_vpred0:
          exp_vpred0 = exp_vpred
        rew_buf.append(orig_rew)
        adv2 = sum(rew_buf) + (1-done)*0.99*exp_next_vpred - exp_vpred0

        if args.rew == 'tor':
          rew = np.exp(-2.0*np.sum((act - exp_act)**2))
        elif args.rew == 'tor10': 
          rew = np.exp(-2.0*np.sum((act - exp_act)**2))*10
        elif args.rew == 'adv':
          # rew = (1 - min(adv**2, 1))*vpred/100
          rew = (1 - min(adv**2, 1))*exp_vpred/100
        elif args.rew == 'exp_adv':
          rew = adv
        elif args.rew == 'adv2':
          rew = (1 - min(adv2**2, 1))*exp_vpred/100
        elif args.rew in ['rew', 'gae']:
          rew = orig_rew
        elif args.rew == 'const':
          rew = 2 
      # if (box_cross_steps and ((not use_term and box_cross_steps + ep_max_length > env.steps) or (use_term and not setup_done)) and env.order[env.box_num] == args.obstacle_type) or (args.dqn):
      if (box_cross_steps and (box_cross_steps + ep_max_length > env.steps)) or args.dqn or args.supervised:
        
        setup_done = done

        if not args.dqn and args.term:
          # print(term)
          terms.append(term)
          if term and box_cross_steps + ep_min_length < env.steps:
            setup_done = 2

        if not args.test_pol and not evaluate:
          if args.rew == 'gae':
            rews.append(orig_rew)
            exp_vpreds.append(exp_vpred)
            dones.append(prev_setup_done)
          if args.dqn or args.supervised:
            # setup_pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp, next_vpred, term])
            # print
            setup_pol.add_to_buffer([ob, im, act, rew, prev_done, dqn_vpred[term], nlogp, dqn_next_vpred[next_term], term])
          elif args.term:
            setup_pol.add_to_buffer([ob, im, act, rew, prev_setup_done, vpred, nlogp, next_vpred, term, use_term])
          else:
            # print(rank, env.steps, total_steps)
            setup_pol.add_to_buffer([ob, im, act, rew, prev_setup_done, vpred, nlogp, next_vpred])
        # print(ep_steps)
        ep_steps += 1
        ep_ret += rew
        ep_len += 1
        prev_setup_done = setup_done

      # if not evaluate:
      if not eval_next:
        train_steps += 1
      total_steps += 1

      # if not args.test_pol and ((args.dqn and train_steps % horizon == 0) or (not args.dqn and len(setup_pol.data['done']) == horizon)): 
      # if not args.test_pol and not evaluate and ((( args.dqn or args.supervised ) and train_steps % horizon == 0) or (not (args.dqn or args.supervised) and (train_steps) % int(training_number*horizon) == 0)): 
      if not args.test_pol and not evaluate and ((( args.dqn or args.supervised ) and train_steps % horizon == 0) or (not (args.dqn or args.supervised) and (train_steps) % int(6*horizon) == 0)): 
      # if not args.test_pol and ep_steps % horizon == 0 and ep_steps != 0: 
        # print(rank, len(setup_pol.data['done']))
        time_to_join_loop = time.time()
        time_to_join_loop = MPI.COMM_WORLD.allgather(time_to_join_loop)
        time_to_join_loop = time.time() - np.array(time_to_join_loop)

        run_times = MPI.COMM_WORLD.allgather(time.time() - run_time)
        all_train_steps = MPI.COMM_WORLD.allgather(train_steps)
        all_total_steps = MPI.COMM_WORLD.allgather(total_steps)
        if rank == 0:
          print("loop_join_times", time_to_join_loop)
          print("All train steps", all_train_steps)
          print("All total steps", all_total_steps)
          print("run_times", run_times)

        things = {}
        things["run_times"] = np.mean(run_times)
        things["train_times"] = np.mean(train_times)

        train_time = time.time()
        
        successes = MPI.COMM_WORLD.allgather(success_buf)
        successes = np.array([d for h in successes for d in h], dtype=np.float32)
        things["Avg_success"] = np.mean(successes)

        if args.term:
          things['terms'] = np.mean(terms)
        things['eps'] = setup_pol.train_model.eps
        setup_pol.log_stuff(things)
        if args.rew == 'gae':
          exp_adv = setup_pol.get_reward(np.array(rews), np.array(exp_vpreds), np.array(dones), exp_next_vpred, setup_done)
          setup_pol.data['rew'] = list((1 - np.clip(np.square(exp_adv), 0, 1))*np.array(exp_vpreds)/100)
        if args.dqn:
          setup_pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=dqn_next_vpred[next_term], last_done=done)  
        else:
          setup_pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=next_vpred, last_done=setup_done)  
        
        # BAD!! --------------------------------
        # if not (args.dqn or args.supervised):
        #   #   # Divy up data.. 
        #   #   self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'term', 'use_term']
        #   # for d in setup_pol.training_input:
        #     # setup_pol.data[d] = all_gather(setup_pol.data[d], d, rank, num_workers)
        #   prev_data_lengths = MPI.COMM_WORLD.allgather(len(setup_pol.data['done']))
          
        #   prev_data_length = min(prev_data_lengths)
        #   # if prev_data_length < horizon:
        #   #   training_number += 1
        #   # print(training_number, int(training_number*horizon))
        #   # if rank == 0:
        #     # print((train_steps) % int(5*(horizon/prev_data_length)*horizon), int(5*(horizon/prev_data_length)*horizon), horizon/prev_data_length)
        #   # total_weight = np.zeros(1, np.float32)
        #   # comm.Allreduce(np.array([len(setup_pol.data['done'])], dtype=np.float32), total_weight, op=MPI.MIN)
        #   # if rank == 0:
        #     # print(len(setup_pol.data['done']), total_weight)
        #   # prev_data_length = int(total_weight[0])
        #   # total_weight = np.zeros(1, np.float32)
        #   # comm.Allreduce(np.array([self.mpi_rank_weight], dtype=np.float32), total_weight, op=MPI.SUM)
        #   if rank == 0:
        #     print(prev_data_length, prev_data_lengths)
        #   for d in setup_pol.training_input:
        #     setup_pol.data[d] = setup_pol.data[d][:]
        #     if len(setup_pol.data[d].shape) == 1:
        #       setup_pol.data[d] = setup_pol.data[d][:prev_data_length]
        #     else:
        #       setup_pol.data[d] = setup_pol.data[d][:prev_data_length, ::]
        #     # print(setup_pol.data[d].shape)
        # BAD!! --------------------------------
        
        if not (args.dqn or args.supervised):
          for d in setup_pol.training_input:
            all_d = MPI.COMM_WORLD.allgather(setup_pol.data[d])
            temp_d = np.concatenate(all_d)
            length = temp_d.shape[0]//num_workers
            start = rank*length
            end = start + length
            if len(setup_pol.data[d].shape) == 1:
              setup_pol.data[d] = temp_d[start:end]
            else:
              setup_pol.data[d] = temp_d[start:end, ::]
            if d == 'done' and rank == 0:
              print("data shape", setup_pol.data[d].shape, [len(ad) for ad in all_d])
        
        setup_pol.run_train(ep_rets,ep_lens)

        run_time = time.time()

        train_times = MPI.COMM_WORLD.allgather(time.time() - train_time)

        if rank == 0:
          print("prev train_times", train_times)
          process = psutil.Process(os.getpid())
          print("Total RAM usage Gb", num_workers*process.memory_info().rss/(1024.0 ** 3))  # in bytes 

        if args.rew == 'gae':
          rews = []
          exp_vpreds = []
          dones = []

        params = {"Success":[], "Distance to target":[]}
        success_buf = []
        term_rew = []
        term_not_rew = []
        ep_rets = []
        ep_lens = []
        ep_steps = 0

        if args.term:
          terms = []

        if args.eval and not eval_next and ((( args.dqn or args.supervised ) and train_steps % (horizon*20) == 0) or (not (args.dqn or args.supervised) and (train_steps) % int (6*20*horizon) == 0)): 
        # if args.eval and not eval_next and total_steps % (horizon*100) == 0: 
        # if args.eval and not eval_next and ep_steps % (horizon*100) == 0: 
          # if args.eval and train_steps % 20000 == 0 and train_steps != 0: 
          eval_next = True
          success = []
          t_eval = time.time()
          done = 2
          # if rank == 0:
          #   print(rank, "evaluating")

      # if box_cross_steps:
        # print( box_cross_steps + ep_max_length == env.step
      if box_cross_steps and ((box_cross_steps + ep_max_length == env.steps) or (use_term and setup_done)) and env.order[env.box_num] == args.obstacle_type:
        current_pol = args.obstacle_type
        setup_done = 2

      if setup_done and not args.dqn:
        # print("before", setup_pol.data['rew'][-1])
        if isinstance(setup_pol.data['rew'], list) and len(setup_pol.data['rew']) > 0:
          if args.rew != 'gae':
            setup_pol.data['rew'][-1] += rew
          else:
            rews[-1] += rew
            exp_vpreds[-1] = exp_vpred
            dones[-1] = setup_done
          ep_ret += rew
      prev_done = done
      ob = next_ob
      im = next_im
      act = next_act
      vpred = next_vpred
      if args.dqn or args.supervised:
        dqn_vpred = dqn_next_vpred
      exp_vpred = exp_next_vpred
      nlogp = next_nlogp
      term = next_term

      # if detected and env.steps - detection <= setup_length:
      if args.dqn:
        current_pol_data.append(term)
      else:
        if env.order[env.box_num] == args.obstacle_type and current_pol != args.obstacle_type:
          current_pol_data.append(len(obstacle_types) + 1)
        else:
          current_pol_data.append(obstacle_types.index(current_pol))

      switch_data.append((1 - min(adv**2, 1))*exp_vpred/100)
      adv_term_data.append((1 - min(adv**2, 1)))
      adv_data.append(1 - min((adv/5)**2, 1))
      vpred_data.append(exp_vpred/100)

      if env.order[env.box_num] == args.obstacle_type and current_pol != args.obstacle_type:
        _, ob_vpred, _, _ = pol[current_pol].step(ob, im, stochastic=False)
        vpred_buffer.append(ob_vpred)
        rew_buffer.append(rew)
        # switch_data.append(rew)
      #   if len(vpred_buffer) > 1:
      #     scaled_vpred_buffer.append((1 - min(adv**2, 1))*vpred_buffer[-2])
      #     switch_data.append(((1 - min(adv**2, 1))*vpred_buffer[-2])/max_vpred)
      #   else:
      #     switch_data.append(0)
      # else:
      #   switch_data.append(0)

      
      terrain_data.append(env.z_offset)
      if abs(env.foot_pos['left'][0] - env.foot_pos['right'][0]) < 0.05:
        feet_data['left'].append(env.foot_pos['left'][2])
        feet_data['right'].append(env.foot_pos['right'][2])
      else:
        feet_data['left'].append(np.nan)
        feet_data['right'].append(np.nan)
      if not env.ob_dict['left_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.05:
        com_data.append(env.body_xyz[2])
      elif not env.ob_dict['right_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.05:
        com_data.append(env.body_xyz[2])
      else:
        com_data.append(np.nan)

      if args.render and args.debug:
        if env.order[env.box_num] == args.obstacle_type and current_pol != args.obstacle_type:
          replace_Id2 = p.addUserDebugText("setup",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,1,0],textSize=3,replaceItemUniqueId=replace_Id2)
        else:
          replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,1,0],textSize=3,replaceItemUniqueId=replace_Id2)

      # print(box_cross_steps, env.box_num)

      if not box_cross_steps and env.box_num > 2:
      # if not box_cross_steps:
        flat_obs_buffer.append(ob)
        flat_com_buffer.append(env.body_xyz)
        flat_imgs_buffer.append(im)
      elif current_pol == args.obstacle_type and env.box_num < 7:
      # elif current_pol == args.obstacle_type:
        hj_obs_buffer.append(ob)
        hj_com_buffer.append(env.body_xyz)
        hj_imgs_buffer.append(im)
      elif box_cross_steps and current_pol != args.obstacle_type:
        # print("setup", box_cross_steps, current_pol != args.obstacle_type,  env.steps)
        setup_obs_buffer.append(ob)
        setup_com_buffer.append(env.body_xyz)
        setup_imgs_buffer.append(im)

      # ==================================================================================================
      # ==================================================================================================
      if done:  
        if args.multi_robots:
          # while True:
          env.step(np.zeros(env.ac_size), frozen=True)
          time.sleep(10)
        # use_term = not use_term
        if args.get_data:
          print(len(setup_obs_buffer))
          if len(setup_obs_buffer) > 2000:
            np.save(PATH + 'flat_obs.npy',  np.array(flat_obs_buffer))
            np.save(PATH + 'flat_com.npy',  np.array(flat_com_buffer))
            np.save(PATH + 'flat_imgs.npy', np.array(flat_imgs_buffer))
            np.save(PATH + 'setup_obs.npy',  np.array(setup_obs_buffer))
            np.save(PATH + 'setup_com.npy',  np.array(setup_com_buffer))
            np.save(PATH + 'setup_imgs.npy', np.array(setup_imgs_buffer))
            np.save(PATH + 'hj_obs.npy',  np.array(hj_obs_buffer))
            np.save(PATH + 'hj_com.npy',  np.array(hj_com_buffer))
            np.save(PATH + 'hj_imgs.npy', np.array(hj_imgs_buffer))
            exit()

        vel = np.sqrt(env.vx**2 + env.vy**2 + env.vz**2)
        params["Distance to target"].append(ep_ret)
        # if (args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') and (env.body_xyz[2] - env.z_offset) > 0.88 and vel < 1.25 and env.steps - detection > setup_length:
        # if (env.box_info[1][env.box_num][0] < env.body_xyz[0]) and (done == 2):
        # if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.body_xyz[0]) and (done == 2):
        # if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.body_xyz[0]) and env.steps >= env.max_steps - 2:
        if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.x_min):
        # if ((env.box_info[1][7][0] + env.box_info[2][7][0]) < env.body_xyz[0]):
          label = True  
          colour = [0.1,0.9,0.1,0.5]
          rew = 0
        else:
          label = False
          colour = [0.9,0.1,0.1,0.5]
          # print("fell over!")
        success_buf.append(label)
        if args.dqn_cur_decay:
          ep_success.append(label)
        
        if evaluate:
          success.append(label)

        # if args.dqn and isinstance(setup_pol.data['rew'], list) and len(setup_pol.data['rew']) > 0:
        #   setup_pol.data['rew'][-1] += 300*label
          
        if args.test_pol:
          print(episode_count)
        # if rank == 0 and episode_count % 10 == 0 and episode_count != 0:
        if rank == 0 and episode_count % 10 == 0 or args.test_pol:
          colours = [np.array([51, 204, 51])/255.0, np.array([51, 51, 204])/255.0, np.array([204, 51, 51])/255.0, np.array([204, 102, 204])/255.0, np.array([204,102,51])/255.0,]
          num_axes = 6
          fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))

          # if args.test_pol:
            # switch_data = smooth_data(switch_data, window=20)
            # adv_data = smooth_data(adv_data, window=20)

          axes[0].plot([s for s in range(len(switch_data))], switch_data, c=np.array([204, 102, 0])/255.0, alpha=1.0)  
          axes[1].plot([s for s in range(len(adv_data))], adv_term_data, c=np.array([204, 102, 0])/255.0, alpha=1.0)  
          axes[2].plot([s for s in range(len(adv_data))], np.clip(adv_data, -2, 2), c=np.array([204, 102, 0])/255.0, alpha=1.0)  
          axes[3].plot([s for s in range(len(adv_data))], vpred_data, c=np.array([204, 102, 0])/255.0, alpha=1.0)  
          
          axes[num_axes-2].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
          # axes[num_axes-2].plot([_ for _ in range(len(next_pol_data))], next_pol_data, c=np.array([0, 204, 102])/255.0, alpha=1.0)  
          axes[num_axes-2].set_ylim([-0.9,len(obstacle_types) + 1 + 0.1])
          if args.dqn:
            for s, n in enumerate(['setup', 'policy', 'flat']):
              axes[num_axes-2].text(0,s*0.75+0.5,str(s) + ". " + n)
          else:
            for s, n in enumerate(obstacle_types + ["setup"]):
              axes[num_axes-2].text(0,s*0.75+0.5,str(s) + ". " + n)
          axes[num_axes-2].set_title("Current policy", loc='left')
          axes[num_axes-2].legend(["current_policy"])  

          axes[num_axes-1].plot([_ for _ in range(len(feet_data['left']))], feet_data['left'], c=colours[0], alpha=1.0)  
          axes[num_axes-1].plot([_ for _ in range(len(feet_data['right']))], feet_data['right'], c=colours[1], alpha=1.0)  
          axes[num_axes-1].plot([_ for _ in range(len(com_data))], com_data, c=colours[2], alpha=1.0)  
          axes[num_axes-1].plot([t for t in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
          axes[num_axes-1].set_title("terrain height", loc='left')
          axes[num_axes-1].set_ylim([-0.1, max_z_height + 0.1])

          fig.tight_layout(pad=4.0)
          # plt.savefig(PATH + 'switch.png', bbox_inches='tight')
          if args.test_pol:
            plt.savefig(PATH + args.obstacle_type + '_plot' + str(episode_count) + '.png', bbox_inches='tight')
          else:
            plt.savefig(PATH + args.obstacle_type + '_plot.png', bbox_inches='tight')
          plt.close()

        current_pol_data = []
        adv_data = []
        adv_term_data = []
        vpred_data = []

        feet_data = {'left':[],'right':[]}
        com_data = []
        switch_data = []
        terrain_data = []
        max_z_height = 0

        episode_count += 1
        # prev_next_pol = current_pol = np.random.choice(start_obstacle_types) 
        term = 2
        prev_next_pol = current_pol = 'flat'
        initial_pol = start_obstacle_types.index(current_pol)

        myseed += 1
        np.random.seed(myseed)

        if evaluate:
          env.save_sim_data(evaluate=True)

        if eval_next:
          evaluate = True
        else:
          evaluate = False

        stochastic = not evaluate
        env.eval = evaluate

        ob = env.reset()
        im = env.get_im()
        if use_term:
          term_rew.append(ep_ret)
        else:
          term_not_rew.append(ep_ret)
        ep_rets.append(ep_ret)  
        ep_lens.append(ep_len)    
        ep_ret = 0
        ep_len = 0
   
        detection_dist = 0.9
        artifact_x = 0
        dist_to_artifact = 0
        prev_detected = detected = None
        in_local_roa = False
        box_cross_x = 0
        prev_setup_done = True
        setup_done = False
        box_cross_steps = None
        if scaled_vpred_buffer:
          max_vpred = max(max(scaled_vpred_buffer), max_vpred)
        vpred_buffer = []
        rew_buffer = []
        if args.dqn or args.supervised:
          term, dqn_vpred = setup_pol.get_term(ob, im, stochastic=stochastic)
        act, vpred, _, nlogp = pol[current_pol].step(ob, im, stochastic=False)
        exp_next_vpred = exp_vpred = 0
        exp_vpred0 = None
        rew_buf = []

        

        
  except KeyboardInterrupt:
    print()
    print("exiting nicely")
    MPI.COMM_WORLD.Abort(1)


if __name__ == '__main__':
  # My hack for loading defaults, but still accepting run specific defaults
  import defaults
  from dotmap import DotMap
  args1 = defaults.get_defaults() 
  parser = argparse.ArgumentParser()
  # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
  parser.add_argument('--folder', default='setup')
  parser.add_argument('--obstacle_type', default='high_jumps')
  parser.add_argument('--single_pol', default=True, action='store_false')
  # parser.add_argument('--dqn', default=True, action='store_false')
  parser.add_argument('--rew', default='adv')
  parser.add_argument('--dqn', default=False, action='store_true')
  parser.add_argument('--old_rew', default=True, action='store_false')
  parser.add_argument('--difficulty', default=10, type=int)
  args2, _ = parser.parse_known_args()
  args2 = vars(args2)
  for key in args2:
    args1[key] = args2[key]
  args = DotMap(args1)
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)


  # parser = argparse.ArgumentParser()
   
  # parser.add_argument('--decay_rate', default=1.0, type=float)
  # parser.add_argument('--min_decay', default=0.001, type=float)
  # parser.add_argument('--use_expert', default=False, action='store_true')
  # parser.add_argument('--multi_robots', default=False, action='store_true')

  # parser.add_argument('--cur_decay', default='exp')
  # parser.add_argument('--rew', default='adv')
  # parser.add_argument('--eval', default=True, action='store_false')
  # parser.add_argument('--eval_first', default=False, action='store_true')
  # parser.add_argument('--supervised', default=False, action='store_true')
  # parser.add_argument('--term_stop_gradient', default=False, action='store_true')
  # parser.add_argument('--dqn_cur_decay', default=False, action='store_true')
  # # parser.add_argument('--dqn_cur_decay', default=True, action='store_false')
  # parser.add_argument('--get_data', default=False, action='store_true')
  # parser.add_argument('--show_detection', default=False, action='store_true')
  # parser.add_argument('--term', default=False, action='store_true')
  # parser.add_argument('--new_reward', default=False, action='store_true')
  # parser.add_argument('--on_detection', default=False, action='store_true')
  # parser.add_argument('--debug', default=False, action='store_true')
  # parser.add_argument('--expert', default=False, action='store_true')
  # parser.add_argument('--blank_pol', default=False, action='store_true')
  # parser.add_argument('--two', default=False, action='store_true')
  # # parser.add_argument('--single_pol', default=False, action='store_true')
  # parser.add_argument('--terrain_first', default=True, action='store_false')
  # parser.add_argument('--single_pol', default=True, action='store_false')
  # parser.add_argument('--dist_off_ground', default=True, action='store_false')
  # parser.add_argument('--min_eps', default=0.01, type=float)
  # parser.add_argument('--more_power', default=1.0, type=float)
  # parser.add_argument('--test_pol', default=False, action='store_true')
  # parser.add_argument('--const_lr', default=False, action='store_true')
  # parser.add_argument('--cur_len', default=1, type=int)
  # parser.add_argument('--cur_num', default=3, type=int)
  # parser.add_argument('--camera_rate', default=6, type=int)
  # parser.add_argument('--num_artifacts', default=1, type=int)

  # # parser.add_argument('--eps_decay', default=1000, type=int)
  # parser.add_argument('--eps_decay', default=3000, type=int)
  # parser.add_argument('--dqn', default=False, action='store_true')
  # parser.add_argument('--spike', default=False, action='store_true')
  # parser.add_argument('--render', default=False, action='store_true')
  # parser.add_argument('--const_std', default=False, action='store_true')
  # # parser.add_argument('--const_std', default=True, action='store_false')
  # parser.add_argument('--advantage2', default=True, action='store_false')
  # parser.add_argument('--hpc', default=False, action='store_true')
  # parser.add_argument('--vis', default=True, action='store_false')
  # parser.add_argument('--vis_type', default="depth")
  # parser.add_argument('--exp', default="test")
  # parser.add_argument('--folder', default="s")
  # # parser.add_argument('--obstacle_type', default="jumps", help="flat, stairs, path, jump")
  # parser.add_argument('--obstacle_type', default="high_jumps", help="flat, stairs, path, jump")
  # parser.add_argument('--seed', default=84, type=int)
  # parser.add_argument('--max_ts', default=10e7, type=int)
  # # parser.add_argument('--max_ts', default=5e6, type=int)
  # parser.add_argument('--lr', default=3e-4, type=float)
  # args = parser.parse_args()