#!/bin/bash
# Terrains="jumps gaps stairs steps"
# Terrains="flat"
# Terrains="gaps"

# declare -a Experiments=("exp_local"
#                         "linear_local")

# declare -a Arguments=("_no_exp"
#                       "_no_link"
#                       "_no_stage1_no_stage2"
#                       "_no_stage1_stage2"
#                       "_stage1_no_stage2"
#                       "_ours")
                        

# mpirun -np 16 python3 run.py --folder test_eval_ --test_pol --obstacle_type jumps --exp "fieval_/s12/jumps" --difficulty 10 
# wait
# mpirun -np 16 python3 run.py --folder test_eval_ --test_pol --obstacle_type jumps --exp "eval_/s3/jumps" --difficulty 10 --eval_dist

Terrains="flat jumps gaps stairs steps"
# Terrains="steps"
# Terrains="jumps"

# declare -a Arguments=("_stage1_stage2"
                    #   "_stage3")


declare -a Arguments=("_2000")

# declare -a Arguments=("_stage1_stage2"
#                       "_stage3")

# # declare -a Arguments=("_ours2")
# declare -a Arguments=("_1500"
#                       "_2000"
#                       "_stage3_1500"
#                       "_stage3_2000")


# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
# #         # sbatch ./base.sh $terrain ${Experiments[$i]} "${Argumen ts[$i]}"
# #         # mpirun -np 16 python3 run.py --folder d --test_pol --obstacle_type $terrain --hpc --exp "b_comparisons/$terrain${Arguments[$i]}" 
#         # mpirun -np 16 python3 run.py --folder long --test_pol --obstacle_type $terrain --hpc --exp "final_long/$terrain${Arguments[$i]}" --difficulty 10
#         mpirun -np 16 python3 run.py --folder final_submitted --test_pol --hpc --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" --difficulty 10 --const_std --eval_dist
#         # mpirun -np 16 python3 run.py --folder 2000 --test_pol --obstacle_type $terrain --exp "final_65/$terrain${Arguments[$i]}" --difficulty 10
# #         mpirun -np 16 python3 run.py --folder red2 --hpc --test_pol --obstacle_type $terrain --exp "final/$terrain${Arguments[$i]}" --difficulty 8
# #         # mpirun -np 16 python3 run.py --folder d_orig --test_pol --obstacle_type $terrain --hpc --exp "b/$terrain${Arguments[$i]}"
#         wait
# #         # echo $terrain ${Experiments[$i]} ${Arguments[$i]}
#     done
# done

for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
        mpirun -np 16 python3 run.py --folder final_2000 --hpc --test_pol --const_std --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" --difficulty 10
    done
done

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         mpirun -np 16 python3 run.py --folder red_final2 --hpc --test_pol --const_std --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" --difficulty 8
#     done
# done

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         mpirun -np 16 python3 run.py --folder inc_final2 --hpc --test_pol --const_std --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" --difficulty 12
#     done
# done

# Terrains="flat jumps gaps stairs steps"

# declare -a Arguments=("_ours2")
# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         # sbatch ./base.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#         # mpirun -np 16 python3 run.py --folder d --test_pol --obstacle_type $terrain --hpc --exp "b_comparisons/$terrain${Arguments[$i]}" 
#         mpirun -np 16 python3 run.py --folder n1 --test_pol --obstacle_type $terrain --hpc --exp "b/$terrain${Arguments[$i]}" --eval_dist --difficulty 12
#         # mpirun -np 16 python3 run.py --folder n --test_pol --obstacle_type $terrain --exp "hpc/d/$terrain${Arguments[$i]}" --eval_dist
#         # mpirun -np 16 python3 run.py --folder d_orig --test_pol --obstacle_type $terrain --hpc --exp "b/$terrain${Arguments[$i]}"
#         wait
#         # echo $terrain ${Experiments[$i]} ${Arguments[$i]}
#     done
# done

# mpirun -np 16 python3 run.py --folder final --hpc --test_pol --obstacle_type stairs --exp "final/stairs_2000" --eval_dist --difficulty 10
# wait
# mpirun -np 16 python3 run.py --folder final --hpc --test_pol --obstacle_type flat --exp "final/flat_stage1_stage2_stage3" --eval_dist --difficulty 10
