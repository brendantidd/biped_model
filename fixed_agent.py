import sys
import pybullet as p
import time
import numpy as np
from numpy import cos as c
from numpy import sin as s
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--hex', default=False, action='store_true')
parser.add_argument('--torque', default=False, action='store_true')
parser.add_argument('--horizontal', default=False, action='store_true')
parser.add_argument('--euler', default=False, action='store_true')
parser.add_argument('--nicks', default=False, action='store_true')
parser.add_argument('--expert', default=False, action='store_true')
parser.add_argument('--single_pol', default=False, action='store_true')
parser.add_argument('--test_pol', default=False, action='store_true')
parser.add_argument('--cur_len', default=1200, type=int)
parser.add_argument('--num_artifacts', default=1, type=int)
args = parser.parse_args()

if args.hex:
	from assets.hex_env import Env
else:
	from assets.env import Env

env = Env(render=True, PATH=None, record_step = False, args=args)


obs = env.reset()
# if args.euler:
# 	angles = ['roll', 'pitch', 'yaw']
# 	angle_target = {name: {'target': p.addUserDebugParameter(name + "_target",-2.0,2.0,0)} for name in angles}
# else:
# 	angles = ['qx', 'qy', 'qz','qw']
# 	angle_target = {name: {'target': p.addUserDebugParameter(name + "_target",-1.0,1.0,0)} for name in angles}
if args.torque:
	joint_target = {name: {'target': p.addUserDebugParameter(name + "_target",-75.0,75.0,0)} for name in env.motor_names}
else:
	joint_target = {name: {'target': p.addUserDebugParameter(name + "_target",j[1],j[2],0)} for name, j in zip(env.motor_names, env.ordered_joints)}

# pos = ['x', 'y', 'z']
# pos_target = {name: {'target': p.addUserDebugParameter(name + "_target",-1.5,1.5,0)} for name in pos if name in ['x', 'y']}
# pos_target['z'] = {'target': p.addUserDebugParameter("z_target",-0.5,2.5,1.0)}



# cam_target = {}
# cam_target['dx'] = {'target': p.addUserDebugParameter("dx_target",-1.5,1.5,0.6)}
# cam_target['dy'] = {'target': p.addUserDebugParameter("dy_target",-1.5,1.5,0.0)}
# cam_target['dz'] = {'target': p.addUserDebugParameter("dz_target",-1.5,1.5,0.-0.05)}
# cam_target['cam_x'] = {'target': p.addUserDebugParameter("cam_x_target",-1.5,1.5,0.45)}
# cam_target['cam_y'] = {'target': p.addUserDebugParameter("cam_y_target",-1.5,1.5,0.0)}
# cam_target['cam_z'] = {'target': p.addUserDebugParameter("cam_z_target",-1.5,1.5,0.0)}



# insert_floor()

actions = np.zeros(12)
vels = ['vx', 'vy', 'vz', 'vx_swing', 'vy_swing', 'vz_swing']
vel_dict = {v:0.0 for v in vels}
roll, pitch, yaw = 0.0, 0.0, 0.0
actions = np.zeros(18)
while (1):
	time.sleep(0.01)
	# if args.euler:
	# 	roll = p.readUserDebugParameter(angle_target['roll']['target'])
	# 	pitch = p.readUserDebugParameter(angle_target['pitch']['target'])
	# 	yaw = p.readUserDebugParameter(angle_target['yaw']['target'])
	# else:
	# 	qx = p.readUserDebugParameter(angle_target['qx']['target'])
	# 	qy = p.readUserDebugParameter(angle_target['qy']['target'])
	# 	qz = p.readUserDebugParameter(angle_target['qz']['target'])
	# 	qw = p.readUserDebugParameter(angle_target['qw']['target'])

	# x = p.readUserDebugParameter(pos_target['x']['target'])
	# y = p.readUserDebugParameter(pos_target['y']['target'])
	# z = p.readUserDebugParameter(pos_target['z']['target'])
	
	# dx = p.readUserDebugParameter(cam_target['dx']['target'])
	# dy = p.readUserDebugParameter(cam_target['dy']['target'])
	# dz = p.readUserDebugParameter(cam_target['dz']['target'])
	# cam_x = p.readUserDebugParameter(cam_target['cam_x']['target'])
	# cam_y = p.readUserDebugParameter(cam_target['cam_y']['target'])
	# cam_z = p.readUserDebugParameter(cam_target['cam_z']['target'])
	# if not args.euler:
	# 	# orn = [0,0,-0.707, 0.707]
	# 	orn = [qx, qy, qz, qw]
	# else:
	# 	orn = p.getQuaternionFromEuler([roll, pitch, yaw])
	orn = [0,0,0,1]
	actions = [0]*env.ac_size
	for i,j in enumerate(env.motor_names):
		actions[i] = p.readUserDebugParameter(joint_target[j]['target'])
		
	env.set_position([0,0,1.0], orn)
	# env.set_position([x,y,z], orn)
	# env.set_position([0,0,1.4], [0,0,0,1], actions)
	# print(actions)
	if args.torque:
		p.setJointMotorControlArray(env.Id, env.motors,controlMode=p.TORQUE_CONTROL, forces=actions)
	else:
		p.setJointMotorControlArray(env.Id, env.motors,controlMode=p.POSITION_CONTROL, targetPositions=actions)
	# forces = [0.] * len(env.motor_names)
	# # print(len(forces), len(env.motors), len(env.motor_power), len(actions))
	# # print(actions)
	# for m in range(len(env.motor_names)):
	# 	forces[m] = env.motor_power[m]*actions[m]*0.082
	# 	p.setJointMotorControl2(env.Id, m, controlMode=p.TORQUE_CONTROL, force=forces[i])
	# print(forces)
	p.stepSimulation()
	# vel = np.array([vel_dict[v] for v in vels])
	# ob, rew_llc, rew_hlc, done, _ = env.step(np.zeros(12), vel)
	# ob, rew_hlc, done, _ = env.step(np.zeros(18))

	# env.get_debug_image(display=True)
	# env.get_image([dx, dy, dz], [cam_x, cam_y, cam_z])
	# env.get_observation()
	# print(env.ob_dict['left_foot_y'], env.ob_dict['right_foot_y'])
	# print(env.ob_dict['left_foot_x'], env.ob_dict['right_foot_x'])
    # pos, q = p.getBasePositionAndOrientation(Id)
    # euler = p.getEulerFromQuaternion(q)
    # print(roll, pitch, yaw, euler)

	# p.stepSimulation()
