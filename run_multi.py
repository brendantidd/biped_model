import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
import pybullet as p

def run(args):

  PATH = home + '/results/biped_model/latest/multi/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  # myseed = int(time.time() + 10000 * rank)
  myseed = int(args.seed + 10000 * rank)
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  if args.multi_robots:
    from assets.env_multi import Env
  else:
    from assets.env import Env

  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False, multi=args.multi)

  from models.ppo_vf import Model

  # obstacle_types = ['gaps', 'jumps', 'base']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs','turn','zero']
  obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs']
  # obstacle_types = ['gaps', 'base', 'jumps']
  # obstacle_types = ['gaps', 'base', 'jumps','zero']

  if args.multi:
    pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}  
    if args.use_vf:
      vf = {name:Model(name + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types}
    # else:
    vf_orig = {name:Model(name + "_vf_orig", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}
    # if args.use_roa:
    from models.doa import RoANet
    if args.vis:
      im_size = [60,40,1]
    else:
      im_size = env.im_size[0]
    # roa = {name:RoANet(name, args=args, ob_size=env.ob_size, im_size=im_size, train_pol=False, no_vis=args.no_vis) for name in ['gaps','jumps','stairs','base','zero']}  
    roa = {name:RoANet(name, args=args, ob_size=env.ob_size, im_size=im_size, train_pol=False, no_vis=args.no_vis) for name in ['gaps','jumps','stairs','base']}  
    if args.rl_roa:
      roa = {name:Model(name + "_switch", env=env, ob_size=env.ob_size, ac_size=1, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}  
      args.roa_thres = 0.5

    if args.dqn:
      from models.dqn_adv import DQN
      # dqn = DQN("dqn", env=env, ob_size=env.ob_size, ac_size=len(obstacle_types), im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=3e-3)
      dqn = DQN("dqn", env=env, ob_size=1, ac_size=len(obstacle_types), im_size=[1], args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=3e-3)

  else:
    pol = Model(args.exp, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
    vf = Model(args.exp + "_vf", ob_size=env.ob_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  
  initialize()
  if not args.multi:
    vf.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if args.vis:
    args.folder += "_vis"

  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
  else:
    WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder  
  
  if args.multi:
    for name in obstacle_types:
      # if args.use_roa and name in ['gaps','jumps','stairs','base','zero']:
      # if name in ['gaps','jumps','stairs','base']:
      if name in ['gaps']:
        # if args.use_roa:
          # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_new_criteria2/best/')
        # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_single2/best/')
        roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_hard/best/')
      elif name in ['jumps','stairs']:
        roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '/best/')

        # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_new_all/best/')
        # elif args.rl_roa:
        #   roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '2/best/')
        
      if args.use_vf:
        vf[name].load(home + '/results/biped_model/latest/vf1/' + name + '/') 
      if name == 'zero': continue
      pol[name].load(WEIGHTS_PATH + '/' + name + '/')
      vf_orig[name].load_base(pol[name].name, WEIGHTS_PATH + '/' + name + '/')
      # else:
      #   if name == 'zero': continue
      #   pol[name].load(WEIGHTS_PATH + '/' + name + '/')
      #   vf_orig[name].load_base(pol[name].name, WEIGHTS_PATH + '/' + name + '/') 
      
    if args.dqn:
      # dqn.load(home + '/results/biped_model/latest/' + args.folder + '/dqn/')
      # dqn.load(home + '/results/biped_model/latest/dqn/new_pols_0001/')
      # dqn.load(home + '/results/biped_model/latest/dqn/longer/')
      # dqn.load(home + '/results/biped_model/latest/dqn/longer_exp2/')
      # dqn.load(home + '/results/biped_model/latest/dqn/no_state8/best/')
      dqn.load(home + '/results/biped_model/latest/dqn/no_state8/')
  else:
    pol.load(WEIGHTS_PATH + '/' + args.exp + '/')
    vf.load_base(pol.name, WEIGHTS_PATH + '/' + args.exp + '/') 


  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()

  prev_done = True
  if not args.baseline:
    env.obstacle_type = 'mix'
  env.difficulty = args.difficulty
  env.height_coeff = args.height_coeff
  prev_done = True
  # ob = env.reset()

  np.random.seed(myseed)

  ob = env.reset(base_before=args.base_before)
  im = env.get_im()
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  ep_steps = 0
  timesteps_so_far = 0
  stochastic = False

  vpred_data = []
  vpred_orig_data = []
  probs_data = []
  stable_vpred = []
  terrain_data = []
  current_pol_data = []
  next_pol_data = []
  # switch_data = []
  switch_data = {name:[] for name in ['gaps','base', 'jumps','stairs']}


  stable_count = 120


  # obstacle_types = ['gaps', 'base', 'jumps']
  msv   = [121, 62, 28.7]

  # means = [147, 165, 115, 165]
  # stds  = [15, 14.7, 10.9, 14.7]

  means_orig = [109.80566, 149.17542, 94.06625, 134.47719, 139.7048][:len(obstacle_types)]
  stds_orig  = [12.566977, 5.6720533, 6.8909297, 3.953171, 6.167252][:len(obstacle_types)]

  # # obstacles:  ['gaps', 'base', 'jumps', 'stairs', 'zero']
  # means =  [94.36735, 100.704414, 56.201767, 83.965126, 113.21892][:len(obstacle_types)]
  # stds =  [28.819658, 27.49625, 23.5789, 21.56003, 20.222857][:len(obstacle_types)]

  # means =  [141.7557, 146.01744, 96.32929, 129.65916, 129.5527]
  # stds =  [8.266491, 7.647293, 25.360434, 4.3983607, 15.288723]

  # means =  [93.526474, 109.56233, 43.29318, 82.98333, 121.838554]
  # stds =  [38.435516, 39.08348, 36.178516, 31.103722, 27.6118]

  # obstacles:  ['gaps', 'base', 'jumps', 'stairs', 'zero']
  means =  [139.37276, 154.82619, 130.85301, 139.18336, 142.60199]
  # stds =  [17.898663, 4.119842, 8.607745, 3.2854395, 12.185635]
  stds =  [5, 5, 5, 5, 5]


  # eval_length = 100
  eval_length = 500

  # eval_length = 500
  # eval_length = 5
  # eval_length = 200
  metrics = {"episodes": 0, "dist_travelled": deque(maxlen=eval_length), "total_distance":0, "time_alive":deque(maxlen=eval_length), "success":deque(maxlen=eval_length), "switch":deque(maxlen=eval_length)}

  if args.dqn: 
    dqn_ob = np.array([obstacle_types.index("base")])
    dqn_im = np.array([0])
    dqn_vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
    switch_data.append(dqn_vpreds)
    current_pol = np.argmax(dqn_vpreds)
    temp_pol = next_pol = prev_pol = current_pol
  else:
    temp_pol = next_pol = prev_pol = current_pol = obstacle_types.index("base")
    # temp_pol = next_pol = prev_pol = current_pol = obstacle_types.index("jumps")

  print("starting evaluation, running for ", eval_length, " episodes")
  artifact_count = 0
  if args.render and args.debug:
    replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)
    replace_Id3 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.7],[0,0,1],textSize=3)
  # if args.switch or args.exp_switch or args.use_roa:
    # replace_Id = p.addUserDebugText("Switch",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.2],[0,1,0],textSize=3)
      # print(obstacle_types[current_pol])

  # prev_pol = current_pol = 0
  t1 = time.time()
  # roa_thres = 0.3
  if args.use_roa:
    roa_thres = args.roa_thres
    # ['gaps','jumps','stairs']
    # roa_thres = [0.87,0.87,0.87]
    # roa_thres = [0.83,0.83,0.83]
  else:
    roa_thres = 0.0
    # roa_thres = [0.8,0.8,0.8]
  stats = {'gaps':[],'jumps':[],'stairs':[], 'base':[]}

  q_table = {}
  q_table['jumps'] = {'gaps':4,'base':19,'stairs':0}
  q_table['gaps'] = {'base':10,'jumps':3,'stairs':10}
  q_table['stairs'] = {'gaps':7,'base':15,'jumps':38}


  # scalar = 0.99
  scalar = 0.98
  # scalar = 0.97 
  # if args.roa_table:
  roa_table = {}
    
    # roa_table['jumps'] = {'gaps':0.9781324763412345,'base':0.9778185374138691,'stairs':0.9721070138025987}
    # roa_table['stairs'] = {'gaps':0.8925965853783278,'base':0.8758598659187555,'jumps':0.8881034492627042}
    # roa_table['gaps'] = {'base':0.901837277177133,'jumps':0.8908433543489964,'stairs':0.8800138408208594}

    # roa_table['gaps'] = {'base':0.8135579174341158,'jumps':0.8479823220082393,'stairs':0.8463695993253945}
    # roa_table['jumps'] = {'gaps':0.6829121111362588,'base':0.6713067322412384,'stairs':0.6451377580780763}
    # roa_table['stairs'] = {'gaps':0.6134906093196391,'base':0.5998596904516833,'jumps':0.6023048559824625}

  # roa_table['gaps'] = {'base':0.8912404358542619,'jumps':0.8796955116770484,'stairs':0.8967811886478415}
  # roa_table['jumps'] = {'gaps':0.8529617829817016,'base':0.8592505388789706,'stairs':0.8345029192706269}
  # roa_table['stairs'] = {'gaps':0.50,'base':0.5,'jumps':0.5}

  roa_table['jumps'] = {'gaps':33,'base':32,'stairs':32}
  roa_table['stairs'] = {'gaps':5,'base':3,'jumps':5}
  roa_table['gaps'] = {'base':0,'jumps':3,'stairs':6}

  box_cross_step = None
  prev_box = env.order[env.box_num]
  max_z_height = 0
  feet_data = {'left':[],'right':[]}
  com_data = []
  switch_points = []
  detections = []

  if args.record_failures:  
    num_axes = 3    
    fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))        
  prev_box_num = env.box_num
  switch_time = None
  switch_window = 80


  while True:
  
    if env.body_xyz[2] > max_z_height:
      max_z_height = env.body_xyz[2]

    # if env.order[env.box_num] == 'zero':
    # else:
    temp_next_pol = next_pol
   
    if env.box_num + 3 < len(env.box_info[0]):
      up_coming_edges = [env.box_info[1][env.box_num + 2][0] - env.box_info[2][env.box_num + 2][0], env.box_info[1][env.box_num + 3][0] - env.box_info[2][env.box_num + 3][0]]
      distances = (np.array(up_coming_edges) - env.body_xyz[0])
      dists = np.array([d < 0.91 and d > 0.89 for d in distances])
      if (dists).any():
        idx = np.argmax(dists)
        height_diff = (env.box_info[1][env.box_num + idx + 2][2] + env.box_info[2][env.box_num + idx + 2][2]) - (env.box_info[1][env.box_num + idx + 2 - 1][2] + env.box_info[2][env.box_num + idx + 2 - 1][2])
        # if not next_pol:
        if height_diff > 0.25 and height_diff < 0.4:
          next_pol = obstacle_types.index('jumps')
        elif height_diff < -0.4:
          next_pol = obstacle_types.index('gaps')
        elif abs(height_diff) > 0.12 and abs(height_diff) < 0.21:
          next_pol = obstacle_types.index('stairs')
    else:
      next_pol = obstacle_types.index('zero')      

    if temp_next_pol != next_pol:
      box_cross_step = env.steps
      detections.append(box_cross_step)
      if args.rand_pol:
        switch_time = np.random.randint(0,switch_window) + env.steps

    # if env.box_num != prev_box_num:
    #   if env.box_num + 2 < len(env.box_info[0]):
    #     height_change = (env.box_info[1][env.box_num + 2][2] + env.box_info[2][env.box_num + 2][2]) - (env.box_info[1][env.box_num + 1][2] + env.box_info[2][env.box_num + 1][2])
    #     if height_change > 0.2 and height_change < 0.35:
    #       next_pol = obstacle_types.index('jumps')
    #     elif height_change < -0.4:
    #       next_pol = obstacle_types.index('gaps')
    #     elif abs(height_change) > 0.12 and abs(height_change) < 0.16:
    #       next_pol  = obstacle_types.index('stairs')
    #   else:
    #     next_pol = obstacle_types.index('zero')      

    # if temp_next_pol != next_pol:
    #   box_cross_step = env.steps
    #   if args.rand_pol:
    #     switch_time = np.random.randint(0,switch_window) + env.steps

    # next_pol = obstacle_types.index(env.order[env.box_num])
    prev_box_num = env.box_num

    if args.use_roa and args.roa_table and box_cross_step and env.order[env.box_num] != obstacle_types[current_pol] and not env.order[env.box_num] == 'zero':
      roa_thres = roa_table[env.order[env.box_num]][obstacle_types[current_pol]] * scalar
      # print(roa_thres, env.order[env.box_num],obstacle_types[current_pol])          
    # else:
      # roa_thres = args.roa_thres

    # print(env.difficulty)
    if timesteps_so_far > args.max_ts:
      break 
    zero_ob = copy.copy(ob)
    zero_ob[-2] = 0.0
    if current_pol == len(obstacle_types) - 1:
      act, _, _, nlogp = pol['base'].step(zero_ob, im, stochastic=stochastic)
    else:
      act, _, _, nlogp = pol[obstacle_types[current_pol]].step(ob, im, stochastic=stochastic)
    
    switch = False

    roa_output = 0
    temp_next_pol = current_pol
    if args.dqn:
      dqn_ob = np.array([obstacle_types.index(env.order[env.box_num])])
      dqn_im = np.array([0])
      dqn_vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
      # print(dqn_vpreds)
      switch_data.append(dqn_vpreds)
      temp_next_pol = np.argmax(dqn_vpreds)
    else:
      if args.use_roa:
        roas = []
        for name in ['stairs','gaps','jumps']:
          roas.append(roa[name].step(ob,im)) 
      
      if env.order[env.box_num] == 'zero':
        temp_next_pol = obstacle_types.index('zero')
      elif next_pol != current_pol:
        if args.use_roa:
          if obstacle_types[next_pol] in ['gaps','jumps','stairs']:
            roa_output = roa[obstacle_types[next_pol]].step(ob, im)
            if box_cross_step and (env.steps - box_cross_step) == switch_window:
              print("didn't threshold roa", obstacle_types[next_pol])
              temp_next_pol = next_pol              
            elif obstacle_types[next_pol] in ['gaps']:
              # if roa_output > 0.5:
              # print(roa_output)
              if roa_output >= 0.8:
              # if roa_output >= 0.5:
                temp_next_pol = next_pol              
            else:
              temp_next_pol = next_pol              
            # else:
              # if roa_output > 0.5:
                # temp_next_pol = next_pol              
        elif args.q_table:
          if (env.steps - box_cross_step == q_table[obstacle_types[next_pol]][obstacle_types[current_pol]]):              
            temp_next_pol = next_pol            
        else:          
          if args.feet and args.feet_contact:
            if not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground']:
              print(np.sqrt(np.sum((np.array(env.foot_pos['right']) - np.array(env.foot_pos['left']))**2)), abs(min(env.body_xyz[0] - env.foot_pos['right'][0], env.body_xyz[0] - env.foot_pos['left'][0])))

          elif args.feet:

            if (abs(env.foot_pos['left'][0] - env.foot_pos['right'][0]) < 0.1) and ((not env.ob_dict['left_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.1) or (not env.ob_dict['right_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.1)):
              
            # if abs(env.foot_pos['right'][0] - env.foot_pos['left'][0]) < 0.25 and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.25 and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.25:
              temp_next_pol = next_pol

          elif args.feet_contact:
            if not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground']:
              temp_next_pol = next_pol
          else:
            if args.rand_pol:
              if switch_time and env.steps == switch_time:
                temp_next_pol = next_pol
                switch_time = None
            else:
              temp_next_pol = next_pol
        
    if args.render and args.debug:
      if switch:
        replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,1,0],textSize=3,replaceItemUniqueId=replace_Id2)
        # replace_Id3 = p.addUserDebugText(obstacle_types[next_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.7],[0,1,0],textSize=3,replaceItemUniqueId=replace_Id3)
        # replace_Id = p.addUserDebugText("Switch",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.2],[0,1,0],textSize=3, replaceItemUniqueId=replace_Id)
      else:
        replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[1,0,0],textSize=3,replaceItemUniqueId=replace_Id2)
        # replace_Id3 = p.addUserDebugText(obstacle_types[next_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.7],[1,0,0],textSize=3,replaceItemUniqueId=replace_Id3)
        # replace_Id = p.addUserDebugText("Don't switch",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.2],[1,0,0],textSize=3, replaceItemUniqueId=replace_Id)
    
    if temp_next_pol != current_pol:
      prev_pol = current_pol
      current_pol = temp_next_pol

    current_pol_data.append(current_pol)
    next_pol_data.append(next_pol)
    
    terrain_data.append(env.z_offset)

    torques = act
    
    if args.multi_robots:
      if temp_pol != current_pol:
        next_ob, rew, done, _ = env.step(torques, freeze_robot=True)
      else:
        next_ob, rew, done, _ = env.step(torques, freeze_robot=False)
      next_im = env.get_im()
    else:
      next_ob, rew, done, _ = env.step(torques)
      next_im = env.get_im()

    # if env.order[env.box_num] != prev_box:
    #   box_cross_step = env.steps
    # prev_box = env.order[env.box_num]

    for name in ['gaps','base','jumps','stairs']:
      if name == obstacle_types[next_pol] and box_cross_step and (env.steps - box_cross_step) < switch_window:
        switch_data[name].append(roa[obstacle_types[next_pol]].step(ob,im))
      else:
        switch_data[name].append(0.0)

    if abs(env.foot_pos['left'][0] - env.foot_pos['right'][0]) < 0.1:
      feet_data['left'].append(env.foot_pos['left'][2])
      feet_data['right'].append(env.foot_pos['right'][2])
    else:
      feet_data['left'].append(np.nan)
      feet_data['right'].append(np.nan)
    if not env.ob_dict['left_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.1:
      com_data.append(env.body_xyz[2])
    elif not env.ob_dict['right_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.1:
      com_data.append(env.body_xyz[2])
    else:
      com_data.append(np.nan)

    if temp_pol != current_pol:
      switch_points.append(env.steps)
    # if death and death_countdown is None and env.steps > 10:
    #   death_countdown = env.steps
    #   print("gonna die!", env.steps)
    #   env.speed = 0.0
    # print(args.test_pol, args.multi, not args.test_pol or not args.multi)
    if not args.test_pol and not args.multi:
      vf.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])

    prev_done = done
    ob = next_ob
    im = next_im
    ep_ret += rew
    ep_len += 1
    ep_steps += 1
    
    temp_pol = current_pol

    # prev_vpred = vpred
    # prev_prob = prob

    if not args.test_pol and not args.multi and ep_steps % horizon == 0:
      vpred = vf.get_value(next_ob, next_im)
      vf.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
      vf.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []
    
    if done:    
      # print("====================================================================")
      # print"number of episodes", metrics["episodes"], " time alive: ", np.mean(metrics['time_alive']), " distance travelled: ", metrics['dist_travelled'])
      # print("====================================================================")
      metrics["time_alive"].append(env.steps/(120*(env.box_info[1][-1][0] + env.box_info[2][-1][0]) + 200))
      metrics["dist_travelled"].append(env.body_xyz[0])
      metrics["total_distance"] += env.box_info[1][-1][0] + env.box_info[2][-1][0]
      metrics["success"].append(env.order[env.box_num] == 'zero')
      
      if env.order[env.box_num] == 'zero':
        metrics["switch"].append(True)
      else:
        metrics["switch"].append(obstacle_types[current_pol] == env.order[env.box_num])
      # metrics["switched"].append(switch)
      if metrics["episodes"] > eval_length - 1:
        break
      # print("Episode ", metrics["episodes"], "env_steps ", env.steps, "env x pos", env.body_xyz[0], " success: ",  np.mean(metrics['success']))
      # print("episode {0:2d} Env steps {1:4d} x pos {2:7.4f}  alive: {3:0.4f} Dist: {4:0.4f}  success: {5:0d} total: {6:0.4f} switch: {7:0d} total: {8:0.4f} Last {9:s}->{10:s}".format(metrics["episodes"], env.steps, env.body_xyz[0], np.mean(metrics['time_alive']), np.sum(metrics['dist_travelled'])/metrics["total_distance"], metrics['success'][-1], np.mean(metrics['success']), metrics['switch'][-1], np.mean(metrics['switch']), obstacle_types[prev_pol], obstacle_types[current_pol]))
      print("episode {0:2d} Env steps {1:4d} x pos {2:7.4f}  alive: {3:0.4f} Dist: {4:0.4f}  success: {5:0d} total: {6:0.4f} Last {9:s}->{10:s}".format(metrics["episodes"], env.steps, env.body_xyz[0], np.mean(metrics['time_alive']), np.sum(metrics['dist_travelled'])/metrics["total_distance"], metrics['success'][-1], np.mean(metrics['success']), metrics['switch'][-1], np.mean(metrics['switch']), obstacle_types[prev_pol], obstacle_types[current_pol]))


      metrics["episodes"] += 1

      if not metrics["success"][-1]:
        stats[env.order[env.box_num]].append(metrics["episodes"])
      
      if args.record_failures and not metrics['success'][-1] and env.order[env.box_num] == 'gaps':  
        try:
          # os.mkdir(PATH + 'failure' + str(len(metrics['success']) - sum(metrics['success'])))
          os.mkdir(PATH + 'failure' + str(metrics["episodes"]))
        except:
          pass
        # env.save_sim_data(PATH + 'failure' + str(len(metrics['success']) - sum(metrics['success'])) + '/', last_steps=True)
        env.save_sim_data(PATH + 'failure' + str(metrics["episodes"]) + '/', last_steps=True)

        num_axes = 3    
        fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))        

        # colours = [np.array([51, 204, 51])/255.0, np.array([0, 102, 204])/255.0, np.array([200, 100, 51])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([100, 204, 0])/255.0, ]
        colours = [np.array([51, 204, 51])/255.0, np.array([51, 51, 204])/255.0, np.array([204, 51, 51])/255.0, np.array([204, 102, 204])/255.0, np.array([204,102,51])/255.0,]
        for i,name in enumerate(['gaps','base','jumps','stairs']):
          axes[0].plot([_ for _ in range(len(switch_data[name]))], switch_data[name], c=colours[i], linewidth=1.0, alpha=1.0)  
        axes[0].legend(obstacle_types)  
        
        axes[1].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[1].plot([_ for _ in range(len(next_pol_data))], next_pol_data, c=np.array([0, 204, 102])/255.0, alpha=1.0)  
        axes[1].set_ylim([-0.9,len(obstacle_types) + 0.1])
        for s, n in enumerate(obstacle_types):
          axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
        axes[1].set_title("Current policy", loc='left')


        axes[2].plot([_ for _ in range(len(feet_data['left']))], feet_data['left'], c=colours[0], alpha=1.0)  
        axes[2].plot([_ for _ in range(len(feet_data['right']))], feet_data['right'], c=colours[1], alpha=1.0)  
        axes[2].plot([_ for _ in range(len(com_data))], com_data, c=colours[2], alpha=1.0)  
        axes[2].plot([_ for _ in range(len(terrain_data))], terrain_data, c=colours[3], alpha=1.0)  
        axes[2].set_title("terrain height", loc='left')
        axes[2].set_ylim([-0.1, max_z_height + 0.1])

        for i in range(num_axes):
          for xc in switch_points:
            # axes[i].axvline(x=xc, label='line at x = {}'.format(xc), c=c)
            axes[i].axvline(x=xc,c=[colours[2][0],colours[2][1],colours[2][2],0.6], linestyle='--', linewidth=0.5)
          for xc in detections:
            axes[i].axvline(x=xc,c=[colours[0][0],colours[0][1],colours[0][2],0.6], linestyle='--', linewidth=0.5)


        # Feet?
        # axes[2].plot([_ for _ in range(len(probs_data))], np.clip(logp(np.array(vpred_data), np.array(means), np.array(stds)), -5,0), alpha=1.0)  
        # axes[2].legend(obstacle_types)  
        # axes[2].set_title("Log Likelihood in goal set (higher is better)", loc='left')
        
        # axes[3].plot([_ for _ in range(len(switch_data))], switch_data)  
        # axes[3].set_title("switch", loc='left')

        
        fig.tight_layout(pad=4.0)
        plt.savefig(PATH + 'failure' + str(metrics["episodes"])  + '/plot.png', bbox_inches='tight')
        plt.close()

      vpred_data = []
      vpred_orig_data = []
      probs_data = []
      # stable_vpred = []
      terrain_data = []
      current_pol_data = []
      next_pol_data = []
      switch_data = {name:[] for name in ['gaps','base', 'jumps','stairs']}
      box_cross_step = None
      prev_box = env.order[env.box_num]
      feet_data = {'left':[],'right':[]}
      com_data = []
      switch_points = []
      detections = []
      # Need to reseed so that the worlds are the same
      myseed += 1
      np.random.seed(myseed)

      ob = env.reset(base_before=args.base_before)
      im = env.get_im()
      ep_rets.append(ep_ret)  
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
      max_z_height = 0

      if args.dqn: 
        dqn_ob = np.array([obstacle_types.index("base")])
        dqn_im = np.array([0])
        dqn_vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
        switch_data.append(dqn_vpreds)
        current_pol = np.argmax(dqn_vpreds)
        temp_pol = next_pol = prev_pol = current_pol
      else:
        temp_pol = prev_pol = next_pol = current_pol = obstacle_types.index("base")
        # temp_pol = next_pol = prev_pol = current_pol = obstacle_types.index("jumps")

      prev_box_num = env.box_num
      switch_time = None


  print("============================================================================================================================")
  print("number of episodes {0:d}, time: {1:0.4f} ".format(metrics["episodes"], time.time() - t1))
  # print("Thres {0:0.2f} time alive: {1:0.4f} distance travelled: {2:0.4f}  success: {3:0.4f} switch: {4:0.4f}".format(roa_thres, np.mean(metrics['time_alive']), np.sum(metrics['dist_travelled'])/metrics['total_distance'], np.mean(metrics['success']), np.mean(metrics['switch'])))
  print("Thres {0:0.2f} time alive: {1:0.4f} distance travelled: {2:0.4f}  success: {3:0.4f}".format(roa_thres, np.mean(metrics['time_alive']), np.sum(metrics['dist_travelled'])/metrics['total_distance'], np.mean(metrics['success']), np.mean(metrics['switch'])))
  print("success", np.mean(metrics['success']))
  print(stats)
  print([thing + " : " + str(len(stats[thing])) for thing in stats])
  print("============================================================================================================================")


def logp(x, mean, std):
  return -1*(0.5 * ((x - mean) / std)**2 + 0.5 * np.log(2.0 * np.pi) + np.log(std))

def mahalanobois_distance(x, mean, std):
  return np.sqrt((x-mean)**2/std**2)

def normpdf(x, mean, std):
    var = std**2
    denom = (2*np.pi*var)**.5
    num = np.exp(-(x-mean)**2/(2*var))
    return num/denom

def calc_probs(data, all_data):
  data = np.array(data)
  all_data = np.array(all_data)
  # print(all_data.mean(), all_data.std())
  # print(all_data.shape)
  # return (data - all_data.mean())/all_data.std()
  return (data - 87.97)/1.97

# def reject_outliers(data, m=2.):
#   d = np.abs(data - np.median(data))
#   mdev = np.median(d)
#   s = d / (mdev if mdev else 1.)
#   return data[s < m]

if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--sub_folder', default='b')
  # parser.add_argument('--folder', default='b4')
  # parser.add_argument('--folder', default='b7')
  # parser.add_argument('--folder', default='b8')
  # parser.add_argument('--folder', default='b9')
  # parser.add_argument('--folder', default='b10_vis')
  parser.add_argument('--folder', default='b10')
  # parser.add_argument('--folder', default='b11')

  parser.add_argument('--difficulty', default=10, type=int)
  parser.add_argument('--height_coeff', default=0.07)
  parser.add_argument('--roa_thres', default=0.8, type=float)

  parser.add_argument('--cur_len', default=1000, type=int)
  parser.add_argument('--inc', default=2, type=int)

  parser.add_argument('--rand_pol', default=False, action='store_true')
  parser.add_argument('--rand_flat', default=False, action='store_true')
  parser.add_argument('--roa_table', default=False, action='store_true')
  parser.add_argument('--q_table', default=False, action='store_true')
  parser.add_argument('--rl_roa', default=False, action='store_true')
  parser.add_argument('--no_vis', default=False, action='store_true')
  parser.add_argument('--step_ahead', default=False, action='store_true')
  parser.add_argument('--feet', default=False, action='store_true')
  parser.add_argument('--feet_contact', default=False, action='store_true')
  parser.add_argument('--multi_robots', default=False, action='store_true')
  parser.add_argument('--debug', default=False, action='store_true')
  # parser.add_argument('--record_failures', default=False, action='store_true')
  parser.add_argument('--record_failures', default=True, action='store_false')
  parser.add_argument('--base_before', default=False, action='store_true')
  parser.add_argument('--single_pol', default=False, action='store_true')
  parser.add_argument('--no_reg', default=False, action='store_true')
  parser.add_argument('--dqn', default=False, action='store_true')

  parser.add_argument('--baseline', default=False, action='store_true')
  # parser.add_argument('--baseline', default=True, action='store_false')
  parser.add_argument('--switch', default=False, action='store_true')
  parser.add_argument('--exp_switch', default=False, action='store_true')
  parser.add_argument('--use_roa', default=False, action='store_true')

  parser.add_argument('--use_vf', default=False, action='store_true')
  parser.add_argument('--multi', default=True, action='store_false')
  parser.add_argument('--goal_set', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')
  parser.add_argument('--gae', default=False, action='store_true')
  parser.add_argument('--doa', default=True, action='store_false')
  # parser.add_argument('--doa', default=False, action='store_true')

  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  # parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=True, action='store_false')
  # parser.add_argument('--vis', default=False, action='store_true')
  parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="mix", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=1e5, type=int)
  parser.add_argument('--lr', default=1e-5, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)