import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from six.moves import cPickle as pickle
import tensorflow as tf
from sklearn import manifold
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# import seaborn as sns
import pybullet as p
# from scripts.utils import subplot


tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
          (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
          (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
          (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
          (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
for i in range(len(tableau20)):
  r, g, b = tableau20[i]
  tableau20[i] = (r / 255., g / 255., b / 255.)

def subplot(y_data, PATH=None, legend=None, title=None, x_initial=0, timestep=1/120, ylim=None):
  num_axes = len(y_data)
#   fig, axes = plt.subplots(num_axes, figsize=(20*num_axes, 30))
  # fig, axes = plt.subplots(num_axes, figsize=(20, 30))
  fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
  cols = [0,2,8,6,4,8,12,14,0,2,6,4,8]
  for j,c in zip(range(num_axes),cols):
    num_plots = len(y_data[j])
    for i in range(num_plots):
      # axes[j].plot([(_ + x_initial)*timestep for _ in range(len(y_data[j][i]))], y_data[j][i], c=tableau20[c+i+3], alpha=1.0) 
      # axes[j].plot([(_ + x_initial)*timestep for _ in range(len(y_data[j][i]))], y_data[j][i], c=tableau20[cols[i]*2], alpha=1.0) 
      # if title is not None and title[j] == 'selection':
      #   axes[j].plot([(_ + x_initial)*timestep for _ in range(len(y_data[j][i]))], y_data[j][i], '*', c=tableau20[cols[i]], alpha=1.0) 
      # else:
      axes[j].plot([(_ + x_initial)*timestep for _ in range(len(y_data[j][i]))], y_data[j][i], c=tableau20[cols[i]], alpha=1.0) 

      if title is not None and title[j] == 'selection':
        for s, n in enumerate(['up_stairs', 'down_stairs', 'jumps', 'gaps']):
          # axes[j].annotate(n, xy=(3,0.5), xytext=(5,0.8))
          # axes[j].annotate(n, xytext=(0,i))
          axes[j].text(-0.3,s*0.5+0.5,str(s) + ". " + n)
        # print(len(y_data[j][i])*timeste p)
        axes[j].set_xlim([-0.4, len(y_data[j][i])*timestep])

      if legend is not None and title[j] != 'selection':
        axes[j].legend(legend[j])  
      # if 
      if title is not None:
        if title[j] == 'doa':
          axes[j].set_ylim([-0.1, 1.1])
        if title[j] == 'selection':
          axes[j].set_ylim([-0.1, 3.1])
        
      #   axes[j].set_title(title[j], loc='left')
      #   if 'vel' in title[j]:
      #       axes[j].set_ylabel('rad/s')  
      #   elif 'pos' in title[j]:
      #       axes[j].set_ylabel('rad')  
      #   else:
      #       axes[j].set_ylabel('Nm')  
      #   if legend[j] == 'selection':
      #       axes[j].set_ylim([0,1])

        axes[j].set_xlabel('seconds')  
  if PATH is not None:
    plt.savefig(PATH + 'plot.png', bbox_inches='tight',dpi=fig.dpi)
  else:
    plt.show()

class Plots():
  def __init__(self, names=['jumps', 'gaps'], params=['selection','doa', 'terrain'], PATH=None):
    self.PATH = PATH
    self.names = names
    self.params = params
    self.reset_params()

  def reset_params(self):
    self.data = {}
    self.data['doa'] = {n:[] for n in self.names}
    self.data['selection'] = []
    self.data['terrain'] = {n:[] for n in ['z']}
     
  # def add_data(self, data):
    # for d, p in zip(data, self.params):
    #   self.params[p].append(d)
  
  def do_plot(self):
    plot_data = []
    legend = []
    title = []
    for key in self.params:
      if key == 'doa':
        plot_data.append([self.data[key][name] for name in self.names])
        legend.append([name for name in self.names])
      elif key == 'terrain':
        plot_data.append([self.data[key][name] for name in ['z']])
        legend.append([name for name in ['z']])
      else:
        plot_data.append([self.data[key]])
        legend.append([key])
      title.append(key)
    # [[x1,x2,x3],[y1,y2,y3]]  
    subplot(plot_data, legend=legend, title=title, PATH=self.PATH)
    self.reset_params()

    # axes[5].plot(xs[1:],self.params['z'][1:],'c', xs[1:], self.params['left_foot_x'][1:],'r', xs[1:], self.params['right_foot_x'][1:], 'm')
    # axes[5].plot(xs[1:],self.params['ground'][1:],color=tableau20[4], drawstyle='steps-post')
    # axes[5].legend(["z height", "left foot", "right foot", "step"])
    # axes[5].set_title("Terrain (roughly)")