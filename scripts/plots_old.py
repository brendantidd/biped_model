import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from six.moves import cPickle as pickle
import tensorflow as tf
from sklearn import manifold
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
import pybullet as p

tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
# Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
for i in range(len(tableau20)):
    r, g, b = tableau20[i]
    tableau20[i] = (r / 255., g / 255., b / 255.)


def save_dict(di_, filename_):
    with open(filename_, 'wb') as f:
        pickle.dump(di_, f)

def load_dict(filename_):
    with open(filename_, 'rb') as f:
        ret_di = pickle.load(f)
    return ret_di

class Plots():
    def __init__(self, PATH, data=['vpred','option','pol_vx','pol_yaw','pol_vy','vx','yaw_v','vy','ground','z','left_foot_x','right_foot_x','term', 'doas']):
        self.PATH = PATH
        self.data = data
        self.reset_params()

    def reset_params(self):
        self.params = {d:[] for d in self.data}

    def append_data(self, data):
        for d, p in zip(data, self.params):
            self.params[p].append(d)  
   
    def do_plot(self, act_idx, args=None, level=0):
        xs = [i for i in range(len(self.params['vpred']))]
        if level == 0 and not args.mcp:
            f, axes = plt.subplots(3,figsize=(15, 25))
            axes[0].plot(xs,self.params['vpred'],color=tableau20[0])
            axes[1].plot(xs[1:],self.params['z'][1:],'c', xs[1:], self.params['left_foot_x'][1:],'r', xs[1:], self.params['right_foot_x'][1:], 'm')
            axes[1].plot(xs[1:],self.params['ground'][1:],color=tableau20[4], drawstyle='steps-post')
            axes[1].legend(["z height", "left foot", "right foot", "step"])
            axes[1].set_title("Terrain (roughly)")
            axes[2].plot(xs,self.params['doa_pred'],color=tableau20[0])
            axes[2].set_ylim([-0.1, 1.1])
        else:
            f, axes = plt.subplots(8,figsize=(15, 25))
            c = ['g','b','r','c','m','k','y']
            axes[0].plot(xs,self.params['vpred'],c[0])
            if args.mcp:
                self.params['option'] = np.array(self.params['option']).reshape([-1, 4])
                axes[1].plot(xs,self.params['option'][:, 0],color=tableau20[0])
                axes[1].plot(xs,self.params['option'][:, 1]+1,color=tableau20[2])
                axes[1].plot(xs,self.params['option'][:, 2]+2,color=tableau20[4])
                axes[1].plot(xs,self.params['option'][:, 3]+3,color=tableau20[6])
                axes[1].set_title("Weights")
                axes[1].set_ylim([-0.1, 4.1])
                # axes[1].legend(["down", "up", "flat", "balance"], loc="upper left")
                axes[1].legend(["down", "up", "flat", "balance"], loc="upper left")
            else:
                axes[1].plot(xs,self.params['option'],color=tableau20[0])
                mins = np.argmin(self.params['doas'], axis=1)
                # print(mins.tolist())
                axes[1].plot(xs,mins,color=tableau20[9])
                axes[1].set_title("Policy selection")
                if args.num_pols > 3:
                    axes[1].legend(['down = 0 | up = 1 | flat = 2 | balance = 3'], loc="upper left")
                    axes[1].set_ylim([-0.1, 3.3])
                else:
                    axes[1].legend(['down = 0 | up = 1 | flat = 2'], loc="upper left")
                    axes[1].set_ylim([-0.1, 2.3])

            axes[2].plot(xs,self.params['pol_vx'],color='r')
            axes[2].plot(xs,self.params['vx'],color=tableau20[0])
            # axes[2].plot(xs,self.params['des_vx'],color='r')
            axes[2].legend(['policy', 'actual'], loc="upper right")
            axes[2].set_title("Speed of current policy")
            axes[2].set_ylim([0.0, 2.0])

            axes[3].plot(xs,self.params['pol_yaw'],color='r')
            axes[3].plot(xs,self.params['yaw_v'],color=tableau20[0])
            axes[3].legend(['policy', 'actual'], loc="upper right")
            axes[3].set_title("Yaw speed of current policy")
            axes[3].set_ylim([-2.0, 2.0])

            axes[4].plot(xs,self.params['pol_vy'],color='r')
            axes[4].plot(xs,self.params['vy'],color=tableau20[0])
            axes[4].legend(['policy', 'actual'], loc="upper right")
            axes[4].set_title("Y speed of current policy")
            axes[4].set_ylim([-1.0, 1.0])

            axes[5].plot(xs[1:],self.params['z'][1:],'c', xs[1:], self.params['left_foot_x'][1:],'r', xs[1:], self.params['right_foot_x'][1:], 'm')
            axes[5].plot(xs[1:],self.params['ground'][1:],color=tableau20[4], drawstyle='steps-post')
            axes[5].legend(["z height", "left foot", "right foot", "step"])
            axes[5].set_title("Terrain (roughly)")
            
            axes[6].plot(xs,self.params['term'],color=tableau20[0])
            axes[6].set_title("Termination prediction")
            axes[6].set_ylim([-0.1, 1.1])

            # print(self.params['doas'].shape)
            if args.train_doa:
                # self.params['doas'] = np.array(self.params['doas']).reshape([-1, 4])
                axes[7].plot(xs,self.params['doas'],color=tableau20[0])
            else:
                self.params['doas'] = np.array(self.params['doas']).reshape([-1, 4])
                axes[7].plot(xs,self.params['doas'][:, 0],color=tableau20[0])
                axes[7].plot(xs,self.params['doas'][:, 1],color=tableau20[2])
                axes[7].plot(xs,self.params['doas'][:, 2],color=tableau20[4])
                axes[7].plot(xs,self.params['doas'][:, 3],color=tableau20[6])
            axes[7].set_title("Doas")
            axes[7].set_ylim([-0.1, 1.1])
            axes[7].legend(["down", "up", "flat", "balance"], loc="upper left")

            
            # for i,ax in enumerate(axes):
            #     # if i == : continue
            #     # for start, end in zip(count['start'], count['end']):
                # for x_val in self.params['change']:
                #     ax.axvline(x=x_val, color=tableau20[4], linestyle='--')
            #         # ax.axvline(x=end, color='r', linestyle='--')
        plt.savefig(self.PATH + 'fig' + str(act_idx) + '.png', bbox_inches='tight')
        plt.close(f)
        save_dict(self.params, self.PATH + 'params' + str(act_idx) + '.pkl')


    def quick_doa_pca(self, env, pols):
        # self.state = ['right_hip_z_pos','right_hip_x_pos','right_hip_y_pos','right_knee_pos','right_ankle_x_pos', 'right_ankle_y_pos']
        # self.state += ['left_hip_z_pos', 'left_hip_x_pos', 'left_hip_y_pos', 'left_knee_pos','left_ankle_x_pos', 'left_ankle_y_pos']
        # self.state += ['right_hip_z_vel','right_hip_x_vel','right_hip_y_vel','right_knee_vel','right_ankle_x_vel', 'right_ankle_y_vel']
        # self.state += ['left_hip_z_vel', 'left_hip_x_vel', 'left_hip_y_vel', 'left_knee_vel','left_ankle_x_vel', 'left_ankle_y_vel']
        # self.state += ['com_z']
        # self.state += ['vx','vz','pitch','pitch_vel']
        # # Needs negatives when inverting:
        # self.state += ['vy','roll','yaw']
        # # self.state += ['roll_vel','yaw_vel', 'yaw_buf_vel']
        # self.state += ['roll_vel','yaw_vel']
        # self.state += ['right_heel1','right_heel2','right_toe']
        # self.state += ['left_heel1','left_heel2','left_toe']
        # self.state += ['prev_right_foot_left_ground','right_foot_left_ground']
        # self.state += ['prev_left_foot_left_ground','left_foot_left_ground']
        # self.state += ['left_foot_swing', 'right_foot_swing']
        # self.state += ['impact']    
        # 0-12 joint_pos, 12-24 joint_vel
        # pos, orn, joints, base_vel, joint_vel = [0,0,box_z+self.start_z],[0,0,0,1], [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len
        # initials = [1.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        # initial_state = joints + joint_vel + 
        # [pos, y_pos, orn, rot, vel, joint, joint_vel]
        # c = [0.4, 0.2, 0.1, 0.5, 1.0, 1.0, 1.0]   
        env.disturbances = True; env.max_disturbance = 50.0; env.up_down_flat = True; env.height_coeff = 0.07            
        # select = 2
        # if select == 0: env.up_down_flat_arg = ['down' for _ in range(55)]
        # if select == 1: env.up_down_flat_arg = ['up' for _ in range(55)]
        # if select == 2: env.up_down_flat_arg = ['flat' for _ in range(55)]
        # if select == 3: env.up_down_flat_arg = [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(20)]

        # self.data = 

        obs = []
        doas = []
        while len(obs) < 6000:
            env.up_down_flat_arg = [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(55)]

            done = False
            ob = env.reset()
            # self.reset_params()
            # obs = []
            # obs.append(ob)
            print(len(obs))
            im = env.get_image('lower')
            
            while not done:
                # if select == 3:
                #     velocity = pols['sp'][select].check_limits([0.2,0,0])
                # else:            
                # env.add_disturbance()
                velocity = pols['sp'][0].check_limits([1,0,0])
                ob = env.update_speed(velocity[0], velocity[1],velocity[2])
                obs.append(ob)
                sp_doas, _ = pols['doa'].step(ob)
                if pols['sp'][0].args.train_doa:
                    sp_doas = [sp_doas[0], 0,0,0]
                select, _, _, _ = pols['select'].step(ob, im, stochastic=False)
                doas.append(sp_doas[select])

                # doas.append(sp_doas[select])
                # select = np.argmin(sp_doas)
                actions, *other_stuff = pols['sp'][select].step(ob, im, stochastic=False)
                ob, rew, done, _ = env.step(actions)
                im = env.get_image('lower')
                left_foot = p.getLinkState(env.Id, env.feet_dict["left_heel1"])[0][2]
                right_foot = p.getLinkState(env.Id, env.feet_dict["right_heel1"])[0][2]
                self.append_data([0,select,velocity[0],velocity[1],velocity[2],env.ob_dict["vx"],env.ob_dict["yaw_vel"],env.ob_dict["vy"],env.z_offset,env.body_xyz[2],left_foot,right_foot,0, sp_doas])
        import random
        self.do_plot(random.sample([_ for _ in range(12)],1)[0], pols['sp'][0].args, level=1)
        # self.reset_params()
        # c = [3.0,3.0,3.0,3.0,3.0]
        # c = [2.0,2.0,2.0,2.0,2.0]
        # c = [1.0,1.0,1.0,1.0,1.0]
        # c = [0.5,0.5,0.5,0.5,1.0]
        c = [0.2,0.2,0.2,0.2,0.2]
        # c = [0,0,0,0,0]
        num_points = 20000
            
        joints = np.random.uniform(low=-c[0], high=c[0], size=(num_points,12))
        for i, (j, m) in enumerate(zip(env.ordered_joints, env.motor_names)):
            joints[:,i] = np.clip(joints[:,i], j[1], j[2])
        joint_vel = np.random.uniform(low=-c[1], high=c[1], size=(num_points,12))

        com_z = np.random.uniform(low=0.8, high=1.1, size=(num_points,1))

        # [vx, vz, pitch, pitch_vel, vy, roll, yaw, roll_vel, yaw_vel]
        terms = np.random.uniform(low=-c[2], high=c[2], size=(num_points,9))
        # self.state += ['right_heel1','right_heel2','right_toe']
        # self.state += ['left_heel1','left_heel2','left_toe']
        # self.state += ['prev_right_foot_left_ground','right_foot_left_ground']
        # self.state += ['prev_left_foot_left_ground','left_foot_left_ground']
        # self.state += ['left_foot_swing', 'right_foot_swing']
        bin_terms = np.random.randint(2, size=(num_points,12))
        impact = np.random.choice([0,1], size=(num_points,1), p=[0.98,0.02])
        vel1 = np.random.uniform(low=0.0,high=1.5, size=(num_points,1))  
        vel2 = np.random.uniform(low=-env.max_yaw,high=env.max_yaw,size=(num_points,1))  
        vel3 = np.random.uniform(low=-env.max_vy,high=env.max_vy,size=(num_points,1))  
          
        # print(joints.shape, joint_vel.shape, com_z.shape, terms.shape, bin_terms.shape, impact.shape)
        # data = np.concatenate([joints, joint_vel, com_z, terms, bin_terms, impact, vel1, vel2, vel3], axis=1)
        data = np.array(obs)
        
        sp_doas, _ = pols['doa'].step(data,multi=True)
        # selects = self.params['option']
        # print(len(selects), data.shape)
        # colours = []
        # for s in selects:
        #     if s == 0: colours.append(tableau20[0])
        #     if s == 1: colours.append(tableau20[2])
        #     if s == 2: colours.append(tableau20[4])
        #     if s == 3: colours.append(tableau20[6])
        tf_pca = TF_PCA(data)
        tf_pca.fit()
        pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions

        fig = plt.figure(1, figsize=(8, 6))
        if pols['sp'][0].args.train_doa:
            greys = [idx for idx, d in enumerate(sp_doas) if d > 0.2]
        else:
            greys = [idx for idx, d in enumerate(doas) if d > 0.2]
        plt.scatter(pca[:, 0], pca[:,1], s=10, c=tableau20[5])
        # plt.scatter(pca[:, 0], pca[:,1], s=10, c=colours)
        plt.scatter(pca[greys, 0], pca[greys,1], s=10, c=tableau20[14])
        # plt.legend(['Down', 'Up', 'Flat', 'Balance', 'Switched', 'Died'])
        plt.savefig(self.PATH + '_pca_fig' + 'select' + '.png', bbox_inches='tight')
        plt.close(fig)

        # for i in range(4):
        #     fig = plt.figure(1, figsize=(8, 6))
        #     greys = [idx for idx, d in enumerate(sp_doas) if d[i] > 0.5]
        #     plt.scatter(pca[:, 0], pca[:,1], s=10, c=tableau20[5])
        #     plt.scatter(pca[greys, 0], pca[greys,1], s=10, c=tableau20[14])
        #     # plt.legend(['Down', 'Up', 'Flat', 'Balance', 'Switched', 'Died'])
        #     plt.savefig(self.PATH + '_pca_fig' + str(i) + '.png', bbox_inches='tight')
        #     plt.close(fig)

        # fig = plt.figure(1, figsize=(8, 2))
        # xs = [i for i in range(len(self.params['z']))]
        # plt.plot(xs[1:],self.params['z'][1:],'c', xs[1:], self.params['left_foot_x'][1:],'r', xs[1:], self.params['right_foot_x'][1:], 'm')
        # plt.plot(xs[1:],self.params['ground'][1:],color=tableau20[4], drawstyle='steps-post')
        # plt.legend(["z height", "left foot", "right foot", "step"])
        # # plt.set_title("Terrain (roughly)")   
        # plt.ylabel('Height (m)')
        # plt.xlabel('Time steps')

        # plt.savefig(self.PATH + '_pca_fig' + 'steps' + '.png', bbox_inches='tight')
        # plt.close(fig)
        
        # fig = plt.figure(1, figsize=(8, 2))
        # self.params['doas'] = np.array(self.params['doas']).reshape([-1, 4])
        # plt.plot(xs,self.params['doas'][:, 0],color=tableau20[0])
        # plt.plot(xs,self.params['doas'][:, 1],color=tableau20[2])
        # plt.plot(xs,self.params['doas'][:, 2],color=tableau20[4])
        # plt.plot(xs,self.params['doas'][:, 3],color=tableau20[6])
        # # plt.set_title("Doas")
        # plt.ylim([-0.1, 1.1])
        # plt.ylabel('DoA Estimate')
        # plt.xlabel('Time steps')
        # plt.legend(["down", "up", "flat", "balance"], loc="upper left")
        # plt.savefig(self.PATH + '_pca_fig' + 'doas' + '.png', bbox_inches='tight')
        # plt.close(fig)

        
        # greys0 = [idx for idx, d in enumerate(sp_doas) if d[0] > 0.5]
        # greys1 = [idx for idx, d in enumerate(sp_doas) if d[1] > 0.5]
        # greys2 = [idx for idx, d in enumerate(sp_doas) if d[2] > 0.5]
        # greys3 = [idx for idx, d in enumerate(sp_doas) if d[3] > 0.5]

        # fig, axes = plt.subplots(4,figsize=(15, 25))
        # axes[0].scatter(pca[:, 0], pca[:,1], s=10, c=tableau20[5])
        # axes[1].scatter(pca[:, 0], pca[:,1], s=10, c=tableau20[5])
        # axes[2].scatter(pca[:, 0], pca[:,1], s=10, c=tableau20[5])
        # axes[3].scatter(pca[:, 0], pca[:,1], s=10, c=tableau20[5])
        # axes[0].scatter(pca[greys0, 0], pca[greys0,1], s=10, c=tableau20[14])
        # axes[1].scatter(pca[greys1, 0], pca[greys1,1], s=10, c=tableau20[14])
        # axes[2].scatter(pca[greys2, 0], pca[greys2,1], s=10, c=tableau20[14])
        # axes[3].scatter(pca[greys3, 0], pca[greys3,1], s=10, c=tableau20[14])
        # plt.savefig(self.PATH + 'mult_pca_fig.png', bbox_inches='tight')
        # plt.close(fig)


    def run_doa_pca(self, data_single, data_multi, pols):


        

        # flat_im = np.array(data['ims']).reshape([-1, 48*48*4])
        # obs = np.array(data['obs'])
        # # all_data = np.concatenate([flat_im, obs], axis=1)
        # print(all_data.shape)
        # self.state = ['right_hip_z_pos','right_hip_x_pos','right_hip_y_pos','right_knee_pos','right_ankle_x_pos', 'right_ankle_y_pos']
        # self.state += ['left_hip_z_pos', 'left_hip_x_pos', 'left_hip_y_pos', 'left_knee_pos','left_ankle_x_pos', 'left_ankle_y_pos']
        # self.state += ['right_hip_z_vel','right_hip_x_vel','right_hip_y_vel','right_knee_vel','right_ankle_x_vel', 'right_ankle_y_vel']
        # self.state += ['left_hip_z_vel', 'left_hip_x_vel', 'left_hip_y_vel', 'left_knee_vel','left_ankle_x_vel', 'left_ankle_y_vel']
        # self.state += ['com_z']
        # self.state += ['vx','vz','pitch','pitch_vel']
        # # Needs negatives when inverting:
        # self.state += ['vy','roll','yaw']
        # # self.state += ['roll_vel','yaw_vel', 'yaw_buf_vel']
        # self.state += ['roll_vel','yaw_vel']
        # self.state += ['right_heel1','right_heel2','right_toe']
        # self.state += ['left_heel1','left_heel2','left_toe']
        # self.state += ['prev_right_foot_left_ground','right_foot_left_ground']
        # self.state += ['prev_left_foot_left_ground','left_foot_left_ground']
        # self.state += ['left_foot_swing', 'right_foot_swing']
        # self.state += ['impact']      
        # joints, vels
        # # 0:24, 25, 26, 28, 29, 32,33
        # idx = np.array([_ for _ in range(24)] + [25, 26, 28, 29, 32, 33])
        # # print(idx)
        # data_size = 5
        # noise = 2*(np.random.random([data_size*len(data['obs']), 30])-0.5)
        # print(noise.shape)
        # obs = data['obs'][idx] + noise
        # print(obs.shape)
        # obs = np.array(data['obs'])
        # obs = np.tile(obs, [data_size, 1])
        # obs[:,idx] += noise
        # print(obs.shape)
        # ims = np.tile(data['ims'], [data_size,1,1,1])
        # print(ims.shape)
        # flat_im = ims.reshape([-1, 48*48*4])
        # all_data = np.concatenate([flat_im, obs], axis=1)
        # all_data = obs
        # output, embed = sp_doa.step(obs, ims)
        # print(output.shape, embed.shape)
        # all_data = embed
        # all_data = np.array(data['acts'])
        # fig, axes = plt.subplots(4,figsize=(15, 25))
        thres = 0.8
        # for i in range(4):
        #     fig = plt.figure(1, figsize=(8, 6))

        #     # data1, _, _, _, _ = pols['sp'][i].step(data_multi['obs'], data_multi['ims'], multi=True)
        #     _, _, data1 = pols['doa'].step(data_multi['obs'], data_multi['ims'], 0, multi=True)

        #     # tf_pca = TF_PCA(data1)
        #     tf_pca = TF_PCA(data1[i])
        #     tf_pca.fit()
        #     pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions
        #     cidx = i*2
        #     greys = [idx for idx, d in enumerate(data_multi['sp_doas']) if d[i] > thres]
        #     sizes = [50 if d[i] > thres else 20 for d in data_multi['sp_doas']]
        #     # print(tableau20[cidx])
        #     plt.scatter(pca[:, 0], pca[:,1], s=sizes, c=tableau20[cidx])
        #     plt.scatter(pca[greys, 0], pca[greys,1], s=sizes, c=tableau20[14])
        #     # axes[i].scatter(pca[:, 0], pca[:,1], s=sizes, c=tableau20[cidx])
        #     # axes[i].scatter(pca[greys, 0], pca[greys,1], s=sizes, c=tableau20[14])
        #     # axes[i].scatter(pca[:, 0], pca[:,1], s=10, c='r')       

        #     # plt.legend(['Down', 'Up', 'Flat', 'Balance'])
        #     plt.savefig(self.PATH + 'multis_doa_pca_fig' + str(i) + '.png', bbox_inches='tight')
        #     plt.close(fig)
      
        # fig = plt.figure(1, figsize=(8, 6))
        # # tf_pca = TF_PCA(all_data)
        # # tf_pca.fit()
        # # pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions   
        # pcas = []
        # greys = []
        # for i in range(4):
        #     # _, _, data2 = pols['doa'].step(data_multi['obs'], data_multi['ims'], select=0, multi=True)
        #     data2, _, _, _, _ = pols['sp'][i].step(data_multi['obs'], data_multi['ims'], multi=True)
        #     # tf_pca = TF_PCA(data2[i])
        #     tf_pca = TF_PCA(data2)
        #     tf_pca.fit()
        #     pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions
        #     cidx = i*2
        #     pcas.append(pca)
        #     greys.append([idx for idx, d in enumerate(data_multi['sp_doas']) if d[i] > thres])
        # for i, pca in enumerate(pcas):
        #     cidx = i*2
        #     plt.scatter(pca[:, 0], pca[:,1], s=10,  c=tableau20[cidx])
        # for grey, pca in zip(greys, pcas):
        #     plt.scatter(pca[grey, 0], pca[grey,1], s=10, c=tableau20[14])
        #     # axes[i].scatter(pca[:, 0], pca[:,1], s=10, c=doa_cols)
        #     # axes[i].scatter(pca[:, 0], pca[:,1], s=10, c='r')       

        # plt.legend(['Down', 'Up', 'Flat', 'Balance'])
        # plt.savefig(self.PATH + 'multi_doa_pca_fig.png', bbox_inches='tight')
        # plt.close(fig)
        # ------------------------------------------------------------------------
        # thres = 0.2
        # all_data = []
        # all_labels = []
        # all_colours = []
        # greys = []
        # for i in range(4):
        #     data2, _, _, _, _ = pols['sp'][i].step(data_multi['obs'], data_multi['ims'], multi=True)
        #     all_data.extend(data2)
        #     cidx = i*2
        #     all_labels.extend([i for _ in range(len(data_multi['obs']))])
        #     all_colours.extend([tableau20[cidx] for _ in range(len(data_multi['obs']))])
        #     greys.append([idx*i for idx, d in enumerate(data_multi['sp_doas']) if d[i] > thres])
        # # print(len(data_multi['sp_doas']))
        # # print(greys)
        # # print(np.array(all_labels).shape)
        # tf_pca = TF_PCA(np.array(all_data), np.array(all_labels))
        # tf_pca.fit()
        # pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions

        # # plt.scatter(pca[grey, 0], pca[grey,1], s=10, c=tableau20[14])
        # color_mapping = {0: tableau20[0], 1: tableau20[2], 2: tableau20[4], 3: tableau20[6]}
        # colors = list(map(lambda x: color_mapping[x], tf_pca.target))
        # for i,g in enumerate(greys):
        #     fig = plt.figure(1, figsize=(8, 6))       
        #     plt.scatter(pca[:, 0], pca[:,1], s=10,  c=colors)
        #     plt.scatter(pca[g, 0], pca[g,1], s=10, c=tableau20[14])
        #     # plt.legend(['Down', 'Up', 'Flat', 'Balance'])
        #     plt.savefig(self.PATH + 'test' + str(i) + '.png', bbox_inches='tight')
        #     plt.close(fig) 
        # ------------------------------------------------------------------------
        # thres = 0.2
        # all_data = []
        # all_labels = []
        # all_colours = []
        # greys = []
        # _, _, data2 = pols['doa'].step(data_multi['obs'], data_multi['ims'], select=0, multi=True)
        # print("actions")
        # for i in [2,4]:
        #     data2, _, _, _, _ = pols['sp'][i].step(data_multi['obs'], data_multi['ims'], multi=True)
        #     all_data.extend(data2)
        #     all_labels.extend([i for _ in range(len(data_multi['obs']))])

        #     cidx = i*2
        #     # all_labels.extend([s for s in data_multi['selects']])
        #     all_colours.extend([tableau20[cidx] for _ in range(len(data_multi['obs']))])
        # # greys.append([idx*i for idx, d in enumerate(data_multi['sp_doas']) if d[i] > thres])
        # # print(len(data_multi['sp_doas']))
        # # print(greys)
        # # print(np.array(all_labels).shape)
        # tf_pca = TF_PCA(np.array(all_data), np.array(all_labels))
        # tf_pca.fit()
        # pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions

        # # plt.scatter(pca[grey, 0], pca[grey,1], s=10, c=tableau20[14])
        # # color_mapping = {0: tableau20[0], 1: tableau20[2], 2: tableau20[4], 3: tableau20[6]}
        # # colors = list(map(lambda x: color_mapping[x], tf_pca.target))
        # # for i,g in enumerate(greys):
        # fig = plt.figure(1, figsize=(8, 6))       
        # plt.scatter(pca[:, 0], pca[:,1], s=10,  c=all_colors)
        # # plt.scatter(pca[g, 0], pca[g,1], s=10, c=tableau20[14])
        # # plt.legend(['Down', 'Up', 'Flat', 'Balance'])
        # plt.savefig(self.PATH + 'test' + str(i) + '.png', bbox_inches='tight')
        # plt.close(fig) 
        
        # exp = 2
        exp = 4
       
        # np.save('/home/brendan/results/elements/flats2/obs' + str(exp) + '.npy', np.array(data_multi['obs']))
        # np.save('/home/brendan/results/elements/flats2/ims' + str(exp) + '.npy', np.array(data_multi['ims']))

        _, _, embed1 = pols['doa'].step(data_multi['obs'], data_multi['ims'], select=0, multi=True)
        print(embed1.shape)
        np.save('/home/brendan/results/elements/flats3/embed' + '_flat' + '.npy', np.array(embed1))
        # np.save('/home/brendan/results/elements/flats3/embed' + '_flat_alt' + '.npy', np.array(embed1))
        # --------------------------------------------------------------------------------------------
        # np.save('/home/brendan/results/elements/select_doa/embed' + '_not_select' + '.npy', np.array(embed1))
        # np.save('/home/brendan/results/elements/select_doa/embed' + ' _select' + '.npy', np.array(embed1))
        # single = True
        single = False
        if single:
            exit()
        elif len(data_multi['obs']) > 4000:

            exit()


    def run_pca(self, data, name):

    
        # iris_dataset = datasets.load_iris()
        # print(iris_dataset.data.shape, iris_dataset.target.shape)
        # tf_pca = TF_PCA(iris_dataset.data, iris_dataset.target)
        # print(data)
        # for key in data:
        # data = 
        # if len(data[key][0]) < 45:
            # continue
        all_data = []
        all_labels = []
        selected = []
        env_steps = []
        for key in data:
            # if data[key] and key in [0,1,2,3]:
            # if data[key] and key in [0,1,2,3]:
            if data[key] and key in [0,1]:
                for i, d in enumerate(data[key][0]):
                    # print(d)
                    all_data.append(d)
                    all_labels.append(key)
                    selected.append(data[key][1][i])
                    env_steps.append(data[key][2][i])
                    # if data[key][2][i] < (bin_size*i) and data[key][2][i] > (bin_size*i+bin_size):
                    #     env_steps.append(data[key][2][i])
        all_data = np.array(all_data)
        all_labels = np.array(all_labels)
        selected = np.array(selected)

        # print(all_data.shape, all_labels.shape)
        # print(all_data.shape, all_labels.shape)


        # print(len(all_labels), 48*48*4, all_data.shape)
        if len(all_labels) < all_data.shape[1]:
            return
    

        ind1 = np.where(all_labels == 0)
        ind2 = np.where(all_labels == 1)
        ind3 = np.where(all_labels == 2)
        ind4 = np.where(all_labels == 3)
        ind5 = np.where(selected == 1)
        ind6 = np.where(selected == 2)
        # print(ind5, ind6)
        tf_pca = TF_PCA(all_data, all_labels)
        tf_pca.fit()
        pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions

        n_components = 2
        perplexity = 30
        tsne = manifold.TSNE(n_components=n_components, init='random',
                            random_state=0, perplexity=perplexity, n_iter=400)
        # Y = tsne.fit_transform(all_data)
        # Y1 = tsne.fit_transform(pca)
        Y2 = tsne.fit_transform(all_data)
        
        # new_colors = []
        # red_colors = []
        # new_pca = []
        # red_pca = []
        # black_pca = []
        # black_colors = []
        # for i, (s, c) in enumerate(zip(selected, colors)):
        #     if s == 1:
        #         red_colors.append(sns.xkcd_rgb['red'])
        #         red_pca.append(pca[i,:])        
        #     elif s == 2:
        #         black_colors.append(sns.xkcd_rgb['black'])
        #         black_pca.append(pca[i,:])      
        #     else:
        #         new_colors.append(c)
        #         new_pca.append(pca[i,:])      
        # new_pca.extend(red_pca)
        # new_colors.extend(red_colors)
        # new_pca.extend(black_pca)
        # new_colors.extend(black_colors)
        # colours = [sns.xkcd_rgb['denim blue'], sns.xkcd_rgb['lime'], sns.xkcd_rgb['ochre'], sns.xkcd_rgb['hot pink'], sns.xkcd_rgb['red'], sns.xkcd_rgb['black']]
        # handlelist = [plt.plot([], marker="o", ls="", color=color)[0] for color in colours]

        # fig = plt.figure(1, figsize=(8, 6))
        # plt.scatter(Y1[ind1, 0], Y1[ind1,1], s=10, c='c')
        # plt.scatter(Y1[ind2, 0], Y1[ind2,1], s=10, c='b')
        # plt.scatter(Y1[ind3, 0], Y1[ind3,1], s=10, c='g')
        # plt.scatter(Y1[ind4, 0], Y1[ind4,1], s=10, c='m')
        # plt.scatter(Y1[ind5, 0], Y1[ind5,1], s=10, c='r')
        # plt.scatter(Y1[ind6, 0], Y1[ind6,1], s=10, c='k')
        # plt.legend(['Down', 'Up', 'Flat', 'Balance', 'Switched', 'Died'])
        # plt.savefig(self.PATH + name + '_tsne_pca_fig.png', bbox_inches='tight')
        # plt.close(fig)

        fig = plt.figure(1, figsize=(8, 6))
        plt.scatter(Y2[ind1, 0], Y2[ind1,1], s=10, c='c')
        plt.scatter(Y2[ind2, 0], Y2[ind2,1], s=10, c='b')
        plt.scatter(Y2[ind3, 0], Y2[ind3,1], s=10, c='g')
        plt.scatter(Y2[ind4, 0], Y2[ind4,1], s=10, c='m')
        plt.scatter(Y2[ind5, 0], Y2[ind5,1], s=10, c='r')
        plt.scatter(Y2[ind6, 0], Y2[ind6,1], s=10, c='k')
        plt.legend(['Down', 'Up', 'Flat', 'Balance', 'Switched', 'Died'])

        plt.savefig(self.PATH + name + '_ tsne_fig.png', bbox_inches='tight')
        plt.close(fig)
     

        # all_data = np.array(data[key
        # tf_pca = TF_PCA(np.array(data[key][0]), np.array(data[key][1]))

        # tf_pca = TF_PCA(all_data, all_labels)
        # tf_pca.fit()
        # pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions
        # pca = tf_pca.reduce(n_dimensions=2)
        # return pca
        # color_mapping1 = {0: sns.xkcd_rgb['bright purple'], 1: sns.xkcd_rgb['lime'], 2: sns.xkcd_rgb['ochre']}
        
        # if key == 0:
        #     color_mapping = {0: sns.xkcd_rgb['pale red'], 1: sns.xkcd_rgb['lime'], 2: sns.xkcd_rgb['ochre']}
        # elif key == 1:
        #     color_mapping = {0: sns.xkcd_rgb['medium green'], 1: sns.xkcd_rgb['lime'], 2: sns.xkcd_rgb['ochre']}
        # elif key == 2:
            # color_mapping = {0: sns.xkcd_rgb['denim blue'], 1: sns.xkcd_rgb['lime'], 2: sns.xkcd_rgb['ochre']}
        # elif key == 3:

        # tf_pca = TF_PCA(all_data, all_labels)
        # tf_pca.fit()
        # pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions

        color_mapping = {0: sns.xkcd_rgb['denim blue'], 1: sns.xkcd_rgb['lime'], 2: sns.xkcd_rgb['ochre'], 3: sns.xkcd_rgb['hot pink']}
        colors = list(map(lambda x: color_mapping[x], tf_pca.target))
        
        # new_colors = []
        # red_colors = []
        # new_pca = []
        # red_pca = []
        # black_pca = []
        # black_colors = []
        # for i, (s, c) in enumerate(zip(selected, colors)):
        #     if s == 1:
        #         red_colors.append(sns.xkcd_rgb['red'])
        #         red_pca.append(pca[i,:])        
        #     elif s == 2:
        #         black_colors.append(sns.xkcd_rgb['black'])
        #         black_pca.append(pca[i,:])      
        #     else:
        #         new_colors.append(c)
        #         new_pca.append(pca[i,:])      
        # new_pca.extend(red_pca)
        # new_colors.extend(red_colors)
        # new_pca.extend(black_pca)
        # new_colors.extend(black_colors)
        # # colors[selected]
        fig = plt.figure(1, figsize=(8, 6))

        # ind1 = np.where(all_labels == 1)
        # ind2 = np.where(all_labels == 0)
        # ind2 = np.where(all_labels == 0)
        # ind2 = np.where(all_labels == 0)
        # print(ind1)
        # print(ind2)
        for i in range(min(len(pca[0,:]), 4)):
            m1, m2, m3, m4 = np.mean(pca[:,i][ind1]), np.mean(pca[:,i][ind2]), np.mean(pca[:,i][ind3]), np.mean(pca[:,i][ind4])
            s1, s2, s3, s4 = np.std(pca[:,i][ind1]), np.std(pca[:,i][ind2]), np.std(pca[:,i][ind3]), np.std(pca[:,i][ind4])
            # print(i,m1, m2, m3, m4 )
            # print("  ", s1, s2, s3, s4 )


        # ax = Axes3D(fig, elev=-150, azim=110)
        # ax = Axes3D(fig)
        # select = np.array([p for s,p in zip(selected,pca) if s == 1])
        # non_select = np.array([p for s,p in zip(selected,pca) if s == 0])
        # print(selected.shape, pca.shape)
        # ax.scatter(pca[:, 0], pca[:, 1], pca[:,2], s=25, c=colors)
        # ax.scatter(pca[:, 0], pca[:, 1], pca[:,2], s=50, c=colors)
        # ax.scatter(pca[:, 0], pca[:, 1], s=50, c=colors)
        # print(pca.shape)
        # plt.scatter(np.array(new_pca)[:, 0], np.array(new_pca)[:, 1], s=10, c=new_colors)
        # for p in pca:
        # print(tf_pca.target.shape)
        
        # fig = plt.figure(1, figsize=(8, 6))    
        # print(pca)
        # pca = np.array(pca)
        # print(pca.shape)
        # plt.scatter(pca[:, 0], pca[:,1], s=10, c='c')
        # plt.scatter(pca[ind2, 0], pca[ind2,1], s=10, c='b')
        # fig = plt.figure(1, figsize=(8, 6))

        plt.scatter(pca[ind1, 0], pca[ind1,1], s=10, c='c')
        plt.scatter(pca[ind2, 0], pca[ind2,1], s=10, c='b')
        plt.scatter(pca[ind3, 0], pca[ind3,1], s=10, c='g')
        plt.scatter(pca[ind4, 0], pca[ind4,1], s=10, c='m')
        plt.scatter(pca[ind5, 0], pca[ind5,1], s=10, c='r')
        plt.scatter(pca[ind6, 0], pca[ind6,1], s=10, c='k')
        plt.legend(['Down', 'Up', 'Flat', 'Balance', 'Switched', 'Died'])
        plt.savefig(self.PATH + name + '_pca_fig.png', bbox_inches='tight')
        plt.close(fig)

        max_step = max(env_steps)
        # print(env_steps)
        # print(max_step)
        # num_bins = 50
        # num_in_bins = int(max_step/num_bins)
        num_in_bins = 60
        env_steps = np.array(env_steps)
        indicies = []
        # for i in range(0,max_step, num_in_bins):
        # print(int(max_step//num_in_bins))
        for i in range(0,int(max_step//num_in_bins)):
            # print(i)
            # print(i*num_in_bins,i*num_in_bins+num_in_bins)
            indicies.append(np.where((env_steps>(i*num_in_bins) ) & (env_steps<(i*num_in_bins+num_in_bins))))
            # env_steps[i*num_in_bins:i*num_in_bins+num_in_bins]
        # all_labels
        # 4 classes, 2 bins
        c1 = 0,1,18,19
        c2 = 2,3,16,17
        # print(tableau20[0], sns.xkcd_rgb['red'])
        new_colours = {c:{b:tableau20[c*b] for b in [0,1]} for c in [0,1,2,3]}
        colours = []
        killed_colour = []
        killed_pca = []
        select_colour = []
        select_pca = []
        all_pca = []
        for i, (c, p, s) in enumerate(zip(all_labels, env_steps, selected)):
            if s == 1:
                # colours.append(tableau20[6])
                select_colour.append(tableau20[6])
                select_pca.append(pca[i, :])
                continue
            if s == 2:
                killed_colour.append(tableau20[6])
                killed_pca.append(pca[i, :])
                # colours.append(tableau20[14])
                continue
            if p%120 >= 90:
                if c == 0: 
                    colours.append(tableau20[0])
                elif c == 1:
                    colours.append(tableau20[2])
            elif p%120 >= 60 and p%120 < 90:
                if c == 0: 
                    colours.append(tableau20[1])
                elif c == 1:
                    colours.append(tableau20[3])
            elif p%120 >= 30 and p%120 < 60:
                if c == 0: 
                    colours.append(tableau20[18])
                elif c == 1:
                    colours.append(tableau20[16])
            elif p%120 < 30:
                if c == 0: 
                    colours.append(tableau20[19])
                elif c == 1:
                    colours.append(tableau20[17])
            all_pca.append(pca[i,:])
        all_pca.extend(select_pca)
        all_pca.extend(killed_pca)
        colours.extend(select_colour)
        colours.extend(killed_colour)
        # print(colours)
        # print(indicies)
        # fig = plt.figure(1, figsize=(8, 6))
        # cs = ['r','b','c','m','g','k','y']
        # # for idx, c in zip(indicies,tableau20):
        # plt.scatter(Y2[:, 0], Y2[:,1], s=10, c=colours)
        # # plt.legend(['Down', 'Up', 'Flat', 'Balance', 'Switched', 'Died'])
        # plt.savefig(self.PATH + name + '_steps_tsne.png', bbox_inches='tight')
        # plt.close(fig)
        all_pca = np.array(all_pca)
        fig = plt.figure(1, figsize=(8, 6))
        # cs = ['r','b','c','m','g','k','y']
        # for idx, c in zip(indicies,tableau20):
        plt.scatter(all_pca[:, 0], all_pca[:,1], s=10, c=colours)
        # plt.legend(['Down', 'Up', 'Flat', 'Balance', 'Switched', 'Died'])
        plt.savefig(self.PATH + name + '_steps_pca.png', bbox_inches='tight')
        plt.close(fig)
        
        # plt.scatter(np.array(new_pca)[:, 0], np.array(new_pca)[:, 1], s=10, c=new_colors)

        # plt.legend(handlelist,['Down', 'Up', 'Flat', 'Balance', 'Switched', 'Died'])
        # plt.legend('Down', 'Up', 'Switched')
        # plt.scatter(pca[:, 0], pca[:, 1], s=25, c=sns.xkcd_rgb['denim blue'])
        # hold.on()
        # plt.show()
        # plt.savefig(PATH + 'pca_fig.png', bbox_inches='tight')
        # plt.close(fig)


class TF_PCA():
    def __init__(self, data, X=None):
        # self.X = X
        self.data = data
        self.target = X
        self.dtype = tf.float32

    def fit(self):
        # self.graph = tf.Graph()
        # with self.graph.as_default():
        self.X = tf.placeholder(self.dtype, shape=self.data.shape)
        # Perform SVD
        singular_values, u, _ = tf.svd(self.X)
        # Create sigma matrix
        sigma = tf.diag(singular_values)
        # with tf.Session(graph=self.graph) as session:
        session =  tf.get_default_session()
        self.u, self.singular_values, self.sigma = session.run([u, singular_values, sigma],
                                                                feed_dict={self.X: self.data})
    def reduce(self, n_dimensions=None, keep_info=None):
        if keep_info:
            # Normalize singular values
            normalized_singular_values = self.singular_values / sum(self.singular_values)
            # Create the aggregated ladder of kept information per dimension
            ladder = np.cumsum(normalized_singular_values)
            # Get the first index which is above the given information threshold
            index = next(idx for idx, value in enumerate(ladder) if value >= keep_info) + 1
            n_dimensions = index
        # with self.graph.as_default():
        # Cut out the relevant part from sigma
        sigma = tf.slice(self.sigma, [0, 0], [self.data.shape[1], n_dimensions])
        # PCA
        pca = tf.matmul(self.u, sigma)
        session = tf.get_default_session()
        # with tf.Session(graph=self.graph) as session:
        return session.run(pca, feed_dict={self.X: self.data})


def individ_select_pca_plot():  
    '''
    single pca
    '''
    import gc
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.80)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
    select = np.load('/home/brendan/results/elements/select_doa/embed_select.npy')
    not_select = np.load('/home/brendan/results/elements/select_doa/embed_not_select.npy')
    
    print(select.shape, not_select.shape)
    not_select_data = np.concatenate([not_select[0], not_select[1], not_select[2], not_select[3]])
    labels1 = [i for i in range(4) for _ in range(not_select.shape[1])]
    data1 = np.concatenate([not_select_data, select[0]])
    # print(data1.shape)
    labels1.extend(4 for _ in range(select.shape[1]))
    pca = []
    colors = []
    for i in range(5):
        if i == 4:
            tf_pca = TF_PCA(np.array(select[0]))
            data_len = select.shape[1]
        else:
            tf_pca = TF_PCA(np.array(not_select[i]))
            data_len = not_select.shape[1]
        tf_pca.fit()
        pca.append(tf_pca.reduce(keep_info=0.9))
        colors.extend([tableau20[i*2] for _ in range(data_len)])

    # pca = np.array(pca)
    # print(pca.shape)
    # color_mapping = {0: tableau20[0], 1: tableau20[2], 2: tableau20[4], 3: tableau20[6], 4: tableau20[8]}
    # color_mapping = {2: tableau20[0], 4: tableau20[2]}
    # colors = list(map(lambda x: color_mapping[x], tf_pca.target))
    fig = plt.figure(1, figsize=(8, 6))       
    plt.scatter(pca[0][:, 0], pca[0][:,1], s=10,  c=tableau20[0])
    plt.scatter(pca[1][:, 0], pca[1][:,1], s=10,  c=tableau20[2])
    plt.scatter(pca[2][:, 0], pca[2][:,1], s=10,  c=tableau20[4])
    plt.scatter(pca[3][:, 0], pca[3][:,1], s=10,  c=tableau20[6])
    plt.scatter(pca[4][:, 0], pca[4][:,1], s=10,  c=tableau20[8])
    # plt.scatter(pca[g, 0], pca[g,1], s=10, c=tableau20[14])
    # plt.legend(['Down', 'Up', 'Flat', 'Balance'])
    plt.savefig('/home/brendan/results/elements/select_doa/' + 'sep.png', bbox_inches='tight')
    plt.close(fig) 

def select_pca_plot():  
    '''
    Grouped pca
    '''
    import gc
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.80)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
    flat = np.load('/home/brendan/results/elements/flats3/embed_flat.npy')
    flat_alt = np.load('/home/brendan/results/elements/flats3/embed_flat_alt.npy')
    print(flat.shape, flat_alt.shape)
    data1 = np.concatenate([flat_alt[2], flat[2]])
    labels1 = [4 for _ in range(flat_alt.shape[1])] + [2 for _ in range(flat.shape[1])]
    # data1 = np.concatenate([flat[2], flat_alt[2]])
    # labels1 = [2 for _ in range(flat.shape[1])] + [4 for _ in range(flat_alt.shape[1])]

    # select = np.load('/home/brendan/results/elements/select_doa/embed_select.npy')
    # not_select = np.load('/home/brendan/results/elements/select_doa/embed_not_select.npy')
    # print(select.shape, not_select.shape)
    # not_select_data = np.concatenate([not_select[0], not_select[1], not_select[2], not_select[3]])
    # labels1 = [i for i in range(4) for _ in range(not_select.shape[1])]
    # data1 = np.concatenate([not_select_data, select[0]])
    # print(data1.shape)
    # labels1.extend(4 for _ in range(select.shape[1]))

    tf_pca = TF_PCA(np.array(data1), np.array(labels1))
    tf_pca.fit()
    pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions
    color_mapping = {0: tableau20[0], 1: tableau20[2], 2: tableau20[4], 3: tableau20[6], 4: tableau20[8]}
    # color_mapping = {2: tableau20[0], 4: tableau20[2]}
    colors = list(map(lambda x: color_mapping[x], tf_pca.target))
    fig = plt.figure(1, figsize=(8, 6))       
    plt.scatter(pca[:, 0], pca[:,1], s=10,  c=colors)
    # plt.scatter(pca[g, 0], pca[g,1], s=10, c=tableau20[14])
    # plt.legend(['Down', 'Up', 'Flat', 'Balance'])
    plt.savefig('/home/brendan/results/elements/flats3/' + 'both2.png', bbox_inches='tight')
    # plt.savefig('/home/brendan/results/elements/select_doa/' + 'both2.png', bbox_inches='tight')
    plt.close(fig) 

def multi_pca_plot():  
    import gc
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.80)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
    obs2 = np.load('/home/brendan/results/elements/flats2/obs2.npy')
    ims2 = np.load('/home/brendan/results/elements/flats2/ims2.npy')
    obs4 = np.load('/home/brendan/results/elements/flats2/obs4.npy')
    ims4 = np.load('/home/brendan/results/elements/flats2/ims4.npy')
    # print(obs2.shape, ims2.shape, obs4.shape, ims4.shape)
    # data1 = np.concatenate([obs4, obs2],axis=0)

    vis_input2 =  np.reshape(ims2, (-1, 48*48*4))
    input2 = np.concatenate([obs2[:6000,:],vis_input2[:6000,:]],axis=1)
    ims2, obs2 = None, None
    gc.collect()
    vis_input4 =  np.reshape(ims4, (-1, 48*48*4))
    input4 = np.concatenate([obs4[:6000,:],vis_input4[:6000,:]],axis=1)
    ims4, obs4 = None, None
    gc.collect()
    # vis_input4 =  np.reshape(ims4, (-1, 48*48*4))
    # print(obs4.shape, vis_input4.shape)
    # input4 = np.concatenate([obs4[:12000,:],vis_input4[:12000,:]], axis=1)

    # print(input1.shape)
    # data1 = input2
    data1 = np.concatenate([input4, input2],axis=0)
    # data2 = np.concatenate([embed2[2], embed4[2]], axis=0)
    # labels1 = [4 for _ in range(obs4.shape[0])]+[2 for _ in range(obs2.shape[0])]
    # labels1 = [2 for _ in range(6000)] + [4 for _ in range(6000)]
    labels1 = [4 for _ in range(6000)] + [2 for _ in range(6000)]
    # labels1 = [2 for _ in range(obs2.shape[0])]+[4 for _ in range(obs4.shape[0])]
    # labels2 = [2 for _ in range(embed2[1].shape[0])]+[4 for _ in range(embed4[1].shape[0])]


    # actions2 = np.load('/home/brendan/results/elements/flats2/action2.npy')
    # actions4 = np.load('/home/brendan/results/elements/flats2/action4.npy')
    # embed2 = np.load('/home/brendan/results/elements/flats2/embed2.npy')
    # embed4 = np.load('/home/brendan/results/elements/flats2/embed4.npy')
    # print(actions2.shape, actions4.shape)
    # print(embed2[2].shape, embed4[2].shape)
    # data1 = np.concatenate([actions2, actions4],axis=0)
    # data1 = np.concatenate([actions4, actions2],axis=0)
    # data2 = np.concatenate([embed2[2], embed4[2]], axis=0)
    # labels1 = [4 for _ in range(actions4.shape[0])]+[2 for _ in range(actions2.shape[0])]
    # labels2 = [2 for _ in range(embed2[1].shape[0])]+[4 for _ in range(embed4[1].shape[0])]


    tf_pca = TF_PCA(np.array(data1), np.array(labels1))
    tf_pca.fit()
    pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions
    # color_mapping = {0: tableau20[0], 1: tableau20[2], 2: tableau20[4], 3: tableau20[6]}
    color_mapping = {2: tableau20[0], 4: tableau20[2]}
    colors = list(map(lambda x: color_mapping[x], tf_pca.target))
    fig = plt.figure(1, figsize=(8, 6))       
    plt.scatter(pca[:, 0], pca[:,1], s=10,  c=colors)
    # plt.scatter(pca[g, 0], pca[g,1], s=10, c=tableau20[14])
    # plt.legend(['Down', 'Up', 'Flat', 'Balance'])
    plt.savefig('/home/brendan/results/elements/flats2/' + 'both2.png', bbox_inches='tight')
    plt.close(fig) 

    # thres = 0.2
    # all_data = []
    # all_labels = []
    # all_colours = []
    # greys = []
    # _, _, data2 = pols['doa'].step(data_multi['obs'], data_multi['ims'], select=0, multi=True)
    # print("actions")
    # for i in [2,4]:
    #     data2, _, _, _, _ = pols['sp'][i].step(data_multi['obs'], data_multi['ims'], multi=True)
    #     all_data.extend(data2)
    #     all_labels.extend([i for _ in range(len(data_multi['obs']))])

    #     cidx = i*2
    #     # all_labels.extend([s for s in data_multi['selects']])
    #     all_colours.extend([tableau20[cidx] for _ in range(len(data_multi['obs']))])
    # # greys.append([idx*i for idx, d in enumerate(data_multi['sp_doas']) if d[i] > thres])
    # # print(len(data_multi['sp_doas']))
    # # print(greys)
    # # print(np.array(all_labels).shape)


    # # plt.scatter(pca[grey, 0], pca[grey,1], s=10, c=tableau20[14])
    # # color_mapping = {0: tableau20[0], 1: tableau20[2], 2: tableau20[4], 3: tableau20[6]}
    # # colors = list(map(lambda x: color_mapping[x], tf_pca.target))
    # # for i,g in enumerate(greys):
    # fig = plt.figure(1, figsize=(8, 6))       
    # # plt.scatter(pca[g, 0], pca[g,1], s=10, c=tableau20[14])
    # # plt.legend(['Down', 'Up', 'Flat', 'Balance'])
    # plt.savefig(self.PATH + 'test' + str(i) + '.png', bbox_inches='tight')
    # plt.close(fig) 


def doa_do_plot(params, PATH, act_idx, args=None):
    xs = [i for i in range(len(params['term']))]
    # print(len(params['term']), len(params['vx']))
    # f, axes = plt.subplots(3,figsize=(15, 20))
    f, axes = plt.subplots(5,figsize=(15, 15))
    c = ['g','b','r','c','m','k','y']

    # v1 = [v[0] for v in params['vpred']]
    # v2 = [v[1] for v in params['vpred']]
    axes[0].plot(xs,params['vpred'],c[0])

    # for op in range(len(params['vpred'][0])):
    #     axes[0].plot(xs,[v[op] for v in params['vpred']],c[op])
    # axes[0].set_title("Value function")
    # axes[0].legend([str(op) for op in range(args.opt)])
    # q1 = [q[0] for q in params['pol_vpred']]
    # q2 = [q[1] for q in params['pol_vpred']]
    # axes[1].plot(xs,q1,'g',xs,q2,'b')
    # axes[1].legend(['down','up'], loc="upper right")
    # axes[1].set_title("Sub-policy values")
    axes[1].plot(xs,params['option'],color=tableau20[0])
    axes[1].set_title("Policy selection")
    # axes[0].legend(['down = 0 | up = 1'], loc="upper left")
    axes[1].legend(['down = 0 | flat = 1 | up = 2'], loc="upper left")
    axes[1].set_ylim([-0.1, 2.3])
    # axes[3].set_title("MSE between policies")
    # axes[3].plot(xs,params['ac_mse'],color=tableau20[0])
    # axes[3].set_ylim([0.0, 0.8])
    axes[2].plot(xs,params['term'],color=tableau20[5])
    axes[2].plot(xs,params['vx'],color=tableau20[0])
    axes[2].plot(xs,params['des_vx'],color='r')
    axes[2].set_ylim([0.0, 2.5])
    axes[2].legend(['policy', 'actual', 'desired'], loc="upper right")
    axes[2].set_title("Speed of current policy")
    # ts = [params['z'][0]-1]
    # val = params['z'][0]-1
    # for i in terrain[act_idx]:
    #     if i == 'up':
    #         val += 0.1
    #     elif i == 'down':
    #         val -= 0.1
    #     ts.append(val)
    # ts.append(val)
    # step_nb = (2 + len(params['term'])//50)
    # x_terrain = range(0,(step_nb)*len(params['term']), len(params['term'])//(step_nb-1))
    # axes[3].plot(xs,params['z'],'c', xs, params['foot_x'], 'r', xs,params['vx'],'b', x_terrain[:step_nb],ts[:step_nb],'g', drawstyle='steps-post')
    # axes[3].legend(["z height", "foot x", "vx", "step"])
    # axes[3].plot(xs,params['z'],'c', xs, params['foot_x'], 'r', x_terrain[:step_nb],ts[:step_nb],'g', drawstyle='steps-post')
    # axes[3].plot(xs,params['z'],'c', xs, params['foot_x'], 'r', xs,params['ground'],'g', drawstyle='steps-post')
    axes[3].plot(xs[1:],params['z'][1:],'c', xs[1:], params['left_foot_x'][1:],'r', xs[1:], params['right_foot_x'][1:], 'm')
    axes[3].plot(xs[1:],params['ground'][1:],color=tableau20[4], drawstyle='steps-post')
    axes[3].legend(["z height", "left foot", "right foot", "step"])
    axes[3].set_title("Terrain (roughly)")
    axes[4].plot(xs, params['doa_pred'], color=tableau20[0])
    axes[4].plot(xs, params['doa'], color=tableau20[7])
    axes[4].set_ylim([-0.1, 1.1])
    axes[4].legend(["doa_pred", "pushed out of doa"])
    axes[4].set_title("DoA for policy")
    for x_val in params['disturbances']:
        axes[4].axvline(x=x_val, color=tableau20[4], linestyle='--')
    # axes[5].plot(xs,params['kl'],'b')

    # for i,ax in enumerate(axes):
    #     # if i == : continue
    #     # for start, end in zip(count['start'], count['end']):
    #     for x_val in params['change']:
    #         ax.axvline(x=x_val, color=tableau20[4], linestyle='--')
    #         # ax.axvline(x=end, color='r', linestyle='--')

    plt.savefig(PATH + 'fig' + str(act_idx) + '.png', bbox_inches='tight')
    plt.close(f)

def phase_do_plot(params, PATH, act_idx, args=None):
    f = plt.figure(1)
    ax = f.gca()
    # xs = len(params['x_'])
    doa_idx = np.where(np.array(params['doa_']) >= 0.1)
    not_doa_idx = np.where(np.array(params['doa_']) < 0.1)
    # print(doa_idx)
    # print(not_doa_idx)
    np_x = np.array(params['x_'])
    np_vx = np.array(params['vx_'])
    ax.scatter(np_x[doa_idx], np_vx[doa_idx], s=2, c='g')
    ax.scatter(np_x[not_doa_idx], np_vx[not_doa_idx], s=2, c='r')
    # ax.legend(["doa_pred", "pushed out of doa"])
    ax.set_title("DoA for policy")

    plt.savefig(PATH + 'doa' + str(act_idx) + '.png', bbox_inches='tight')
    plt.close(f)

def see_colours():
    # PATH = '/home/brendan/results/plots/'
    PATH = '/home/brendan/results/plots/corl/'
    x = np.linspace(1,60,60)
    i = 1
    for c in tableau20:
        y = [i for _ in range(len(x))]
        plt.plot(x,y,color=c)
        i += 1
    plt.savefig(PATH + 'colours.png', bbox_inches='tight')

def plot_sp():
    # params, PATH, terrain, act_idx
    # PATH = '/home/brendan/results/sp/'
    PATH = '/home/brendan/hpc-home/results/select/'
    FOLDER_PATH = 'both_dude/params3.pkl'
    # FOLDER_PATH = 'vis1/params11.pkl'
    # FOLDER_PATH = 'new_seq/select_boot/params5.pkl'
    # FOLDER_PATH = 'new_seq/vel/params7.pkl'
    # FOLDER_PATH = 'new_seq/vis/params6.pkl'
    # FOLDER_PATH = 'baseline2/params1.pkl'
    params = load_dict(PATH + FOLDER_PATH)
    xs = [i for i in range(len(params['term']))]
    # print(len(params['term']), len(params['vx']))
    # f, axes = plt.subplots(3,figsize=(15, 20))
    f, axes = plt.subplots(3,figsize=(15, 15))
    c = ['g','b','r','c','m','k','y']

    # v1 = [v[0] for v in params['vpred']]
    # v2 = [v[1] for v in params['vpred']]
    # axes[0].plot(xs,params['vpred'],c[0])

    # for op in range(len(params['vpred'][0])):
    #     axes[0].plot(xs,[v[op] for v in params['vpred']],c[op])
    # axes[0].set_title("Value function")
    # axes[0].legend([str(op) for op in range(args.opt)])
    # q1 = [q[0] for q in params['pol_vpred']]
    # q2 = [q[1] for q in params['pol_vpred']]
    # axes[1].plot(xs,q1,'g',xs,q2,'b')
    # axes[1].legend(['down','up'], loc="upper right")
    # axes[1].set_title("Sub-policy values")
    axes[0].plot(xs,params['option'],color=tableau20[0])
    axes[0].set_title("Policy")
    axes[0].legend(['down = 0 | up = 1 | flat = 2'], loc="upper right")
    # axes[0].ylabel("Policy")
    axes[1].plot(xs,params['term'],color=tableau20[5])
    axes[1].plot(xs,params['vx'],color=tableau20[0])
    # axes[1].ylabel("Velocity (m/s)")
    axes[1].plot(xs,params['des_vx'],color='r')
    axes[1].legend(['policy', 'actual', 'desired'], loc="upper right")
    axes[1].set_title("Velocity (m/s)")
    # ts = [params['z'][0]-1]
    # val = params['z'][0]-1
    # for i in terrain[act_idx]:
    #     if i == 'up':
    #         val += 0.08
    #     elif i == 'down':
    #         val -= 0.08
    #     ts.append(val)
    # ts.append(val)
    # step_nb = (2 + len(params['term'])//50)
    # x_terrain = range(0,(step_nb)*len(params['term']), len(params['term'])//(step_nb-1))
    # axes[3].plot(xs,params['z'],'c', xs, params['foot_x'], 'r', xs,params['vx'],'b', x_terrain[:step_nb],ts[:step_nb],'g', drawstyle='steps-post')
    # axes[3].legend(["z height", "foot x", "vx", "step"])
    # axes[3].plot(xs,params['z'],'c', xs, params['foot_x'], 'r', x_terrain[:step_nb],ts[:step_nb],'g', drawstyle='steps-post')
    # axes[3].plot(xs,params['z'],'c', xs, params['foot_x'], 'r', xs,params['ground'],'g', drawstyle='steps-post')
    axes[2].plot(xs[1:],params['z'][1:],'c', xs[1:], params['left_foot_x'][1:],'r', xs[1:], params['right_foot_x'][1:], 'm')
    axes[2].plot(xs[1:],params['ground'][1:],color=tableau20[4], drawstyle='steps-post')
    axes[2].legend(["z height", "left foot", "right foot", "step"])
    axes[2].set_title("Height (m)")
    # axes[5].plot(xs,params['kl'],'b')
    SAVE_PATH = '/home/brendan/results/plots/'
    plt.savefig(SAVE_PATH + 'fig.png', bbox_inches='tight')
    plt.close(f)

def plot_errors():

    # PATH = '/home/brendan/results/sp/'
    LOAD_PATH = '/home/brendan/hpc-home/results/elements/'
    # LOAD_PATH = '/home/brendan/hpc-home/results/select/'
    
    SAVE_PATH = '/home/brendan/results/plots/corl/'
    # SAVE_PATH = '/home/brendan/results/Dropbox/corl/'
    locations = ['select']
    # locations = ['vel'] 
    SAME_LENGTH = True
    # SAME_LENGTH = False
    leg_pos = 'best'
    # Edit these things.. 
    # Main one!!
    # -----------------------------------------------------------------------
    # # Doa
    # groups = [['doa','doa1']] 
    # items = [] # In legend
    # name = 'doa'
    # -----------------------------------------------------------------------
    # # Vel methods
    # groups = [['traffic','traffic1'],['traffic_no_vis','traffic_no_vis1'],['nav','nav1'],['nav_no_vis','nav_no_vis1'],['walls','walls1']] 
    # items = ['Traffic Lights','Traffic Lights no Vision' , 'Waypoints', 'Waypoints no vision', 'Avoid Obstacles'] # In legend
    # name = 'Velocity tasks'
    # -----------------------------------------------------------------------
    # # walls
    # groups = [['walls','walls1']] 
    # items = [] # In legend
    # name = 'walls'
    # -----------------------------------------------------------------------
    # nav
    # groups = [['nav','nav1'],['nav_no_vis','nav_no_vis1']] 
    # items = ['Waypoints', 'Waypoints no vision'] # In legend
    # name = 'nav'
    # -----------------------------------------------------------------------
    # # Traffic
    # groups = [['traffic','traffic1'],['traffic_no_vis','traffic_no_vis1']] 
    # items = ['Traffic Lights','Traffic Lights no Vision'] # In legend
    # name = 'traffic'
    # # -----------------------------------------------------------------------
    # Select methods
    # groups = [['dqn','dqn1'], ['hvc'], ['mcp', 'mcp1']] 
    # items = ['Discrete', 'Length = 5', 'Weighted Sum'] # In legend
    # name = 'Select Methods With Steering'
    # leg_pos = 'lower right'
    # # -----------------------------------------------------------------------
    # Select methods
    groups = [['a2/select_mcp_add', 'a2/select_mcp_add1'], ['a3/doa_select_mcp_add', 'a3/doa_select_mcp_add1']] 
    items = [] # In legend
    name = 'mcp_doa_compare'
    colours_I_like = [8,14] #blue 0, orange 2, green 4, red 6, purple 8?  
    ylimit = [0,2200]
    xlimit = [0,1.7e7]

    # # -----------------------------------------------------------------------
    # # Select methods
    # groups = [['a2/select_hvc', 'a2/select_hvc1'], ['a3/doa_select_hvc', 'a3/doa_select_hvc1']] 
    # items = [] # In legend
    # name = 'hvc_doa_compare'
    # colours_I_like = [0,18] #blue 0, orange 2, green 4, red 6, purple 8?  
    # ylimit = [0,2200]
    # xlimit = [0,1.7e7]

    # # -----------------------------------------------------------------------    
    # # Hvc
    # groups = [['select_no_term','select_no_term1'], ['select_hvc_5', 'select_hvc1_5'],['select_hvc','select_hvc1']] 
    # items = ['Length = 1', 'Length = 5', 'Length = 10'] # In legend
    # name = 'Fixed length policy selection'
    # -----------------------------------------------------------------------
    # # Select
    # groups = [['select','select1','select_dc','select_dc1'], ['select_no_term', 'select_no_term1'],['select_hvc','select_hvc1'],['select_mcp_add','select_mcp_add1'],['a1/select_mcp', 'a1/select_mcp1'],['e2e', 'e2e1'],['doa','doa1']] 
    # items = ['Discrete','Discrete without switching', 'Fixed length', 'Weighted Sum', 'Weighted Exponent', 'Single policy', 'DoA estimate'] # In legend
    # name = 'select'
    # -----------------------------------------------------------------------\
    # # Select with Doa
    # group1 = [['select','select1','select_dc','select_dc1'], ['select_hvc','select_hvc1'],['a1/select_mcp_add','a1/select_mcp_add1']] 
    # items1 = ['Discrete', 'Fixed length', 'Weighted Sum'] # In legend
    # # name = 'select'
    # group2 = [['a1/doa_select','a1/doa_select1'],['a1/doa_select_hvc','a1/doa_select_hvc1'],['a1/doa_select_mcp_add','a1/doa_select_mcp_add1']] 
    # items2 = ['Discrete with DoA', 'Fixed length with DoA', 'Weighted Sum with DoA'] # In legend
    # name = 'select_with_doa'
    # groups = group1 + group2
    # items = items1 + items2
    # -----------------------------------------------------------------------

    xlabel, ylabel = "Time steps", "Reward"
    # colours_I_like = [0,2,4,6,8,10,1,3,5,7,9,12,13] #blue 0, orange 2, green 4, red 6, purple 8?  
    data = []
    for location in locations:
        for individual in groups:
            ds = []
            for k in individual:
                # print(k)
                ds.append(np.load(LOAD_PATH + location + '/' + k + '/rewards.npy'))
            l = min([len(d) for d in ds])
            data.append([d[:l] for d in ds])
    if SAME_LENGTH:
        length = min([len(d) for ds in data for d in ds])
        print(length)
        # length = 300
        new_data = [np.mean(d, axis=0)[:length] for d in data]
        new_error = [np.std(d, axis=0)[:length] for d in data]
    else:
        length = max([len(d) for ds in data for d in ds])
        print(length) 
        new_data = [np.mean(d, axis=0) for d in data]
        new_error = [np.std(d, axis=0) for d in data]

    # steps = [i*2048*16 for i in range(length)]
    # light_colours_I_like = [0,4,6,8]
    for d,e,c in zip(new_data, new_error, colours_I_like):
        steps = [i*2048*16 for i in range(len(d))]
        plt.plot(steps,d,color=tableau20[c])
        plt.fill_between(steps, d-e, d+e, facecolor=tableau20[c+1], alpha=0.6)
    plt.legend(items, loc=leg_pos, fancybox=True, framealpha=0.2)
    plt.ylabel(ylabel, fontsize=14)
    plt.xlabel(xlabel, fontsize=14)
    plt.ylim(ylimit)
    if xlimit:
        plt.xlim(xlimit)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.savefig(SAVE_PATH + name + '.png', bbox_inches='tight')

def plot_lr():
    # PATH = '/home/brendan/results/sp/'
    LOAD_PATH = '/home/brendan/hpc-home/results/sp/'
    # LOAD_PATH = '/home/brendan/hpc-home/results/select/'
    SAVE_PATH = '/home/brendan/results/plots/'
    items = ['Symmetry in Reward', 'Sym coeff = 2', 'Sym coeff = 4']
    # items = ['flat', 'up', 'down']
    # items = ['Supervised', 'Supervised and Velocity', 'DQN', 'DQN and Velocity']
    # locations = ['flat_speed_0', 'flat_speed_5', 'flat_speed2', 'flat_speed4']
    locations = ['up','up_speed2', 'up_speed4']
    # locations = ['flat_speed_0', 'down_speed_0', 'up_speed_0']
    # locations = ['flat', 'cur11', 'cur14']
    # locations = ['baseline', 'vel', 'vis', 'both']
    data = []
    for item, location in zip(items, locations):
        data.append(np.load(LOAD_PATH + location + '' + '/rewards.npy'))
    length = min([len(d) for d in data])
    print(length)
    new_data = []
    for d in data:
        new_data.append(d[:length])
    # print(lr1)
    steps = [i*2048*16 for i in range(length)]
    # light_colours_I_like = [0,4,6,8]
    for d, c in zip(new_data, colours_I_like):
        plt.plot(steps,d,color=tableau20[c])
        # plt.plot(steps,lr2,color=tableau20[4])
    plt.legend(items)
    plt.ylabel("Reward", fontsize=14)
    plt.xlabel("Time step", fontsize=14)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.savefig(SAVE_PATH + 'lr.png', bbox_inches='tight')

def plot_speed():
    # PATH = '/home/brendan/results/sp/data/'
    PATH = '/home/brendan/results/sp/plots/'
    for i in range(1,4):
    # params = load_dict(PATH + 'params.pkl')
        params = load_dict(PATH + 'params_' + str(i) + '.pkl')

        NUM_PLOTS = 1
        # x = np.linspace(1,60,60)
        x = [_ for _ in range(len(params['vx']))]
        # print(params)
        plt.plot(x,params['vx'],color=tableau20[4])
        plt.plot(x,params['des_vx'],color=tableau20[0])
        # plt.plot(x,params['vx'],'g',x,params['des_vx'],'b')
        plt.legend(['Velocity','Desired velocity'])
        plt.tick_params(axis="both", which="both", bottom="on", top="off",
                        labelbottom="on", left="on", right="off", labelleft="on")
        plt.ylabel("Velocity (m/s)", fontsize=14)
        plt.xlabel("Time step", fontsize=14)
        # f, axes = plt.subplots(NUM_PLOTS,figsize=(15, 20))
        # # print(b_next_obs.shape, outputs.shape)
        # for i, a in enumerate(axes):
        #     a.plot(x,np.array(b_next_obs)[:60,i],'g',x,outputs[:60,i],'b')
        #     a.legend(['Output','Predictions'])

        # plt.savefig(PATH + 'vel_plot.png', bbox_inches='tight')
        plt.savefig(PATH + 'vel_plot' + str(i) + '.png', bbox_inches='tight')
        plt.close()


def run():
    mpl.use('Qt4Agg')
    import matplotlib.pyplot as plt
    # x = np.linspace(-0.5,0.5,100)
    x = np.linspace(-2.0,2.0,100)
    # x = np.linspace(-2.0,2.0,100)
    # x = np.array([i for i in range(5)])
    # y = 400
    # ys = []
    # print_y = True
    # for i in range(200):
    #     if print_y and y < 5:
    #         print(i)
    #         print_y = False
    #     ys.append(y)
        # y = y*0.999
    # print(x)
    # ys = 1/(np.e**x + 2 + np.e**-x)*(1.5/0.25)
    # ys1 = (1/(np.e**x + np.e**-x))*(1.5/0.5)
    # ys2 = (6/(np.e**(x) + np.e**(-x)))
    ys1 = 0.5*(4.5/(np.e**(3*x) + np.e**(-3*x))) - 0.1   
    ys2 = 0.25*(4.5/(np.e**(5*x) + np.e**(-5*x))) - 0.1   
    ys3 = 0.25*(4.5/(np.e**(7*x) + np.e**(-7*x))) - 0.1   

    # ys2 = (0.5/(np.e**(3*x) + np.e**(-3*x))) 
    # ys3 = (1.0/(np.e**(3*x) + np.e**(-3*x))) 

    # ys3 = (6/(np.e**(3*x) + np.e**(-3*x)))
    # ys3 = (3/(np.e**(5*x) + np.e**(-5*x)))
    # plt.plot(x, ys1, 'm', x, ys2, 'c')
    # plt.plot(x, ys, 'c')
    plt.plot(x, ys1, 'r')
    plt.plot(x, ys2, 'c')
    # ys3 = 1.5 - 4*x**2
    # plt.plot(x, ys3, 'r')
    # ys4 = [max(1.5 - 5*el**2, 0) for el in x]
    plt.plot(x, ys3, 'g')

    x2 = np.linspace(0,2,100)
    y4 = np.e**(-4*x2)
    plt.plot(x2, y4, 'm')
    y5 = np.e**(-2*x2)
    plt.plot(x2, y5, 'b')
    y6 = np.e**(-0.5*x2)
    plt.plot(x2, y6, 'g')

    plt.legend(['x', 'yaw', 'y'])
    plt.show()


if __name__=="__main__":
    # run()
    # see_colours()
    plot_errors()
    # multi_pca_plot()
    # select_pca_plot()
    # individ_select_pca_plot()