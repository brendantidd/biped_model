import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt

plt.switch_backend('agg')
# plt.rcParams.update({'font.size': 14})
plt.rcParams.update({'font.size': 18})
import pybullet as p

def run(args):

  PATH = home + '/results/biped_model/latest/multi/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  # myseed = int(time.time() + 10000 * rank)
  myseed = int(args.seed + 10000 * rank)
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  if args.multi_robots:
    from assets.env_multi import Env
  else:
    from assets.env import Env

  if args.old:
    args.vis_type = 'hm'

  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, vis_type=args.vis_type, doa=args.doa, disturbances=False, multi=args.multi)

  from models.ppo import Model

  # obstacle_types = ['gaps', 'jumps', 'base']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs','turn','zero']
  if args.old:
    obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
  else:
    obstacle_types = ['gaps', 'jumps','stairs','steps','high_jumps', 'zero']
    # obstacle_types = ['flat', 'high_jumps']
  # obstacle_types = ['gaps', 'base', 'jumps','stairs']
  # obstacle_types = ['gaps', 'base', 'jumps']
  # obstacle_types = ['gaps', 'base', 'jumps','zero']

  if args.e2e:
    pol = Model('mix', env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)

  elif args.multi:
    pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis, const_std=args.const_std) for name in obstacle_types if name != 'zero'}  
    if args.use_vf:
      vf = {name:Model(name + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis, const_std=args.const_std) for name in obstacle_types}
    # else:
    vf_orig = {name:Model(name + "_vf_orig", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}
    # if args.use_roa:
  from models.doa2 import RoANet
  if args.vis:
    im_size = [60,40,1]
  else:
    im_size = env.im_size[0]
  # roa = {name:RoANet(name, args=args, ob_size=env.ob_size, im_size=im_size, train_pol=False, no_vis=args.no_vis) for name in ['gaps','jumps','stairs','base','zero']}  
  # roa = {name:RoANet(name, args=args, ob_size=env.ob_size, im_size=im_size, train_pol=False, no_vis=args.no_vis) for name in ['gaps','jumps','stairs','base']}  
  roa = {name:RoANet(name, args=args, ob_size=env.ob_size, train_pol=False) for name in ['gaps','jumps','stairs']}  
  # if args.rl_roa:
  #   roa = {name:Model(name + "_switch", env=env, ob_size=env.ob_size, ac_size=1, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}  
  #   args.thres = 0.511

  if args.dqn:
    from models.dqn_adv import DQN
    dqn = DQN("dqn", env=env, ob_size=env.ob_size+len(obstacle_types), ac_size=len(obstacle_types), im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=3e-3)
    # dqn = DQN("dqn", env=env, ob_size=1, ac_size=len(obstacle_types), im_size=[1], args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=3e-3)

  # else:
  #   pol = Model(args.exp, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  #   vf = Model(args.exp + "_vf", ob_size=env.ob_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  
  initialize()
  if args.dqn_train:
    sync_from_root(sess, dqn.vars, comm=comm)
  if not args.multi:
    vf.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if args.vis:
    args.folder += "_vis"

  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
  else:
    WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder  

  for name in obstacle_types:
    # if args.use_roa and name in ['gaps','jumps','stairs','base','zero']:
    if name in ['gaps','jumps','stairs']:
    # if name in ['gaps']:
      # if args.use_roa:
        # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_new_criteria2/best/')
      # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_single2/best/')
      # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_orig/best/')
      # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_large/best/')
      roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_large2/best/')

  if args.e2e:  
    pol.load(WEIGHTS_PATH + '/mix/')  
  elif args.multi:
    for name in obstacle_types:
      # if args.use_roa and name in ['gaps','jumps','stairs','base','zero']:
      # if name in ['gaps','jumps','stairs']:
      # # if name in ['gaps']:
      #   # if args.use_roa:
      #     # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_new_criteria2/best/')
      #   # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_single2/best/')
      #   # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_orig/best/')
      #   # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_large/best/')
      #   roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_large2/best/')
      # elif name in ['jumps','stairs']:
      #   roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '/best/')

        # roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '_new_all/best/')
        # elif args.rl_roa:
        #   roa[name].load(home + '/results/biped_model/latest/' + args.folder + '/' + name + '2/best/')
        
      if args.use_vf:
        vf[name].load(home + '/results/biped_model/latest/vf1/' + name + '/') 
      # if name == 'zero': continue
      # if args.old:
      #   pol[name].load(WEIGHTS_PATH + '/' + name + '/')
      # else:
      #   pol[name].load(home + '/hpc-home/results/biped_model/latest/b11/' + name + '_base2/')
      # vf_orig[name].load_base(pol[name].name, WEIGHTS_PATH + '/' + name + '/')
      # else:
      #   if name == 'zero': continue
      #   pol[name].load(WEIGHTS_PATH + '/' + name + '/')
      #   vf_orig[name].load_base(pol[name].name, WEIGHTS_PATH + '/' + name + '/') 
      
    if args.dqn and not args.dqn_train:
      # dqn.load(home + '/results/biped_model/latest/' + args.folder + '/dqn/')
      # dqn.load(home + '/results/biped_model/latest/dqn/new_pols_0001/')
      # dqn.load(home + '/results/biped_model/latest/dqn/longer/')
      # dqn.load(home + '/results/biped_model/latest/dqn/longer_exp2/')
      # dqn.load(home + '/results/biped_model/latest/dqn/no_state8/best/')
      # dqn.load(home + '/results/biped_model/latest/dqn/no_state8/')
      # dqn.load(WEIGHTS_PATH + '/dqn/')
      # dqn.load(home + '/hpc-home/results/biped_model/latest/dqn/dqn_adv_baseline_reward1/')
      # dqn.load(home + '/hpc-home/results/biped_model/latest/dqn/dqn_adv_baseline_reward/')
      dqn.load(home + '/hpc-home/results/biped_model/latest/dqn/dqn_force_pol/')

  else:
    pol.load(WEIGHTS_PATH + '/' + args.exp + '/')
    vf.load_base(pol.name, WEIGHTS_PATH + '/' + args.exp + '/') 


  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()

  prev_done = True
  if not args.baseline:
    env.obstacle_type = 'mix'
  env.difficulty = args.difficulty
  env.height_coeff = args.height_coeff
  prev_done = True
  # ob = env.reset()

  np.random.seed(myseed)

  ob = env.reset(base_before=args.base_before)
  im = env.get_im()
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  ep_steps = 0
  timesteps_so_far = 0
  stochastic = False

  vpred_data = []
  vpred_orig_data = []
  probs_data = []
  stable_vpred = []
  terrain_data = []
  current_pol_data = []
  next_pol_data = []
  # switch_data = []

  if args.old:
    pol_list = ['gaps','jumps','stairs'] 
    # pol_list = ['gaps','base', 'jumps','stairs']
  else:
    pol_list = ['gaps','jumps','stairs','steps'] 

  switch_data = {name:[] for name in pol_list}
  advantage_data = {name:[] for name in pol_list}
  prev_advantage_data = {name:[] for name in pol_list}
  vpred_data = {name:[] for name in pol_list}
  rew_data = {name:[] for name in pol_list}


  stable_count = 120


  # obstacle_types = ['gaps', 'base', 'jumps']
  msv   = [121, 62, 28.7]

  # means = [147, 165, 115, 165]
  # stds  = [15, 14.7, 10.9, 14.7]

  means_orig = [109.80566, 149.17542, 94.06625, 134.47719, 139.7048][:len(obstacle_types)]
  stds_orig  = [12.566977, 5.6720533, 6.8909297, 3.953171, 6.167252][:len(obstacle_types)]

  # # obstacles:  ['gaps', 'base', 'jumps', 'stairs', 'zero']
  # means =  [94.36735, 100.704414, 56.201767, 83.965126, 113.21892][:len(obstacle_types)]
  # stds =  [28.819658, 27.49625, 23.5789, 21.56003, 20.222857][:len(obstacle_types)]

  # means =  [141.7557, 146.01744, 96.32929, 129.65916, 129.5527]
  # stds =  [8.266491, 7.647293, 25.360434, 4.3983607, 15.288723]

  # means =  [93.526474, 109.56233, 43.29318, 82.98333, 121.838554]
  # stds =  [38.435516, 39.08348, 36.178516, 31.103722, 27.6118]

  # obstacles:  ['gaps', 'base', 'jumps', 'stairs', 'zero']
  means =  [139.37276, 154.82619, 130.85301, 139.18336, 142.60199]
  # stds =  [17.898663, 4.119842, 8.607745, 3.2854395, 12.185635]
  stds =  [5, 5, 5, 5, 5]


  # eval_length = 100
  # eval_length = 500

  # eval_length = 500
  # eval_length = 5
  # eval_length = 200
  # metrics = {"episodes": 0, "dist_travelled": deque(maxlen=eval_length), "total_distance":0, "time_alive":deque(maxlen=eval_length), "success":deque(maxlen=eval_length), "switch":deque(maxlen=eval_length)}
  metrics = {"episodes": 0, "dist": [],  "success":[]}

  if args.dqn: 
    dqn_ob = ob
    desired = np.zeros(len(obstacle_types))
    desired_pol = obstacle_types.index(env.order[env.box_num])
    desired[desired_pol] = 1
    dqn_ob = np.concatenate([dqn_ob, desired])
    dqn_im = env.get_im()
    dqn_vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
    current_pol = np.argmax(dqn_vpreds)
    temp_pol = next_pol = prev_pol = current_pol
  else:
    # temp_pol = next_pol = prev_pol = current_pol = obstacle_types.index("base")
    temp_pol = next_pol = prev_pol = current_pol = obstacle_types.index("jumps")

  if rank == 0:
    print("starting evaluation, running for ", args.eval_length, " episodes")
  artifact_count = 0
  if args.render and args.debug:
    replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)
    replace_Id3 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.7],[0,0,1],textSize=3)
  # if args.switch or args.exp_switch or args.use_roa:
    # replace_Id = p.addUserDebugText("Switch",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.2],[0,1,0],textSize=3)
      # print(obstacle_types[current_pol])

  # prev_pol = current_pol = 0
  t1 = time.time()
  t2 = time.time()
  # thres = 0.3
  if args.use_roa:
    thres = args.thres
    # ['gaps','jumps','stairs']
    # thres = [0.87,0.87,0.87]
    # thres = [0.83,0.83,0.83]
  else:
    thres = 0.0
    # thres = [0.8,0.8,0.8]
  stats = {'gaps':[],'jumps':[],'stairs':[], 'base':[], 'steps':[], 'zero':[], 'flat':[]}

  q_table = {}
  # q_table['jumps'] = {'gaps':4,'base':19,'stairs':0}
  # q_table['gaps'] = {'base':10,'jumps':3,'stairs':10}
  # q_table['stairs'] = {'gaps':7,'base':15,'jumps':38}
  # q_table['stairs'] = {'gaps':76,'base':88,'jumps':88}
  # q_table['gaps'] = {'base':81,'jumps':84,'stairs':73}
  # q_table['jumps'] = {'gaps':74,'base':65,'stairs':32}
  
  q_table['gaps'] = {'base':89,'jumps':78,'stairs':73}
  q_table['jumps'] = {'gaps':35,'base':54,'stairs':75}
  q_table['stairs'] = {'gaps':55,'base':54,'jumps':88}

  # scalar = 0.99
  scalar = 0.98
  # scalar = 0.97 
  # if args.roa_table:
  roa_table = {}
    
    # roa_table['jumps'] = {'gaps':0.9781324763412345,'base':0.9778185374138691,'stairs':0.9721070138025987}
    # roa_table['stairs'] = {'gaps':0.8925965853783278,'base':0.8758598659187555,'jumps':0.8881034492627042}
    # roa_table['gaps'] = {'base':0.901837277177133,'jumps':0.8908433543489964,'stairs':0.8800138408208594}

    # roa_table['gaps'] = {'base':0.8135579174341158,'jumps':0.8479823220082393,'stairs':0.8463695993253945}
    # roa_table['jumps'] = {'gaps':0.6829121111362588,'base':0.6713067322412384,'stairs':0.6451377580780763}
    # roa_table['stairs'] = {'gaps':0.6134906093196391,'base':0.5998596904516833,'jumps':0.6023048559824625}

  # roa_table['gaps'] = {'base':0.8912404358542619,'jumps':0.8796955116770484,'stairs':0.8967811886478415}
  # roa_table['jumps'] = {'gaps':0.8529617829817016,'base':0.8592505388789706,'stairs':0.8345029192706269}
  # roa_table['stairs'] = {'gaps':0.50,'base':0.5,'jumps':0.5}

  # roa_table['jumps'] = {'gaps':33,'base':32,'stairs':32}
  # roa_table['stairs'] = {'gaps':5,'base':3,'jumps':5}
  # roa_table['gaps'] = {'base':0,'jumps':3,'stairs':6}

  roa_table['jumps'] = {'gaps':80,'base':40,'stairs':74}
  roa_table['gaps'] = {'base':98,'jumps':32,'stairs':6}
  roa_table['stairs'] = {'gaps':79,'base':16,'jumps':11}


  box_cross_step = None
  prev_box = env.order[env.box_num]
  max_z_height = 0
  feet_data = {'left':[],'right':[]}
  com_data = []
  switch_points = []
  detections = []

  if args.record_failures:  
    num_axes = 3    
    fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))        
  prev_box_num = env.box_num
  switch_time = None
  switch_window = 80

  artifact_x = 0
  dist_to_artifact = 0
  detection_dist = 0.9
  # detection_dist = 1.0
  # detection_dist = 0.8

  prev_next_pol = next_pol
  box_cross_x = env.body_xyz[0]
  box_cross_step = env.steps
  # print(env.order)
  dist_list = []
  success_list = []
  while True:
  
    if timesteps_so_far > args.max_ts:
      break 

    zero_ob = copy.copy(ob)
    zero_ob[-2] = 0.0
    
    if args.e2e:
      act, _, _, _ = pol.step(ob, im, stochastic=False)
    else:
      if current_pol == len(obstacle_types) - 1:
        if args.old:
          act, vpred, _, nlogp = pol['base'].step(zero_ob, im, stochastic=stochastic)
        else:
          act, vpred, _, nlogp = pol[obstacle_types[0]].step(zero_ob, im, stochastic=stochastic)
      else:
        act, vpred, _, nlogp = pol[obstacle_types[current_pol]].step(ob, im, stochastic=stochastic)
      
    torques = act
    
    if args.multi_robots:
      if temp_pol != current_pol:
        next_ob, rew, done, _ = env.step(torques, freeze_robot=True)
      else:
        next_ob, rew, done, _ = env.step(torques, freeze_robot=False)
      next_im = env.get_im()
    else:
      next_ob, rew, done, _ = env.step(torques)
      next_im = env.get_im()

    # dist_to_artifact = artifact_x - env.body_xyz[0]        


    # if dist_to_artifact < 0 and env.box_num + 4 < len(env.box_info[0]):
    #   up_coming_edges = [env.box_info[1][env.box_num + 2][0] - env.box_info[2][env.box_num + 2][0], env.box_info[1][env.box_num + 3][0] - env.box_info[2][env.box_num + 3][0], env.box_info[1][env.box_num + 4][0] - env.box_info[2][env.box_num + 4][0]]
    #   distances = (np.array(up_coming_edges) - env.body_xyz[0])
    #   dists = np.array([d < detection_dist + 0.01 and d > detection_dist - 0.01 for d in distances])
    #   if (dists).any():
    #     # if dist_to_artifact < 0:
    #     if dist_to_artifact < -0.8:
    #       idx = np.argmax(dists)
    #       height_diff = (env.box_info[1][env.box_num + idx + 2][2] + env.box_info[2][env.box_num + idx + 2][2]) - (env.box_info[1][env.box_num + idx + 2 - 1][2] + env.box_info[2][env.box_num + idx + 2 - 1][2])
    #       if height_diff > 0.25 and height_diff < 0.4:
    #         next_pol = obstacle_types.index('jumps')
    #         detection = env.steps
    #         detections.append(detection)
    #         artifact_x = env.box_info[1][env.box_num + idx + 2][0] - env.box_info[2][env.box_num + idx + 2][0]
    #         if args.rand_pol:
    #           rand_dist = np.random.randint(0,detection_dist*100)/100
    #       elif height_diff < -0.4:
    #         next_pol = obstacle_types.index('gaps')
    #         detection = env.steps
    #         detections.append(detection)
    #         artifact_x = env.box_info[1][env.box_num + idx + 2][0] - env.box_info[2][env.box_num + idx + 2][0]
    #         if args.rand_pol:
    #           rand_dist = np.random.randint(0,detection_dist*100)/100
    #       elif abs(height_diff) > 0.12 and abs(height_diff) < 0.21:
    #         next_pol = obstacle_types.index('stairs')
    #         detection = env.steps
    #         detections.append(detection)
    #         artifact_x = env.box_info[1][env.box_num + idx + 2][0] - env.box_info[2][env.box_num + idx + 2][0]
    #         if args.rand_pol:
    #           rand_dist = np.random.randint(0,detection_dist*100)/100
    





    if env.order[env.box_num] == 'flat':
      if args.old:
        next_pol = obstacle_types.index('base')
      else:
        next_pol = obstacle_types.index('jumps')
    else:
      next_pol = obstacle_types.index(env.order[env.box_num])
    
    if prev_next_pol != next_pol:
      # print(prev_next_pol, next_pol)
      box_cross_x = env.body_xyz[0]
      # print(box_cross_x)
      # box_cross_step = env.steps
      vpred_buffer = []
      rew_buffer = []
    prev_next_pol = next_pol
   
    dist_to_artifact = box_cross_x  + 0.9 - env.body_xyz[0]   
    # print("disdt", dist_to_artifact)

    # if dist_to_artifact < 0:
    #   box_cross_step = None

    # if not box_cross_step and dist_to_artifact > 0 and dist_to_artifact < detection_dist:
      # box_cross_step = env.body_xyz[0]

    feet = np.array(list(env.local_foot_pos['left']) + list(env.local_foot_pos['right']) + [env.body_xyz[2] - env.foot_pos['left'][2], env.body_xyz[2] - env.foot_pos['right'][2]] + env.contacts + [env.body_xyz[1], env.body_xyz[2]-env.z_offset, env.yaw, env.vx, env.vy, env.vz, dist_to_artifact])

    roa_output = 0
    temp_next_pol = current_pol
    if args.dqn:
      desired_pol = obstacle_types.index(env.order[env.box_num])
      if current_pol == desired_pol:
        rew += 1.0
      elif current_pol != desired_pol:
        rew -= 1.0
      dqn_next_ob = next_ob
      desired = np.zeros(len(obstacle_types))
      desired[desired_pol] = 1
      dqn_next_ob = np.concatenate([dqn_next_ob, desired])
      dqn_next_im = env.get_im()

      if args.dqn_train:
        dqn.add_to_buffer([dqn_ob, dqn_im, current_pol, rew, prev_done, dqn_vpreds[current_pol]])
        dqn.train_model.get_eps()
         

      dqn_vpreds = dqn.get_vpreds(dqn_next_ob, dqn_next_im)
      if args.dqn_train:
        if np.random.random() < dqn.train_model.eps:
          temp_next_pol = np.random.randint(len(obstacle_types))
        else:
          temp_next_pol = np.argmax(dqn_vpreds)
      else:
        temp_next_pol = np.argmax(dqn_vpreds)
      if args.dqn_force:
        if temp_next_pol != desired_pol:
          temp_next_pol = current_pol
      dqn_ob = dqn_next_ob
      dqn_im = dqn_next_im

    else:
      if env.order[env.box_num] == 'zero':
        temp_next_pol = obstacle_types.index('zero')
      elif next_pol != current_pol:
        if args.use_adv:
          if obstacle_types[next_pol] in ['gaps','jumps','stairs']:
            # print(dist_to_artifact)
            # if box_cross_step and dist_to_artifact < 0.01:
            if dist_to_artifact < 0.01:
              print("didn't threshold adv", obstacle_types[next_pol], "dist", dist_to_artifact)
              temp_next_pol = next_pol              
            next_vpred = pol[obstacle_types[next_pol]].get_value(ob, im)
            vpred_buffer.append(next_vpred)
            rew_buffer.append(rew)
            # print(len(vpred_buffer))
            if len(vpred_buffer) > 1:
              # adv = (1-np.clip(abs(rew_data[name][-1] + 0.99*vpred[name][] - vpred_data[name][-1]), 0, 1))

              adv = 1 - np.clip(rew_buffer[-2] + 0.99*vpred_buffer[-1] - vpred_buffer[-2], 0, 1)
              # advantage_data[name].append(np.clip(prev_advantage_data[name][-2]- prev_advantage_data[name][-1],-1, 0))
              # advantage_data[name].append(np.clip(abs(adv), 0, 1))
            
              if adv > args.thres:
                # print(obstacle_types[next_pol], adv)

                # print(adv)

                temp_next_pol = next_pol
            # else:
              # advantage_data[name].append(0.0)


          # print(next_vpred)

        elif args.use_roa:
          if obstacle_types[next_pol] in ['gaps','jumps','stairs']:
            if box_cross_step and dist_to_artifact < 0.01:
              print("didn't threshold roa", obstacle_types[next_pol])
              temp_next_pol = next_pol              
            elif obstacle_types[next_pol] in ['gaps', 'jumps', 'stairs']:
              roa_output = roa[obstacle_types[next_pol]].step(ob, im)
              if args.roa_table:
                if roa_output >= (roa_table[obstacle_types[next_pol]][obstacle_types[current_pol]]/100):
                  temp_next_pol = next_pol              
              elif args.feet and roa_output >= args.thres and (abs(env.foot_pos['left'][0] - env.foot_pos['right'][0]) < 0.25) and ((not env.ob_dict['left_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.25) or (not env.ob_dict['right_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.25)):
                temp_next_pol = next_pol              
              elif roa_output >= args.thres:
              # elif roa_output >= 0.95:
                temp_next_pol = next_pol              
        elif args.q_table:
          # if (env.steps - box_cross_step == q_table[obstacle_types[next_pol]][obstacle_types[current_pol]]):              
          if obstacle_types[current_pol] != 'zero':
            q_dist = q_table[obstacle_types[next_pol]][obstacle_types[current_pol]]/100
          # print(dist_to_artifact, q_dist)
            if q_dist < (dist_to_artifact + 0.1) and q_dist > (dist_to_artifact - 0.01):              
              temp_next_pol = next_pol            
        elif args.feet and args.feet_contact:
          if not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground']:
            print(np.sqrt(np.sum((np.array(env.foot_pos['right']) - np.array(env.foot_pos['left']))**2)), abs(min(env.body_xyz[0] - env.foot_pos['right'][0], env.body_xyz[0] - env.foot_pos['left'][0])))

        elif args.feet:
          if (abs(env.foot_pos['left'][0] - env.foot_pos['right'][0]) < 0.25) and ((not env.ob_dict['left_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.25) or (not env.ob_dict['right_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.25)):
            temp_next_pol = next_pol

        elif args.feet_contact:
          if not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground']:
            temp_next_pol = next_pol
        else:
          if args.rand_pol:
            if rand_dist < (dist_to_artifact + 0.1) and rand_dist > (dist_to_artifact - 0.01):              
              temp_next_pol = next_pol 
          else:
            temp_next_pol = next_pol
      # else:
      #   if args.use_adv:
      #     advantage_data[name].append(0.0)
      
    if args.render and args.debug:
      # if switch:
      replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,1,0],textSize=3,replaceItemUniqueId=replace_Id2)
        # replace_Id3 = p.addUserDebugText(obstacle_types[next_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.7],[0,1,0],textSize=3,replaceItemUniqueId=replace_Id3)
        # replace_Id = p.addUserDebugText("Switch",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.2],[0,1,0],textSize=3, replaceItemUniqueId=replace_Id)
      # else:
        # replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[1,0,0],textSize=3,replaceItemUniqueId=replace_Id2)
        # replace_Id3 = p.addUserDebugText(obstacle_types[next_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.7],[1,0,0],textSize=3,replaceItemUniqueId=replace_Id3)
        # replace_Id = p.addUserDebugText("Don't switch",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.2],[1,0,0],textSize=3, replaceItemUniqueId=replace_Id)
    
    if temp_next_pol != current_pol:
      prev_pol = current_pol
      current_pol = temp_next_pol
    
    if env.order[env.box_num] != 'zero':
      current_pol_data.append(current_pol)
      next_pol_data.append(next_pol)
      
      if env.body_xyz[2] > max_z_height:
        max_z_height = env.body_xyz[2]
      terrain_data.append(env.z_offset)


      for name in pol_list:
      # for name in ['gaps']:
        # print(box_cross_step, dist_to_artifact, detection_dist)
        # if name == obstacle_types[next_pol] and box_cross_step and dist_to_artifact > 0 and dist_to_artifact < detection_dist:
        if name == obstacle_types[next_pol]:
          if args.old:
            switch_data[name].append(roa[obstacle_types[next_pol]].step(ob,im))
          else:
            switch_data[name].append(1)
          # if vpred_data[name][-1] != 0:
            # if prev_advantage_data[name][-1] != 0:          
            # advantage_data[name].append(np.clip(((rew_data[name][-1] + 0.99*vpred - vpred_data[name][-1])**2 - prev_advantage_data[name][-1]), -0.1,0.1))
            # advantage_data[name].append(np.clip(((rew_data[name][-1] + 0.99*vpred - vpred_data[name][-1])**2 - prev_advantage_data[name][-1]), -0.1,0.1))
            
            # prev_advantage_data[name].append((rew_data[name][-1] + 0.99*vpred - vpred_data[name][-1])**2)
          
          # if temp_pol == current_pol:
          # if not args.use_adv:
            # prev_advantage_data[name].append(np.clip((rew_data[name][-1] + 0.99*vpred - vpred_data[name][-1])**2, 0, 5))
            # if len(prev_advantage_data[name]) > 1: 
              # advantage_data[name].append(np.clip(prev_advantage_data[name][-2]- prev_advantage_data[name][-1],-1, 0))
          
          rew_data[name].append(rew)
          vpred_data[name].append(vpred)

          if len(rew_data[name]) > 1:
            # pre_advantage_data[name].append(1-np.clip(abs(rew_data[name][-1] + 0.99*vpred - vpred_data[name][-1]), 0, 1))

            advantage_data[name].append(1-np.clip(abs(rew_data[name][-1] + 0.99*vpred - vpred_data[name][-1]), 0, 1))
            # prev_advantage_data[name].append(1-np.clip(abs(rew_data[name][-1] + 0.99*vpred - vpred_data[name][-1]), 0, 1))
            # if len(prev_advantage_data[name]) > 1: 
              # advantage_data[name].append(prev_advantage_data[name][-1] - prev_advantage_data[name][-2])
              # advantage_data[name].append(1- np.clip(prev_advantage_data[name][-1] - prev_advantage_data[name][-2], -1, 0))
            # else:
              # advantage_data[name].append(0)
          else:
            advantage_data[name].append(0)
          # else:
            # advantage_data[name].append(0)
          

          # switch_data[name].append(roa[obstacle_types[next_pol]].step(feet))
        else:
          switch_data[name].append(0.0)
          rew_data[name].append(0)
          vpred_data[name].append(0)
          # if not args.use_adv:
          advantage_data[name].append(0)
          # prev_advantage_data[name].append(0)

      # com_data.append(env.body_xyz[2])
      # feet_data['left'].append(env.foot_pos['left'][2])
      # feet_data['right'].append(env.foot_pos['right'][2])

      if abs(env.foot_pos['left'][0] - env.foot_pos['right'][0]) < 0.1:
        feet_data['left'].append(env.foot_pos['left'][2])
        feet_data['right'].append(env.foot_pos['right'][2])
      else:
        feet_data['left'].append(np.nan)
        feet_data['right'].append(np.nan)
      if not env.ob_dict['left_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.1:
        com_data.append(env.body_xyz[2])
      elif not env.ob_dict['right_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.1:
        com_data.append(env.body_xyz[2])
      else:
        com_data.append(np.nan)

    if temp_pol != current_pol:
      switch_points.append(env.steps)
    # if death and death_countdown is None and env.steps > 10:
    #   death_countdown = env.steps
    #   print("gonna die!", env.steps)
    #   env.speed = 0.0
    # print(args.test_pol, args.multi, not args.test_pol or not args.multi)
    if not args.test_pol and not args.multi:
      vf.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])

    prev_done = done
    ob = next_ob
    im = next_im
    ep_ret += rew
    ep_len += 1
    ep_steps += 1
    
    temp_pol = current_pol

    # prev_vpred = vpred
    # prev_prob = prob

    if args.dqn_train:
      if ep_steps % 2048 == 0:
        dqn.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=np.max(dqn.get_vpreds(dqn_next_ob, dqn_next_im)), last_done=done)  
        dqn.run_train(ep_rets,ep_lens)
        ep_rets = []
        ep_lens = []

    if not args.test_pol and not args.multi and ep_steps % horizon == 0:
      vpred = vf.get_value(next_ob, next_im)
      vf.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
      vf.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []
    


    if ep_len % 2000 == 0:
      all_eps = MPI.COMM_WORLD.allgather(len(dist_list))
      all_dists = MPI.COMM_WORLD.allgather(dist_list)
      all_success = MPI.COMM_WORLD.allgather(success_list)
      if sum(all_eps) > args.eval_length:
        break
      if rank == 0:
        all_dists = [h for d in all_dists for h in d]
        all_success = [h for d in all_success for h in d]
        print("Episode {0:2d} Dist: {1:0.4f} success: {2:0.4f} time: {3:0.4f}".format(sum(all_eps), np.mean(all_dists), np.mean(all_success), time.time() - t1))

        # print("episode {0:2d} Env steps {1:4d} x pos {2:7.4f} Dist: {3:0.4f}  success: {4:0.4f}  Last {5:s}->{6:s} time: {7:0.4f} ".format(metrics["episodes"], env.steps, env.body_xyz[0], np.mean(metrics['dist']), np.mean(metrics['success']), obstacle_types[prev_pol], obstacle_types[current_pol], time.time() - t1))
        t1 = time.time()
      # if rank == 0:
        # print(sum(all_dists))
      # all_dists = MPI.COMM_WORLD.allgather(dist_list)
      # all_success = MPI.COMM_WORLD.allgather(success_list)
      # all_dists = [h for d in all_dists for h in d]
      # all_success = [h for d in all_success for h in d]

    if done:    
      # for i,name in enumerate(pol_list):
      #   if name == 'walk': continue
      #   # advantage_data[name] = []
      #   # print(len(advantage_data[name]), len(rew_data[name]), len(vpred_data[name]))

      #   # print(advantage_data[name])
      #     # for j in range(len(vpred_data[name])):
      # # if abs(vpred_data[name][j]) > 0:
      #     # def get_advantage(self, last_value, last_done):    
      #   gamma = 0.99; lam = 0.95
      #   # advs = np.zeros_like(self.data['rew'])
      #   # advs = np.zeros(90)
      #   lastgaelam = 0
      #   # for t in reversed(range(j, j+90)):
      #   prev_lastgaelam = 0
      #   for t in reversed(range(len(vpred_data[name]))):
      #     if t == len(vpred_data[name]) - 1:
      #       nextvalues = vpred_data[name][t]
      #     else:
      #       nextvalues = vpred_data[name][t+1]
          
      #     delta = rew_data[name][t] + gamma * nextvalues - vpred_data[name][t]
      #     # print(name, delta, t)
      #     lastgaelam = delta + gamma * lam  * lastgaelam
      #     # print(lastgaelam)
      #     # advantage_data[name].append(np.clip(lastgaelam**2, 0, ))
      #     # advantage_data[name].append(lastgaelam**2)
      #     advantage_data[name][t] = np.clip(abs(lastgaelam) - abs(prev_lastgaelam), 0,1)
      #     # advantage_data[name].append(lastgaelam**2 < prev_lastgaelam**2)
      #     # advantage_data[name].append(lastgaelam**2 < prev_lastgaelam**2)
      #     prev_lastgaelam = lastgaelam
      #     # advantage_data[name][t] = (delta + gamma * lam  * lastgaelam)**2 < prev_advantage_data[name][-1]**2

      #     # prev_advantage_data[name][t] = lastgaelam = delta + gamma * lam  * lastgaelam
      #   # self.data['return'
      #   # for t in reversed(range(len(self.data['rew']))):
      #   #   if t == len(self.data['rew']) - 1:
      #   #     nextnonterminal = 1.0 - last_done
      #   #     nextvalues = last_value
      #   #   else:
      #   #     nextnonterminal = 1.0 - self.data['done'][t+1]
      #   #     nextvalues = self.data['value'][t+1]
      #   #   delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
      #   #   advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
      #   # self.data['return'] = advs + self.data['value']
      #   # j += 90
      # # print(advantage_data)
      # print([len(advantage_data[name]) for name in pol_list], [len(rew_data[name]) for name in pol_list])

      if not args.dqn_train:
        # print("====================================================================")
        # print"number of episodes", metrics["episodes"], " time alive: ", np.mean(metrics['time_alive']), " distance travelled: ", metrics['dist_travelled'])
        # print("====================================================================")
        # metrics["time_alive"].append(env.steps/(120*(env.box_info[1][-1][0] + env.box_info[2][-1][0]) + 200))
        # metrics["dist_travelled"].append(env.body_xyz[0])
        # metrics["total_distance"] += env.box_info[1][-1][0] + env.box_info[2][-1][0]
        metrics["dist"].append(env.body_xyz[0]/(env.box_info[1][-1][0]))
        # metrics["success"].append(env.order[env.box_num] == 'zero')
        metrics["success"].append(float(env.body_xyz[0] > (env.box_info[1][-1][0])))
        dist_list.append(env.body_xyz[0]/(env.box_info[1][-1][0]))
        success_list.append(float(env.body_xyz[0] > (env.box_info[1][-1][0])))
        # # if env.order[env.box_num] == 'zero'
        # if env.body_xyz[0] > (env.box_info[1][-1][0]):
        #   metrics["switch"].append(True)
        # else:
        #   metrics["switch"].append(obstacle_types[current_pol] == env.order[env.box_num])
        # # metrics["switched"].append(switch)
        # if metrics["episodes"] > eval_length - 1:
        #   break
        # print("Episode ", metrics["episodes"], "env_steps ", env.steps, "env x pos", env.body_xyz[0], " success: ",  np.mean(metrics['success']))
        # print("episode {0:2d} Env steps {1:4d} x pos {2:7.4f}  alive: {3:0.4f} Dist: {4:0.4f}  success: {5:0d} total: {6:0.4f} switch: {7:0d} total: {8:0.4f} Last {9:s}->{10:s}".format(metrics["episodes"], env.steps, env.body_xyz[0], np.mean(metrics['time_alive']), np.sum(metrics['dist_travelled'])/metrics["total_distance"], metrics['success'][-1], np.mean(metrics['success']), metrics['switch'][-1], np.mean(metrics['switch']), obstacle_types[prev_pol], obstacle_types[current_pol]))
        # print("episode {0:2d} Env steps {1:4d} x pos {2:7.4f}  alive: {3:0.4f} Dist: {4:0.4f}  success: {5:0d} total: {6:0.4f} Last {9:s}->{10:s}".format(metrics["episodes"], env.steps, env.body_xyz[0], np.mean(metrics['time_alive']), np.sum(metrics['dist_travelled'])/metrics["total_distance"], metrics['success'][-1], np.mean(metrics['success']), metrics['switch'][-1], np.mean(metrics['switch']), obstacle_types[prev_pol], obstacle_types[current_pol]))
        # print(metrics["episodes"], env.steps, env.body_xyz[0], np.mean(metrics['dist']), np.mean(metrics['success']), obstacle_types[prev_pol], obstacle_types[current_pol])
        if rank == 0:
          # print("episode {0:2d} Env steps {1:4d} x pos {2:7.4f} Dist: {3:0.4f}  success: {4:0.4f}  Last {5:s}->{6:s} time: {7:0.4f} ".format(metrics["episodes"], env.steps, env.body_xyz[0], np.mean(metrics['dist']), np.mean(metrics['success']), obstacle_types[prev_pol], obstacle_types[current_pol], time.time() - t1))
          # t1 = time.time()


          metrics["episodes"] += 1

          # print(env.original_speed, env.steps)

          if not metrics["success"][-1]:
            stats[env.order[env.box_num]].append(metrics["episodes"])
          
          # if args.record_failures and not metrics['success'][-1] and env.order[env.box_num] == 'gaps':  
          if args.record_failures and not metrics['success'][-1]:  
          # if True:
            try:
              # os.mkdir(PATH + 'failure' + str(len(metrics['success']) - sum(metrics['success'])))
              os.mkdir(PATH + 'failure' + str(metrics["episodes"]))
            except:
              pass
            # env.save_sim_data(PATH + 'failure' + str(len(metrics['success']) - sum(metrics['success'])) + '/', last_steps=True)
            env.save_sim_data(PATH + 'failure' + str(metrics["episodes"]) + '/', last_steps=True)

            num_axes = 4
            # fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))      
            #   
            # fig, axes = plt.subplots(num_axes, figsize=(13, 4.5*num_axes), gridspec_kw = {'wspace':0, 'hspace':0.05})        
            fig, axes = plt.subplots(num_axes, figsize=(13, 3.0*num_axes), gridspec_kw = {'wspace':0, 'hspace':0.4})        
            # fig.subplots_adjust(hspace=0.01, wspace=0.01)
            # plt.subplots_adjust(wspace=0, hspace=0)
            # fig, axes = plt.subplots(num_axes, hspace=0.01, wspace=0.01, figsize=(13, 4.5*num_axes))        

            # fig.subplots_adjust(hspace=, wspace=)
            # colours = [np.array([51, 204, 51])/255.0, np.array([0, 102, 204])/255.0, np.array([200, 100, 51])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([100, 204, 0])/255.0, ]
            colours = [np.array([51, 204, 51])/255.0, np.array([51, 51, 204])/255.0, np.array([204, 51, 51])/255.0, np.array([204, 102, 204])/255.0, np.array([204,102,51])/255.0,]
            for i,name in enumerate(['gaps','jumps','stairs']):
              # if name == 'walk': continue
              axes[0].plot([_ for _ in range(len(switch_data[name]))], switch_data[name], c=colours[i], linewidth=1.0, alpha=1.0)  
            axes[0].legend(['gaps','jumps','stairs'])  
            axes[0].set_title("Switch Estimate", loc='left')
            
            axes[1].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
            axes[1].plot([_ for _ in range(len(next_pol_data))], next_pol_data, c=np.array([0, 204, 102])/255.0, alpha=1.0)  
            axes[1].set_ylim([-0.9,len(obstacle_types)-1 + 0.1])
            for s, n in enumerate(['gaps','walk','jumps','stairs','zero']):
              if n == 'zero': continue
              axes[1].text(0,s*0.75+0.5,str(s) + ". " + n)
            axes[1].set_title("Current and Next Policy", loc='left')
            axes[1].legend(["current_policy", "next"], loc='upper center')  
            # axes[1].legend(["current_policy", "next"], loc='lower right')  

            for i,name in enumerate(['gaps','jumps','stairs']):
              # if name == 'walk': continue
              axes[2].plot([_ for _ in range(len(advantage_data[name]))], advantage_data[name], c=colours[i], linewidth=1.0, alpha=1.0)  
            axes[2].legend(['gaps','jumps','stairs'])  
            axes[2].set_title("Advantage Estimate", loc='left')

            axes[3].plot([_ for _ in range(len(feet_data['left']))], feet_data['left'], c=colours[0], alpha=1.0)  
            axes[3].plot([_ for _ in range(len(feet_data['right']))], feet_data['right'], c=colours[1], alpha=1.0)  
            axes[3].plot([_ for _ in range(len(com_data))], com_data, c=colours[2], alpha=1.0)  
            axes[3].plot([_ for _ in range(len(terrain_data))], terrain_data, c=colours[3], alpha=1.0)  
            axes[3].set_title("Terrain Height (m)", loc='left')
            axes[3].set_ylim([-0.1, max_z_height + 0.1])

            for i in range(num_axes):
              for xc in switch_points:
                # axes[i].axvline(x=xc, label='line at x = {}'.format(xc), c=c)
                axes[i].axvline(x=xc,c=[colours[2][0],colours[2][1],colours[2][2],1.0], linestyle='--', linewidth=1.0)
              for xc in detections:
                axes[i].axvline(x=xc,c=[colours[0][0],colours[0][1],colours[0][2],1.0], linestyle='--', linewidth=1.0)

            axes[0].set_xlabel('Timesteps')
            axes[1].set_xlabel('Timesteps')
            axes[2].set_xlabel('Timesteps')
            axes[3].set_xlabel('Timesteps')

            # fig.tight_layout(pad=4.0)
            # plt.savefig(PATH + 'failure' + str(metrics["episodes"])  + '/plot.png', bbox_inches='tight')
            # plt.savefig(PATH + 'failure' + str(metrics["episodes"])  + '/plot.png', bbox_inches='tight')
            plt.savefig(PATH + 'failure' + str(metrics["episodes"])  + '/plot.png')
            plt.close()

      vpred_data = []
      vpred_orig_data = []
      probs_data = []
      # stable_vpred = []
      terrain_data = []
      current_pol_data = []
      next_pol_data = []
      switch_data = {name:[] for name in pol_list}
      prev_advantage_data = {name:[] for name in pol_list}
      advantage_data = {name:[] for name in pol_list}
      vpred_data = {name:[] for name in pol_list}
      rew_data = {name:[] for name in pol_list}
      box_cross_step = None
      prev_box = env.order[env.box_num]
      feet_data = {'left':[],'right':[]}
      com_data = []
      switch_points = []
      detections = []
      # Need to reseed so that the worlds are the same
      myseed += 1
      np.random.seed(myseed)

      ob = env.reset(base_before=args.base_before)
      im = env.get_im()
      ep_rets.append(ep_ret)  
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0
      max_z_height = 0

      if args.dqn: 
        dqn_ob = ob
        desired = np.zeros(len(obstacle_types))
        desired_pol = obstacle_types.index(env.order[env.box_num])
        desired[desired_pol] = 1
        dqn_ob = np.concatenate([dqn_ob, desired])
        dqn_im = env.get_im()
        dqn_vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
        current_pol = np.argmax(dqn_vpreds)
        temp_pol = next_pol = prev_pol = current_pol
      elif args.baseline:
        temp_pol = prev_pol = next_pol = current_pol = obstacle_types.index(args.obstacle_type)
      else:
        if args.old:
          temp_pol = prev_pol = next_pol = current_pol = obstacle_types.index("base")
        else:
          temp_pol = next_pol = prev_pol = current_pol = obstacle_types.index("jumps")

      prev_box_num = env.box_num
      switch_time = None
      next_pol = current_pol
      switch_point = 0
      detection = 0
      artifact_x = 0
      dist_to_artifact = 0
  
  all_dists = MPI.COMM_WORLD.allgather(dist_list)
  all_success = MPI.COMM_WORLD.allgather(success_list)

  if rank == 0:
    all_dists = [h for d in all_dists for h in d]
    all_success = [h for d in all_success for h in d]
    print("============================================================================================================================")
    print("number of episodes {0:d}, time: {1:0.4f} ".format(metrics["episodes"], time.time() - t2))
    print()
    print("Thres {0:0.2f} episode {1:2d} Dist: {2:0.4f} success: {3:0.4f}".format(args.thres, len(all_dists), np.mean(all_dists), np.mean(all_success)))
    print()

    # print("Thres {0:0.2f} time alive: {1:0.4f} distance travelled: {2:0.4f}  success: {3:0.4f} switch: {4:0.4f}".format(thres, np.mean(metrics['time_alive']), np.sum(metrics['dist_travelled'])/metrics['total_distance'], np.mean(metrics['success']), np.mean(metrics['switch'])))
    # print("Thres {0:0.2f} time alive: {1:0.4f} distance travelled: {2:0.4f}  success: {3:0.4f}".format(thres, np.mean(metrics['time_alive']), np.sum(metrics['dist_travelled'])/metrics['total_distance'], np.mean(metrics['success']), np.mean(metrics['switch'])))
    # print("Thres {0:0.2f} episode {0:2d} Dist: {3:0.4f} success: {4:0.4f}".format(metrics["episodes"], thres, metrics["episodes"], np.mean(metrics['dist']), np.mean(metrics['success'])))

    # print("success", np.mean(metrics['success']))
    print(stats)
    print([thing + " : " + str(len(stats[thing])) for thing in stats])
    print("============================================================================================================================")


def logp(x, mean, std):
  return -1*(0.5 * ((x - mean) / std)**2 + 0.5 * np.log(2.0 * np.pi) + np.log(std))

def mahalanobois_distance(x, mean, std):
  return np.sqrt((x-mean)**2/std**2)

def normpdf(x, mean, std):
    var = std**2
    denom = (2*np.pi*var)**.5
    num = np.exp(-(x-mean)**2/(2*var))
    return num/denom

def calc_probs(data, all_data):
  data = np.array(data)
  all_data = np.array(all_data)
  # print(all_data.mean(), all_data.std())
  # print(all_data.shape)
  # return (data - all_data.mean())/all_data.std()
  return (data - 87.97)/1.97

# def reject_outliers(data, m=2.):
#   d = np.abs(data - np.median(data))
#   mdev = np.median(d)
#   s = d / (mdev if mdev else 1.)
#   return data[s < m]

if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--sub_folder', default='b')
  # parser.add_argument('--folder', default='b4')
  # parser.add_argument('--folder', default='b7')
  # parser.add_argument('--folder', default='b8')
  # parser.add_argument('--folder', default='b9')
  # parser.add_argument('--folder', default='b10_vis')
  parser.add_argument('--folder', default='b10')
  # parser.add_argument('--folder', default='b11')
  parser.add_argument('--show_detection', default=False, action='store_true')

  parser.add_argument('--difficulty', default=10, type=int)
  parser.add_argument('--height_coeff', default=0.07)
  parser.add_argument('--thres', default=0.8, type=float)
  # parser.add_argument('--thres', default=0.85, type=float)

  parser.add_argument('--eval_length', default=500, type=int)
  parser.add_argument('--cur_len', default=1000, type=int)
  parser.add_argument('--inc', default=2, type=int)
  parser.add_argument('--cur_num', default=5, type=int)
  parser.add_argument('--num_artifacts', default=10, type=int)

  parser.add_argument('--terrain_first', default=False, action='store_true')
  parser.add_argument('--old', default=False, action='store_true')
  parser.add_argument('--box_pen', default=False, action='store_true')
  parser.add_argument('--advantage2', default=False, action='store_true')
  parser.add_argument('--e2e', default=False, action='store_true')
  parser.add_argument('--dqn_train', default=False, action='store_true')
  parser.add_argument('--dqn_force', default=False, action='store_true')
  parser.add_argument('--rand_pol', default=False, action='store_true')
  parser.add_argument('--rand_flat', default=False, action='store_true')
  parser.add_argument('--roa_table', default=False, action='store_true')
  parser.add_argument('--q_table', default=False, action='store_true')
  parser.add_argument('--rl_roa', default=False, action='store_true')
  parser.add_argument('--no_vis', default=False, action='store_true')
  parser.add_argument('--step_ahead', default=False, action='store_true')
  parser.add_argument('--feet', default=False, action='store_true')
  parser.add_argument('--feet_contact', default=False, action='store_true')
  parser.add_argument('--multi_robots', default=False, action='store_true')
  parser.add_argument('--debug', default=False, action='store_true')
  # parser.add_argument('--record_failures', default=False, action='store_true')
  parser.add_argument('--dist_off_ground', default=True, action='store_false')
  parser.add_argument('--more_power', default=1.0, type=float)
  parser.add_argument('--record_failures', default=True, action='store_false')
  parser.add_argument('--base_before', default=False, action='store_true')
  parser.add_argument('--single_pol', default=False, action='store_true')
  parser.add_argument('--no_reg', default=False, action='store_true')
  parser.add_argument('--dqn', default=False, action='store_true')

  parser.add_argument('--baseline', default=False, action='store_true')
  # parser.add_argument('--baseline', default=True, action='store_false')
  parser.add_argument('--switch', default=False, action='store_true')
  parser.add_argument('--exp_switch', default=False, action='store_true')
  parser.add_argument('--use_roa', default=False, action='store_true')
  parser.add_argument('--use_adv', default=False, action='store_true')

  parser.add_argument('--use_vf', default=False, action='store_true')
  parser.add_argument('--multi', default=True, action='store_false')
  parser.add_argument('--goal_set', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')
  parser.add_argument('--gae', default=False, action='store_true')
  parser.add_argument('--doa', default=True, action='store_false')
  # parser.add_argument('--doa', default=False, action='store_true')

  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  # parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=True, action='store_false')
  # parser.add_argument('--vis', default=False, action='store_true')
  parser.add_argument('--vis_type', default='depth')
  parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--camera_rate', default=1, type=int)
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="mix", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=1e5, type=int)
  parser.add_argument('--lr', default=1e-5, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)