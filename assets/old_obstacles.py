'''
Class to make obstacles
'''
import sys
sys.path.append('../../brendan')
# sys.path.append('/home/n8942251/brendan')
import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import pybullet as p
import time
import math
import numpy as np
import random
# from env import *
# from assets.env import *


class Obstacles():

    def __init__(self,y_offset=0, add_friction=False, colour=False):
        # print(y_offset)
        self.y_offset=y_offset
        self.colour = colour
        self.add_friction = add_friction
        self.boxHalfLength = 2.5
        self.boxHalfWidth = 0.5
        self.boxHalfHeight = 0.1
        # self.stair_width = 0.75
        self.stair_width = 2.0
        segmentLength = 5
        mass = 1
        visualShapeId = -1
        segmentStart = 0
        # colBoxId = p.createCollisionShape(p.GEOM_BOX,halfExtents=[boxHalfLength,boxHalfWidth,boxHalfHeight])

    def insert_stairs(self, stair_size=0.0, pos=None, orn=None, size=None):
        scale = [1.5,stair_size,1]
        # scale = [1,1,1]
        pos = [3, -3.8, 0.05]
        orn = [np.pi/2, 0.0, 0.0]
        colBoxId = p.createCollisionShape(	shapeType=p.GEOM_MESH,
        	flags=p.GEOM_FORCE_CONCAVE_TRIMESH,
        	meshScale=scale, fileName=currentdir + "/stairs.obj",halfExtents=size)
        visBoxId = p.createVisualShape(	shapeType=p.GEOM_MESH,
        	flags=p.GEOM_FORCE_CONCAVE_TRIMESH,
        	meshScale=scale, fileName=currentdir + "/stairs.obj",halfExtents=size, rgbaColor=[0.5,0.5,0.5,1])
        q = p.getQuaternionFromEuler(orn)
        # p.createMultiBody(baseMass=0,baseCollisionShapeIndex = colBoxId,basePosition = pos, baseOrientation=q)
        return p.createMultiBody(baseMass=0, baseCollisionShapeIndex = colBoxId, baseVisualShapeIndex = visBoxId,basePosition = pos, baseOrientation=q)

    def insert_box(self, pos, orn, size, colour=[0.5,0.5,0.5,1], walk_through=False):

        colBoxId = p.createCollisionShape(p.GEOM_BOX, halfExtents=size)
        visBoxId = p.createVisualShape(p.GEOM_BOX, halfExtents=size, rgbaColor=colour)
        # print("visual shape succeeded")
        q = p.getQuaternionFromEuler(orn)
        # p.createMultiBody(baseMass=0,baseCollisionShapeIndex = colBoxId,basePosition = pos, baseOrientation=q)
        # p.createMultiBody(baseVisualShapeIndex = visBoxId,basePosition = pos, baseOrientation=q)
        if walk_through:
            boxId = p.createMultiBody(baseMass=0, baseVisualShapeIndex = visBoxId, basePosition = pos, baseOrientation=q)
        else:
            boxId = p.createMultiBody(baseMass=0, baseCollisionShapeIndex = colBoxId, baseVisualShapeIndex = visBoxId, basePosition = pos, baseOrientation=q)
        if self.add_friction:
            p.changeDynamics(boxId,-1,lateralFriction=0.9, spinningFriction=0.9, rollingFriction=0.1)
        return boxId

    def set_colour(self, box_id, speed):
        colour = list(p.getVisualShapeData(box_id)[0][7])
        speed = np.clip(speed,0.5,1.5)
        colour[0] = (speed - 0.5)/(1.5 - 0.5)
        p.changeVisualShape(box_id, -1, rgbaColor = colour)

    def up_down_flat(self, height_coeff=0.1, length_coeff=0.25, order=None, add_walls=False):
        # lengt/_coeff = 0.19
        height_coeff = height_coeff
        if not order:
            order = ['down','up','down','up','down','up','down','up','down','up','up','up','up','down','up','down','up','down','down','down']
        else:
            order = ['up','flat'] + order
        boxIds = []
        positions = []; sizes = []; frictions = []; colours = []
        if add_walls:
            wall_poses = []; wall_sizes = []; wall_colours = []; walls = []
        nb_boxes = len(order)
        # print(nb_boxes)
        frictions = [0.9]*nb_boxes
        # prev_height = 0.0
        count = 0
        lowest = 0
        for name in order:
            if name == 'down': count += 1
            if name == 'up': count -= 1
            if count > lowest:
                lowest = count
        prev_height = (lowest + 2)*height_coeff + 0.1
        for i, box in enumerate(range(nb_boxes)):
            # length_coeff = 0.3
            # length_coeff = 0.3 + (random.random()-0.5)/10
            # if prev_height < 0.3:
                # up_down_flat = random.random() > 0.3
            # else:
            up_down_flat = random.randint(0,1)
            # height = min(0.0+height_coeff, prev_height+0.1)
            # print(order[i])
            if order[i] == 'down':
                height = prev_height-height_coeff
            elif order[i] == 'up':
                height = prev_height+height_coeff
            elif order[i] == 'flat':
                height = prev_height

            # elif up_down_flat == 2:
            #     height = prev_height
            if height < 0: height = prev_height+height_coeff
            # print(height)
            prev_height = height
            # length = random.uniform(0.3,1 - 0.5)
            # length = random.uniform(0.8 - length_coeff,1 - length_coeff)
            length = length_coeff

            size = [length, self.stair_width, height+0.001]
            if i == 0:
                position =  [size[0]-length, self.y_offset, size[2] + 0.01]
            else:
                position =  [positions[i-1][0] + sizes[i-1][0] + size[0], self.y_offset, size[2] + 0.01]
            # boxIds.append(self.insert_box(position, [0,0,0], size, colour=[0.8,0.2,0.2,1]))
            if i % 2 == 0:
                colour = [0.5,0.5,0.5,1] 
            else:
                colour = [0.5,0.5,0.8,1] 
            boxIds.append(self.insert_box(position, [0,0,0], size, colour=colour))
            positions.append(position)
            sizes.append(size)
            colours.append(colour)
            # self.insert_box([position[0], 0, position[2]+0.75+size[2]], [0,0,0], [0.01, 0.25, 0.75], colour=[0.5,0.8,0.5,1])

            if add_walls and random.random() < 0.2 and i > 3:
                wall_poses.append([position[0], random.choice([-1,1])*1.5*(random.random() + 0.1), position[2]+0.75+size[2]])
                wall_sizes.append([size[0]/2, 0.25, 0.75])
                wall_colours.append([0.5,0.8,0.5,1])

                # colBoxId = p.createCollisionShape(p.GEOM_BOX, halfExtents=wall_sizes[-1])
                # visBoxId = p.createVisualShape(p.GEOM_BOX, halfExtents=wall_sizes[-1], rgbaColor=wall_colours[-1])
                # q = p.getQuaternionFromEuler([0,0,0])
                # # boxId = p.createMultiBody(baseMass=0, baseCollisionShapeIndex = colBoxId, baseVisualShapeIndex = visBoxId, basePosition = wall_poses[-1], baseOrientation=q)
                # boxId = p.createMultiBody(baseMass=0, baseVisualShapeIndex = visBoxId, basePosition = wall_poses[-1], baseOrientation=q)
                # walls.append(boxId)
                
                walls.append(self.insert_box(wall_poses[-1], [0,0,0], wall_sizes[-1], colour=wall_colours[-1]))
        if add_walls:
            boxIds.extend(walls)
            positions.extend(wall_poses)
            sizes.extend(wall_sizes)
            colours.extend(wall_colours)
            # return [boxIds, positions, sizes, frictions, colours, walls]
        # else:
        return [boxIds, positions, sizes, frictions, colours]

    def add_stairs(self, length_coeff = 0.0, height_coeff = 0.0, disturb=False, speed=1.5, mode='flat', num_steps=40):
        add_flat = False
        if mode == 'up_flat':
            add_flat = True
            mode = 'up'
        elif mode == 'down_flat':
            add_flat = True
            mode = 'down'
        disturb = False
        boxIds = []
        positions = []; sizes = []; frictions = []; colours = []
        nb_boxes = int(num_steps/(length_coeff*2))
        # print(nb_boxes)
        frictions = [0.9]*nb_boxes
        if mode == 'down': 
            prev_height = nb_boxes*height_coeff
        else:
            prev_height = 0.0
        colour = [0.5,0.5,0.5,1]
        if add_flat:
            prev_flat = False
            second_flat = False
            third_flat = False
        step_count = 0
        desired_step = 0
        prev_step = False
        for i, box in enumerate(range(nb_boxes)):
            # if disturb:
            #     length_coeff = 0.3 + (random.random()-0.5)/20
            # else:
            #     # length_coeff = 0.2 + (random.random()-0.5)/10
            #     # length_coeff = 0.3
            #     length_coeff = length_coeff
            # height = min(0.0+height_coeff, prev_height+0.1)
            if mode == 'flat':
                # height = prev_height
                height = 0.09
            else:
                # 2-4 steps
                # if add_flat and (second_flat or random.random() < 0.3) and not third_flat:
                if i > 3 and add_flat and (step_count != 0 or random.random() < 0.4) and not prev_step:
                    if step_count == 0:
                        desired_step = random.sample([1,2,3],1)[0]
                        # print(desired_step)
                    step_count += 1
                    if step_count == desired_step:
                        step_count = 0
                        prev_step = True
                    height = prev_height
                    prev_step_count = 0
                    # prev_flat = True
                    # if not second_flat:
                    #     second_flat = True
                    # else:
                    #     second_flat = False
                    #     third_flat = True
                else:
                    if prev_step: prev_step = False
                    if mode == 'up':
                        height = prev_height+height_coeff
                    elif mode == 'down':
                        height = prev_height-height_coeff
                    prev_flat = False
                    third_flat = False
                # if mode == 'up':
                #     height = prev_height+height_coeff
                # elif mode == 'down':
                #     height = prev_height-height_coeff
            # print(height)
            prev_height = height
            # length = random.uniform(0.3,1 - 0.5)
            # length = random.uniform(0.8 - length_coeff,1 - length_coeff)
            length = length_coeff

            size = [length, self.stair_width, height+0.001]
            if i == 0:
                position =  [size[0]-length, self.y_offset, size[2] + 0.01]
            else:
                position =  [positions[i-1][0] + sizes[i-1][0] + size[0], self.y_offset, size[2] + 0.01]
            if self.colour:
                speed = np.clip(speed,0.5,1.5)
                colour[0] = (speed - 0.5)/(1.5 - 0.5)
            if i % 2 == 0:
                colour = [0.5,0.5,0.5,1] 
            else:
                colour = [0.5,0.5,0.8,1] 
            # colour = [np.random.random(),np.random.random(),np.random.random(),1]
            boxIds.append(self.insert_box(position, [0,0,0], size, colour=colour))
            positions.append(position)
            sizes.append(size)
            colours.append(colour)
        return [boxIds, positions, sizes, frictions, colours]

    def insert_boxes(self, pos, size, colour=None):
        if colour is None:
            colour = [0.5,0.5,0.5,1]*len(pos)
        boxIds = []
        for i, (p,s,c) in enumerate(zip(pos, size, colour)):
            p[1] = self.y_offset+p[1]
            # print(c)
            # if c == [0.8, 0.2, 0.2, 0.6]:
            if c[0] == 0.8:
                walk_through = True
            else:
                walk_through = False
            boxIds.append(self.insert_box(p, orn=[0,0,0], size=s, colour=c, walk_through=walk_through))
        return boxIds

    def insert_rand_boxes(self, length_coeff = 0.0, height_coeff = 0.0, frict_coeff = 0.0, positions=None, sizes=None, frictions=None):
        '''
        max frict_coeff = 0.8
        max height_coeff = 0.5
        random number of boxes, random length and height and position
        random friction.
        '''
        colour_options = [[0.8,0.2,0.2,1],[0.2,0.8,0.2,1],[0.8,0.8,0.2,1],[0.2,0.2,0.8,1], [0.2,0.8,0.8,1]]
        boxIds = []
        # print(self.y_offset)
        # print(positions, sizes, frictions)
        if positions is not None:
            if frictions is not None:
                for i, (pos, size, frict) in enumerate(zip(positions, sizes, frictions)):
                    if frict > 0.7: colour = colour_options[0]
                    elif frict > 0.66: colour = colour_options[1]
                    elif frict > 0.54: colour = colour_options[2]
                    elif frict > 0.42: colour = colour_options[3]
                    else: colour = colour_options[4]
            for i, (pos, size) in enumerate(zip(positions, sizes)):
                pos[1] = self.y_offset
                boxIds.append(self.insert_box(pos, [0,0,0], size))
        else:
            positions = []; sizes = []; frictions = []
            nb_boxes = np.random.randint(12,15,size=1)
            prev_height = 0.0
            for i, box in enumerate(range(nb_boxes[0])):
                friction = random.uniform(0.9-frict_coeff,0.9)
                if friction > 0.74: colour = colour_options[0]
                elif friction > 0.58: colour = colour_options[1]
                elif friction > 0.42: colour = colour_options[2]
                elif friction > 0.26: colour = colour_options[3]
                else: colour = colour_options[4]
                height = random.uniform(max(0.0, prev_height-0.1),min(0.0+height_coeff, prev_height+0.1))
                # print(height)
                prev_height = height
                # length = random.uniform(0.3,1 - 0.5)
                length = random.uniform(0.8 - length_coeff,1 - length_coeff)
                size = [length, 0.3, height+0.001]
                if i == 0:
                    position =  [1 + size[0], self.y_offset, size[2] + 0.01]
                else:
                    position =  [positions[i-1][0] + sizes[i-1][0] + size[0], self.y_offset, size[2] + 0.01]
                boxIds.append(self.insert_box(position, [0,0,0], size, colour=colour))
                p.changeDynamics(boxIds[i],-1,lateralFriction=friction, spinningFriction=friction, rollingFriction=0.1)
                positions.append(position)
                sizes.append(size)
                frictions.append(friction)
        return [boxIds, positions, sizes, frictions]

    def insert_doorway(self, pos, orn, width):
        # print(width)
        door_size = 0.75
        wall_size = [0.025, 0.125-width, 0.75]
        p1, p2, p3 = pos[0], pos[1]+door_size/2, pos[2]
        b1 = self.insert_box([p1,p2+self.y_offset,p3], orn, wall_size)
        wall_size = [0.025, 0.125+width, 0.75]
        p1, p2, p3 = pos[0], pos[1]-door_size/2, pos[2]
        b2 = self.insert_box([p1,p2+self.y_offset,p3], orn, wall_size)
        return b1, b2

    def insert_doorways(self, door_size=0.05, rs=None):
        if rs is None:
            # rs = [random.random() for _ in range(5)]
            options = [[0.5,0.5,0.1,0.5,0.9,0.5],
                    [0.5,0.1,0.5,0.9,0.5,0.5],
                    [0.5,0.1,0.5,0.5,0.9,0.5],
                    [0.5,0.5,0.9,0.5,0.1,0.5],
                    [0.5,0.9,0.5,0.1,0.5,0.5],
                    [0.5,0.9,0.5,0.5,0.1,0.5]]
            rs = random.sample(options,1)[0]

        b = []
        for i, r in enumerate(rs):
            if r < 0.3: sign = -1
            elif r > 0.6: sign = 1
            else: sign = 0
            orn = [0,0,0]
            # pos = [1+i*1.5, 0.1*sign, 0.75]
            pos = [1+i*2, door_size*sign, 0.75]
            b.append(self.insert_doorway(pos, orn, pos[1]))
        return rs, b

    def insert_floor(self):

        groundHalfLength = 5
        groundHalfWidth = 0.5
        groundHalfHeight = 0.05

        # segmentLength = 5
        mass = 1
        visualShapeId = -1
        segmentStart = 0
        q = p.getQuaternionFromEuler([0,0,0])
        new = p.createCollisionShape(p.GEOM_BOX,halfExtents=[groundHalfLength,groundHalfWidth,groundHalfHeight])
        return p.createMultiBody(baseMass=0,baseCollisionShapeIndex = new,basePosition = [groundHalfLength-1,self.y_offset, -0.04], baseOrientation=q)


    def insert_floor_seg(self, pos, orn, size,i, friction=0.9, softness=None):

        slip = 0.9
        soft = 0.9
        if i != 0:

            slip = np.random.rand(1)
            soft = np.random.rand(1)
        # colour = random.sample([[1,0.2,0.2,1],[0.2,1,0.2,1],[0.2,0.2,1,1]], 1)[0]
        # colour = random.sample([[1,0.2,0.2,1],[0.2,1,0.2,1],[0.2,0.2,1,1]], 1)[0]
        print(slip, soft)

        if slip > 0.7: slip = 0.9
        if soft > 0.7: soft = 0.9
        colour = [0.8, 1-slip, 1-soft, np.clip(slip,0.9,1)]
        # colour = [0.5, 0, 0, 1]

        colBoxId = p.createCollisionShape(p.GEOM_BOX, halfExtents=size)
        visBoxId = p.createVisualShape(p.GEOM_BOX, halfExtents=size, rgbaColor=colour)
        q = p.getQuaternionFromEuler(orn)
        body = p.createMultiBody(baseMass=0, baseCollisionShapeIndex = colBoxId, baseVisualShapeIndex = visBoxId,basePosition = pos, baseOrientation=q)

        # bunnyId = p.loadSoftBody("bunny.obj")

        # p.changeDynamics(body,-1,lateralFriction=0.9, spinningFriction=0.1, rollingFriction=0.1)
        p.changeDynamics(body,-1,lateralFriction=slip, spinningFriction=slip, rollingFriction=0.1)

    def create_seg_floor(self):
        nb_segs = 10
        floor_length = 15
        length_first_seg = 1
        segs = [length_first_seg]
        rest = np.random.random(nb_segs-1)
        # print(segs)
        rest /= sum(rest)/(floor_length/2 - length_first_seg)
        segs.extend(rest)
        pos_x = segs[0] - 1
        for i in range(nb_segs):
            length = segs[i]
            width = 0.5
            height = 0.05
            pos_y = self.y_offset
            pos_z = -0.04
            self.insert_floor_seg([pos_x, pos_y, pos_z], [0,0,0], [length, width, height],i)
            if i < nb_segs-1:
                pos_x = pos_x + segs[i] + segs[i+1]

    def insert_slopes_and_blocks(self, pos, orn, size,i, friction=0.9, softness=None):
        slip = 0.9
        soft = 0.9
        colour = [0.8,0.2,0.2,1]

        var1 = [-0.07966193, -0.05976184,  0.03328811, -0.0237603 ,  0.0728631 ,
        0.0714272 ,  0.0329268 ,  0.08790418,  0.01429615,  0.09085933,
        -0.05950714,  0.00955094, -0.06922905,  0.09239556, -0.06460182,
        0.05924719,  0.09841543,  0.04504866, -0.08426859,  0.01059466]
        var2 = [0.066857  , 0.05876799, 0.08464971, 0.07351712,  0.05061032,
        0.03537836,  0.08825955,  0.02922436, -0.01514781, -0.04271312,
        0.01244085, -0.01003935,  0.02707815,  0.01936557,  0.01361162,
        0.05929006, 0.0251214 , 0.02269067, -0.00102385, -0.00220988]
        var3 = [-0.05561428, -0.07052667, -0.06429269, -0.02204357,  0.07757174,
        -0.02324822,  0.05596848,  0.00097226,  0.08329649,  0.04830461,
        -0.07099909, -0.02005697,  0.06700455,  0.04264556, -0.05697414,
        0.07894493, -0.02617598, -0.04067026,  0.07092011,  0.04885414]
        var4 = [ 0.06703684,  0.02866392,  0.06802942, -0.07989179,  0.04396196,
        0.04390753,  0.08620727,  0.07887236, -0.03753709, -0.08261022,
        -0.07453294, -0.02023046,  0.08303315,  0.05094449, -0.03121727,
        -0.01355843, -0.04578197, -0.07502652,  0.01648281, -0.01735465]
        # print(var1)
        if i != 0:
            # size[1] += (np.random.rand()-0.5)/5
            # size[2] += (np.random.rand()-0.5)/5
            # orn[0] += (np.random.rand()-0.5)/5
            # orn[1] += (np.random.rand()-0.5)/5
            # print(i)
            size[1] += var1[i]
            size[2] += var2[i]
            orn[0] += var3[i]
            orn[1] += var4[i]

            slip = np.random.rand(1)
            soft = np.random.rand(1)
            colour = random.sample([[0.8,0.2,0.2,1],[0.2,0.8,0.2,1],[0.2,0.2,0.8,1]], 1)[0]
            # colour = random.sample([[1,0.2,0.2,1],[0.2,1,0.2,1],[0.2,0.2,1,1]], 1)[0]
            # print(slip, soft)
            slip = 0.9
            soft = 0.9

        if slip > 0.7: slip = 0.9
        if soft > 0.7: soft = 0.9
        # colour = [0.8, 1-slip, 1-soft, np.clip(slip,0.9,1)]

        colBoxId = p.createCollisionShape(p.GEOM_BOX, halfExtents=size)
        visBoxId = p.createVisualShape(p.GEOM_BOX, halfExtents=size, rgbaColor=colour,specularColor=[0.4,.4,0])
        q = p.getQuaternionFromEuler(orn)
        body = p.createMultiBody(baseMass=0, baseCollisionShapeIndex = colBoxId, baseVisualShapeIndex = visBoxId,basePosition = pos, baseOrientation=q)

        # bunnyId = p.loadSoftBody("bunny.obj")

        # p.changeDynamics(body,-1,lateralFriction=0.9, spinningFriction=0.1, rollingFriction=0.1)
        p.changeDynamics(body,-1,lateralFriction=slip, spinningFriction=slip, rollingFriction=0.1)

    def create_rand_floor(self):
        nb_segs = 20
        floor_length = 15
        length_first_seg = 1
        segs = [length_first_seg]
        rest = np.random.random(nb_segs-1)
        # print(segs)
        rest /= sum(rest)/(floor_length/2 - length_first_seg)
        segs.extend(rest)
        pos_x = segs[0] - 1
        for i in range(nb_segs):
            length = segs[i]
            width = 0.5
            height = 0.05
            pos_y = self.y_offset
            pos_z = -0.03
            # pos_z = 0.01
            self.insert_slopes_and_blocks([pos_x, pos_y, pos_z], [0,0,0], [length, width, height],i)
            if i < nb_segs-1:
                pos_x = pos_x + segs[i] + segs[i+1]

    def insert_line(self, speed=None):

        colour = [0.5,0.5,0.5,1]
        if self.colour and speed is not None:
            speed = np.clip(speed,0.5,1.5)
            colour[0] = (speed - 0.5)/(1.5 - 0.5)

        boxHalfLength = 10
        boxHalfWidth = 0.01
        boxHalfHeight = 0.05

        groundHalfLength = 2.5
        groundHalfWidth = 1
        groundHalfHeight = 0.1

        # segmentLength = 5
        mass = 1
        visualShapeId = -1
        segmentStart = 0
        q = p.getQuaternionFromEuler([0,0,0])
        new = p.createCollisionShape(p.GEOM_BOX,halfExtents=[boxHalfLength,0.3,boxHalfHeight])
        vis_shape1 = p.createVisualShape(p.GEOM_BOX,halfExtents=[boxHalfLength,0.3,boxHalfHeight], rgbaColor=colour)
        b1 = p.createMultiBody(baseMass=0,baseCollisionShapeIndex = new,baseVisualShapeIndex=vis_shape1, basePosition = [boxHalfLength-1,self.y_offset, -0.04], baseOrientation=q)
        return b1
        # vis_shape2 = p.createVisualShape(p.GEOM_BOX,halfExtents=[boxHalfLength,0.15,boxHalfHeight],rgbaColor=[0.8,0.2,0.2,1])
        # b2 = p.createMultiBody(baseMass=0,baseCollisionShapeIndex = new,baseVisualShapeIndex=vis_shape2, basePosition = [boxHalfLength-1,0.16+self.y_offset, -0.04], baseOrientation=q)
        # return b1, b2
    def add_house(self):
        boxIds = []
        positions = []; sizes = []; frictions = []; colours = []
        nb_boxes = 5
        # num_steps = 10
        length_coeff, height_coeff = 0.2, 0.08
        # nb_boxes = int(num_steps/(length_coeff*2))
        # print(nb_boxes)
        frictions = [0.9]*nb_boxes
        colour = [0.5,0.5,0.5,1]
        y_offset = 1.5
        width = 0.4
        # Stairs
        base_height = 0.5        
        size = [2.05, y_offset+width, base_height/2 + 0.001]       
        position =  [size[1]/2 - width, 0, size[2] + 0.01]
        colour = [np.random.random(),np.random.random(),np.random.random(),1]
        boxIds.append(self.insert_box(position, [0,0,0], size, colour=colour))
        positions.append(position)
        sizes.append(size)
        colours.append(colour)

        # base_height = size[2] + position[2]
        for j in range(2):
            y_offset *= -1
            prev_height = 0
            # print(y_offset)
            for i, box in enumerate(range(nb_boxes)):
                height = prev_height+height_coeff
                prev_height = height
                length = length_coeff
                size = [length, width, height+0.001]
                if i == 0:
                    position =  [size[0]-length, y_offset, size[2] + 0.01 + base_height]
                else:
                    position =  [positions[-1][0] + sizes[-1][0] + size[0], y_offset, size[2] + 0.01 + base_height]
                # if i % 2 == 0:
                    # colour = [0.5,0.5,0.5,1] 
                # else:
                colour = [np.random.random(),np.random.random(),np.random.random(),1]
                boxIds.append(self.insert_box(position, [0,0,0], size, colour=colour))
                positions.append(position)
                sizes.append(size)
                colours.append(colour)

            if j == 1: # Top
                # size = [0.2, y_offset+width, height+height_coeff+0.001]       
                size = [0.4, y_offset+width, height+0.001]       
                position =  [positions[-1][0] + sizes[-1][0] + size[0], 0, size[2] + 0.01 + base_height]
                colour = [np.random.random(),np.random.random(),np.random.random(),1]
                boxIds.append(self.insert_box(position, [0,0,0], size, colour=colour))
                positions.append(position)
                sizes.append(size)
                colours.append(colour)


        return [boxIds, positions, sizes, frictions, colours]


def run(obstacles=None):

    # p.connect(p.GUI)
    # obstacles = Obstacles()
    # p.resetSimulation()
    # p.loadMJCF(currentdir + "/ground.xml")
    # p.setGravity(0,0,-9.8)
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--urdf', default=True,action='store_false')
    parser.add_argument('--display', default=True,action='store_false')
    parser.add_argument('--balance', default=False,action='store_true')
    args = parser.parse_args()

    from env import Env
    env = Env()
    env.arguments(RENDER=True, record_step=False, args=args)
    env.reset()
    # env.obstacles.add_house(
    # env.obstacles.up_down_flat(height_coeff = 0.07, rand=False, order=[np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(30)], add_walls=False)
    ob = Obstacles()
    ob.up_down_flat(height_coeff = 0.07, rand=False, order=[np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(30)], add_walls=False)
    # pos = [1., -3.8, 0.0]
    # orn = [np.pi/2, 0.0, 0.0]
    # # Length, Width, Height (half size)
    # size = [1, 0.1, 0.1]
    # obstacles.insert_stairs(pos, orn, size)

    # env = Env()
    # env.arguments(record_step=False, down_stairs=False, up_stairs=True)

    # env.height_coeff = 0.08
    # env.obstacles.up_down_flat(rand=True)

    # action_idx = 4
    # env.up_down_flat_arg = seq[action_idx]


    # print(env.z_offset, env.box_info[2][0][2]+env.box_info[1][0][2], env.box_info[2][1][2]+env.box_info[1][1][2])
    while True:
    	time.sleep(0.01)

if __name__ == "__main__":
	obstacles = Obstacles()
	run(obstacles)
	# run()
