import sys
sys.path.append('../../brendan')
import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import pybullet as p
import gym
from gym import spaces
import time
import numpy as np
from numpy import cos as c
from numpy import sin as s
from gym.utils import seeding
from collections import deque
import cv2
from assets.obstacles import Obstacles
import copy
import random
from scipy.spatial.transform import Rotation as R
from mpi4py import MPI


class Env(gym.Env):
    # 44 state, 1 vx control
    # ob_size = 58
    # ob_size = 94
    # ob_size = 79
    ob_size = 65
    # ob_size = 67
    # ob_size = 70
    ac_size = 18
    im_size = [48,48,4]
    # ac_size = 9
    
    # Sim contact solver, default 240Hz. 
    simtimeStep = 1/240
    timeStep = 1/60
    # Gazebo seems limited to 60Hz, so add actionRepeat
    actionRepeat = 4


    initial_reward_flag = True
    initial_reward = 0
    total_reward = 0
    Kp = Kd = 400
    exp_target = -0.28
    initial_Kp = 400
    Kd_x = Kp_coxa = Kd
    steps = 0
    total_steps = 0
    box_info = None
    height_coeff = 0.0
    # height_coeff = 0.1
    step_length = 0.6
    speed = 1.5
    speed_range = 0
    # height_coeff = 0.1
    frict_coeff = 0.0
    # length_coeff = 0.0
    length_coeff = 0.3
    fov = 60
    nearPlane = 0.01
    farPlane = 1000
    aspect = 1
    episodes = -1
    sample_pointer = 0
    sum_pos_errors = 0
    yaw_limit = 0
    max_yaw = 0
    min_v = 0.5
    max_v = 1.5
    x = 1
    total_pos_errors = []
    cur_counter = 0
    projectionMatrix = p.computeProjectionMatrixFOV(fov, aspect, nearPlane, farPlane);
    ep_lens = deque(maxlen=5)
    ep_rets = deque(maxlen=5)
    mean_pos_errors = deque(maxlen=5)
    des_roll = 0
    def __init__(self, render=False, PATH=None, MASTER=True, cur=False, down_stairs=False, up_stairs=False, disturbances=False, record_step=True, camera_rotation=80, variable_start=False, add_flat=False, colour=False, y_offset=0.0, z_offset=0.0, speed_cur=False, other_rewards = False, control=True, up_down_flat=False, limit=False, horizontal=False, torque=True, dm_reward=True, knobs=False, args=None, add_friction=False, display=False, use_weights=False, expert=None, exp_env=None):
        self.expert = expert
        self.exp_env = exp_env
    # def arguments(self, ):
        # if up_stairs or down_stairs:
        #     self.control = False
        # else:
        self.use_weights = use_weights
        self.use_act_pol = False
        self.add_friction = add_friction
        self.args = args
        self.knobs = knobs
        self.horizontal = horizontal
        self.limit = limit
        self.torque_control = torque
        # if self.torque_control:
        #     self.rew_Kp = 0.1
        # else4
        self.rew_Kp = 0.2

        self.dm_reward = dm_reward
        self.control = control
        if self.control:
            self.ob_size += 1 
        
        high = 5*np.ones(self.ac_size)
        low = -high
        self.action_space  = spaces.Box(low, high)

        high = np.inf*np.ones(self.ob_size)
        low = -high
        self.observation_space = spaces.Box(low,high)
        # self.arguments(False, False, '4')
        self.colour = colour
        self.render = render
        self.record_step = record_step
        self.camera_rotation = camera_rotation
        self.PATH = PATH
        self.cur = cur
        self.down_stairs = down_stairs
        self.up_stairs = up_stairs
        self.speed_cur = speed_cur
        self.other_rewards = other_rewards
        self.disturbances = disturbances
        self.variable_start = variable_start
        self.up_down_flat = up_down_flat
        self.add_flat = add_flat
        self.y_offset = y_offset
        self.z_offset = z_offset
        # self.obstacles = Obstacles(y_offset=self.y_offset, add_friction=add_friction, colour=self.colour)
        self.MASTER = MASTER
        if self.render and self.MASTER:
        	self.physicsClientId = p.connect(p.GUI)
        else:
            self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the running gait
        self.sim_data = []
        self.best_reward = -100

        # Right and Left Joint positions, velocities, comz position, base velocities, orientation and velocity, feet contacts, prev feet contacts
        self.state = [  "AR_coxa_joint_pos","AR_femur_joint_pos","AR_tibia_joint_pos",
                        "AL_coxa_joint_pos","AL_femur_joint_pos","AL_tibia_joint_pos",
                        "BR_coxa_joint_pos","BR_femur_joint_pos","BR_tibia_joint_pos",
                        "BL_coxa_joint_pos","BL_femur_joint_pos","BL_tibia_joint_pos",
                        "CR_coxa_joint_pos","CR_femur_joint_pos","CR_tibia_joint_pos",
                        "CL_coxa_joint_pos","CL_femur_joint_pos","CL_tibia_joint_pos"]
        self.state += [ "AR_coxa_joint_vel","AR_femur_joint_vel","AR_tibia_joint_vel",
                        "AL_coxa_joint_vel","AL_femur_joint_vel","AL_tibia_joint_vel",
                        "BR_coxa_joint_vel","BR_femur_joint_vel","BR_tibia_joint_vel",
                        "BL_coxa_joint_vel","BL_femur_joint_vel","BL_tibia_joint_vel",
                        "CR_coxa_joint_vel","CR_femur_joint_vel","CR_tibia_joint_vel",
                        "CL_coxa_joint_vel","CL_femur_joint_vel","CL_tibia_joint_vel"]
        self.state += [ "AR_coxa_joint_effort","AR_femur_joint_effort","AR_tibia_joint_effort",
                        "AL_coxa_joint_effort","AL_femur_joint_effort","AL_tibia_joint_effort",
                        "BR_coxa_joint_effort","BR_femur_joint_effort","BR_tibia_joint_effort",
                        "BL_coxa_joint_effort","BL_femur_joint_effort","BL_tibia_joint_effort",
                        "CR_coxa_joint_effort","CR_femur_joint_effort","CR_tibia_joint_effort",
                        "CL_coxa_joint_effort","CL_femur_joint_effort","CL_tibia_joint_effort"]
        # self.state += [ "AR_coxa_joint_prev_act","AR_femur_joint_prev_act","AR_tibia_joint_prev_act",
        #                 "AL_coxa_joint_prev_act","AL_femur_joint_prev_act","AL_tibia_joint_prev_act",
        #                 "BR_coxa_joint_prev_act","BR_femur_joint_prev_act","BR_tibia_joint_prev_act",
        #                 "BL_coxa_joint_prev_act","BL_femur_joint_prev_act","BL_tibia_joint_prev_act",
        #                 "CR_coxa_joint_prev_act","CR_femur_joint_prev_act","CR_tibia_joint_prev_act",
        #                 "CL_coxa_joint_prev_act","CL_femur_joint_prev_act","CL_tibia_joint_prev_act"]

        # self.state += ["AR_foot_link_left_ground","AL_foot_link_left_ground","BR_foot_link_left_ground",
        #                 "BL_foot_link_left_ground","CR_foot_link_left_ground","CL_foot_link_left_ground"]
        # self.state += ["prev_AR_foot_link_left_ground","prev_AL_foot_link_left_ground","prev_BR_foot_link_left_ground",
        #                 "BL_foot_link_left_ground","prev_CR_foot_link_left_ground","prev_CL_foot_link_left_ground"]
        # if self.horizontal:
        #     self.gait_type = 'not_tripod'
        # else:
        self.gait_type = 'tripod'
        # if self.gait_type == 'tripod':
        # self.state += ["left_swing", "right_swing"]
        # else:
        #     self.ob_size += 1
        #     self.state += ["left_swing", "middle_swing", "right_swing"]

        # Needs negatives when inverting:
        self.state += ['qx','qy','qz','qw']
        self.state += ['ax','ay','az']
        self.state += ['roll_vel','pitch_vel','yaw_vel']
        # self.state = [  "AR_coxa_joint_pos","AR_femur_joint_pos","AR_tibia_joint_pos",
        #                 "AL_coxa_joint_pos","AL_femur_joint_pos","AL_tibia_joint_pos",
        #                 "BR_coxa_joint_pos","BR_femur_joint_pos","BR_tibia_joint_pos",
        #                 "BL_coxa_joint_pos","BL_femur_joint_pos","BL_tibia_joint_pos",
        #                 "CR_coxa_joint_pos","CR_femur_joint_pos","CR_tibia_joint_pos",
        #                 "CL_coxa_joint_pos","CL_femur_joint_pos","CL_tibia_joint_pos"]
        # self.state += [ "AR_coxa_joint_vel","AR_femur_joint_vel","AR_tibia_joint_vel",
        #                 "AL_coxa_joint_vel","AL_femur_joint_vel","AL_tibia_joint_vel",
        #                 "BR_coxa_joint_vel","BR_femur_joint_vel","BR_tibia_joint_vel",
        #                 "BL_coxa_joint_vel","BL_femur_joint_vel","BL_tibia_joint_vel",
        #                 "CR_coxa_joint_vel","CR_femur_joint_vel","CR_tibia_joint_vel",
        #                 "CL_coxa_joint_vel","CL_femur_joint_vel","CL_tibia_joint_vel"]

        self.state += ['com_z']
        # self.state += ['vx','vz','pitch','pitch_vel']

        # # Needs negatives when inverting:
        # self.state += ['vy','roll','yaw']
        # self.state += ['roll_vel','yaw_vel']

        # # Not continous values, don't normalise ================================
        # self.state += ["AR_foot_link_left_ground","AL_foot_link_left_ground","BR_foot_link_left_ground",
        #                 "BL_foot_link_left_ground","CR_foot_link_left_ground","CL_foot_link_left_ground"]
        # self.state += ["prev_AR_foot_link_left_ground","prev_AL_foot_link_left_ground","prev_BR_foot_link_left_ground",
        #                 "BL_foot_link_left_ground","prev_CR_foot_link_left_ground","prev_CL_foot_link_left_ground"]
        # self.state += ["left_swing", "right_swing"]
        # self.state += ['left_foot_left_ground', 'right_foot_left_ground']
        # self.state += ['swing_foot', 'walk', 'phase']
        # self.state += ['walk']
        
        
        # Previous trajectory targets
        # self.state += ['ready_to_walk']
        # Y states, and roll and yaw that need to be negatived..
        # States that need to have logic switched: State flags, left = 0, right = 1
        # self.state += ['swing_foot']
        #=======================================================================
        # State flags, left = 0, right = 1
        # ======================================================================
        # self.sample_nb = 6
        # self.sample_max = self.sample_nb
        if self.expert:
            self.samples = np.load("samples/expert_samples.npy")
        else:
            self.saved_state = []
        self.reward_breakdown = {'positive':deque(maxlen=1000), 'negative':deque(maxlen=1000), 'negative2':deque(maxlen=1000), 'symmetry':deque(maxlen=1000), 'expert':deque(maxlen=200)}

    def load_model(self):

        if self.MASTER: p.loadMJCF(currentdir + "/ground.xml")
        # objs = p.loadMJCF(currentdir + "/mybot.xml",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        # objs = p.loadMJCF(currentdir + "/mybot.xml",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
        # print(currentdir + "/robot.urdf")
        # print(currentdir + "/biped.urdf")
        # objs = p.loadURDF(currentdir + "/biped.urdf",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
        # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
        # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION + p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        # objs = p.loadURDF(currentdir + "/robot2.urdf",flags = p.URDF_USE_SELF_COLLISION + p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        objs = p.loadURDF(currentdir + "/robot2.urdf")
        # objs = p.loadURDF(currentdir + "/robot.urdf")
        # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION)
        # print(objs)
        self.Id = objs
        # print(objs)
        p.setTimeStep(self.simtimeStep)
        p.setGravity(0,0,-9.8)
        # ======================================================================
        # time.sleep(5)
        numJoints = p.getNumJoints(self.Id)
        
        # Camera following robot:
        # body_xyz, (qx, qy, qz, qw) = p.getBasePositionAndOrientation(self.Id)
        # p.resetDebugVisualizerCamera(2.0, self.camera_rotation, -10.0, body_xyz)
        # p.resetDebugVisualizerCamera(2.0, 50, -10.0, body_xyz)

        self.jdict = {}
        self.feet_dict = {}
        self.leg_dict = {}

        # self.feet = ["left_heel1", "left_heel2", "right_heel1", "right_heel2", "left_toe", "right_toe"]
        self.feet = ["AR_foot_link","AL_foot_link","BR_foot_link",
                    "BL_foot_link","CR_foot_link","CL_foot_link"]
        self.tibia_links = ["AR_tibia_link","AL_tibia_link","BR_tibia_link",
                    "BL_tibia_link","CR_tibia_link","CL_tibia_link"]
        
        self.feet_contact = {f:True for f in self.feet}
        self.tibias = {}
        self.ordered_joints = []
        self.ordered_joint_indices = []
        for j in range( p.getNumJoints(self.Id) ):
            info = p.getJointInfo(self.Id, j)
            link_name = info[12].decode("ascii")
            if link_name in self.feet: self.feet_dict[link_name] = j
            if link_name in self.tibia_links: self.tibias[link_name] = j
            self.ordered_joint_indices.append(j)
            if info[2] != p.JOINT_REVOLUTE: continue
            jname = info[1].decode("ascii")
            # print("loaded joint ", jname)

            # print(jname)
            lower, upper = (info[8], info[9])
            p.enableJointForceTorqueSensor(self.Id, j, True)
            self.ordered_joints.append( (j, lower, upper) )
            self.jdict[jname] = j
        
        self.tip_pos = {t:-1 for t in self.feet_dict} 
        self.motor_names = [
                            "AR_coxa_joint","AR_femur_joint","AR_tibia_joint",
                            "AL_coxa_joint","AL_femur_joint","AL_tibia_joint",
                            "BR_coxa_joint","BR_femur_joint","BR_tibia_joint",
                            "BL_coxa_joint","BL_femur_joint","BL_tibia_joint",
                            "CR_coxa_joint","CR_femur_joint","CR_tibia_joint",
                            "CL_coxa_joint","CL_femur_joint","CL_tibia_joint"]


        # self.motor_power =  [10, 15, 10]
        # self.motor_power += [10, 15, 10]
        # self.motor_power += [10, 15, 10]
        # self.motor_power += [10, 15, 10]
        # self.motor_power += [10, 15, 10]
        # self.motor_power += [10, 15, 10]
        self.motor_power =  [15, 22, 15]
        self.motor_power += [15, 22, 15]
        self.motor_power += [15, 22, 15]
        self.motor_power += [15, 22, 15]
        self.motor_power += [15, 22, 15]
        self.motor_power += [15, 22, 15]
        # self.motor_power =  [20, 30, 20]
        # self.motor_power += [20, 30, 20]
        # self.motor_power += [20, 30, 20]
        # self.motor_power += [20, 30, 20]
        # self.motor_power += [20, 30, 20]
        # self.motor_power += [20, 30, 20]
        self.vel_max = [8,11,8]
        self.vel_max += [8,11,8]
        self.vel_max += [8,11,8]
        self.vel_max += [8,11,8]
        self.vel_max += [8,11,8]
        self.vel_max += [8,11,8]
        self.tor_max = [80,112,80]
        self.tor_max += [80,112,80]
        self.tor_max += [80,112,80]
        self.tor_max += [80,112,80]
        self.tor_max += [80,112,80]
        self.tor_max += [80,112,80]

        self.motors = [self.jdict[n] for n in self.motor_names]
        forces = np.ones(len(self.motors))*240
        self.actions = {key:0.0 for key in self.motor_names}
        
        # Disable motors to use torque control:
        # if self.torque_control:
        # if not self.use_act_pol:
        p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

        # Increase the friction on the feet, we have heaps of friction at our feet.
        for key in self.feet_dict:
            p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9, rollingFriction=0.1)

        self.restore()


    def load_blocks(self):
        if (self.up_stairs or self.down_stairs or self.up_down_flat or self.horizontal) and self.reset_sim and not self.episodes == 0:
            for b in self.box_info[0]:
                p.removeBody(b)
        if self.episodes == 0 or self.reset_sim:
            if self.up_down_flat:
                self.up_down_flat_arg = [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(40)] 
                # self.height_coeff = 0.03
                self.length_coeff = np.random.uniform(0.2, 0.26)       
                height_coeff = self.height_coeff + (random.random()/200 - 0.0025)

                self.box_info = self.obstacles.up_down_flat(height_coeff = height_coeff, length_coeff=self.length_coeff,order=self.up_down_flat_arg, add_walls=self.args.add_walls)
                self.world_map = self.get_world_map(self.box_info, world_shape=[30,10], grid_size=0.05)
            else:
                if self.up_stairs:
                    # order = ['up']*np.random.randint(1,5) + ['down'] + ['up']*40
                    order = ['up']*45
                elif self.down_stairs:
                    # order = ['down']*np.random.randint(1,5) + ['up'] + ['down']*40
                    order = ['down']*45
                    # order = ['down']*30
                # elif self.bump:
                #     order = ['down']*10
                #     order = [['down','down','up','up','flat'],['up','up','down','down','flat'],['down','down','up','down','flat'],['down','down','down','up','flat']]

                else:
                    order = ['flat']*45  
                self.length_coeff = np.random.uniform(0.2, 0.26)       
                if self.height_coeff > 0:
                    height_coeff = self.height_coeff + (random.random()/200 - 0.0025)
                    # height_coeff = self.height_coeff 
                else:
                    height_coeff = 0
                # height_coeff = 0.03
                if (self.up_stairs or self.down_stairs or self.up_down_flat or self.horizontal) and not self.args.display:
                    if self.horizontal and not (self.up_stairs or self.down_stairs or self.up_down_flat):
                        height_coeff = 0.01
                        order = [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(40)] 
                        self.box_info = self.obstacles.up_down_flat(height_coeff=height_coeff,length_coeff=self.length_coeff, order=order)
                    else:
                        self.box_info = self.obstacles.up_down_flat(height_coeff=height_coeff,length_coeff=self.length_coeff, order=order)
                    self.world_map = self.get_world_map(self.box_info, world_shape=[30,10], grid_size=0.05)
                else:
                    # self.box_info = self.obstacles.up_down_flat(height_coeff=height_coeff,length_coeff=self.length_coeff, order=order)
                    if self.knobs:
                        self.obstacles.knobs()
                    self.box_info = [[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]], [[0,0,0],[0,0,0]], [[0,0,0],[0,0,0]]] 
                    self.world_map = None
            # Get the world height map using box_info (positions and sizes of shapes). World_shape and grid_size
            # in meters

        self.z_offset = 0
        self.box_num = 0

        self.body_xyz, _ = p.getBasePositionAndOrientation(self.Id)
        if self.box_info is not None:
            if len(self.box_info[0]) > (self.box_num+1) and self.body_xyz[0] > (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]):
                self.box_num += 1
                self.stepped_on_box = False
                self.second_step_on_box = False
            if self.box_num > 0 and (self.box_info[1][self.box_num][2] - self.box_info[1][self.box_num-1][2]) > 0 and (self.body_xyz[0] < (self.box_info[1][self.box_num][0])):
                self.z_offset = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
            else:
                self.z_offset = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]

    def get_elevation_and_friction(self):
        # [boxIds, positions, sizes, frictions, colour/]
        if self.args.display or not (self.up_stairs or self.down_stairs or self.up_down_flat):
            frictions, elevations = [0.9]*4, [0.0]*4
        else:
            frictions = []
            elevations = []
            for i in range(self.box_num,4+self.box_num):
                frictions.append(self.box_info[3][i])
                elevation = (self.box_info[1][i][2] + self.box_info[2][i][2]) - (self.box_info[1][i-1][2] + self.box_info[2][i-1][2])
                elevations.append(elevation)
        # print(self.box_num, self.box_info)
        # print(frictions, elevations, len(frictions + elevations))
        return frictions + elevations

    def restore(self, sample=None):
        self.load_blocks()

        # Restore state every second time (need to always pick a state to restore starting from 0).
        # print(self.variable_start and self.episodes % 2 == 0 and self.episodes != 0 and self.saved_state)
        if self.variable_start and self.episodes % 2 == 0 and self.episodes != 0 and self.saved_state:
            pos, orn, joints, base_vel, joint_vel, saved_dict, others = self.saved_state
            self.ob_dict = saved_dict
            self.box_num, self.cur_foot, self.steps = others
            # [pos, orn, rot, vel, joint, joint_vel]
            # c = [0.01, 0.001, 0.01, 0.01, 0.0, 0.0]
            # c = [0.015, 0.006, 0.012, 0.4, 0.01, 0.01]
            # c = [0.015, 0.006, 0.012, 0.4, 0.25, 0.2]
            c = [0.015, 0.006, 0.012, 0.4, 0.15, 0.1]
            self.saved_state = []
        else:
            self.ob_dict = {"AR_foot_link_left_ground":True,"AL_foot_link_left_ground":True,"BR_foot_link_left_ground":True,
                        "BL_foot_link_left_ground":True,"CR_foot_link_left_ground":True,"CL_foot_link_left_ground":True}
            self.ob_dict.update({"prev_AR_foot_link_left_ground":True,"prev_AL_foot_link_left_ground":True,"prev_BR_foot_link_left_ground":True,
                        "prev_BL_foot_link_left_ground":True,"prev_CR_foot_link_left_ground":True,"prev_CL_foot_link_left_ground":True})
            self.ob_dict.update({"left_foot_left_ground":True,"right_foot_left_ground":True})
            self.ob_dict.update({'roll':0.0,'pitch':0.0,'yaw':0.0})
            self.ob_dict['ready_to_walk'] = False
            if self.gait_type == 'tripod':
                self.ob_dict.update({'left_swing':False, 'right_swing':True})
            else:
                self.ob_dict.update({'left_swing':False, 'middle_swing':False, 'right_swing':True})
            self.ob_dict['walk'] = False
            self.prev_vx, self.prev_vy, self.prev_vz = 0, 0, 0
            self.cur_foot = 'right'
            # pos, orn, joints, base_vel, joint_vel = [0,0,self.z_offset+0.4],[0,0,0,1], [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            joints = []
            
            for m in self.motor_names:
                if 'tibia' in m:
                    joints.append(2.4)
                elif 'femur' in m:
                    joints.append(-1.1)
                elif 'AR' in m or 'CL' in m:
                    joints.append(0.4)
                elif 'AL' in m or 'CR' in m:
                    joints.append(-0.4)
                else:
                    joints.append(0.0)

            if self.horizontal:
                # pos, orn, joints, base_vel, joint_vel = [0.5,0,self.z_offset+0.5],[0,0,-0.707, 0.707], joints, [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
                # pos, orn, joints, base_vel, joint_vel = [0.5,0,self.z_offset+0.15],[0,0,-0.707, 0.707], joints, [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
                pos, orn, joints, base_vel, joint_vel = [0.8,0,self.z_offset+0.15],[0,0,-0.707, 0.707], joints, [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            else:
                pos, orn, joints, base_vel, joint_vel = [0.8,0,self.z_offset+0.15],[0,0,0,1], joints, [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
                # pos, orn, joints, base_vel, joint_vel = [0.5,0,self.z_offset+0.4],[0,0,0,1], joints, [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            self.sample = random.sample([_ for _ in range(150)], 1)[0]
            self.save_state = []
            # [pos, orn, rot, vel, joint, joint_vel]
            # c = [0.015, 0.006, 0.012, 0.4, 0.01, 0.01]
            # c = [0.015, 0.006, 0.012, 0.4, 0.15, 0.1]
            c = [0.015, 0.006, 0.012, 0.4, 0.1, 0.05]

        if self.disturbances:
            pos[0] += np.random.uniform(low=-c[0], high=c[0])
            pos[1] += np.random.uniform(low=-c[0], high=c[0])
            pos[2] += np.random.uniform(low=-c[0], high=c[0])
            orn[0] += np.random.uniform(low=-c[1], high=c[1])
            orn[1] += np.random.uniform(low=-c[1], high=c[1])
            orn[2] += np.random.uniform(low=-c[1], high=c[1])
            orn[3] += np.random.uniform(low=-c[1], high=c[1])
            for i in range(len(base_vel[0])):
                base_vel[0][i] += np.random.uniform(low=-c[2], high=c[2])
                base_vel[1][i] += np.random.uniform(low=-c[3], high=c[3])
            for i, j in enumerate(self.ordered_joints):
                joints[i] += np.clip(np.random.uniform(low=-c[4], high=c[4]), j[1], j[2])
                joint_vel[i] += np.random.uniform(low=-c[5], high=c[5])
        self.set_position(pos, orn, joints, base_vel, joint_vel)

    def get_sample(self):
        pos = list(self.body_xyz)
        orn = [self.qx, self.qy, self.qz, self.qw]
        joints = [self.ob_dict[m + '_pos'] for m in self.motor_names]
        joint_vel = [self.ob_dict[m + '_vel'] for m in self.motor_names]
        base_vel = [list(self.body_vxyz), list(self.base_rot_vel)]
        saved_dict = self.ob_dict
        others = [self.box_num, self.cur_foot, self.steps]
        self.saved_state = copy.deepcopy([pos, orn, joints, base_vel, joint_vel, saved_dict, others])
        return self.saved_state

    def seed(self, seed=None):
    	self.np_random, seed = seeding.np_random(seed)
    	return [seed]

    def close(self):
    	print("closing")
   
    def reset(self, door_data=None):
        if self.steps != 0 and self.args.plot_act: 
            self.plot_act_pol()
            exit()
        self.exp_cur = {}
        self.exp_cur.update({"AL_coxa_joint":-0.4,"AL_femur_joint":-1.1,"AL_tibia_joint":2.4}) 
        self.exp_cur.update({"AR_coxa_joint":0.4,"AR_femur_joint":-1.1,"AR_tibia_joint":2.4}) 
        self.exp_cur.update({"BL_coxa_joint":0.0,"BL_femur_joint":-1.1,"BL_tibia_joint":2.4}) 
        self.exp_cur.update({"BR_coxa_joint":0.0,"BR_femur_joint":-1.1,"BR_tibia_joint":2.4}) 
        self.exp_cur.update({"CL_coxa_joint":0.4,"CL_femur_joint":-1.1,"CL_tibia_joint":2.4}) 
        self.exp_cur.update({"CR_coxa_joint":-0.4,"CR_femur_joint":-1.1,"CR_tibia_joint":2.4}) 

        self.AL_coxa, self.AL_femur, self.AL_tibia = -0.4, -1.1, 2.4
        self.AR_coxa, self.AR_femur, self.AR_tibia = 0.4, -1.1, 2.4
        self.BL_coxa, self.BL_femur, self.BL_tibia = 0.0, -1.1, 2.4
        self.BR_coxa, self.BR_femur, self.BR_tibia = 0.0, -1.1, 2.4
        self.CL_coxa, self.CL_femur, self.CL_tibia = 0.4, -1.1, 2.4
        self.CR_coxa, self.CR_femur, self.CR_tibia = -0.4, -1.1, 2.4
        

        self.exp_actions = np.zeros(self.ac_size)
        # self.pause_time = 0
        self.pause_time = 100
        self.t1 = time.time()
        self.t2 = time.time()
        self.sum_pos_errors = 0
        # print("===============================================")
        # print(self.prev_speed, self.speed)
        self.foot_count = np.zeros(6)
        if self.expert:
            # self.sample_pointer = np.random.randint(0,5000)
            self.sample_pointer = 0

        self.episodes += 1
        self.cur_foot = 'right'
        self.left_on_box = 0.0
        self.right_on_box = 0.0
        # if self.speed_cur:
            # self.speed = np.random.uniform(low=self.min_v,high=self.max_v)
        self.speed = 0
        
        # self.step_param = 25
        # self.time_of_step = int(self.step_param/self.speed)
        self.time_of_step = 1
        

        self.vel_buffer = deque(maxlen=self.time_of_step)
        self.action_store = deque(maxlen=self.time_of_step)
        self.action_pos_store = deque(maxlen=self.time_of_step)
        # self.height_coeff = 0.05

        if self.steps > 0:
            self.ep_lens.append(self.body_xyz[0])
            # print(self.ep_lens)
            # self.ep_lens.append(self.steps)
            self.ep_rets.append(self.total_reward)
            self.mean_pos_errors.append(np.mean(self.total_pos_errors))
        if self.up_down_flat:
            self.reset_sim = True
        else:
            self.reset_sim = False
        self.cur_counter += self.steps
        # if self.horizontal:
        #     cur_thres = 100
        # else:
        #     cur_thres = 300
        if self.episodes == 1: 
            # print(self.total_reward)
            self.initial_reward = self.total_reward*2
        if self.up_stairs or self.down_stairs or self.up_down_flat:
            # if (self.horizontal and (np.array(self.mean_pos_errors)<cur_thres).all()) or (not self.horizontal and (np.array(self.mean_pos_errors)<cur_thres).all()):
            # if not self.cur and self.steps != 0 and len(self.ep_lens) == 7 and (np.array(self.ep_lens)>500).all() and np.mean(self.episode_speeds) >= 0.9:         
            # if not self.cur and self.steps != 0 and len(self.ep_lens) == 5 and (np.array(self.ep_lens)>3).all():         
            if not self.cur and self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.ep_lens)>4:         

                    # pass
                # if self.steps > 0:
                self.ep_lens = deque(maxlen=5)                
                self.ep_rets = deque(maxlen=5)                
                # if self.height_coeff < 0.07:
                if self.horizontal:
                    if self.height_coeff < 0.07:
                        self.height_coeff += 0.005
                        self.reset_sim = True
                else:
                    if self.height_coeff < 0.05:
                        self.height_coeff += 0.005
                        self.reset_sim = True

        if self.steps != 0 and (self.body_xyz[0] > 5) :
        # if (self.steps*self.timeStep) > 7 and self.steps != 0:
        # if (self.total_reward) > 500 and self.steps != 0:
                self.Kp_coxa *= 0.9
                if self.Kp_coxa < 10: self.Kp_coxa = 0
        comm = MPI.COMM_WORLD
        # if comm.Get_rank() == 0 and self.cur:
            # print(self.steps, self.mean_pos_errors, (np.array(self.ep_lens)<1).all())
            # print(self.cur_counter)

        self.max_cur_len = 50000
        # self.max_cur_len = 6000

        if self.cur:
        
            # if self.speed_cur:
            #     if self.horizontal:
            #         if self.Kp > 220:
            #             exp_target = -0.4
            #         elif self.Kp > 168:
            #             exp_target = -0.45
            #         else:
            #             exp_target = -0.5
            # else:
            # if self.Kp > 300:
            #     self.exp_target = -0.3
            # if self.Kp > 220:
            #     self.exp_target = -0.4
            # elif self.Kp > 94:
            #     self.exp_target = -0.5
            # else:
            self.exp_target = -0.5
            # else:
            # if self.horizontal:
                # exp_target = -0.25 -0.45
            # else:
                # exp_target = -0.3
                # exp_target = 0.25*(self.Kp/self.initial_Kp) - 0.525
            # if self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.ep_lens) > 300: 
            # if (self.horizontal and (np.array(self.mean_pos_errors)<cur_thres).all()) or (not self.horizontal and (np.array(self.mean_pos_errors)<cur_thres).all()):
            # if (self.horizontal and (np.array(self.mean_pos_errors)<cur_thres).all()) or (not self.horizontal and (np.array(self.mean_pos_errors)<cur_thres).all()) or self.cur_counter > self.max_cur_len:
            # if (self.steps != 0 and len(self.ep_lens) == 7 and (np.array(self.ep_lens)>500).all() and np.mean(self.episode_speeds) >= 0.9):         
            # if (self.steps != 0 and len(self.ep_lens) == 5 and (np.array(self.ep_lens)>3).all()):     
            
            # if (self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.ep_lens)>3):     
            # if (self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.ep_lens)>1.5):     
            # if (self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.ep_lens)>1.5 and np.mean(self.reward_breakdown['expert']) > self.exp_target):     
            if (self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.ep_lens)>4):     
            # if (self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.ep_lens)>300):     
            # if (self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.reward_breakdown['expert']) > -0.2):     
            # if (self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.reward_breakdown['expert']) < 0.2):     
            # if (self.steps != 0 and len(self.ep_lens) == 5 and np.mean(self.ep_lens)>3):     
                # print(np.mean(self.reward_breakdown['expert']))
                self.reward_breakdown['expert'] = deque(maxlen=200)
                # if self.rew_Kp < 5:
                #     self.rew_Kp += 0.5
                # # ==================
                # self.rew_Kp = 1
                # ==================

                self.ep_lens = deque(maxlen=5)
                self.ep_rets = deque(maxlen=5)                
                self.mean_pos_errors = deque(maxlen=5)
                # if self.cur_counter > self.max_cur_len:
                #     print("oh no bro! your curriculum sucks..")
                # else:
                #     print("Nice curriculum my dude!")
                # self.cur_counter = 0

                # self.Kp = self.Kd = self.Kd_x = self.initial_Kp*self.x
                # self.Kp = self.Kd = self.Kd_x = self.initial_Kp * (-self.x**2 + 1)

                # self.Kp = self.Kd = self.Kd_x = self.Kp*0.95
                # self.Kp = self.Kd = self.Kd_x = self.Kp*0.8
                # self.Kp = self.Kd = self.Kd_x = self.Kp*0.75
                # self.Kp = self.Kd = self.Kd_x = self.Kp - 25
                self.Kp = self.Kd = self.Kd_x = self.Kp - 16
                if self.Kp < 10:
                    self.Kd_x = self.Kp = self.Kd = 0
                    self.cur = False   


        self.total_pos_errors = []
        self.episode_speeds = []
        if self.record_step:
            if self.best_reward < self.total_reward and self.total_reward != 0:
                self.best_reward = self.total_reward
                self.save_sim_data(best=True)
                self.save_sim_data()
            else:
                self.save_sim_data(best=False)
            self.sim_data = []
        # ======================================================================
        # if self.variable_start and self.episodes % 2 == 0 and self.episodes != 0:
        if self.variable_start and self.episodes != 0:
            self.restore()
        else:
            # if self.episodes == 0 or self.reset_sim:
            if self.episodes == 0:
                if self.MASTER:
                    p.resetSimulation()
                self.load_model()
                if self.exp_env:
                    self.exp_env.reset()
            else:
                self.restore()
        if self.colour:
            self.obstacles.set_colour(self.box_info[0][self.box_num], self.speed)
            self.obstacles.set_colour(self.box_info[0][self.box_num + 1], self.speed)
        self.steps = 0
        self.total_reward = 0
        self.prev_actions = np.zeros(self.ac_size)
        self.zeros = np.zeros(0)
        self.get_observation(np.zeros(self.ac_size))
        self.prev_ob_dict = copy.deepcopy(self.ob_dict)

        # self.prev_state = [self.ob_dict[s] for s in self.state]
        self.prev_state = [self.ob_dict[j + '_pos'] for j in self.motor_names] + [self.ob_dict[j + '_vel'] for j in self.motor_names]
        self.prev_pos_error = [0]*self.ac_size
        self.prev1_pos_error = [0]*self.ac_size
        self.prev2_pos_error = [0]*self.ac_size
        self.prev_pos = [0]*self.ac_size
        self.prev1_pos = [0]*self.ac_size
        self.prev2_pos = [0]*self.ac_size

        self.prev_pos = [0]*self.ac_size

        self.prev_vel = [0]*self.ac_size
        self.prev1_vel = [0]*self.ac_size
        self.prev2_vel = [0]*self.ac_size
        self.prev_effort = [0]*self.ac_size
        self.prev1_effort = [0]*self.ac_size
        self.prev2_effort = [0]*self.ac_size
        # el_and_fr = self.get_elevation_and_friction()
        if self.control:
            # return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed] + el_and_fr))
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed]))
        else:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state]))

    def step(self, actions):
        # print(self.ob_dict['vy'], self.ob_dict['AR_tibia_joint_pos'],self.ob_dict['BL_tibia_joint_pos'],self.ob_dict['CR_tibia_joint_pos'])
        # if self.speed > 0:
        #     print(self.params['c_max'], self.time_of_step, np.mean(self.vel_buffer))
        # print(np.mean(self.vel_buffer))
        # print(self.steps*self.timeStep)
        # if (self.steps*self.timeStep) % 6 == 0:
        if (self.speed_cur and int(self.steps) % 300 == 0 and self.steps != 0 and self.steps > self.pause_time) or self.steps == self.pause_time:
            if not self.speed_cur:
                self.speed = 0.5
                # self.speed = 0.75
            else:
                # if self.cur:
                self.speed = 0 if np.random.random() < 0.05 else np.random.uniform(low=0.3,high=0.75)  

                # else:
                    # self.speed = 0 if np.random.random() < 0.05 else np.random.uniform(low=0.3,high=1.5)           
            if self.speed > 0:
                self.episode_speeds.append(self.speed)

            if self.colour:
                for box in range(self.box_num, len(self.box_info[0])):
                    self.obstacles.set_colour(self.box_info[0][box], self.speed)
            if self.speed > 0:
                if self.gait_type == 'tripod':
                    self.time_of_step = int(-33*self.speed + 35)
                else:
                    self.time_of_step = int(2*(-33*self.speed + 35)/3)
                
                self.vel_buffer = deque(maxlen=self.time_of_step)
                self.action_store = deque(maxlen=self.time_of_step)
                self.action_pos_store = deque(maxlen=self.time_of_step)
                if self.horizontal:
                    self.params = {}
                    # self.params['t_max'] = 0.6*self.speed + 1.42
                    # self.params['t_max'] = 0.36*self.speed + 1.63
                    self.params['t_max'] = 2.1
                    self.params.update({'c_min': 0.0, 'c_max': 0.0})
                    self.params.update({'f_min':-0.05, 'f_max':0.7})
                    self.params.update({'t_min': 1.2, 'b':0.0})
                else:
                    self.params = {}
                    # self.params['c_max'] = 0.6*self.speed + 0.42
                    self.params['c_max'] = 0.9
                    self.params.update({'c_min': 0.0})
                    self.params.update({'f_min':-0.05, 'f_max':0.2})
                    self.params.update({'t_min': 1.55, 't_max':1.8, 'b':(self.params['c_max'] - self.params['c_min'])/2})
            self.orig_speed = self.speed
        elif self.steps < self.pause_time:
            self.speed = 0
            self.params = {}
            self.params['c_max'] = 0.6
            self.params.update({'c_min': 0.0})
            self.params.update({'f_min':-0.05, 'f_max':0.2})
            self.params.update({'t_min': 1.55, 't_max':1.8, 'b':(self.params['c_max'] - self.params['c_min'])/2})
            self.orig_speed = self.speed
        
        # Acceleration at the start
        if self.steps > self.pause_time and self.steps < self.pause_time + 50:   
            self.speed = np.clip(self.orig_speed*(self.steps-self.pause_time)/50, 0, self.orig_speed)
            if self.gait_type == 'tripod':
                self.time_of_step = int(-33*self.speed + 40)
            else:
                self.time_of_step = int(2*(-33*self.speed + 35)/3)
            # print(self.orig_speed, self.speed)
        # self.apply_foot_forces()
                
        if self.steps > 150 and self.disturbances and random.random() < 0.02:
            self.add_disturbance()
        if self.cur:
            self.apply_forces()
        # self.exp_actions = self.get_expert_actions()
        if self.horizontal:
            if self.gait_type == 'tripod':
                self.exp_actions_pos = self.get_expert_actions_pos_horizontal_tripod()
            else:
                self.exp_actions_pos = self.get_expert_actions_pos_horizontal()
        else:
            self.exp_actions_pos = self.get_expert_actions_pos()
        if self.torque_control:
            self.exp_actions = [(80*(exp - self.ob_dict[m + '_pos']) + 0.1*(0.0 - self.ob_dict[m + '_vel']))/self.motor_power[n] for n, (exp, m) in enumerate(zip(self.exp_actions_pos, self.motor_names))]
        else:
            self.exp_actions = self.exp_actions_pos

        # print(self.exp_actions_pos)
        # self.exp_actions_torque = self.get_expert_forces()
        # if self.expert:
        #     if self.steps == 0 and not self.horizontal:
        #         self.restore_sample(self.samples[self.sample_pointer], self.steps)
        # self.exp_actions = self.restore_sample(self.samples[self.sample_pointer], self.steps, restore=False)
        #     self.exp_env.restore_sample(self.samples[self.sample_pointer], self.steps)
        #     if self.steps == 0:
        #         self.prev_exp_pos = {m + '_pos': self.exp_env.sample_dict[m + '_pos'] for m in self.motor_names}
        #     if self.speed > 0:
        # self.sample_pointer += int(self.speed*4)  
        # print(self.exp_actions)
        # self.exp_env.set_position([0,1.5,0.31], [0,0,0,1], self.exp_actions)
        # self.act = np.array(actions)
        # actions = np.zeros(18)
        # actions = self.exp_actions_torque
    

        if self.use_act_pol:
            # self.set_position([0,0,1],[0,0,0,1])
            # post_actions = self.exp_actions
            # Position in:
            if self.args.eth:
                if self.cur:
                    actions = [(1-(self.Kp/self.initial_Kp))*act + (self.Kp/self.initial_Kp)*(self.exp_actions[n]) for n, act in enumerate(actions)]
                    # actions = [(1-(self.Kp/self.initial_Kp))*act + (self.Kp/self.initial_Kp)*(self.exp_actions[n]) for n, act in enumerate(actions)]
                    # actions = [(self.Kp/self.initial_Kp)*(self.exp_actions[n]) for n, act in enumerate(actions)]
                else:
                    actions = actions
            # Torque in:
            else:
                if self.cur:
                    actions = [act*self.motor_power[n] + 80*(self.Kp/self.initial_Kp)*(self.exp_actions[n] - self.ob_dict[m + "_pos"]) + 0.1*(self.Kp/self.initial_Kp)*(0-self.ob_dict[m+"_vel"]) for n,(act,m) in enumerate(zip(actions, self.motor_names))]
                else:                    
                    actions = [act*m for act, m in zip(actions,self.motor_power)]
            post_actions = self.step_act_pol(actions)
            
        for _ in range(self.actionRepeat):
            # if not self.use_act_pol:
            if not self.use_act_pol:
                # exp_gain = 1000
                if self.torque_control:
                    torques = [0.] * len(self.motor_names)
                    for m, name in enumerate(self.motor_names):
                        if self.cur:
                            torques[m] = actions[m]*self.motor_power[m] + (self.Kp/self.initial_Kp)*self.exp_actions[m]*self.motor_power[m]
                            # torques[m] = (self.Kp/self.initial_Kp)*self.exp_actions[m]*self.motor_power[m]
                        else:
                            torques[m] = actions[m]*self.motor_power[m]
                        torques[m] = np.clip(torques[m], -self.tor_max[m], self.tor_max[m])
                    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=torques)      
                else:
                    for m, name in enumerate(self.motor_names):
                        if self.cur:
                            act = actions[m] + (self.Kp/self.initial_Kp)*self.exp_actions[m]
                            # act = actions[m] + 0.5*(self.Kp/self.initial_Kp)*self.exp_actions[m]
                            # act = (self.Kp/self.initial_Kp)*self.exp_actions[m]
                            act = self.exp_actions[m]
                        else:
                            act = actions[m]     
                        act = np.clip(act,self.ordered_joints[m][1],self.ordered_joints[m][2])   
                        # p.setJointMotorControl2(self.Id, self.motors[m], controlMode=p.POSITION_CONTROL, targetPosition=act,force=self.tor_max[m], maxVelocity=self.vel_max[m])
                        p.setJointMotorControl2(self.Id, self.motors[m], controlMode=p.POSITION_CONTROL, targetPosition=act,force=200, maxVelocity=self.vel_max[m])
                        # p.setJointMotorControl2(self.Id, self.motors[m], controlMode=p.POSITION_CONTROL, targetPosition=act, maxVelocity=self.vel_max[m])
            else:
                p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.TORQUE_CONTROL, forces=post_actions)
                # for m, po, vm in zip(self.motors, post_actions, self.vel_max):
                #     p.setJointMotorControl2(self.Id, m, controlMode=p.POSITION_CONTROL, targetPosition=po, maxVelocity=vm)
                    # p.setJointMotorControl2(self.Id, m, controlMode=p.POSITION_CONTROL, targetPosition=po)
                # print([self.ob_dict[m + '_effort'] for m in self.motor_names if 'B' in m])

            self.prev_actions = actions
            #     p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=actions, forces=np.ones(self.ac_size)*75)
            if self.MASTER:
                p.stepSimulation()
        # TODO:
        self.get_observation(actions)
        if self.use_act_pol:
            for i,j in enumerate(self.motor_names):
                if j in [key for key in self.p_pred]:
                    # self.p_actual[j].append(self.ob_dict[j + '_pos'])
                    # self.p_exp[j].append(self.exp_actions[i])
                    self.p_actual[j].append(self.ob_dict[j + '_effort'])
                    self.p_exp[j].append(40*(self.exp_actions[i] - self.ob_dict[j + '_pos']))
        if self.horizontal:
            self.vel_buffer.append(self.ob_dict['vy'])
        else:
            self.vel_buffer.append(self.ob_dict['vx'])
        if self.down_stairs or self.up_stairs or self.up_down_flat:
            if len(self.box_info[0]) > (self.box_num+1) and self.body_xyz[0] > (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]):
                self.box_num += 1
                self.stepped_on_box = False
                self.second_step_on_box = False
            if self.box_num > 0 and (self.box_info[1][self.box_num][2] - self.box_info[1][self.box_num-1][2]) > 0 and (self.body_xyz[0] < (self.box_info[1][self.box_num][0])):
                self.z_offset = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
            else:
                self.z_offset = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
            if len(self.box_info[0]) > (self.box_num+1) and self.box_num > 0:
                next_z = self.box_info[1][self.box_num+1][2] + self.box_info[2][self.box_num+1][2]
                prev_z = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
                next_x = self.box_info[1][self.box_num+1][0] + self.box_info[2][self.box_num+1][0]
                prev_x = self.box_info[1][self.box_num-1][0] + self.box_info[2][self.box_num-1][0]
                self.des_roll = np.arctan2(next_z-prev_z, next_x - prev_x)
                # print(self.des_roll)
            else:
                self.des_roll = 0

        reward, done = self.reward(actions)
        # el_and_fr = self.get_elevation_and_friction()
        self.total_reward += reward
        if self.record_step: self.record_sim_data()
        if self.variable_start and self.steps == self.sample and not done:
            self.get_sample()
        self.steps += 1
        self.total_steps += 1
        if self.total_steps % 2048 == 0:
            self.rew_Kp = self.rew_Kp**0.997
            # if self.Kp > 0:
                # self.Kp = self.Kd = self.Kp-0.65
                # self.Kp = self.Kd = self.Kp-self.args.cur_scale
            # if self.rew_Kp > 0.95:
                # self.cur = False
        if self.render:
            time.sleep(0.02)
        # print("step", time.time() - self.t2)
        if self.control:
            # return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed] + el_and_fr)), reward, done, self.ob_dict
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed])), reward, done, self.ob_dict
        else:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state])), reward, done, self.ob_dict

    def reward(self, actions):
        # print(self.body_xyz[2])
        done = False
        reward = 0
        if not self.horizontal:
            x = abs(self.ob_dict['vx'] - self.speed)
            reward += (3/(np.e**(3*x) + np.e**(-3*x)))
            # reward = 1
            self.reward_breakdown['positive'].append(reward)
            neg_reward = 0
            neg_reward -= 5*((0.4 - self.body_xyz[2] + self.z_offset)**2 + (0 - self.qx)**2 + (0 - self.qy)**2 + (0 - self.qz)**2 + (1 - self.qw)**2)
            neg_reward -= 2*abs(self.body_xyz[1])**2
        else:
            x = abs(self.ob_dict['vy'] - self.speed)
            reward += (3/(np.e**(3*x) + np.e**(-3*x)))
            self.reward_breakdown['positive'].append(reward)
            neg_reward = 0
            # neg_reward -= 5*((0.4 - self.body_xyz[2] + self.z_offset)**2 + (0 - self.qx)**2 + (0 - self.qy)**2 + (-0.707 - self.qz)**2 + ( 0.707 - self.qw)**2)
            roll, pitch, yaw = self.ob_dict['roll'],  self.ob_dict['pitch'], self.ob_dict['yaw']
            neg_reward -= 5*((0.4 - self.body_xyz[2] + self.z_offset)**2 + (self.des_roll - roll)**2 + (0 - pitch)**2 + (-np.pi/2 - yaw)**2) 
            neg_reward -= 2*abs(self.body_xyz[1])**2
        tip_pen = 0.4
        # tip_pen = 0.5
        # Get box edges
        if self.horizontal:
            box_xs = np.array([b1[0]+b2[2] for b1, b2 in zip(self.box_info[1], self.box_info[2])])
        for tip in ['AL', 'AR', 'BL', 'BR', 'CL', 'CR']:
            tip_world = np.array(p.getLinkState(self.Id, self.feet_dict[tip + "_foot_link"])[0])
            # If tip in contact with the ground, penalize near edge of step
            if self.horizontal and not self.ob_dict[tip + '_foot_link_left_ground']:
                dist = np.min(abs(box_xs-tip_world[0]))
                neg_reward -= 50*(0.1 - dist)**2 if 0.1 > abs(dist) else 0
            tip_pos = tip_world - np.array(self.body_xyz) 
            if self.speed > 0:
                
                if not self.ob_dict['right_foot_left_ground']:   
                    dist_to_box = min(abs(prev_box_x - right_x), abs(cur_box_x - right_x), abs(next_box_x - right_x))
                period = (self.steps-1 - self.pause_time) % self.time_of_step
                # if (period > self.time_of_step/3 and period < 2*self.time_of_step/3)
                if period < 2*self.time_of_step/3:
                    if (self.ob_dict['left_swing'] and tip in ['AL', 'BR', 'CL']) or (self.ob_dict['right_swing'] and tip in ['AR', 'BL', 'CR']): 
                        neg_reward -= tip_pen*(not self.ob_dict[tip + '_foot_link_left_ground'])
                        # print(tip_pen*(not self.ob_dict[tip + '_foot_link_left_ground']))
                    else:
                        neg_reward -= tip_pen*(self.ob_dict[tip + '_foot_link_left_ground'])
                        # print(neg_reward)
                        # neg_reward -= 2*abs(tip_world[2] - 0.2)**2
                else:
                    neg_reward -= tip_pen*(self.ob_dict[tip + '_foot_link_left_ground'])
            else:
                neg_reward -= tip_pen*self.ob_dict[tip + '_foot_link_left_ground']
        
        # Velocity check:
        pre_vel = neg_reward
        # self.rew_Kp = 1.0
        for n, m in enumerate(self.motor_names):
            if self.steps > 10 and abs(self.ob_dict[m + '_vel']) > self.vel_max[n]:
                # print("over vel", m, self.vel_max[n])
                if (self.up_stairs or self.down_stairs or self.up_down_flat):
                    neg_reward -= 0.008*abs(self.ob_dict[m + '_vel'])
                else:
                    neg_reward -= self.rew_Kp*0.01*abs(self.ob_dict[m + '_vel'])
        # print(neg_reward - pre_vel)
        # if self.cur and self.steps > 700: done = True
        # if self.cur and self.steps > 400: done = True
        if self.steps > 4000: done = True
        # if self.steps > 600: done = True
        # self.set_position([0,0,1],[0,0,0,1])
        # if (self.body_xyz[2] < (0.25 + self.z_offset)) or abs(self.qx) > 0.3 or abs(self.qy) > 0.3: done = True

        if self.steps > self.pause_time:
            # if (self.body_xyz[2] < (0.27 + self.z_offset)) or abs(self.qx) > 0.3 or abs(self.qy) > 0.3: done = True
            if (self.body_xyz[2] < (0.2 + self.z_offset)) or abs(self.qx) > 0.2 or abs(self.qy) > 0.2: done = True
            # if (self.body_xyz[2] < (0.15 + self.z_offset)) or abs(self.qx) > 0.3 or abs(self.qy) > 0.3: done = True
            # if (self.body_xyz[2] < (0.17 + self.z_offset)) or abs(self.qx) > 0.3 or abs(self.qy) > 0.3: done = True
            # if abs(self.qx) > 0.3 or abs(self.qy) > 0.3: done = True
        else:
            if abs(self.qx) > 0.3 or abs(self.qy) > 0.3: done = True

        # if (self.body_xyz[2] < (0.17 + self.z_offset)) or abs(self.qx) > 0.3 or abs(self.qy) > 0.3: done = True
        # if sum([len(p.getContactPoints(self.Id, -1, self.tibias[t], -1))>0 for t in self.tibias]) > 0:
            # print(0.2*sum([len(p.getContactPoints(self.Id, -1, self.tibias[t], -1))>0 for t in self.tibias]))
        neg_reward -= 0.5*sum([len(p.getContactPoints(self.Id, -1, self.tibias[t], -1))>0 for t in self.tibias])
        # if self.torque_control:
        #     neg_reward -= 0.0001*np.square(actions).sum()
        # else:
        #     neg_reward -= 0.1*np.sum((np.array(actions) - self.prev_actions)**2)
        # print(neg_reward)
        # neg_reward *= self.rew_Kp
        # neg_reward = np.clip(neg_reward, -1.5, 0)
        neg_reward = np.clip(neg_reward, -1.0, 0)
        # neg_reward = 0.5*np.exp(neg_reward)
        reward += neg_reward
        self.reward_breakdown['negative'].append(neg_reward)
            # print([len(p.getContactPoints(self.Id, -1, self.tibias[t], -1))>0 for t in self.tibia
        # "AR_coxa_joint","AR_femur_joint","AR_tibia_joint",
        # "AL_coxa_joint","AL_femur_joint","AL_tibia_joint",
        # "BR_coxa_joint","BR_femur_joint","BR_tibia_joint",
        # "BL_coxa_joint","BL_femur_joint","BL_tibia_joint",
        # "CR_coxa_joint","CR_femur_joint","CR_tibia_joint",
        # "CL_coxa_joint","CL_femur_joint","CL_tibia_joint"]
        
        # sym_scale = 0.0001*self.rew_Kp
        # sym_scale = 0.001*self.rew_Kp
        sym_scale = 0.05
        sym_reward = 0
        
        if self.speed > 0 and len(self.action_store) == (self.time_of_step):
            if self.horizontal:
                sym_reward -= sym_scale*(actions[0] + self.action_store[0][3])**2
                sym_reward -= sym_scale*(actions[1] - self.action_store[0][4])**2
                sym_reward -= sym_scale*(actions[2] + self.action_store[0][5])**2
                sym_reward -= sym_scale*(actions[3] + self.action_store[0][0])**2
                sym_reward -= sym_scale*(actions[4] - self.action_store[0][1])**2
                sym_reward -= sym_scale*(actions[5] + self.action_store[0][2])**2

                sym_reward -= sym_scale*(actions[6] + self.action_store[0][9])**2
                sym_reward -= sym_scale*(actions[7] - self.action_store[0][10])**2
                sym_reward -= sym_scale*(actions[8] + self.action_store[0][11])**2
                sym_reward -= sym_scale*(actions[9] + self.action_store[0][6])**2
                sym_reward -= sym_scale*(actions[10] - self.action_store[0][7])**2
                sym_reward -= sym_scale*(actions[11] + self.action_store[0][8])**2
                
                sym_reward -= sym_scale*(actions[12] + self.action_store[0][15])**2
                sym_reward -= sym_scale*(actions[13] - self.action_store[0][16])**2
                sym_reward -= sym_scale*(actions[14] + self.action_store[0][17])**2
                sym_reward -= sym_scale*(actions[15] + self.action_store[0][12])**2
                sym_reward -= sym_scale*(actions[16] - self.action_store[0][13])**2
                sym_reward -= sym_scale*(actions[17] + self.action_store[0][14])**2
            else:
                sym_reward -= sym_scale*(actions[0] + self.action_store[0][3])**2
                sym_reward -= sym_scale*(actions[1] - self.action_store[0][4])**2
                sym_reward -= sym_scale*(actions[2] - self.action_store[0][5])**2
                sym_reward -= sym_scale*(actions[3] + self.action_store[0][0])**2
                sym_reward -= sym_scale*(actions[4] - self.action_store[0][1])**2
                sym_reward -= sym_scale*(actions[5] - self.action_store[0][2])**2

                sym_reward -= sym_scale*(actions[6] + self.action_store[0][9])**2
                sym_reward -= sym_scale*(actions[7] - self.action_store[0][10])**2
                sym_reward -= sym_scale*(actions[8] - self.action_store[0][11])**2
                sym_reward -= sym_scale*(actions[9] + self.action_store[0][6])**2
                sym_reward -= sym_scale*(actions[10] - self.action_store[0][7])**2
                sym_reward -= sym_scale*(actions[11] - self.action_store[0][8])**2
                
                sym_reward -= sym_scale*(actions[12] + self.action_store[0][15])**2
                sym_reward -= sym_scale*(actions[13] - self.action_store[0][16])**2
                sym_reward -= sym_scale*(actions[14] - self.action_store[0][17])**2
                sym_reward -= sym_scale*(actions[15] + self.action_store[0][12])**2
                sym_reward -= sym_scale*(actions[16] - self.action_store[0][13])**2
                sym_reward -= sym_scale*(actions[17] - self.action_store[0][14])**2
        if self.speed > 0:
            self.action_store.append(actions)
        # print(reward - rew)
        # self.action_pos_store.append([self.ob_dict[m + '_pos'] for m in self.motor_names])
        sym_reward = self.rew_Kp*max(sym_reward, 0.0)
        # reward += sym_reward
        self.reward_breakdown['symmetry'].append(sym_reward)

        # if self.torque_control:
        # else:
            # exp_reward = -fact*np.mean([(actions[n] - self.exp_actions[n])**2 for n, name in enumerate(self.motor_names)])
        # print(actions, self.exp_actions)
        # if self.cur:
        #     exp_reward = max(exp_reward, -0.6)

        exp_reward = 0
        # fact = 0.05*self.rew_Kp
        fact = 0.75
        exp_reward = -fact*np.sum([(self.ob_dict[name + '_pos'] - self.exp_actions_pos[n])**2 for n, name in enumerate(self.motor_names)])
        exp_reward = np.exp(exp_reward)
        # exp_reward = np.clip(exp_reward, -0.5, 0)
        if not self.use_weights:
            reward += exp_reward
        else:
            reward += 1
        self.reward_breakdown['expert'].append(exp_reward)
        
        fact = 0.2
        neg2_reward = 0
        if self.speed > 0.1:
            period = (self.steps-1 - self.pause_time) % self.time_of_step
            for n, name in enumerate(self.motor_names):
                if 'tibia' in name:
                    neg2_reward -= fact*abs(1.4 - self.ob_dict[name + '_pos']) if 1.4 > self.ob_dict[name + '_pos'] else 0
                    neg2_reward -= fact*abs(2.0 - self.ob_dict[name + '_pos']) if 2.0 < self.ob_dict[name + '_pos'] else 0
                if self.ob_dict['left_swing']:
                    if 'femur' in name:
                        if period < 2*self.time_of_step/3:
                            if 'AL' in name or 'BR' in name or 'CL' in name:
                                neg2_reward -= fact*abs(-0.3 - self.ob_dict[name + '_pos']) if -0.3 < self.ob_dict[name + '_pos'] else 0
                            else:
                                neg2_reward -= fact*abs(0 - self.ob_dict[name + '_pos']) if 0 > self.ob_dict[name + '_pos'] else 0
                        else:
                            neg2_reward -= fact*abs(0 - self.ob_dict[name + '_pos']) if 0 > self.ob_dict[name + '_pos'] else 0
                    if 'coxa' in name and period > 1.5*self.time_of_step/3:
                        if 'AR' in name:
                            neg2_reward -= fact*abs(0.1 - self.ob_dict[name + '_pos']) if 0.1 < self.ob_dict[name + '_pos'] else 0
                        if 'BL' in name:
                            neg2_reward -= fact*abs(0.3 - self.ob_dict[name + '_pos']) if 0.3 > self.ob_dict[name + '_pos'] else 0
                        if 'CR' in name:
                            neg2_reward -= fact*abs(-0.7 - self.ob_dict[name + '_pos']) if -0.7 < self.ob_dict[name + '_pos'] else 0
                        if 'AL' in name:
                            neg2_reward -= fact*abs(-0.7 - self.ob_dict[name + '_pos']) if -0.7 < self.ob_dict[name + '_pos'] else 0
                        if 'BR' in name:
                            neg2_reward -= fact*abs(0.3 - self.ob_dict[name + '_pos']) if 0.3 > self.ob_dict[name + '_pos'] else 0
                        if 'CL' in name:
                            neg2_reward -= fact*abs(0.1 - self.ob_dict[name + '_pos']) if 0.1 < self.ob_dict[name + '_pos'] else 0
                if self.ob_dict['right_swing']:
                    if 'femur' in name:
                        if period < 2*self.time_of_step/3:
                            if 'AR' in name or 'BL' in name or 'CR' in name:
                                neg2_reward -= fact*abs(-0.3 - self.ob_dict[name + '_pos']) if -0.3 < self.ob_dict[name + '_pos'] else 0
                            else:
                                neg2_reward -= fact*abs(0 - self.ob_dict[name + '_pos']) if 0 > self.ob_dict[name + '_pos'] else 0
                        else:
                            neg2_reward -= fact*abs(0 - self.ob_dict[name + '_pos']) if 0 > self.ob_dict[name + '_pos'] else 0
                    if 'coxa' in name and period > 1.5*self.time_of_step/3:
                        if 'AR' in name:
                            neg2_reward -= fact*abs(0.7 - self.ob_dict[name + '_pos']) if 0.7 > self.ob_dict[name + '_pos'] else 0
                        if 'BL' in name:
                            neg2_reward -= fact*abs(-0.3 - self.ob_dict[name + '_pos']) if -0.3 < self.ob_dict[name + '_pos'] else 0
                        if 'CR' in name:
                            neg2_reward -= fact*abs(-0.1 - self.ob_dict[name + '_pos']) if -0.1 > self.ob_dict[name + '_pos'] else 0
                        if 'AL' in name:
                            neg2_reward -= fact*abs(-0.1 - self.ob_dict[name + '_pos']) if -0.1 > self.ob_dict[name + '_pos'] else 0
                        if 'BR' in name:
                            neg2_reward -= fact*abs(0.3 - self.ob_dict[name + '_pos']) if -0.3 < self.ob_dict[name + '_pos'] else 0
                        if 'CL' in name:
                            neg2_reward -= fact*abs(0.7 - self.ob_dict[name + '_pos']) if 0.7 > self.ob_dict[name + '_pos'] else 0
        # neg2_reward = self.rew_Kp*neg2_reward
        neg2_reward = 0
        neg2_reward = np.clip(neg2_reward, -0.5, 0)
        self.reward_breakdown['negative2'].append(neg2_reward)
        reward += neg2_reward

        # print(self.reward_breakdown)
        # if self.cur:
        # reward = 0.5 + exp_reward
        # reward = 1.0 + exp_reward + neg_reward
        # reward = 
        return reward, done

    def get_observation(self, actions):
        # Get joint positions and velocities

        jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
        # print(jointStates)
        # ======================================================================
        self.ob_dict.update({self.state[i]:jointStates[j[0]][0] for i,j in enumerate(self.ordered_joints[:int(18)])})
        self.ob_dict.update({self.state[i+18]:jointStates[j[0]][1] for i,j in enumerate(self.ordered_joints[:int(18)])})
        # if not self.torque_control:
        if self.use_act_pol:
            self.ob_dict.update({self.state[i+36]:jointStates[j[0]][3] for i,j in enumerate(self.ordered_joints[:int(18)])})
            # print([self.ob_dict[m + '_effort'] for m in self.motor_names])
        else:
            for m, a in zip(self.motor_names, actions):
                self.ob_dict[m + '_effort'] = a
        # for m, a, t in zip(self.motor_names, 
        # #     self.ob_dict[m + '_prev_act'] = a

            # self.ob_dict.update({self.ob_dict[i+36]:a for m, a in zip(self.motor_names, actions)})
        # self.ob_dict.update({self.state[i]:jointStates[j[0]][0] for i,j in enumerate(self.ordered_joints[:int(self.ac_size)])})
        # self.ob_dict.update({self.state[i+self.ac_size]:jointStates[j[0]][1] for i,j in enumerate(self.ordered_joints[:int(self.ac_size)])})

        # Get relative com
        self.body_xyz, (self.qx, self.qy, self.qz, self.qw) = p.getBasePositionAndOrientation(self.Id)
        # Get angles and velocities
        roll, pitch, yaw = p.getEulerFromQuaternion([self.qx, self.qy, self.qz, self.qw])
        self.ob_dict['qx'],self.ob_dict['qy'],self.ob_dict['qz'],self.ob_dict['qw'] = self.qx, self.qy, self.qz, self.qw

        prev_r, prev_p, prev_y = self.ob_dict['roll'], self.ob_dict['pitch'], self.ob_dict['yaw']
        self.body_vxyz, self.base_rot_vel = p.getBaseVelocity(self.Id)
        self.ob_dict.update({'roll':roll,'pitch':pitch,'yaw':yaw})
        self.ob_dict.update({'roll_vel':self.base_rot_vel[0],'pitch_vel':self.base_rot_vel[1],'yaw_vel':self.base_rot_vel[2]})

        rot_speed = np.array(
        	[[np.cos(-yaw), -np.sin(-yaw), 0],
        	 [np.sin(-yaw), np.cos(-yaw), 0],
        	 [		0,			 0, 1]]
        )
        vx, vy, vz = np.dot(rot_speed, (self.body_vxyz[0],self.body_vxyz[1],self.body_vxyz[2]))  # rotate speed back to body point of view
        self.ob_dict.update({'vx':vx,'vy':vy,'vz':vz})
        self.ob_dict['ax'] = (vx - self.prev_vx)/(self.timeStep)
        self.ob_dict['ay'] = (vy - self.prev_vy)/(self.timeStep)
        self.ob_dict['az'] = (vz - self.prev_vz)/(self.timeStep)
        self.prev_vx, self.prev_vy, self.prev_vz = vx, vy, vz
        # Get feet contacts
        for n in self.feet_dict:
            self.ob_dict['prev_' + n + '_left_ground'] = self.ob_dict[n + '_left_ground']
        for n in self.feet_dict:
            self.ob_dict[n + '_left_ground']= not len(p.getContactPoints(self.Id, -1, self.feet_dict[n], -1))>0
        
        # if (self.steps) % self.time_of_step == 0:
        if self.gait_type == 'tripod':
            if self.speed > 0 and self.steps > self.pause_time and (self.steps - self.pause_time) % self.time_of_step == 0:
                if self.ob_dict['left_swing']:
                    self.ob_dict['left_swing'] = False
                    self.ob_dict['right_swing'] = True
                else:
                    self.ob_dict['left_swing'] = True
                    self.ob_dict['right_swing'] = False
            elif self.speed < 0.1:
                self.ob_dict['left_swing'] = False
                self.ob_dict['right_swing'] = False
        else:
            if self.speed > 0 and self.steps > self.pause_time and (self.steps - self.pause_time) % self.time_of_step == 0:
                if self.ob_dict['left_swing']:
                    self.ob_dict['left_swing'] = False
                    self.ob_dict['middle_swing'] = True
                    self.ob_dict['right_swing'] = False
                elif self.ob_dict['middle_swing']:
                    self.ob_dict['left_swing'] = False
                    self.ob_dict['middle_swing'] = False
                    self.ob_dict['right_swing'] = True
                else:
                    self.ob_dict['left_swing'] = True
                    self.ob_dict['middle_swing'] = False
                    self.ob_dict['right_swing'] = False
            elif self.speed < 0.1:
                self.ob_dict['left_swing'] = False
                self.ob_dict['middle_swing'] = False
                self.ob_dict['right_swing'] = False
        
        self.ob_dict.update({'com_z': self.body_xyz[2] - self.z_offset})


        for tip in self.feet_dict:   
            if not self.ob_dict['prev_' + tip + '_left_ground'] and self.ob_dict['prev_' + tip + '_left_ground']:
                self.tip_pos[tip] = np.array(p.getLinkState(self.Id, self.feet_dict[tip])[0])   
            # if tip_pos[2] < 0.005:
            #     self.tip_pos[tip] = tip_pos
            else:
                self.tip_pos[tip] = None
    
    def step_act_pol(self, actions):

        data = []
        if not self.args.eth:
            for i,j in enumerate(self.motor_names):
                pos1 = self.prev_pos[i]
                pos2 = self.prev1_pos[i]
                pos3 = self.prev2_pos[i]
                vel1 = self.prev_vel[i]
                vel2 = self.prev1_vel[i]
                vel3 = self.prev2_vel[i]
                effort1 = self.prev_effort[i]
                effort2 = self.prev1_effort[i]
                effort3 = self.prev2_effort[i]
                data.extend([actions[i], pos1, pos2, pos3, vel1, vel2, vel3, effort1, effort2, effort3])
            
            output = self.act_pol.step(np.array(data).reshape(-1,10*self.ac_size))
            pos, vel, torque = output[:,:self.ac_size], output[:,self.ac_size:self.ac_size*2], output[:,self.ac_size*2:]
            new_actions = torque
            data.extend([self.ob_dict[j + '_pos'] for j in self.motor_names])
            data.extend([self.ob_dict[j + '_vel'] for j in self.motor_names])
            data.extend([self.ob_dict[j + '_effort'] for j in self.motor_names])
            data.extend([self.ob_dict[j + '_pos'] - self.prev_ob_dict[j + '_pos'] for j in self.motor_names])

            self.prev2_pos = self.prev1_pos
            self.prev1_pos = self.prev_pos
            self.prev_pos = [self.ob_dict[j + '_pos'] for j in self.motor_names] 
            self.prev2_vel = self.prev1_vel
            self.prev1_vel = self.prev_vel
            self.prev_vel = [self.ob_dict[j + '_vel'] for j in self.motor_names]
            self.prev2_effort = self.prev1_effort
            self.prev1_effort = self.prev_effort
            self.prev_effort = [self.ob_dict[j + '_effort'] for j in self.motor_names]
            self.prev_ob_dict = copy.deepcopy(self.ob_dict)
        else:
            error = np.array(actions) - np.array(self.prev_pos)    
            new_actions = []  
            for i,j in enumerate(self.motor_names):  
                pos2 = self.prev1_pos_error[i]
                pos3 = self.prev2_pos_error[i]
                vel1 = self.prev_vel[i]
                vel2 = self.prev1_vel[i]
                vel3 = self.prev2_vel[i]
                data.extend([error[i], pos2, pos3, vel1, vel2, vel3])
                if self.args.single:
                    new_actions.append(self.act_pol.step(np.array([error[i], pos2, pos3, vel1, vel2, vel3]).reshape(-1,6)).reshape(1))
            if self.args.single:
                if self.args.delta:
                    # print(np.array(new_actions).shape, np.array(self.prev_pos).shape)
                    new_actions = np.array(new_actions).reshape(self.ac_size) + np.array(self.prev_pos)
                else:
                    new_actions = np.array(new_actions)
            else:
                new_actions = self.act_pol.step(np.array(data).reshape(-1,6*self.ac_size))

            self.prev_pos = [self.ob_dict[j + '_pos'] for j in self.motor_names] 
            self.prev2_vel = self.prev1_vel
            self.prev1_vel = self.prev_vel
            self.prev_vel = [self.ob_dict[j + '_vel'] for j in self.motor_names]
            self.prev_ob_dict = copy.deepcopy(self.ob_dict)
            self.prev2_pos_error = self.prev1_pos_error
            self.prev1_pos_error = error

        return new_actions.reshape(self.ac_size)
        # eth = True
        # if not eth:
        #     self.gz_motor_names = ['AL_coxa_joint','AL_femur_joint','AL_tibia_joint']
        #     self.gz_motor_names += ['AR_coxa_joint','AR_femur_joint','AR_tibia_joint']
        #     self.gz_motor_names += ['BL_coxa_joint','BL_femur_joint','BL_tibia_joint']
        #     self.gz_motor_names += ['BR_coxa_joint','BR_femur_joint','BR_tibia_joint']
        #     self.gz_motor_names += ['CL_coxa_joint','CL_femur_joint','CL_tibia_joint']
        #     self.gz_motor_names += ['CR_coxa_joint','CR_femur_joint','CR_tibia_joint']
            
        #     temp_actions = []
        #     temp_actions.extend(actions[3:6]) #AL to AR
        #     temp_actions.extend(actions[:3]) #AR to AL
        #     temp_actions.extend(actions[9:12]) #BL to BR
        #     temp_actions.extend(actions[6:9]) #BR to BL
        #     temp_actions.extend(actions[15:]) #CL to CR
        #     temp_actions.extend(actions[12:15]) #CR to CL


        #     delta = self.act_pol.step(np.array(self.prev_state + self.prev1_state + self.prev2_state + temp_actions).reshape(-1,(36)*3+self.ac_size)).reshape(self.ac_size)
        #     # print(delta)
        #     new_delta = []
        #     new_delta.extend(delta[3:6]) #AL to AR
        #     new_delta.extend(delta[:3]) #AR to AL
        #     new_delta.extend(delta[9:12]) #BL to BR
        #     new_delta.extend(delta[6:9]) #BR to BL
        #     new_delta.extend(delta[15:]) #CL to CR
        #     new_delta.extend(delta[12:15]) #CR to CL
        #     # new_delta = delta
            
        #     orig = np.array([self.prev_ob_dict[j + '_pos'] for j in self.gz_motor_names])
        #     preds = orig + new_delta
        #     new_actions = preds
        #     self.prev2_state = self.prev1_state
        #     self.prev1_state = self.prev_state
        #     self.prev_state = [self.ob_dict[j + '_pos'] for j in self.gz_motor_names] + [self.ob_dict[j + '_vel'] for j in self.gz_motor_names]
        #     # self.prev_state = [self.ob_dict[s] for s in self.state]
        #     self.prev_ob_dict = copy.deepcopy(self.ob_dict)
        # else:
        #     data = []
        #     error = np.array(actions) - np.array(self.prev_pos)
        #     # print(error)
        #     for i,j in enumerate(self.motor_names):
        #         pos2 = self.prev1_pos_error[i]
        #         pos3 = self.prev2_pos_error[i]
        #         vel1 = self.prev_vel[i]
        #         vel2 = self.prev1_vel[i]
        #         vel3 = self.prev2_vel[i]
        #         data.extend([error[i], pos2, pos3, vel1, vel2, vel3])
        #     preds = self.act_pol.step(np.array(data).reshape(-1,108)).reshape(self.ac_size)
        #     new_actions = preds
        #     data.extend([self.ob_dict[j + '_effort'] for j in self.motor_names])
        #     self.prev2_pos_error = self.prev1_pos_error
        #     self.prev1_pos_error = error
        #     self.prev_pos = [self.ob_dict[j + '_pos'] for j in self.motor_names] 
        #     self.prev2_vel = self.prev1_vel
        #     self.prev1_vel = self.prev_vel
        #     self.prev_vel = [self.ob_dict[j + '_vel'] for j in self.motor_names]

        # for i,j in enumerate(self.motor_names):
        #     if j in [key for key in self.p_pred]:
        #         self.p_pred[j].append(preds[i])
        # return new_actions

    def plot_act_pol(self):
        import matplotlib.pyplot as plt
        tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
                (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
                (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
                (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
                (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
        # Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.
        for i in range(len(tableau20)):
            r, g, b = tableau20[i]
            tableau20[i] = (r / 255., g / 255., b / 255.)
        # fig, axes = plt.subplots(2, 2, figsize=(20, 30))
        # fig, axes = plt.subplots(3,6, figsize=(20, 30))
        # fig, axes = plt.subplots(4, figsize=(20, 30))
        # cols = [0,4,6,8]
        # z = 0
        # h = 0
        # for i,j in enumerate(self.t_pred):  
        #   # h = i%6
        #   # if i % 6 == 0 and i != 0:
        #   #   z += 1
        #   xs = [i for i in range(len(self.t_pred[j]))]
        #   axes[i].plot(xs, self.t_pred[j], c=tableau20[cols[i]], alpha=1.0)   
        #   axes[i].plot(xs, self.t_actual[j], c=tableau20[cols[i]+1], alpha=1.0)     
        #   # axes[z,h].plot(xs, self.t_pred[j], c=tableau20[cols[z]], alpha=1.0)   
        #   # axes[z,h].plot(xs, self.t_actual[j], c=tableau20[cols[z]+1], alpha=1.0)     
        # plt.savefig(self.PATH + 'torques.png', bbox_inches='tight')
        # plt.close(fig)
        fig, axes = plt.subplots(4, figsize=(20, 30))
        cols = [0,4,6,8]
        z = 0
        h = 0
        for i,j in enumerate(self.p_pred):  
            # h = i%6
            # if i % 6 == 0 and i != 0:
            #   z += 1
            xs = [i for i in range(len(self.p_pred[j]))]
            axes[i].plot(xs, self.p_pred[j], c=tableau20[cols[i]], alpha=1.0)   
            # axes[i].plot(xs, self.p_actual[j], c=tableau20[cols[i]+1], alpha=1.0)     
            axes[i].plot(xs, self.p_exp[j], c=tableau20[cols[i]+4], alpha=1.0)     
            # axes[z,h].plot(xs, self.t_pred[j], c=tableau20[cols[z]], alpha=1.0)   
            # axes[z,h].plot(xs, self.t_actual[j], c=tableau20[cols[z]+1], alpha=1.0)     
        plt.savefig(self.PATH + 'pos.png', bbox_inches='tight')
        plt.close(fig)

    def get_act_pol(self, act_pol):
        self.use_act_pol = True
        self.act_pol = act_pol
        self.t_pred = {j:[] for j in ['AR_coxa_joint','BL_coxa_joint','CR_femur_joint','CL_tibia_joint']}
        self.t_actual = {j:[] for j in ['AR_coxa_joint','BL_coxa_joint','CR_femur_joint','CL_tibia_joint']}
        self.p_pred = {j:[] for j in ['AR_coxa_joint','BL_coxa_joint','CR_femur_joint','CL_tibia_joint']}
        self.p_actual = {j:[] for j in ['AR_coxa_joint','BL_coxa_joint','CR_femur_joint','CL_tibia_joint']}
        self.p_exp = {j:[] for j in ['AR_coxa_joint','BL_coxa_joint','CR_femur_joint','CL_tibia_joint']}
        self.prev1_vel = [0]*self.ac_size
        self.prev2_vel = [0]*self.ac_size
        self.prev3_vel = [0]*self.ac_size
        self.prev1_pos_error = [0]*self.ac_size
        self.prev2_pos_error = [0]*self.ac_size
        self.prev3_pos_error = [0]*self.ac_size
        self.prev_pos = [0]*self.ac_size

    def update_speed(self, speed, yaw=0, vy=0):
        self.speed = speed
        fr_and_el = self.get_elevation_and_friction()
        return np.array([self.ob_dict[s] for s in self.state] + [self.speed] + fr_and_el)

    def dict_from_obs(self, obs):
        # print(obs.shape)
        return {s:o for s, o in zip(self.state, obs)}

    def add_disturbance(self):
        # max_disturbance = 250
        max_disturbance = 4000
        force_x = (random.random() - 0.5)*max_disturbance
        force_y = (random.random() - 0.5)*max_disturbance
        force_z = (random.random() - 0.5)*(max_disturbance/4)
        # print("applying forces", force_x, force_y, force_z)
        p.applyExternalForce(self.Id,-1,[force_x,force_y,0],[0,0,0],p.LINK_FRAME)

    def apply_forces(self, actions=None):

        roll, pitch, yaw = self.ob_dict['roll'], self.ob_dict['pitch'], self.ob_dict['yaw']
        vx, vy, vz = self.ob_dict['vx'], self.ob_dict['vy'], self.ob_dict['vz']
        if self.horizontal:

            y_force = 0.0*self.Kp * (self.speed - vy)

            x_force = 0.0*self.Kp * (0.0 - self.body_xyz[1])
            # z_force = self.Kp * (0.31 - self.body_xyz[2] + vz * -1)
            z_force = 0.0*self.Kp * (1 - self.body_xyz[2] + vz * -1)
            roll_force = 2*self.Kp * -self.ob_dict['roll']         
            pitch_force = 2*self.Kp * -self.ob_dict['pitch']
            yaw_force = 2*self.Kp * (-1.57-self.ob_dict['yaw'])
        else:

            x_force = 1*self.Kp * (self.speed - vx)
            
            y_force = self.Kp * (0.0 - self.body_xyz[1])
            # z_force = 3*self.Kp * (0.31 - self.body_xyz[2])
            # z_force = 5*self.Kp * (1.0 - self.body_xyz[2] + vz * -1)
            z_force = 0*self.Kp * (0.4 - self.body_xyz[2] + vz * -1)
            roll_force = 4*self.Kp * (-self.ob_dict['roll']  +  self.ob_dict['roll_vel'] * -0.1)      
            pitch_force = 6*self.Kp * (-self.ob_dict['pitch']+ self.ob_dict['pitch_vel'] * -0.1)
            yaw_force = 1*self.Kp * -self.ob_dict['yaw']
        # x_force, y_force, z_force, roll_force, pitch_force, yaw_force = 0,0,0,0,0,0
        p.applyExternalForce(self.Id,-1,[x_force,y_force,z_force],[0,0,0],p.LINK_FRAME)

        p.applyExternalTorque(self.Id,-1,[roll_force,pitch_force,yaw_force],p.LINK_FRAME)
 
    def apply_foot_forces(self):
        gain = 10
        for tip in self.tip_pos:
            if self.tip_pos[tip] is not None:
                pos = np.array(p.getLinkState(self.Id, self.feet_dict[tip])[0])
                tip_error = gain*np.array([self.tip_pos[tip][0] - pos[0], self.tip_pos[tip][1] - pos[1]])
                print("applying force to tip ", tip, tip_error, pos, self.tip_pos[tip])
                
                p.applyExternalForce(self.Id,self.feet_dict[tip],[tip_error[0],tip_error[1],0],posObj=[0,0,-0.2],flags=p.LINK_FRAME)

    def get_expert_actions_pos(self):
        # c_max = 0.8; c_min = 0.0
        # f_max = 0.3; f_min = 0.0
        # t_max = 1.9; t_min = 1.5
        # b = 0.4

        gain = 400
        coxa_gain = 800
        new_actions = []
        dt = 0.005
        if self.speed > 0:
            c_max, c_min, f_max, f_min = self.params['c_max'], self.params['c_min'], self.params['f_max'], self.params['f_min']
            t_max, t_min, b = self.params['t_max'], self.params['t_min'], self.params['b']
            period = (self.steps-1 - self.pause_time) % self.time_of_step
            coxa_dt = (c_max - c_min)/(12*self.time_of_step)
            # print(coxa_dt)
        else:
            c_max, c_min, f_max, f_min = self.params['c_max'], self.params['c_min'], self.params['f_max'], self.params['f_min']
            t_max, t_min, b = self.params['t_max'], self.params['t_min'], self.params['b']
            period = (self.steps-1 - self.pause_time) % self.time_of_step
            coxa_dt = (c_max - c_min)/(12*self.time_of_step)
            period = 0
        # if period == 0: print(self.ob_dict['left_swing'], self.ob_dict['right_swing'])       
        for n, name in enumerate(self.motor_names):
            # print(self.ob_dict['left_swing'], self.ob_dict['right_swing'])
            if self.ob_dict['left_swing']:
                if 'tibia' in name:
                    if 'AL' in name or 'CL' in name or 'BR' in name:
                        # if period < 2*self.time_of_step/3:
                        if period < 6:
                            self.AL_tibia = t_max
                            self.BR_tibia = t_max
                            self.CL_tibia = t_max
                        else:
                            self.AL_tibia = t_min
                            self.BR_tibia = t_min
                            self.CL_tibia = t_min   
                    else:
                        self.AR_tibia = t_min
                        self.BL_tibia = t_min
                        self.CR_tibia = t_min
                        new_actions.append(t_min)
                elif 'femur' in name:
                    if 'AL' in name or 'BR' in name or 'CL' in name:
                        # if period < 2*self.time_of_step/3:
                        if period < 6:
                            self.AL_femur = -f_max
                            self.BR_femur = -f_max
                            self.CL_femur = -f_max
                        else:
                            self.AL_femur = f_min
                            self.BR_femur = f_min
                            self.CL_femur = f_min
                    else:
                        self.AR_femur = f_min
                        self.BL_femur = f_min
                        self.CR_femur = f_min
                elif 'coxa' in name:
                    # if period > self.time_of_step/3:
                    if period > 2 and period < 8:
                        self.AR_coxa = max(self.AR_coxa-coxa_dt, c_min)
                        self.BL_coxa = min(self.BL_coxa+coxa_dt, b)
                        self.CR_coxa = max(self.CR_coxa-coxa_dt, -c_max)
                        self.AL_coxa = -c_max
                        self.BR_coxa = b
                        self.CL_coxa = c_min
                    else:
                        self.AR_coxa = max(self.AR_coxa-coxa_dt, c_min)
                        self.BL_coxa = min(self.BL_coxa+coxa_dt, b)
                        self.CR_coxa = max(self.CR_coxa-coxa_dt, -c_max)
                        self.AL_coxa = min(self.AL_coxa+coxa_dt, -c_min)
                        self.BR_coxa = max(self.BR_coxa-coxa_dt, -b)
                        self.CL_coxa = min(self.CL_coxa+coxa_dt, c_max)
            elif self.ob_dict['right_swing']:
                if 'tibia' in name:
                    # if period > self.time_of_step/3 and period < 2*self.time_of_step/3:
                    if 'AR' in name or 'CR' in name or 'BL' in name:                    
                        # if period < 2*self.time_of_step/3:
                        if period < 6:
                            self.AR_tibia = t_max
                            self.BL_tibia = t_max
                            self.CR_tibia = t_max
                        else:                    
                            self.AR_tibia = t_min
                            self.BL_tibia = t_min
                            self.CR_tibia = t_min
                    else:
                        self.AL_tibia = t_min
                        self.BR_tibia = t_min
                        self.CL_tibia = t_min
                elif 'femur' in name:
                    if 'AR' in name or 'BL' in name or 'CR' in name:
                        # if period < 2*self.time_of_step/3:
                        if period < 6:
                            self.AR_femur = -f_max
                            self.BL_femur = -f_max
                            self.CR_femur = -f_max
                        else:
                            self.AR_femur = f_min
                            self.BL_femur = f_min
                            self.CR_femur = f_min
                    else:
                        self.AL_femur = f_min
                        self.BR_femur = f_min
                        self.CL_femur = f_min                        
                elif 'coxa' in name:
                    # if period > self.time_of_step/3:
                    if period > 2 and period < 8:
                        self.AR_coxa = c_max
                        self.BL_coxa = -b
                        self.CR_coxa = -c_min
                        self.AL_coxa = min(self.AL_coxa+coxa_dt, -c_min)
                        self.BR_coxa = max(self.BR_coxa-coxa_dt, -b)
                        self.CL_coxa = min(self.CL_coxa+coxa_dt, c_max)
                    else:
                        self.AR_coxa = max(self.AR_coxa-coxa_dt, c_min)
                        self.BL_coxa = min(self.BL_coxa+coxa_dt, b)
                        self.CR_coxa = max(self.CR_coxa-coxa_dt, -c_max)
                        self.AL_coxa = min(self.AL_coxa+coxa_dt, -c_min)
                        self.BR_coxa = max(self.BR_coxa-coxa_dt, -b)
                        self.CL_coxa = min(self.CL_coxa+coxa_dt, c_max)
            else:
                if 'femur' in name:
                    self.AR_femur = min(self.AR_femur+dt, 0.0)
                    self.BL_femur = min(self.BL_femur+dt, 0.0)
                    self.CR_femur = min(self.CR_femur+dt, 0.0)
                    self.AL_femur = min(self.AL_femur+dt, 0.0)
                    self.BR_femur = min(self.BR_femur+dt, 0.0)
                    self.CL_femur = min(self.CL_femur+dt, 0.0)
                elif 'tibia' in name:                      
                    self.AR_tibia = max(self.AR_tibia-dt, 1.5)
                    self.BL_tibia = max(self.BL_tibia-dt, 1.5)
                    self.CR_tibia = max(self.CR_tibia-dt, 1.5)
                    self.AL_tibia = max(self.AL_tibia-dt, 1.5)
                    self.BR_tibia = max(self.BR_tibia-dt, 1.5)
                    self.CL_tibia = max(self.CL_tibia-dt, 1.5)
                else:
                    self.AR_coxa = 0.4
                    self.BL_coxa = 0.0
                    self.CR_coxa = -0.4
                    self.AL_coxa = -0.4
                    self.BR_coxa = 0.0
                    self.CL_coxa = 0.4                      
        # if self.speed > 0:
        #     print(self.AR_coxa , c_max, c_min)      
        new_actions = [self.AR_coxa, self.AR_femur, self.AR_tibia]
        new_actions += [self.AL_coxa, self.AL_femur, self.AL_tibia]
        new_actions += [self.BR_coxa, self.BR_femur, self.BR_tibia]       
        new_actions += [self.BL_coxa, self.BL_femur, self.BL_tibia]
        new_actions += [self.CR_coxa, self.CR_femur, self.CR_tibia]
        new_actions += [self.CL_coxa, self.CL_femur, self.CL_tibia]
        return new_actions

    def get_expert_actions_pos_horizontal(self):
        new_actions = []
        dt = 0.05
        if self.speed > 0:
            c_max, c_min, f_max, f_min = self.params['c_max'], self.params['c_min'], self.params['f_max'], self.params['f_min']
            t_max, t_min, b = self.params['t_max'], self.params['t_min'], self.params['b']
            period = (self.steps-1 - self.pause_time) % self.time_of_step
            tibia_dt = (t_max - t_min)/(2*self.time_of_step)
            # print(tibia_dt)
        else:
            c_max, c_min, f_max, f_min = self.params['c_max'], self.params['c_min'], self.params['f_max'], self.params['f_min']
            t_max, t_min, b = self.params['t_max'], self.params['t_min'], self.params['b']
            period = (self.steps-1 - self.pause_time) % self.time_of_step
            tibia_dt = (t_max - t_min)/(2*self.time_of_step)
            period = 0
        # if period == 0: print(self.ob_dict['left_swing'], self.ob_dict['right_swing'])       
        for n, name in enumerate(self.motor_names):
            if self.speed > 0:
                # Tibias
                if 'tibia' in name:
                    if period > 2 and period < 8 and ((self.ob_dict['left_swing'] and ('AL_tibia' in name or 'CR_tibia' in name)) or 
                        (self.ob_dict['middle_swing'] and ('BL_tibia' in name or 'BR_tibia' in name)) or 
                            (self.ob_dict['right_swing'] and ('AR_tibia' in name or 'CL_tibia' in name))):
                        if 'L' in name:
                            self.exp_cur[name] = t_min
                        else:
                            self.exp_cur[name] = t_max
                    elif 'AR_tibia' in name or 'CR_tibia' in name or 'BR_tibia' in name:
                        self.exp_cur[name] = max(self.exp_cur[name]-tibia_dt, t_min)
                    elif 'AL_tibia' in name or 'CL_tibia' in name or 'BL_tibia' in name:
                        self.exp_cur[name] = min(self.exp_cur[name]+tibia_dt, t_max)
                # Femurs
                elif 'femur' in name:
                    if period < 6 and ((self.ob_dict['left_swing'] and ('AL_femur' in name or 'CR_femur' in name)) or 
                        (self.ob_dict['middle_swing'] and ('BL_femur' in name or 'BR_femur' in name)) or 
                            (self.ob_dict['right_swing'] and ('AR_femur' in name or 'CL_femur' in name))):
                        self.exp_cur[name] = -f_max
                        # print(name, -f_max)
                    else:
                        self.exp_cur[name] = f_min
                # Coxas
                elif 'coxa' in name:
                    if 'AR' in name or 'CL' in name:
                        self.exp_cur[name] = 0.2
                    if 'BL' in name or 'BR' in name:
                        self.exp_cur[name] = 0.0
                    if 'CR' in name or 'AL' in name:
                        self.exp_cur[name] = -0.2
            else:
                if 'femur' in name:
                    self.exp_cur[name] = min(self.exp_cur[name]+dt, 0.0)
                    # self.exp_cur[name] = 0.0
                elif 'tibia' in name:                      
                    self.exp_cur[name] = max(self.exp_cur[name]-dt, 1.5)
                    # self.exp_cur[name] = 1.5
                elif 'coxa' in name:
                    if 'AR' in name or 'CL' in name:
                        self.exp_cur[name] = 0.4
                    if 'BL' in name or 'BR' in name:
                        self.exp_cur[name] = 0.0
                    if 'CR' in name or 'AL' in name:
                        self.exp_cur[name] = -0.4
        # if self.speed > 0:
        # print(self.speed, "AR coxa", self.exp_cur["AR_coxa_joint"])
        # print([self.exp_cur[m] for m in self.motor_names])

        #     print(self.AR_tibia , t_min, t_max)      
        new_actions = [self.exp_cur[m] for m in self.motor_names]
        return new_actions

    def get_expert_actions_pos_horizontal_tripod(self):
        # c_max = 0.8; c_min = 0.0
        # f_max = 0.3; f_min = 0.0
        # t_max = 1.9; t_min = 1.5
        # b = 0.4

        gain = 400
        coxa_gain = 800
        new_actions = []
        dt = 0.005
        if self.speed > 0:
            c_max, c_min, f_max, f_min = self.params['c_max'], self.params['c_min'], self.params['f_max'], self.params['f_min']
            t_max, t_min, b = self.params['t_max'], self.params['t_min'], self.params['b']
            period = (self.steps-1 - self.pause_time) % self.time_of_step
            tibia_dt = (t_max - t_min)/(10*self.time_of_step)
            # print(tibia_dt)
        else:
            c_max, c_min, f_max, f_min = self.params['c_max'], self.params['c_min'], self.params['f_max'], self.params['f_min']
            t_max, t_min, b = self.params['t_max'], self.params['t_min'], self.params['b']
            period = (self.steps-1 - self.pause_time) % self.time_of_step
            tibia_dt = (t_max - t_min)/(10*self.time_of_step)
            period = 0
        # if period == 0: print(self.ob_dict['left_swing'], self.ob_dict['right_swing'])       
        for n, name in enumerate(self.motor_names):
            # print(self.ob_dict['left_swing'], self.ob_dict['right_swing'])
            if self.ob_dict['left_swing']:
                if 'tibia' in name:
                    if period > 2 and period < 8:
                        self.AR_tibia = max(self.AR_tibia-tibia_dt, t_min)
                        self.BL_tibia = min(self.BL_tibia+tibia_dt, t_max)
                        self.CR_tibia = max(self.CR_tibia-tibia_dt, t_min)
                        self.AL_tibia = t_min
                        self.BR_tibia = t_max
                        self.CL_tibia = t_min
                    else:
                        self.AR_tibia = max(self.AR_tibia-tibia_dt, t_min)
                        self.BL_tibia = min(self.BL_tibia+tibia_dt, t_max)
                        self.CR_tibia = max(self.CR_tibia-tibia_dt, t_min)
                        self.AL_tibia = min(self.AL_tibia+tibia_dt,t_max)
                        self.BR_tibia = max(self.BR_tibia-tibia_dt, t_min)
                        self.CL_tibia = min(self.CL_tibia+tibia_dt, t_max)

                elif 'femur' in name:
                    if 'AL' in name or 'BR' in name or 'CL' in name:
                        # if period < 2*self.time_of_step/3:
                        if period < 6:
                            self.AL_femur = -f_max
                            self.BR_femur = -f_max
                            self.CL_femur = -f_max
                        else:
                            self.AL_femur = f_min
                            self.BR_femur = f_min
                            self.CL_femur = f_min
                    else:
                        self.AR_femur = f_min
                        self.BL_femur = f_min
                        self.CR_femur = f_min
                elif 'coxa' in name:
                    self.AR_coxa = 0.2
                    self.BL_coxa = 0.0
                    self.CR_coxa = -0.2
                    self.AL_coxa = -0.2
                    self.BR_coxa = 0.0
                    self.CL_coxa = 0.2
            elif self.ob_dict['right_swing']:
                if 'tibia' in name:
                    if period > 2 and period < 8:
                        self.AR_tibia = t_max
                        self.BL_tibia = t_min
                        self.CR_tibia = t_max
                        self.AL_tibia = min(self.AL_tibia+tibia_dt,t_max)
                        self.BR_tibia = max(self.BR_tibia-tibia_dt, t_min)
                        self.CL_tibia = min(self.CL_tibia+tibia_dt, t_max)
                    else:
                        self.AR_tibia = max(self.AR_tibia-tibia_dt, t_min)
                        self.BL_tibia = min(self.BL_tibia+tibia_dt, t_max)
                        self.CR_tibia = max(self.CR_tibia-tibia_dt, t_min)
                        self.AL_tibia = min(self.AL_tibia+tibia_dt,t_max)
                        self.BR_tibia = max(self.BR_tibia-tibia_dt, t_min)
                        self.CL_tibia = min(self.CL_tibia+tibia_dt, t_max)

                elif 'femur' in name:
                    if 'AR' in name or 'BL' in name or 'CR' in name:
                        # if period < 2*self.time_of_step/3:
                        if period < 6:
                            self.AR_femur = -f_max
                            self.BL_femur = -f_max
                            self.CR_femur = -f_max
                        else:
                            self.AR_femur = f_min
                            self.BL_femur = f_min
                            self.CR_femur = f_min
                    else:
                        self.AL_femur = f_min
                        self.BR_femur = f_min
                        self.CL_femur = f_min                        
                elif 'coxa' in name:
                    self.AR_coxa = 0.2
                    self.BL_coxa = 0.0
                    self.CR_coxa = -0.2
                    self.AL_coxa = -0.2
                    self.BR_coxa = 0.0
                    self.CL_coxa = 0.2
            else:
                if 'femur' in name:
                    self.AR_femur = min(self.AR_femur+dt, 0.0)
                    self.BL_femur = min(self.BL_femur+dt, 0.0)
                    self.CR_femur = min(self.CR_femur+dt, 0.0)
                    self.AL_femur = min(self.AL_femur+dt, 0.0)
                    self.BR_femur = min(self.BR_femur+dt, 0.0)
                    self.CL_femur = min(self.CL_femur+dt, 0.0)
                elif 'tibia' in name:                      
                    self.AR_tibia = max(self.AR_tibia-dt, 1.5)
                    self.BL_tibia = max(self.BL_tibia-dt, 1.5)
                    self.CR_tibia = max(self.CR_tibia-dt, 1.5)
                    self.AL_tibia = max(self.AL_tibia-dt, 1.5)
                    self.BR_tibia = max(self.BR_tibia-dt, 1.5)
                    self.CL_tibia = max(self.CL_tibia-dt, 1.5)
                else:
                    self.AR_coxa = 0.4
                    self.BL_coxa = 0.0
                    self.CR_coxa = -0.4
                    self.AL_coxa = -0.4
                    self.BR_coxa = 0.0
                    self.CL_coxa = 0.4                      
        # if self.speed > 0:
        #     print(self.AR_tibia , t_min, t_max)      
        new_actions = [self.AR_coxa, self.AR_femur, self.AR_tibia]
        new_actions += [self.AL_coxa, self.AL_femur, self.AL_tibia]
        new_actions += [self.BR_coxa, self.BR_femur, self.BR_tibia]       
        new_actions += [self.BL_coxa, self.BL_femur, self.BL_tibia]
        new_actions += [self.CR_coxa, self.CR_femur, self.CR_tibia]
        new_actions += [self.CL_coxa, self.CL_femur, self.CL_tibia]
        return new_actions
  

    def save_sim_data(self, best=False, worst=False, trans=False):
        comm = MPI.COMM_WORLD
        if comm.Get_rank() == 0:
            if trans: path = self.PATH + 'trans_'
            elif best: path = self.PATH + 'best_'
            elif worst: path = self.PATH + 'worst_'
            else: path = self.PATH
            try:
                np.save(path + 'sim_data.npy', np.array(self.sim_data))
                if path != self.PATH:
                    np.save(self.PATH + 'sim_data.npy', np.array(self.sim_data))
                # if (self.up_stairs or self.down_stairs or self.up_down_flat or self.mh) and self.box_info is not None:
                if self.box_info is not None:
                    np.save(path + 'box_info_pos.npy', np.array(self.box_info[1]))
                    np.save(path + 'box_info_size.npy', np.array(self.box_info[2]))
                    np.save(path + 'box_info_frict.npy', np.array(self.box_info[3]))
                    np.save(path + 'box_info_colour.npy', np.array(self.box_info[4]))
                    if path != self.PATH:
                        np.save(self.PATH + 'box_info_pos.npy', np.array(self.box_info[1]))
                        np.save(self.PATH + 'box_info_size.npy', np.array(self.box_info[2]))
                        np.save(self.PATH + 'box_info_frict.npy', np.array(self.box_info[3]))
                        np.save(self.PATH + 'box_info_colour.npy', np.array(self.box_info[4]))
            except Exception as e:
                print("Save sim data error:")
                print(e)

    # Function to save joint states, 
    def record_sim_data(self):
    	if len(self.sim_data) > 100000: return
    	data = [self.body_xyz, [self.qx, self.qy, self.qz, self.qw]]
    	joints = p.getJointStates(self.Id, self.motors)
    	data.append([i[0] for i in joints])
    	self.sim_data.append(data)
    
    def restore_sample(self, sample, steps, restore=True):
        sample_list = ['AL_coxa_joint_pos','BL_coxa_joint_pos','CL_coxa_joint_pos']
        sample_list += ['AL_coxa_joint_vel','BL_coxa_joint_vel','CL_coxa_joint_vel']
        sample_list += ['AL_coxa_joint_effort','BL_coxa_joint_effort','CL_coxa_joint_effort']
        # Right coxas
        sample_list += ['AR_coxa_joint_pos','BR_coxa_joint_pos','CR_coxa_joint_pos']
        sample_list += ['AR_coxa_joint_vel','BR_coxa_joint_vel','CR_coxa_joint_vel']
        sample_list += ['AR_coxa_joint_effort','BR_coxa_joint_effort','CR_coxa_joint_effort']
        # Left vels and efforts
        sample_list += ['AL_femur_joint_pos','AL_tibia_joint_pos','BL_femur_joint_pos']
        sample_list += ['BL_tibia_joint_pos','CL_femur_joint_pos','CL_tibia_joint_pos']   
        sample_list += ['AL_femur_joint_vel','AL_tibia_joint_vel','BL_femur_joint_vel']
        sample_list += ['BL_tibia_joint_vel','CL_femur_joint_vel','CL_tibia_joint_vel']   
        sample_list += ['AL_femur_joint_effort','AL_tibia_joint_effort','BL_femur_joint_effort']
        sample_list += ['BL_tibia_joint_effort','CL_femur_joint_effort','CL_tibia_joint_effort']   
        # right vels and efforts    
        sample_list += ['AR_femur_joint_pos','AR_tibia_joint_pos','BR_femur_joint_pos']
        sample_list += ['BR_tibia_joint_pos','CR_femur_joint_pos','CR_tibia_joint_pos']   
        sample_list += ['AR_femur_joint_vel','AR_tibia_joint_vel','BR_femur_joint_vel']
        sample_list += ['BR_tibia_joint_vel','CR_femur_joint_vel','CR_tibia_joint_vel']   
        sample_list += ['AR_femur_joint_effort','AR_tibia_joint_effort','BR_femur_joint_effort']
        sample_list += ['BR_tibia_joint_effort','CR_femur_joint_effort','CR_tibia_joint_effort']   
        sample_list += ['x', 'y', 'z']
        sample_list += ['orn_x', 'orn_y', 'orn_z', 'orn_w']
        sample_list += ['AL_tip_x', 'AL_tip_y', 'AL_tip_z']
        sample_list += ['AR_tip_x', 'AR_tip_y', 'AR_tip_z']
        sample_list += ['BL_tip_x', 'BL_tip_y', 'BL_tip_z']
        sample_list += ['BR_tip_x', 'BR_tip_y', 'BR_tip_z']
        sample_list += ['CL_tip_x', 'CL_tip_y', 'CL_tip_z']
        sample_list += ['CR_tip_x', 'CR_tip_y', 'CR_tip_z']
        
        self.sample_dict = {}

        self.sample_dict.update({name: val for name, val in zip(sample_list, sample)})
        joints = [self.sample_dict[j + '_pos'] for j in self.motor_names]
        effort = [self.sample_dict[j + '_effort'] for j in self.motor_names]
        if steps == 0:
            self.x_offset = self.sample_dict['x']
            self.sample_y_offset = self.sample_dict['y']
            self.sample_quat_offset = [self.sample_dict['orn_x'], self.sample_dict['orn_y'], self.sample_dict['orn_z'], self.sample_dict['orn_w']]
            pos = [self.sample_dict['x'] - self.x_offset + 0.5 , self.sample_dict['y'] + self.y_offset - self.sample_y_offset, self.sample_dict['z'] + 0.2 + self.z_offset]
            # self.prev_pos = pos
            # self.sample_speed = deque(maxlen=10)
        else:
            pos = [self.sample_dict['x'] - self.x_offset + 0.5 , self.sample_dict['y'] + self.y_offset - self.sample_y_offset, self.sample_dict['z']+ 0.1 + self.z_offset]
            # print((pos[0] - self.prev_pos[0])/(self.timeStep*2))
            # self.sample_speed.append((pos[0] - self.prev_pos[0])/(self.timeStep*2))
            # print(np.mean(self.sample_speed))
            # self.prev_pos = pos

        # r1 = R.from_quat(self.sample_quat_offset)
        # r2 = R.from_quat([self.sample_dict['orn_x'], self.sample_dict['orn_y'], self.sample_dict['orn_z'], self.sample_dict['orn_w']])
        # q2 * Quaternion.Inverse(q3)
        # r3 = r2.apply(r1.inv())
        orn = self.subtract_quaterion([self.sample_dict['orn_x'], self.sample_dict['orn_y'], self.sample_dict['orn_z'], self.sample_dict['orn_w']], self.sample_quat_offset)
        if restore:
            self.set_position(pos, orn, joints)
        return np.array(effort)
        # return np.array(joints)

    def subtract_quaterion(self, q1, q2):
        return self.multiply_quaternion(q1, self.inv_quaternion(q2))

    def inv_quaternion(self, q):
        x, y, z, w = q
        return [-x, -y, -z, w]
    
    def multiply_quaternion(self, q1, q2):
        x1, y1, z1, w1 = q1
        x2, y2, z2, w2 = q2
        w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
        x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
        y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
        z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2
        return [x, y, z, w]

    def set_position(self, pos, orn, joints=None, velocities=None, joint_vel=None):
        pos = [pos[0], pos[1]+self.y_offset, pos[2]]
        p.resetBasePositionAndOrientation(self.Id, pos, orn)
        if joints is not None:
            if joint_vel is not None:
                for j, jv, m in zip(joints, joint_vel, self.motors):
                    p.resetJointState(self.Id, m, targetValue=j, targetVelocity=jv)
            else:
                for j, m in zip(joints, self.motors):
                    p.resetJointState(self.Id, m, targetValue=j)
        if velocities is not None:
            p.resetBaseVelocity(self.Id, velocities[0], velocities[1])

    def transform_no_rot(self, pos, point):
        trans = np.array([[1,0,0,pos[0]],[0,1,0,pos[1]],[0,0,1,pos[2]],[0,0,0,1]])
        return np.dot(trans,[point[0],point[1],point[2], 1])

    # def get_image(self, cam_pos='lower'):
    #     return np.zeros(self.im_size)
    
    def transform_rot(self, yaw, pos, point):
        rot_mat = np.array(
        	[[np.cos(-yaw), np.sin(-yaw), 0],
        	 [-np.sin(-yaw), np.cos(-yaw), 0],
        	 [		0,			 0, 1]] )
        x,y,z = np.dot(rot_mat,[point[0],point[1],point[2]])
        return pos[0] + x, pos[1] + y, pos[2] + z

    def transform_rot(self, yaw, points):
        rot_mat = np.array(
                [[np.cos(yaw), -np.sin(yaw)],
                [np.sin(yaw), np.cos(yaw)]])
        return np.dot(rot_mat,points.T).T.astype(np.int32)

    def transform_rot_and_add(self, yaw, pos, points):
        # rot_mat = np.array(
        #         [[np.cos(yaw), -np.sin(yaw)],
        #         [np.sin(yaw), np.cos(yaw)]])
        # # return np.dot(rot_mat,points.T).T.astype(np.int32)
        # return np.add(np.dot(rot_mat,points.T).T,pos).astype(np.int32)
        rot_mat = np.array(
                [[np.cos(yaw), -np.sin(yaw), pos[0]],
                [np.sin(yaw), np.cos(yaw), pos[1]],
                [0, 0, 1]])
        return np.dot(rot_mat,points.T).T.astype(np.int32)

    def transform_pts(self, wm_size, arr):
        rot_mat = np.array(
            [[np.cos(np.pi/2), -np.sin(np.pi/2)],
            [np.sin(np.pi/2), np.cos(np.pi/2)]])
        arr =  np.dot(rot_mat,np.array(arr).T).T.astype(np.int32)
        if len(arr.shape)>1:
            new_arr = []
            for a in arr:
                new_arr.append([-1*a[1] + int(wm_size[1]/2),wm_size[0] + a[0]])
        else:
            new_arr = [-1*arr[1] + int(wm_size[1]/2),wm_size[0] + arr[0]]
        return np.array(new_arr)

    def get_world_map(self, box_info=None, world_shape=[30,10], grid_size=0.05):
        world_shape = [int((box_info[1][-1][0]+box_info[2][-1][0]+1)/grid_size), 2*int((box_info[1][-1][1]+box_info[2][-1][1]+1)/grid_size)]
        hm = np.zeros([world_shape[0], world_shape[1]]).astype(np.float32)
        if box_info is not None:
            positions = box_info[1]
            sizes = box_info[2]
            for pos, size in zip(positions, sizes):
                x, y, z = pos[0]/grid_size, pos[1]/grid_size, pos[2]
                x_size, y_size, z_size = size[0]/grid_size, size[1]/grid_size, size[2]
                # ij = np.array([[i,j,1] for i in range(int(x-x_size), int(x+x_size)) for j in range(int(y-y_size), int(y+y_size))])
                ij = np.array([[i,j,1] for i in range(int(0-x_size), int(0+x_size)+1) for j in range(int(0-y_size), int(0+y_size)+1)])
                hm_pts = self.transform_rot_and_add(0, [x, y+(world_shape[1])/2], ij)
                # hm_pts = self.transform_rot_and_add(0, [0, (world_shape[1])/2], ij)
                for pt in hm_pts:
                    hm[pt[0], pt[1]] = (z + z_size)

        self.grid_size = grid_size              
        self.dx_forward, self.dx_back, self.dy = 40, 20, 20        
        self.ij = np.array([[i,j,1] for i in range(-self.dx_back, self.dx_forward) for j in range(-self.dy, self.dy)])
        self.hm_ij = np.array([[i,j] for i in range(0, self.dx_back + self.dx_forward) for j in range(0, self.dy + self.dy)])
        return hm
    
    def get_hm(self):
        if self.world_map is not None:
            x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.ob_dict['yaw']
            if self.horizontal:
                yaw = yaw - np.pi/2
            self.hm = np.zeros([self.dx_back+self.dx_forward, 2*self.dy], dtype=np.float32)
            t1 = time.time()
            hm_pts = self.transform_rot_and_add(1*yaw, [x, y+self.world_map.shape[1]/2], self.ij)
            hm_pts[:,0] = np.clip(hm_pts[:,0], 0, self.world_map.shape[0]-1)
            hm_pts[:,1] = np.clip(hm_pts[:,1], 0, self.world_map.shape[1]-1)
            for ij_pts, pts in zip(self.hm_ij, hm_pts[:,:2]):
                try:
                    self.hm[ij_pts[0], ij_pts[1]] = self.world_map[pts[0], pts[1]]
                except Exception as e:
                    print(e)
                    print(ij_pts, pts, self.hm.shape, self.world_map.shape)
            self.hm = np.clip(self.hm - self.z_offset, -1, 1)
        else:
            self.hm = np.zeros(self.im_size)
        # print(time.time() - t1)
        # print(self.hm)
        if self.args.display_im:
            self.display_im()        

    def get_image(self, mode=None):
        '''
        Return height map, need to offset z, 
        '''
        self.get_hm()
        return self.hm

    def display_im(self):
        grid_size = 0.05
        dx_forward, dx_back, dy = 40, 20, 20
        x1, y1, x2, y2, x3, y3, x4, y4 = dx_forward, dy, -dx_back, dy, -dx_back, -dy, dx_forward, -dy
        # x,y,yaw = self.body_xyz[0], self.body_xyz[1], self.ob_dict['yaw']
        x,y,yaw = self.body_xyz[0]/grid_size, self.body_xyz[1]/grid_size, self.ob_dict['yaw']
        if self.horizontal:
            yaw = yaw - np.pi/2
        norm_wm = self.world_map/np.max(self.world_map)
        wm_col = cv2.cvtColor(norm_wm,cv2.COLOR_GRAY2RGB)
        rect_pts = np.array([[y1,x1,1],[y2,x2,1],[y3,x3,1],[y4,x4,1]])
        pts = self.transform_rot_and_add(-1*yaw, [y+self.world_map.shape[1]/2, x], rect_pts)        
        pts = pts[:, :2]
        cv2.polylines(wm_col, [pts], True, (0,255,0), 1)   
        norm_hm = (np.min(self.hm) - self.hm)/(np.max(self.hm) - np.min(self.hm))
        # print(np.min(self.hm), np.max(self.hm) )
        # norm_hm = self.hm/np.max(self.world_map)
        # norm_hm = self.hm/np.max(self.hm)
        
        norm_hm = cv2.cvtColor(norm_hm,cv2.COLOR_GRAY2RGB)
        norm_hm = cv2.flip(norm_hm, 0)
        norm_hm = cv2.flip(norm_hm, 1)
        wm_col = cv2.flip(wm_col, 0)
        wm_col = cv2.flip(wm_col, 1)
        # print(self.hm.shape, norm_hm.shape)
        # h,w = norm_hm.shape[0], norm_hm.shape[1] 
        # center = (h / 2, w / 2)
        # M = cv2.getRotationMatrix2D(center, 270, 1.0)
        # norm_hm = cv2.warpAffine(norm_hm, M, (h, w))
        # cv2.imshow('frame',norm_hm)
        # cv2.waitKey(1)
        # print(norm_hm.shape, self.hm.shape, wm_col.shape, self.world_map.shape)
        # resized_hm = np.zeros(wm_col.shape)
        # print(norm_hm.shape)
        # resized_hm[:norm_hm.shape[0],:norm_hm.shape[1],:] = norm_hm
        # resized_hm = cv2.resize(norm_hm, (wm_col.shape[1], wm_col.shape[0]))
        # resized_hm = cv2.resize(norm_hm, None, fx=2, fy=2)
        # cv2.imshow('frame', wm_col)
        # cv2.imshow('frame', norm_hm)
        cv2.imshow('world_map', wm_col)
        cv2.waitKey(1)
        cv2.imshow('local_map', norm_hm)
        cv2.waitKey(1)


if __name__ == "__main__":
    from mpi4py import MPI
    import argparse
    import cv2
    parser = argparse.ArgumentParser()
    parser.add_argument('--render',default=False,action='store_true')
    parser.add_argument('--cur',default=False,action='store_true')
    parser.add_argument('--up_stairs',default=False,action='store_true')
    parser.add_argument('--down_stairs',default=False,action='store_true')
    args = parser.parse_args()
    env = Env()

    PATH = '/home/brendan/results/thing/'
    env.arguments(RENDER=args.render, PATH=PATH, cur=args.cur, record_step=False, up_stairs=args.up_stairs, down_stairs=args.down_stairs)
    env.height_coeff = 0.1

    ob = env.reset()
    i = 0
    while True:
        # print(boxes)
        actions = (np.random.rand(18)-0.5)*2

        ob, reward, done, ob_dict = env.step(actions)
        env.get_image()
        # time.sleep(0.05)
        # cv2.imshow("frame", cv2.cvtColor(image[2], cv2.COLOR_BGR2RGB))
        # cv2.imshow("frame", image)
        # cv2.waitKey(1)
        if done:
            # print(done)
            env.reset()
            # env.obstacles.create_rand_floor()
        i += 1
