import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import numpy as np
import pybullet as p
from collections import deque
from pathlib import Path
home = str(Path.home())
from mpi4py import MPI
comm = MPI.COMM_WORLD
import time
import cv2
import random

class Env():
  rank = comm.Get_rank()
  # Sim preferes 240Hz
  # simStep = 1/240
  simStep = 1/120
  # Mocap rate is 120Hz
  timeStep = 1/120
  ob_size = (12*2+8 + 6 + 7 + 2)
  im_size = [60,40,1]
  ac_size = 12
  total_steps = 0
  steps = 0
  Kp = 400
  initial_Kp = Kp
  Kd = 0.1
  height_coeff = 0.01
  difficulty = 1
  max_yaw = 0.0
  episodes = 0
  iters_so_far = 0
  max_disturbance = 500
  rew_Kp = 1
  grid_size=0.025

  def __init__(self, render=False, PATH=None,  args=None, record_step=True, display_im=False, cur=False, obstacle_type=None, control_type=None, vis=True, disturbances=True):

    disturbances = False

    self.render = render
    self.PATH = PATH
    self.args = args
    self.record_step = record_step
    self.display_im = display_im
    self.cur = cur
    self.obstacle_type = obstacle_type
    self.control_type = control_type
    self.vis = vis
    self.stopped_start = True
    # self.stopped_start = False
    self.disturbances = disturbances

    self.reward_breakdown = {'goal':deque(maxlen=100), 'pos':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100), 'com':deque(maxlen=100), 'sym':deque(maxlen=100)}
  
    # subject = '113'
    # file_name = '_19'
    subject = '02'
    file_name = '_01'
    self.samples = np.load('samples/' + subject + file_name + '_samples.npy')
    print('loaded samples from ' + home + '/data/iros_data/mocap/02/' + subject + file_name + '_samples.npy with shape: ', self.samples.shape)
    self.sample_size = self.samples.shape[0]

    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot


    self.sim_data = []
    self.exp_sim_data = []
    self.load()
    self.prev_exp_pos = 0.0
    self.prev_my_pos = 0.0

    self.world_map = None
    self.z_offset = 0
    try:
      from assets.obstacles import Obstacles
    except:
      from obstacles import Obstacles
    self.obstacles = Obstacles()
    self.obstacles.add_domain_training_world()
    self.box_info = self.obstacles.get_box_info()
    self.world_map = self.get_world_map(self.box_info) 
    self.full_buffer = []


  def load(self):
    
    p.loadMJCF(currentdir + "/ground.xml")
    # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
    objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
    # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION )
    self.Id = objs

    p.setTimeStep(self.simStep)
    p.setGravity(0,0,-9.8)
    # ======================================================================

    numJoints = p.getNumJoints(self.Id)
    # Camera following robot:
    # body_xyz, (qx, qy, qz, qw) = p.getBasePositionAndOrientation(self.Id)
    # p.resetDebugVisualizerCamera(2.0, self.camera_rotation, -10.0, body_xyz)
    # p.resetDebugVisualizerCamera(2.0, 50, -10.0, body_xyz)

    self.jdict = {}
    self.feet_dict = {}
    self.leg_dict = {}
    self.body_dict = {}
    self.feet = ["left_heel1", "left_heel2", "right_heel1", "right_heel2", "left_toe", "right_toe"]
    self.feet_contact = {f:True for f in self.feet}
    self.ordered_joints = []
    self.ordered_joint_indices = []

    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name=="left_heel1": self.feet_dict["left_heel1"] = j
      if link_name=="left_heel2": self.feet_dict["left_heel2"] = j
      if link_name=="right_heel1": self.feet_dict["right_heel1"] = j
      if link_name=="right_heel2": self.feet_dict["right_heel2"] = j
      if link_name=="left_toe": self.feet_dict["left_toe"] = j
      if link_name=="right_toe": self.feet_dict["right_toe"] = j
      # print(link_name)
      if link_name=="base_link": self.body_dict["body_link"] = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
    
    self.motor_names = ["right_hip_z", "right_hip_x", "right_hip_y", "right_knee"]
    self.motor_names += ["right_ankle_x", "right_ankle_y"]
    self.motor_names += ["left_hip_z", "left_hip_x", "left_hip_y", "left_knee"]
    self.motor_names += ["left_ankle_x", "left_ankle_y"]
    self.motor_power = [300, 300, 900, 600]
    self.motor_power += [150, 300]
    self.motor_power += [300, 300, 900, 600]
    self.motor_power += [150, 300]
    
    # self.motor_power = [100, 100, 300, 200]
    # self.motor_power += [50, 100]
    # self.motor_power += [100, 100, 300, 200]
    # self.motor_power += [50, 100]
    

    # self.motor_scale = [50, 50, 75, 50]
    # self.motor_scale += [25, 25]
    # self.motor_scale += [50, 50, 900, 600]
    # self.motor_scale += [150, 300]
    # self.motor_scale = [self.motor_scale*m for m in self.motor_scale]
    
    self.motors = [self.jdict[n] for n in self.motor_names]
    # print(self.motors)

    # self.tor_max = np.array([20,20,20,20,20,20,20,20,20,20,20,20])

    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}

    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

    for key in self.feet_dict:
      p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9)

    self.ep_lens = deque(maxlen=5)


  def reset(self, params=None):
    if self.episodes > 0 and self.record_step:
      self.save_sim_data()
      # if self.episodes > 1:
      #   exit()
    if self.episodes > 0 and self.args.display_doa and self.rank == 0:
    # if self.episodes > 0 and self.rank == 0:
      if not self.args.test_pol:
        self.episode_buffer = np.array(self.episode_buffer)
        self.episode_buffer[-30:,-1] = 0.5
        self.episode_buffer[-1,-1] = 1
      # print(self.episode_buffer)
      self.full_buffer = list(self.episode_buffer)
      # self.full_buffer.extend(list(self.episode_buffer))
      # print(len(self.full_buffer))
      # self.update_display()
      t1 = time.time()
      for status in self.full_buffer:
        
        y, x = int(status[1]/self.grid_size + (-self.min_y+1)/self.grid_size), int(status[0]/self.grid_size +(-self.min_x+1)/self.grid_size)

        # y2, x2 = 0.25*np.sin(status[2]) + status[1], 0.25*np.cos(status[2]) + status[0]
        # y2, x2 = int(y2/self.grid_size + (-self.min_y+1)/self.grid_size), int(x2/self.grid_size +(-self.min_x+1)/self.grid_size)
        if not self.args.test_pol:
          if status[3] == 1:
            # cv2.arrowedLine(self.doa_wm_col, (y,x),(y2,x2), (0,0,255), 1)
            cv2.circle(self.doa_wm_col,(y, x), 3, (0,0,255), -1)
          elif status[3] == 0.5:
            # cv2.arrowedLine(self.doa_wm_col, (y,x),(y2,x2), (255,0,0), 1)
            cv2.circle(self.doa_wm_col,(y, x), 1, (255,0,0), -1)
          elif status[3] == 0:
            # cv2.arrowedLine(self.doa_wm_col, (y,x),(y2,x2), (0,255,0), 1)
            cv2.circle(self.doa_wm_col,(y, x), 1, (0,255,0), -1)
        else:
          # print(status[3])
          if status[3] > 0.3:
            cv2.circle(self.doa_wm_col,(y, x), 1, (0,0,int(255*status[3])), -1)
          # elif status[3] > 0.1:
            # cv2.circle(self.doa_wm_col,(y, x), 1, (int(255*status[3]),0,0), -1)
          else:
            cv2.circle(self.doa_wm_col,(y, x), 1, (0,int(255*status[3]),0), -1)
      # print(time.time()-t1)
      if self.args.display_doa:
        cv2.imshow('doa_map', self.doa_wm_col)
        cv2.waitKey(1)
      else:
        cv2.imwrite(self.PATH + 'doa_map.png', self.doa_wm_col)

    self.episode_buffer = []

    self.vel_buffer = deque(maxlen=100)
    self.initial_xyz = self.exp_body_xyz = np.array([0.0,0.0,0.0])
  
    self.ep_lens.append(self.steps)
  
          
    self.initial_xyz = self.exp_body_xyz
    # temp = self.samples[self.sample_pointer,:]
    # self.exp_body_xyz, self.exp_body_rot, self.exp_joints, self.exp_joint_vel = temp[:3], p.getQuaternionFromEuler(temp[3:6]), temp[6:self.ac_size+6], temp[self.ac_size+6:]
    # start_rot = [0.0,0.0,np.random.uniform(-np.pi, np.pi)]
    start_rot = [0.0,0.0,0.0]
    start_orn = p.getQuaternionFromEuler(start_rot)
    # self.body_xyz, self.yaw = [self.exp_body_xyz[0], self.exp_body_xyz[1], self.exp_body_xyz[2] + 0.95 ], start_rot[2]
    
    # Randomly select location on the map, reselect if not on the path
    self.box_num = np.random.randint(50, len(self.box_info[0]))
    box_x, box_y = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1]
    self.body_xyz, self.yaw = [box_x, box_y + np.random.uniform(-0.2, 0.2), 0.0], np.random.uniform(-np.pi, np.pi)
    self.get_hm()
    while self.z_offset < 0.75:
      self.body_xyz = [np.random.uniform(self.min_x, self.max_x), np.random.uniform(self.min_y, self.max_y), 0.0]
      self.get_hm()

    self.ob_dict = {'prev_right_foot_left_ground':False,'prev_left_foot_left_ground':False,'left_foot_left_ground':False,'right_foot_left_ground':False}
    self.ob_dict.update({'exp_prev_right_foot_left_ground':False,'exp_prev_left_foot_left_ground':False,'exp_left_foot_left_ground':False,'exp_right_foot_left_ground':False})
    
    self.ob_dict['prev_right_heel1'] = self.ob_dict['right_heel1'] = False
    self.ob_dict['prev_right_heel2'] = self.ob_dict['right_heel2'] = False
    self.ob_dict['prev_right_toe']   = self.ob_dict['right_toe']   = False
    self.ob_dict['prev_left_heel1']  = self.ob_dict['left_heel1']  = False
    self.ob_dict['prev_left_heel2']  = self.ob_dict['left_heel2']  = False
    self.ob_dict['prev_left_toe']    = self.ob_dict['left_toe']    = False
    
    # self.ob_dict['ready_to_walk'] = False

    self.ob_dict['right_foot_swing'] = False
    self.ob_dict['left_foot_swing'] = False

    
    pos, orn, joints, base_vel, joint_vel = [self.body_xyz[0],self.body_xyz[1],0.95 + self.z_offset],p.getQuaternionFromEuler([0,0,self.yaw]), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
  
    # pos, orn, base_vel = [0,0,0.95 + self.z_offset],[0,0,0,1], [[0,0,0],[0,0,0]]    
    # joints, joint_vel = self.exp_joints, self.exp_joint_vel
    
    if self.disturbances:
      c = [0.01, 0.01, 0.01, 0.012, 0.1, 0.01, 0.01]
      pos[0] += np.random.uniform(low=0.2, high=c[0])
      pos[1] += np.random.uniform(low=-c[0], high=c[0])
      # No noise to z, can get stuck in the floor
      # pos[2] += np.random.uniform(low=-c[0], high=c[0])
      orn[0] += np.random.uniform(low=-c[2], high=c[2])
      orn[1] += np.random.uniform(low=-c[2], high=c[2])
      orn[2] += np.random.uniform(low=-c[2], high=c[2])
      # Noise on w term does funky things
      # orn[3] += np.random.uniform(low=-c[2], high=c[2])
      for i in range(len(base_vel[0])):
        base_vel[0][i] += np.random.uniform(low=-c[3], high=c[3])
        base_vel[1][i] += np.random.uniform(low=-c[4], high=c[4])

      # for i, (j, m) in enumerate(zip(self.ordered_joints, self.motor_names)):
      #   if 'ankle_y' not in m: #Don't want to at noise to ankle y, foot can end up stuck in the floor
      #     joints[i] += np.clip(np.random.uniform(low=-c[5], high=c[5]), j[1], j[2])
      #     joint_vel[i] += np.random.uniform(low=-c[6], high=c[6])

    self.set_position(pos, orn, joints, base_vel, joint_vel)

    self.prev_speed = self.speed = 0.0
    self.total_reward = 0
    self.steps = 0
    self.episodes += 1
    self.get_observation()
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel
    self.prev_exp_joints = [0.0]*self.ac_size

    # self.time_of_step = int(60/abs(1.0))
    self.time_of_step = int(self.sample_size/2.0)
    self.action_store = deque(maxlen=self.time_of_step)
    
    self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.speed, 0.0]
    return np.array(self.state)

  def step(self, actions, doa=None):
    # if (self.speed_cur and ((self.steps*self.timeStep) % 3) == 0 and self.steps != 0) or self.steps == 50:
    #     self.speed = np.random.uniform(low=self.min_v,high=self.max_v)           

    #     self.episode_speeds.append(self.speed)
    #     self.yaw_v = np.random.uniform(low=-self.max_yaw,high=self.max_yaw)           
    #     if self.strafe:
    #         self.y_v = np.random.uniform(low=-self.max_vy,high=self.max_vy)          
    #     if abs(self.speed) >= 0.25:
    #         self.time_of_step = int(60/abs(self.speed))
    #     else:
    #         self.time_of_step = 0

    #     # # yaw and no x and no y, no y no yaw, no x no yaw 
    #     if abs(self.yaw_v) < 0.25 and abs(self.yaw) < 0.25:
    #         self.yaw_v = 0
    #         self.no_yaw = True
    #     else:
    #         self.no_yaw = False
    #     self.action_store = deque(maxlen=self.time_of_step)
    if self.disturbances and random.random() < 0.02:
      self.add_disturbance(self.max_disturbance)
        
    # self.phase = self.sample_pointer/self.sample_size
    
    self.prev_speed = self.speed
    if self.steps > 50 or not self.stopped_start:
    # if self.steps > 100 or not self.stopped_start:
      self.speed = 1.0
      self.time_of_step = int(self.sample_size/2.0)
      # self.time_of_step = int(60/abs(1.0))
    else: 
      self.speed = 0.0
      self.time_of_step = 0
    # print(self.speed)

    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel
    
    self.actions = actions

    forces = np.array(self.motor_power)*np.array(self.actions)*0.082 
    forces = np.clip(forces, -100, 100)


    # self.set_position([0.0, 0.0, self.exp_body_xyz[2] + 0.90 + self.z_offset], self.exp_body_rot)

    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=forces)

    for _ in range(int(self.timeStep/self.simStep)):
      p.stepSimulation()

    if self.render:
      time.sleep(0.01)
    self.get_observation()
    if self.record_step: 
      self.record_sim_data()
    reward, done = self.get_reward()
    self.prev_actions = self.actions
    self.total_reward += reward
    self.steps += 1
    self.total_steps += 1

    if self.args.test_pol:
      self.episode_buffer.append([self.body_xyz[0], self.body_xyz[1], self.yaw, doa])
    else:
      self.episode_buffer.append([self.body_xyz[0], self.body_xyz[1], self.yaw, 0])

    self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.speed, 0.0]
    return np.array(self.state), reward, done, None

  def get_reward(self):
    reward = 0
    done = False
    goal, pos, vel, neg, tip, com, sym = 0, 0, 0, 0, 0, 0, 0
    self.reward_breakdown['goal'].append(goal)
    self.reward_breakdown['pos'].append(pos)
    self.reward_breakdown['vel'].append(vel)
    self.reward_breakdown['neg'].append(neg)
    self.reward_breakdown['tip'].append(tip)
    self.reward_breakdown['com'].append(com)
    self.reward_breakdown['sym'].append(sym)

    if self.body_xyz[2] < (0.8 + self.z_min) or (abs(self.pitch) > np.pi/4 or abs(self.roll) > np.pi/4):   
      done = True
    return reward, done

  def get_observation(self):
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.uniform(-0.5,0.5,self.ac_size))
    
    self.ob_dict.update({n + '_pos':j for n,j in zip(self.motor_names, self.joints)})


    self.body_xyz, (self.qx, self.qy, self.qz, self.qw) = p.getBasePositionAndOrientation(self.Id)
    self.roll, self.pitch, self.yaw = p.getEulerFromQuaternion([self.qx, self.qy, self.qz, self.qw])

    self.body_vxyz, self.base_rot_vel = p.getBaseVelocity(self.Id)
    
    self.roll_vel = self.base_rot_vel[0]
    self.pitch_vel = self.base_rot_vel[1]
    self.yaw_vel = self.base_rot_vel[2]

    rot_speed = np.array(
      [[np.cos(-self.yaw), -np.sin(-self.yaw), 0],
        [np.sin(-self.yaw), np.cos(-self.yaw), 0],
        [		0,			 0, 1]]
    )

    self.vx, self.vy, self.vz = np.dot(rot_speed, (self.body_vxyz[0],self.body_vxyz[1],self.body_vxyz[2]))
    
    # Policy shouldn't know yaw
    # self.body = [self.vx, self.vy, self.vz, self.roll, self.pitch, self.roll_vel, self.pitch_vel, self.yaw_vel]

    self.body = [self.vx, self.vy, self.vz, self.roll, self.pitch, self.roll_vel, self.pitch_vel, self.yaw_vel, self.body_xyz[2] - self.z_offset]


    self.ob_dict['prev_right_heel1'] = self.ob_dict['right_heel1'] 
    self.ob_dict['prev_right_heel2'] = self.ob_dict['right_heel2'] 
    self.ob_dict['prev_right_toe']   = self.ob_dict['right_toe']   
    self.ob_dict['prev_left_heel1']  = self.ob_dict['left_heel1']  
    self.ob_dict['prev_left_heel2']  = self.ob_dict['left_heel2']  
    self.ob_dict['prev_left_toe']    = self.ob_dict['left_toe']    

    self.ob_dict['right_heel1'] = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel1'], -1))>0
    self.ob_dict['right_heel2'] = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel2'], -1))>0
    self.ob_dict['right_toe']   = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_toe'], -1))>0
    self.ob_dict['left_heel1']  = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel1'], -1))>0
    self.ob_dict['left_heel2']  = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel2'], -1))>0
    self.ob_dict['left_toe']    = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_toe'], -1))>0

    # Update feet that have left the ground.
    self.ob_dict['prev_right_foot_left_ground'] = self.ob_dict['right_foot_left_ground']
    self.ob_dict['prev_left_foot_left_ground'] = self.ob_dict['left_foot_left_ground']
    self.ob_dict['right_foot_left_ground'] = not self.ob_dict['right_heel1'] and not self.ob_dict['right_heel2'] and not self.ob_dict['right_toe']
    self.ob_dict['left_foot_left_ground'] = not self.ob_dict['left_heel1'] and not self.ob_dict['left_heel2'] and not self.ob_dict['left_toe']

    right_contacts = [self.ob_dict['right_heel1'],self.ob_dict['right_heel2'],self.ob_dict['right_toe']]
    left_contacts = [self.ob_dict['left_heel1'],self.ob_dict['left_heel2'],self.ob_dict['left_toe']]
    prev_right_contacts = [self.ob_dict['prev_right_heel1'],self.ob_dict['prev_right_heel2'],self.ob_dict['prev_right_toe']]
    prev_left_contacts = [self.ob_dict['prev_left_heel1'],self.ob_dict['prev_left_heel2'],self.ob_dict['prev_left_toe']]
    
    self.contacts = right_contacts + left_contacts + prev_right_contacts + prev_left_contacts

    right_contacts = [self.ob_dict['right_heel1'],self.ob_dict['right_heel2'],self.ob_dict['right_toe']]
    left_contacts = [self.ob_dict['left_heel1'],self.ob_dict['left_heel2'],self.ob_dict['left_toe']]
    prev_right_contacts = [self.ob_dict['prev_right_heel1'],self.ob_dict['prev_right_heel2'],self.ob_dict['prev_right_toe']]
    prev_left_contacts = [self.ob_dict['prev_left_heel1'],self.ob_dict['prev_left_heel2'],self.ob_dict['prev_left_toe']]
    
    self.contacts = right_contacts + left_contacts + prev_right_contacts + prev_left_contacts

    i = self.box_num
    total_i = 0
    while True:
      x, y = self.body_xyz[0], self.body_xyz[1]
      x1, y1 = self.box_info[1][i][0] - self.box_info[2][i][0], self.box_info[1][i][1] - self.box_info[2][i][1]
      x2, y2 = self.box_info[1][i][0] + self.box_info[2][i][0], self.box_info[1][i][1] + self.box_info[2][i][1]
      if x1 > x2:
        x_temp = x1
        x1 = x2
        x2 = x_temp
      elif y1 > y2:
        y_temp = y1
        y1 = y2
        y2 = y_temp
      if x1 < x < x2 and y1 < y < y2:
        self.box_num = i
        # print("robot is over box number", self.box_num)
        break
      i += 1
      total_i += 1
      if i > (len(self.box_info[0]) - 1):
        i = 0
      if total_i > (len(self.box_info[0]) - 1):
        # print("com not over box, box number frozen")
        break

    # if self.obstacle_type != 'flat':
    zs = []
    zs.append(self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2])
    if self.box_num - 1 > 0:
      zs.append(self.box_info[1][self.box_num - 1][2] + self.box_info[2][self.box_num - 1][2])
    if self.box_num + 1 < len(self.box_info[0]):
      zs.append(self.box_info[1][self.box_num + 1][2] + self.box_info[2][self.box_num + 1][2])
    self.z_min = min(np.clip(zs, 0.5,None))
    self.z_max = max(zs)

  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))       
        
        try:
          box_info = self.obstacles.get_box_info()
          np.save(path + 'box_info.npy', np.array(box_info))
        except Exception as e:
          print("trying to save box info", e)
        self.sim_data = []
    
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)
    
  def get_hm(self):
    if self.world_map is not None:
      x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.yaw
      try:
        x_pix = int(x+(-self.min_x+1)/self.grid_size)
        y_pix = int(y+(-self.min_y+1)/self.grid_size)
        ordered_pix = np.sort(self.world_map[x_pix-5:x_pix+5, y_pix-5:y_pix+5], axis=None)
        self.z_offset = max(ordered_pix[-1], 0.5)
      except Exception as e:
        # pass
        # print(e)
        # print("We're off the map harry!")
        self.z_offset = 0.5
      self.hm = np.ones([self.dx_back+self.dx_forward, 2*self.dy], dtype=np.float32)*-1
      t1 = time.time()
      hm_pts = self.transform_rot_and_add(1*yaw, [x+(-self.min_x+1)/self.grid_size, y+(-self.min_y+1)/self.grid_size], self.ij)
      hm_pts[:,0] = np.clip(hm_pts[:,0], 0, self.world_map.shape[0]-1)
      hm_pts[:,1] = np.clip(hm_pts[:,1], 0, self.world_map.shape[1]-1)
      
      self.hm[self.hm_ij[:,0],self.hm_ij[:,1]] = self.world_map[hm_pts[:,0],hm_pts[:,1]]
      # for ij_pts, pts in zip(self.hm_ij, hm_pts[:,:2]):
      #   try:
      #     self.hm[ij_pts[0], ij_pts[1]] = self.world_map[pts[0], pts[1]]
      #   except Exception as e:
      #     print(e)
      #     print(ij_pts, pts, self.hm.shape, self.world_map.shape)
      self.hm = np.clip(self.hm - self.z_offset, -1, 1).reshape(self.im_size)
      # Normalise
      self.hm = (self.hm+1)/2
    else:
      self.hm = np.zeros(self.im_size, dtype=np.float32)
      # self.z_offset = 0.5
    if self.display_im:
      self.display()        
    return self.hm
  
  def display(self):
    
    # dx_forward, dx_back, dy = 40, 20, 20
    x1, y1, x2, y2, x3, y3, x4, y4 = self.dx_forward, self.dy, -self.dx_back, self.dy, -self.dx_back, -self.dy, self.dx_forward, -self.dy
    
    dx_forward_col, dx_back_col, dy_col = 30, 12, 12
    x1_col, y1_col, x2_col, y2_col, x3_col, y3_col, x4_col, y4_col = dx_forward_col, dy_col, -dx_back_col, dy_col, -dx_back_col, -dy_col, dx_forward_col, -dy_col
      
    if self.world_map is not None:
      norm_wm = (np.min(self.world_map) - self.world_map)/(np.max(self.world_map) - np.min(self.world_map))
    else:
      norm_wm = np.zeros([100,100])
    wm_col = cv2.cvtColor(norm_wm,cv2.COLOR_GRAY2RGB)
   
    x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.yaw
    rect_pts = np.array([[y1,x1,1],[y2,x2,1],[y3,x3,1],[y4,x4,1]])
    pts = self.transform_rot_and_add(-1*yaw, [y+ (-self.min_y+1)/self.grid_size, x+ (-self.min_x+1)/self.grid_size], rect_pts)        
    pts = pts[:, :2]
    cv2.polylines(wm_col, [pts], True, (0,255,0), 1)
    cv2.circle(wm_col,(int(y + (-self.min_y+1)/self.grid_size), int(x +(-self.min_x+1)/self.grid_size)), 3, (0,0,255), -1)
   
    norm_hm = np.array(self.hm*255.0, dtype=np.uint8)
    norm_hm = cv2.rotate(norm_hm, cv2.ROTATE_180)
    px,py = self.hm.shape[0], self.hm.shape[1]

    if self.rank == 0:
      cv2.imshow('world_map', wm_col)
      cv2.waitKey(1)
      cv2.imshow('local_map', norm_hm)
      cv2.waitKey(1)

  def get_world_map(self, box_info=None, grid_size=0.025):
    gz = False
    if gz:
      self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0]/2)
      self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1]/2)
      self.max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0]/2)
      self.max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1]/2)
    else:
      self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0])
      self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1])
      self.max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0])
      self.max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1])
    world_shape = [int((self.max_x - self.min_x + 2)/grid_size), int((self.max_y - self.min_y + 2)/grid_size)]

    hm = np.ones([world_shape[0], world_shape[1]]).astype(np.float32)*-1
    if box_info is not None:
      positions = box_info[1]
      sizes = box_info[2]
      for pos, size in zip(positions, sizes):
        x, y, z = pos[0]/grid_size, pos[1]/grid_size, pos[2]
        if gz:
          x_size, y_size, z_size = (size[0]/2)/grid_size, (size[1]/2)/grid_size, size[2]/2
        else:
          x_size, y_size, z_size = (size[0])/grid_size, (size[1])/grid_size, size[2]
        ij = np.array([[i,j,1] for i in range(int(0-x_size), int(0+x_size)) for j in range(int(0-y_size), int(0+y_size))])

        hm_pts = self.transform_rot_and_add(1*pos[5], [x+(-self.min_x+1)/grid_size, y+(-self.min_y+1)/grid_size], ij)
        for pt in hm_pts:
          try:
            # Not sure why this doesn't work for larger z heights? Should be box z pos + box z size (half extent)
            # if hm[pt[0], pt[1]] < (z + z_size):
            #   hm[pt[0], pt[1]] = (z + z_size)
            if hm[pt[0], pt[1]] < (z * 2):
              hm[pt[0], pt[1]] = (z * 2)
          except Exception as e:
            print(e)
            print(pt)

    self.grid_size = grid_size              
    self.dx_forward, self.dx_back, self.dy = 36, 24, 20        
    self.ij = np.array([[i,j,1] for i in range(-self.dx_back, self.dx_forward) for j in range(-self.dy, self.dy)])
    self.hm_ij = np.array([[i,j] for i in range(0, self.dx_back + self.dx_forward) for j in range(0, self.dy + self.dy)])

    # if self.display_im:
    norm_wm = (np.min(hm) - hm)/(np.max(hm) - np.min(hm))
    self.doa_wm_col = cv2.cvtColor(norm_wm,cv2.COLOR_GRAY2RGB)

    return hm

  def transform_rot_and_add(self, yaw, pos, points):
    rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw), pos[0]],
            [np.sin(yaw), np.cos(yaw), pos[1]],
            [0, 0, 1]])
    return np.dot(rot_mat,points.T).T.astype(np.int32)

  def add_disturbance(self, max_dist=None):
    force_x = (random.random() - 0.5)*max_dist
    force_y = (random.random() - 0.5)*max_dist
    force_z = (random.random() - 0.5)*(max_dist/4)
    # print("applying forces", force_x, force_y, force_z)
    p.applyExternalForce(self.Id,-1,[force_x,force_y,force_z],[0,0,0],p.LINK_FRAME)
    force_roll = (random.random() - 0.5)*max_dist/2
    force_pitch = (random.random() - 0.5)*max_dist/2
    force_yaw = (random.random() - 0.5)*(max_dist/2)
    p.applyExternalTorque(self.Id,-1,[force_roll,force_pitch,force_yaw],p.LINK_FRAME)

  def log_stuff(self, logger, writer, iters_so_far):
    self.iters_so_far = iters_so_far
    writer.add_scalar("breakdown/goal", np.mean(self.reward_breakdown['goal']), self.iters_so_far)        
    writer.add_scalar("breakdown/pos", np.mean(self.reward_breakdown['pos']), self.iters_so_far)        
    writer.add_scalar("breakdown/vel", np.mean(self.reward_breakdown['vel']), self.iters_so_far)   
    writer.add_scalar("breakdown/tip", np.mean(self.reward_breakdown['tip']), self.iters_so_far)   
    writer.add_scalar("breakdown/com", np.mean(self.reward_breakdown['com']), self.iters_so_far)   
    writer.add_scalar("breakdown/neg", np.mean(self.reward_breakdown['neg']), self.iters_so_far)   
    writer.add_scalar("breakdown/sym", np.mean(self.reward_breakdown['sym']), self.iters_so_far)   
    writer.add_scalar("difficulty", self.difficulty, self.iters_so_far)   
    writer.add_scalar("height", self.height_coeff, self.iters_so_far)   
           
    # writer.add_scalar("breakdown/pos", self.reward_breakdown['pos'], self.iters_so_far)        
    # writer.add_scalar("breakdown/vel", self.reward_breakdown['vel'], self.iters_so_far)   
    # writer.add_scalar("breakdown/tip", self.reward_breakdown['tip'], self.iters_so_far)   
    # writer.add_scalar("breakdown/com", self.reward_breakdown['com'], self.iters_so_far)   
    # writer.add_scalar("breakdown/neg", self.reward_breakdown['neg'], self.iters_so_far)   
    # writer.add_scalar("breakdown/sym", self.reward_breakdown['sym'], self.iters_so_far)   
    writer.add_scalar("Kp", self.Kp, self.iters_so_far)
    logger.record_tabular("Kp", self.Kp)
    logger.record_tabular("max yaw", self.max_yaw)
    if self.obstacle_type == 'path':
      logger.record_tabular("difficulty", self.difficulty)
    elif self.obstacle_type == 'stairs':
      logger.record_tabular("height_coeff", self.height_coeff)
    
