import sys
# sys.path.append('../../brendan')
# sys.path.append('assets')
# print(sys.path)
import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import pybullet as p
# import gym
# from gym import spaces
import time
import numpy as np
from numpy import cos as c
from numpy import sin as s
# from gym.utils import seeding
from collections import deque
import cv2
import time
# from subpolicies.obstacles import Obstacles
from assets.obstacles import Obstacles
# from obstacles import Obstacles
import copy
import random
from mpi4py import MPI

# class Env(gym.Env):
class Env():
    # 44 state, 1 vx control
    ob_size = 47
    ac_size = 12
    im_size = [48, 48, 4]
    # im_size = [64, 64, 4]
    # im_size = [128, 128, 4]
    timeStep = 0.005
    initial_reward_flag = True
    initial_reward = 0
    total_reward = 0
    Kp = Kd = 400
    Kd_x = Kd
    steps = 0
    total_steps = 0
    actionRepeat = 2
    box_info = None
    height_coeff = 0.0
    # height_coeff = 0.1
    step_length = 0.3
    speed = 1.25
    yaw_v = 0
    y_v = 0
    yawing = False
    speed_range = 0
    mode = 'flat'
    frict_coeff = 0.0
    length_coeff = 0.3
    episodes = -1
    # max_v = 2.0
    max_v = 1.5
    min_v = 0.5
    # min_v = 0.0
    # Yaw max velocity
    # max_yaw = 2.0
    max_yaw = 1.25
    # Strafe max velocity
    # max_vy = 0.75
    max_vy = 0.4
    # Yaw limit 1 rad yaw limit (57 deg)
    # yaw_limit = 0.5
    # Set as 20 deg
    yaw_limit = 0.35
    ep_lens = deque(maxlen=5)
    def __init__(self):
        pass
    def arguments(self, RENDER=False, PATH=None, MASTER=True, cur=False, down_stairs=False, up_stairs=False, disturbances=False, record_step=True, camera_rotation=80, variable_start=False, add_flat=False, colour=False, y_offset=0.0, z_offset=0.0, speed_cur=False, other_rewards = False, control=True, up_down_flat=False, mh=False, motor_control='torque', sym=False, args=None, e2e=False, rand_initial=True, doa=False):
        self.start_z = 1.02
        # disturbances = False
        self.motor_control = motor_control
        self.control = control
        self.rand_initial=rand_initial
        # self.strafe = False
        self.strafe = True
        if self.control:
            if self.strafe:
                self.ob_size += 3
            else:
                self.ob_size += 2
        # else:
        #     self.ob_size = 44
        # high = 5*np.ones(self.ac_size)
        # low = -high
        # self.action_space  = spaces.Box(low, high, dtype=np.float32)

        # high = np.inf*np.ones(self.ob_size)
        # low = -high
        # self.observation_space = spaces.Box(low,high, dtype=np.float32)
        # self.arguments(False, False, '4')
        self.args = args
        self.e2e = e2e
        self.motor_scale = args.motor_scale
        self.colour = colour
        self.render = RENDER
        self.record_step = record_step
        self.camera_rotation = camera_rotation
        self.PATH = PATH
        self.cur = cur
        self.sym = sym
        self.down_stairs = down_stairs
        self.up_stairs = up_stairs
        self.speed_cur = speed_cur
        self.doa = doa
        self.other_rewards = other_rewards
        self.disturbances = disturbances
        if self.args.balance:   
            # self.max_disturbance = 3000
            self.max_disturbance = 1500
            # self.max_disturbance = 1000
        elif (not self.up_stairs and not self.down_stairs and not self.e2e):
            self.max_disturbance = 1500
        else:
            self.max_disturbance = 1000
            # self.max_disturbance = 250
        self.variable_start = variable_start
        self.up_down_flat = up_down_flat
        self.add_flat = add_flat
        self.y_offset = y_offset
        self.z_offset = z_offset
        self.mh = mh
        self.obstacles = Obstacles(y_offset=self.y_offset, add_friction=None, colour=self.colour)
        self.MASTER = MASTER
        if self.render and self.MASTER:
        	self.physicsClientId = p.connect(p.GUI)
        else:
            self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the running gait
        self.sim_data = []
        self.best_reward = -100

        # Continuous values for normalising ====================================
        # Right and Left Joint positions, velocities, feet contacts, feet positions
        if self.args.urdf:
            self.state = ['right_hip_z_pos','right_hip_x_pos','right_hip_y_pos','right_knee_pos','right_ankle_x_pos', 'right_ankle_y_pos']
            self.state += ['left_hip_z_pos', 'left_hip_x_pos', 'left_hip_y_pos', 'left_knee_pos','left_ankle_x_pos', 'left_ankle_y_pos']
            self.state += ['right_hip_z_vel','right_hip_x_vel','right_hip_y_vel','right_knee_vel','right_ankle_x_vel', 'right_ankle_y_vel']
            self.state += ['left_hip_z_vel', 'left_hip_x_vel', 'left_hip_y_vel', 'left_knee_vel','left_ankle_x_vel', 'left_ankle_y_vel']
        else:
            self.state = ['right_hip_x_pos','right_hip_z_pos','right_hip_y_pos','right_knee_pos','right_ankle_x_pos', 'right_ankle_y_pos']
            self.state += ['left_hip_x_pos', 'left_hip_z_pos', 'left_hip_y_pos', 'left_knee_pos','left_ankle_x_pos', 'left_ankle_y_pos']
            self.state += ['right_hip_x_vel','right_hip_z_vel','right_hip_y_vel','right_knee_vel','right_ankle_x_vel', 'right_ankle_y_vel']
            self.state += ['left_hip_x_vel', 'left_hip_z_vel', 'left_hip_y_vel', 'left_knee_vel','left_ankle_x_vel', 'left_ankle_y_vel']

        self.state += ['com_z']
        self.state += ['vx','vz','pitch','pitch_vel']

        # Needs negatives when inverting:
        self.state += ['vy','roll','yaw']
        # self.state += ['roll_vel','yaw_vel', 'yaw_buf_vel']
        self.state += ['roll_vel','yaw_vel']

        # Not continous values, don't normalise ================================

        self.state += ['right_heel1','right_heel2','right_toe']
        self.state += ['left_heel1','left_heel2','left_toe']
        self.state += ['prev_right_foot_left_ground','right_foot_left_ground']
        self.state += ['prev_left_foot_left_ground','left_foot_left_ground']
        self.state += ['left_foot_swing', 'right_foot_swing']
        self.state += ['impact']      

        # print(len(self.state), "state length")
        # Previous trajectory targets
        # self.state += ['ready_to_walk']
        # Y states, and roll and yaw that need to be negatived..
        # States that need to have logic switched: State flags, left = 0, right = 1
        #=======================================================================
        # State flags, left = 0, right = 1
        # ======================================================================
        # self.sample_nb = 6
        # self.sample_max = self.sample_nb

        # Swap left and right leg order, i.e. left position is mapped onto right position etc, including contacts.
        self.inverted_state = ['left_hip_x_pos', 'left_hip_z_pos', 'left_hip_y_pos', 'left_knee_pos','left_ankle_x_pos', 'left_ankle_y_pos']
        self.inverted_state += ['right_hip_x_pos','right_hip_z_pos','right_hip_y_pos','right_knee_pos','right_ankle_x_pos', 'right_ankle_y_pos']
        self.inverted_state += ['left_hip_x_vel', 'left_hip_z_vel', 'left_hip_y_vel', 'left_knee_vel','left_ankle_x_vel', 'left_ankle_y_vel']
        self.inverted_state += ['right_hip_x_vel','right_hip_z_vel','right_hip_y_vel','right_knee_vel','right_ankle_x_vel', 'right_ankle_y_vel']
        self.inverted_state += ['com_z']
        self.inverted_state += ['vx','vz','pitch','pitch_vel']

        # Needs negatives when inverting:
        self.inverted_state += ['vy','roll','yaw']
        self.inverted_state += ['roll_vel','yaw_vel']

        self.inverted_state += ['left_heel1','left_heel2','left_toe']
        self.inverted_state += ['right_heel1','right_heel2','right_toe']
        self.inverted_state += ['prev_left_foot_left_ground','left_foot_left_ground']
        self.inverted_state += ['prev_right_foot_left_ground','right_foot_left_ground']

        self.saved_state = []
        self.reward_breakdown = {'hip_z':[], 'angles':[], 'actions':[], 'sym':[], 'positive':[], 'foot':[]}
        self.cum_reward_breakdown = {'hip_z':[], 'angles':[], 'actions':[], 'sym':[], 'positive':[], 'foot':[]}
        self.positive_record = []
        
    def reset(self, door_data=None):
        # for key in self.reward_breakdown:
        #     # if key == 'positive':
        #     print(key, sum(self.reward_breakdown[key]))
        #     self.cum_reward_breakdown[key].append(sum(self.reward_breakdown[key])) 
        #     print("cum " + key + ": ", np.mean(self.cum_reward_breakdown[key]))
        # print()
        self.reward_breakdown = {'hip_z':[], 'angles':[], 'actions':[], 'sym':[], 'positive':[], 'foot':[]}

        if self.args.balance:
            self.max_v = 0.5
            self.min_v = 0  

        # if self.up_stairs or self.down_stairs:
            # self.min_v = 0.5
        self.episodes += 1
        self.ep_lens.append(self.steps)
        self.cur_foot = 'right'
        self.left_on_box = 0.0
        self.right_on_box = 0.0
        self.reset_sim = False
        # if (self.up_stairs or self.down_stairs or self.mh or self.e2e):
        if self.height_coeff < 0.07 and not self.cur and len(self.ep_lens) == 5 and (np.mean(self.ep_lens)*self.timeStep*2) > 4 and self.steps != 0 and (np.mean(self.episode_speeds) >= 0.9 or self.args.balance):
            self.ep_lens = deque(maxlen=5)
            self.height_coeff += 0.01
            self.reset_sim = True
        if self.cur:
            if len(self.ep_lens) == 5 and (np.mean(self.ep_lens)*self.timeStep*2) > 3 and self.steps != 0 and (np.mean(self.episode_speeds) >= 0.9 or self.args.balance):
                self.ep_lens = deque(maxlen=5)
                self.Kp = self.Kd = self.Kd_x = self.Kp*0.75
                if self.Kp < 10:
                    self.Kd_x = self.Kp = self.Kd = 0
                    self.cur = False
        self.episode_speeds = []
        if self.speed_cur:
            # # self.speed = np.random.uniform(low=self.min_v,high=self.max_v)
            # self.speed = np.random.uniform(low=0,high=0.5) if np.random.random() < 0.1 else np.random.uniform(low=0.5,high=self.max_v)           
            # self.episode_speeds.append(self.speed)            
            # self.yaw_v = np.random.uniform(low=-self.max_yaw,high=self.max_yaw)           
            # if self.strafe:
            #     self.y_v = np.random.uniform(low=-self.max_vy,high=self.max_vy)           
            self.speed, self.yaw_v, self.y_v = 0,0,0
        if abs(self.speed) >= 0.25:
            self.time_of_step = int(60/abs(self.speed))
        else:
            self.time_of_step = 0
        self.vel_buffer = deque(maxlen=200)
        self.action_store = deque(maxlen=self.time_of_step)

        if self.record_step:
            if self.best_reward < self.total_reward and self.total_reward != 0:
                self.best_reward = self.total_reward
                self.save_sim_data(best=True)
                self.save_sim_data()
            else:
                self.save_sim_data(best=False)
            self.sim_data = []
        # ======================================================================
        # if self.episodes == 0 or self.reset_sim:
        #     p.resetSimulation()
        #     self.load_model()
        # else:
        #     self.restore()
        
        # Original -----------------------------------------------------------
        if self.variable_start and (self.episodes % 2) == 0 and self.episodes != 0:
            self.restore()
        else:
            p.resetSimulation()
            self.load_model()
        # =====================================================
        if self.colour:
            self.obstacles.set_colour(self.box_info[0][self.box_num], self.speed)
            self.obstacles.set_colour(self.box_info[0][self.box_num + 1], self.speed)
        self.steps = 0
        self.total_reward = 0
        self.prev_actions = np.zeros(12)
        self.get_observation()

        # # yaw and no x and no y, no y no yaw, no x no yaw 
        if abs(self.yaw_v) < 0.25 and abs(self.ob_dict['yaw']) < 0.25:
            self.yaw_v = 0
            self.no_yaw = True
        else:
            self.no_yaw = False

        if self.control:
            if self.strafe:
                return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed] + [self.yaw_v] + [self.y_v]))
            else:
                return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed] + [self.yaw_v]))
    
    def load_model(self):

        if self.MASTER: p.loadMJCF(currentdir + "/ground.xml")
        # objs = p.loadMJCF(currentdir + "/mybot.xml",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        if self.args.urdf:
            # objs = p.loadURDF(currentdir + "/robot.urdf",basePosition=[0,0,self.start_z],baseOrientation=[0,0,0,1],flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
            objs = p.loadURDF(currentdir + "/robot.urdf",basePosition=[0,0,self.start_z],baseOrientation=[0,0,0,1],flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
            # objs = p.loadURDF(currentdir + "/robot.urdf",basePosition=[0,0,self.start_z],baseOrientation=[0,0,0,1])
            self.Id = objs
        else:
            # objs = p.loadMJCF(currentdir + "/mybot.xml",flags = p.URDF_USE_SELF_COLLISION + p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
            objs = p.loadMJCF(currentdir + "/mybot.xml",flags = p.URDF_USE_SELF_COLLISION)
            self.Id = objs[0]


        p.setTimeStep(self.timeStep)
        p.setGravity(0,0,-9.8)
        # ======================================================================

        numJoints = p.getNumJoints(self.Id)
        # Camera following robot:
        # body_xyz, (qx, qy, qz, qw) = p.getBasePositionAndOrientation(self.Id)
        # p.resetDebugVisualizerCamera(2.0, self.camera_rotation, -10.0, body_xyz)
        # p.resetDebugVisualizerCamera(2.0, 50, -10.0, body_xyz)

        self.jdict = {}
        self.feet_dict = {}
        self.shin_dict = {}
        self.thigh_dict = {}
        self.leg_dict = {}
        self.body_dict = {}
        self.feet = ["left_heel1", "left_heel2", "right_heel1", "right_heel2", "left_toe", "right_toe"]
        self.shins = ["left_shin", "right_shin"]
        self.thighs = ["left_thigh_link", "right_thigh_link"]
        self.feet_contact = {f:True for f in self.feet}
        self.shin_contact = {f:True for f in self.shins}
        self.ordered_joints = []
        self.ordered_joint_indices = []

        for j in range( p.getNumJoints(self.Id) ):
            info = p.getJointInfo(self.Id, j)
            link_name = info[12].decode("ascii")
            if link_name=="left_heel1": self.feet_dict["left_heel1"] = j
            if link_name=="left_heel2": self.feet_dict["left_heel2"] = j
            if link_name=="right_heel1": self.feet_dict["right_heel1"] = j
            if link_name=="right_heel2": self.feet_dict["right_heel2"] = j
            if link_name=="left_toe": self.feet_dict["left_toe"] = j
            if link_name=="right_toe": self.feet_dict["right_toe"] = j
            # print(link_name)
            if link_name=="base_link": self.body_dict["body_link"] = j
            if link_name in self.thighs:
                self.thigh_dict[link_name] = j
            if link_name in self.shins:
                self.shin_dict[link_name] = j
            self.ordered_joint_indices.append(j)
            if info[2] != p.JOINT_REVOLUTE: continue
            jname = info[1].decode("ascii")
            lower, upper = (info[8], info[9])
            p.enableJointForceTorqueSensor(self.Id, j, True)
            self.ordered_joints.append( (j, lower, upper) )
            self.jdict[jname] = j
        if self.args.urdf:
            self.motor_names = ["right_hip_z", "right_hip_x", "right_hip_y", "right_knee"]
            self.motor_names += ["right_ankle_x", "right_ankle_y"]
            self.motor_names += ["left_hip_z", "left_hip_x", "left_hip_y", "left_knee"]
            self.motor_names += ["left_ankle_x", "left_ankle_y"]
            self.motor_power = [300, 300, 900, 600]
            self.motor_power += [150, 300]
            self.motor_power += [300, 300, 900, 600]
            self.motor_power += [150, 300]
            # self.motor_power = [200, 200, 600, 400]
            # self.motor_power += [100, 200]
            # self.motor_power += [200, 200, 600, 400]
            # self.motor_power += [100, 200]
            # self.motor_power = [100, 100, 300, 200]
            # self.motor_power += [50, 100]
            # self.motor_power += [100, 100, 300, 200]
            # self.motor_power += [50, 100]
            self.motor_power = [self.motor_scale*m for m in self.motor_power]
            # print(self.motor_power)
        else:
            # self.motor_names = ["right_hip_x", "right_hip_z", "right_hip_y", "right_knee"]
            # self.motor_names += ["left_hip_x", "left_hip_z", "left_hip_y", "left_knee"]
            # self.motor_names += ["right_ankle_x", "right_ankle_y"]
            # self.motor_names += ["left_ankle_x", "left_ankle_y"]
            # self.motor_power = [100, 100, 300, 200]
            # self.motor_power += [100, 100, 300, 200]
            # self.motor_power += [50, 100, 50, 100]
            self.motor_names = ["right_hip_x", "right_hip_z", "right_hip_y", "right_knee"]
            self.motor_names += ["right_ankle_x", "right_ankle_y"]
            self.motor_names += ["left_hip_x", "left_hip_z", "left_hip_y", "left_knee"]
            self.motor_names += ["left_ankle_x", "left_ankle_y"]
            self.motor_power = [100, 100, 300, 200]
            self.motor_power += [50, 100]
            self.motor_power += [100, 100, 300, 200]
            self.motor_power += [50, 100]
        self.motors = [self.jdict[n] for n in self.motor_names]
        # print(self.motors)

        forces = np.ones(len(self.motors))*240
        self.actions = {key:0.0 for key in self.motor_names}

        # Disable motors to use torque control:
        if self.motor_control == 'torque':
            p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

        # Increase the friction on the feet, we have heaps of friction at our feet.
        for key in self.feet_dict:
        	p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9)
        if self.up_down_flat:
            self.box_info = self.obstacles.up_down_flat(height_coeff = self.height_coeff, order=self.up_down_flat_arg, add_walls=self.args.add_walls)
        else:
            if self.up_stairs:
                # self.mode = 'up_flat'
                # order =  ['up']*5 + [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(50)] if random.random() < 0.1 else ['up']*55
                order = ['up']*55
            elif self.down_stairs:
                order = ['down']*55
                # order =  ['down']*5 + [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(50)] if random.random() < 0.1 else ['down']*55
            elif self.e2e:
                order = [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(55)]
            elif self.args.balance:
                order = random.sample([['flat']*20, ['up']*20, ['down']*20], 1)[0]
                # order =  order
                # order = ['flat'] + ['up']*10
                self.order = order[0]
                # self.height_coeff = 0.07
            else:
                # order =  ['flat']*5 + [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(50)] if random.random() < 0.1 else ['flat']*55
                order = ['flat']*55
            # self.height_coeff = 0.07
            # self.length_coeff = 0.16 + random.random()/10
            # self.length_coeff = 0.18 + random.random()/50  
            # self.length_coeff = np.random.uniform(0.18, 0.2)         
            # self.length_coeff = np.random.uniform(0.2, 0.25)     
            self.length_coeff = np.random.uniform(0.24, 0.26)     
            # self.length_coeff = 0.26
            # self.height_coeff = 0.07    
            if self.height_coeff > 0:
                height_coeff = self.height_coeff + (random.random()/200 - 0.0025)
            else:
                height_coeff = 0
            if self.args.display:
                self.box_info = [[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]], [[0,0,0],[0,0,0]], [[0,0,0],[0,0,0]]] 
            else:        
                # self.box_info = self.obstacles.add_stairs(length_coeff = self.length_coeff, height_coeff = height_coeff, disturb=self.disturbances, 
                # speed=self.speed, mode=self.mode, num_steps=self.num_steps)
                # print(order)
                self.box_info = self.obstacles.up_down_flat(height_coeff=self.height_coeff,length_coeff=self.length_coeff, order=order)

        self.z_offset = 0
        self.box_num = 0

        self.body_xyz, _ = p.getBasePositionAndOrientation(self.Id)
        if len(self.box_info[0]) > (self.box_num+1) and self.body_xyz[0] > (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]):
            self.box_num += 1
        if self.box_num > 0 and (self.box_info[1][self.box_num][2] - self.box_info[1][self.box_num-1][2]) > 0 and (self.body_xyz[0] < (self.box_info[1][self.box_num][0])):
            self.z_offset = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
        else:
            self.z_offset = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        self.restore()

    def restore(self, sample=None):
        # Restore state every second time (need to always pick a state to restore starting from 0).
        if self.variable_start and (self.episodes % 2) == 0 and self.episodes != 0 and self.saved_state:
            pos, orn, joints, base_vel, joint_vel, saved_dict, others = self.saved_state
            self.ob_dict = saved_dict
            self.box_num, self.cur_foot, self.steps = others
            # [pos, orn, rot, vel, joint, joint_vel]
            c = [0.015, 0.006, 0.012, 0.4, 0.05, 0.05]
            self.saved_state = []
        else:
            self.ob_dict = {'prev_right_foot_left_ground':False,'prev_left_foot_left_ground':False,'left_foot_left_ground':False,'right_foot_left_ground':False}
            self.ob_dict.update({'roll':0.0,'pitch':0.0,'yaw':0.0})
            self.ob_dict['ready_to_walk'] = False
            if not self.args.balance:
                self.ob_dict['left_foot_swing'] = random.sample([0,1],1)[0]
                # self.ob_dict['left_foot_swing'] = 1
                self.ob_dict['right_foot_swing'] = not self.ob_dict['left_foot_swing']
            else:
                self.ob_dict['left_foot_swing'] = 0
                self.ob_dict['right_foot_swing'] = 0
            if self.ob_dict['right_foot_swing']:
                self.cur_foot = 'right'
            else:
                self.cur_foot = 'left'
            box_z = self.box_info[1][0][2] + self.box_info[2][0][2]
            pos, orn, joints, base_vel, joint_vel = [0,0,box_z+self.start_z],[0,0,0,1], [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            # Which time step to record for next restart
            self.sample = random.sample([_ for _ in range(150)], 1)[0]
            self.save_state = []
            # [pos, y_pos, orn, rot, vel, joint, joint_vel]
            if self.rand_initial:
                if self.doa:
                    # c = [0.4, 0.2, 0.1, 0.5, 1.0, 1.0, 1.0]                  
                    # c = [0.4, 0.1, 0.1, 0.25, 0.25, 0.4, 0.5]
                    c = [0.4, 0.1, 0.05, 0.1, 0.1, 0.3, 0.5]
                elif self.args.balance:
                    # c = [0.4, 0.2, 0.01, 0.05, 0.5, 0.75, 0.1]
                    c = [0.4, 0.2, 0.01, 0.05, 0.5, 0.2, 0.01]
                else:
                    c = [0.4, 0.2, 0.01, 0.012, 0.4, 0.01, 0.01]
            else:
                c = [0.01, 0.01, 0.01, 0.02, 0.01, 0.01, 0.01]

        if self.disturbances:
            pos[0] += np.random.uniform(low=0.2, high=c[0])
            pos[1] += np.random.uniform(low=-c[0], high=c[0])
            # No noise to z, can get stuck in the floor
            # pos[2] += np.random.uniform(low=-c[0], high=c[0])
            orn[0] += np.random.uniform(low=-c[2], high=c[2])
            orn[1] += np.random.uniform(low=-c[2], high=c[2])
            orn[2] += np.random.uniform(low=-c[2], high=c[2])
            # Noise on w term does funky things
            # orn[3] += np.random.uniform(low=-c[2], high=c[2])
            for i in range(len(base_vel[0])):
                base_vel[0][i] += np.random.uniform(low=-c[3], high=c[3])
                base_vel[1][i] += np.random.uniform(low=-c[4], high=c[4])

            for i, (j, m) in enumerate(zip(self.ordered_joints, self.motor_names)):
                if 'ankle_y' not in m: #Don't want to at noise to ankle y, foot can end up stuck in the floor
                    if self.args.balance:
                        if m in ['right_hip_y', 'right_knee', 'left_hip_y', 'left_knee']:
                            joints[i] += np.clip(np.random.uniform(low=-c[5], high=c[5]), j[1], j[2])     
                        else:               
                            joints[i] += np.clip(np.random.uniform(low=-c[6], high=c[6]), j[1], j[2])                    
                    else:
                        joints[i] += np.clip(np.random.uniform(low=-c[5], high=c[5]), j[1], j[2])
                    joint_vel[i] += np.random.uniform(low=-c[6], high=c[6])

        # pos, orn, joints, base_vel, joint_vel = [c[0],c[0],box_z+self.start_z],[c[2],c[2],c[2],1], [c[5]]*len(self.motor_names), [[c[3],c[3],c[3]],[c[4],c[4],c[4]]], [c[6]]*len(self.motor_names)
        # print(pos, orn, joints, base_vel, joint_vel)
        self.set_position(pos, orn, joints, base_vel, joint_vel)

    def get_sample(self):
        pos = list(self.body_xyz)
        orn = [self.qx, self.qy, self.qz, self.qw]
        joints = [self.ob_dict[m + '_pos'] for m in self.motor_names]
        joint_vel = [self.ob_dict[m + '_vel'] for m in self.motor_names]
        base_vel = [list(self.body_vxyz), list(self.base_rot_vel)]
        saved_dict = self.ob_dict
        others = [self.box_num, self.cur_foot, self.steps]
        self.saved_state = copy.deepcopy([pos, orn, joints, base_vel, joint_vel, saved_dict, others])
        return self.saved_state

    def seed(self, seed=None):
    	self.np_random, seed = seeding.np_random(seed)
    	return [seed]

    def close(self):
    	print("closing")

    def balance_step(self, actions):
        for _ in range(self.actionRepeat):
            forces = [0.] * len(self.motor_names)
            for m in range(len(self.motor_names)):
                forces[m] = self.motor_power[m]*actions[m]*0.082
            p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=forces)
            p.stepSimulation()
        self.get_observation()
        if len(self.box_info[0]) > (self.box_num+1) and self.body_xyz[0] > (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]):
            self.box_num += 1
        if self.box_num > 0 and (self.box_info[1][self.box_num][2] - self.box_info[1][self.box_num-1][2]) > 0 and (self.body_xyz[0] < (self.box_info[1][self.box_num][0])):
            self.z_offset = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
        else:
            self.z_offset = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        reward, done = self.reward(actions)
        return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed] + [self.yaw_v] + [self.y_v])), reward, done, self.ob_dict

    def step(self, actions):
        # Trial with balancing before walking
        if (self.speed_cur and ((self.steps*self.timeStep) % 3) == 0 and self.steps != 0) or self.steps == 50:
            self.speed = np.random.uniform(low=self.min_v,high=self.max_v)           
            # self.speed = 0 if np.random.random() < 0.05 else np.random.uniform(low=0.5,high=self.max_v)         
            # self.speed = np.random.uniform(low=0,high=0.5) if np.random.random() < 0.1 else np.random.uniform(low=0.5,high=self.max_v)           
            self.episode_speeds.append(self.speed)
            self.yaw_v = np.random.uniform(low=-self.max_yaw,high=self.max_yaw)           
            if self.strafe:
                self.y_v = np.random.uniform(low=-self.max_vy,high=self.max_vy)           
            
            if self.colour:
                for box in range(self.box_num, len(self.box_info[0])):
                    self.obstacles.set_colour(self.box_info[0][box], self.speed)
            if abs(self.speed) >= 0.25:
                self.time_of_step = int(60/abs(self.speed))
            else:
                self.time_of_step = 0

            # # yaw and no x and no y, no y no yaw, no x no yaw 
            if abs(self.yaw_v) < 0.25 and abs(self.ob_dict['yaw']) < 0.25:
                self.yaw_v = 0
                self.no_yaw = True
            else:
                self.no_yaw = False
            self.action_store = deque(maxlen=self.time_of_step)

        if self.disturbances and random.random() < 0.02:
            self.add_disturbance()
        t1 = time.time()
        actions = np.clip(actions, -10, 10)
        if self.cur:
            actions = self.apply_forces(actions)
        for _ in range(self.actionRepeat):
            if self.motor_control == 'torque':
                forces = [0.] * len(self.motor_names)
                for m in range(len(self.motor_names)):
                	forces[m] = self.motor_power[m]*actions[m]*0.082
                p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=forces)
            elif self.motor_control == 'position':
                p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=actions, forces=self.motor_power)
            if self.MASTER:
                p.stepSimulation()
        self.get_observation()
       
        if self.speed_cur:
            max_y_pos = 0.5    
            if (self.ob_dict['yaw'] > self.yaw_limit):
                self.yaw_v = -self.max_yaw/4
            elif (self.ob_dict['yaw'] < -self.yaw_limit):
                self.yaw_v = self.max_yaw/4
            if self.body_xyz[1] > max_y_pos: 
                self.yaw_v = -self.max_yaw/4
                if self.speed < 0.3:
                    self.y_v = -self.max_vy/2
            elif self.body_xyz[1] < -max_y_pos: 
                self.yaw_v = self.max_yaw/4
                if self.speed < 0.3:
                    self.y_v = self.max_vy/2 

        self.vel_buffer.append(self.ob_dict['vx'])
        if len(self.box_info[0]) > (self.box_num+1) and self.body_xyz[0] > (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]):
            self.box_num += 1
        if self.box_num > 0 and (self.box_info[1][self.box_num][2] - self.box_info[1][self.box_num-1][2]) > 0 and (self.body_xyz[0] < (self.box_info[1][self.box_num][0])):
            self.z_offset = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
        else:
            self.z_offset = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]

        reward, done = self.reward(actions)
        self.total_reward += reward
        if self.record_step: self.record_sim_data()
        if self.variable_start and self.steps == self.sample and not done:
            self.get_sample()
        self.steps += 1
        self.total_steps += 1
        if self.render: time.sleep(0.01)
        if self.control:
            if self.strafe:
                return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed] + [self.yaw_v] + [self.y_v])), reward, done, self.ob_dict
            else:
                return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state] + [self.speed] + [self.yaw_v])), reward, done, self.ob_dict
        else:
            return np.nan_to_num(np.array([self.ob_dict[s] for s in self.state])), reward, done, self.ob_dict

    def reward(self, actions):
        done = False
        x = abs(self.ob_dict['vx'] - self.speed)
        reward = 0.5*(4.5/(np.e**(3*x) + np.e**(-3*x))) - 0.02

        x = np.clip(abs(self.ob_dict['yaw_vel'] - self.yaw_v), 0, 25)    
        reward += 0.25*(4.5/(np.e**(5*x) + np.e**(-5*x))) - 0.02    

        if self.strafe:
            x = np.clip(abs(self.ob_dict['vy'] - self.y_v), 0, 25)   
            reward += 0.25*(4.5/(np.e**(7*x) + np.e**(-7*x))) - 0.02    
        self.reward_breakdown['positive'].append(reward)
        # yaw and no x and no y, no y no yaw, no x no yaw 
        if self.no_yaw:
            reward -= abs(self.ob_dict['yaw'])
            if self.steps < 300:
                reward -= abs(self.body_xyz[1])

        # reward = min(self.ob_dict['vx'], self.speed)
        # reward -= 0.005*np.square(actions).sum()
        reward -= 0.002*np.square(actions).sum()
        self.reward_breakdown['actions'].append(0.0025*np.square(actions).sum())
        if self.steps > 2000: done = True
        # print(self.body_xyz[2] < (0.70 + self.z_offset))
        if (self.body_xyz[2] < (0.70 + self.z_offset)):
            done = True
        
        # Keep com steady/no anglular displacement
        reward -= (0.75*abs(self.ob_dict['pitch']) + 0.75*abs(self.ob_dict['roll']))
        self.reward_breakdown['angles'].append((0.75*abs(self.ob_dict['pitch']) + 0.75*abs(self.ob_dict['roll'])))

        reward -= 0.2 if self.ob_dict['left_shin'] else 0
        reward -= 0.2 if self.ob_dict['right_shin'] else 0

        # No twisting of feet
        reward -= self.ob_dict['left_hip_z_pos']**2 + self.ob_dict['right_hip_z_pos']**2
        # reward -= abs(self.ob_dict['left_hip_z_pos'] + abs(self.ob_dict['right_hip_z_pos'])
        self.reward_breakdown['hip_z'].append(self.ob_dict['left_hip_z_pos']**2 + self.ob_dict['right_hip_z_pos']**2)

        # Ensure no jumping/running        
        if self.ob_dict['ready_to_walk'] and self.ob_dict['left_foot_left_ground'] and self.ob_dict['right_foot_left_ground'] :
            reward -= 1.0
            self.reward_breakdown['foot'].append(-0.2)
        
        # Get distance between feet
        left_foot_toe_link = p.getLinkState(self.Id, self.feet_dict["left_toe"], computeLinkVelocity=True)
        right_foot_toe_link = p.getLinkState(self.Id, self.feet_dict["right_toe"], computeLinkVelocity=True)
        left_foot_heel_link = p.getLinkState(self.Id, self.feet_dict["left_heel1"], computeLinkVelocity=True)
        right_foot_heel_link = p.getLinkState(self.Id, self.feet_dict["right_heel1"], computeLinkVelocity=True)
        left_foot_toe = left_foot_toe_link[0] 
        right_foot_toe = right_foot_toe_link[0] 
        left_foot_heel = left_foot_heel_link[0] 
        right_foot_heel = right_foot_heel_link[0]
        rot_mat = np.array(
        	[[np.cos(-self.ob_dict['yaw']), -np.sin(-self.ob_dict['yaw']), 0],
        	 [np.sin(-self.ob_dict['yaw']), np.cos(-self.ob_dict['yaw']), 0],
        	 [		0,			 0, 1]])
        left_toe_x, left_toe_y, _ = np.dot(rot_mat, (left_foot_toe[0],left_foot_toe[1],1))  # rotate speed back to body point of view
        right_toe_x, right_toe_y, _ = np.dot(rot_mat,  (right_foot_toe[0],right_foot_toe[1],1))  # rotate speed back to body point of vie
        left_heel_x, left_heel_y, _ = np.dot(rot_mat, (left_foot_heel[0],left_foot_heel[1],1))  # rotate speed back to body point of view
        right_heel_x, right_heel_y, _ = np.dot(rot_mat,  (right_foot_heel[0],right_foot_heel[1],1))  # rotate speed back to body point of vie    
      
        right_x = (right_foot_toe[0] + right_foot_heel[0])/2
        left_x = (left_foot_toe[0] + left_foot_heel[0])/2
        # # Don't step over a step
 
        if abs(left_x - right_x) > (self.length_coeff*3):
            reward -= 0.5

        # if self.speed > 0.5 and self.box_num > 0:
        #     if not self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground'] and self.box_num:
        #         reward -= 0.2 if abs(right_x-left_x)< 0.2 else 0
        #         # print(0.2 if abs(right_x-left_x)< 0.2 else 0)
        
        # if self.ob_dict['impact']:
        #     reward -= min(left_foot_heel_link[6][2], 0) + min(right_foot_heel_link[6][2], 0)

        # Make feet suitable width apart
        toe_distance = np.sqrt((left_toe_x - right_toe_x)**2 + (left_toe_y - right_toe_y)**2)
        heel_distance = np.sqrt((left_heel_x - right_heel_x)**2 + (left_heel_y - right_heel_y)**2)
        reward -= 0.1 if (left_heel_y - right_heel_y) < 0.05 else 0.0
        reward -= 0.1 if (left_toe_y - right_toe_y) < 0.05 else 0.0

        # If standing still, keep both feet on the ground, keep feet level, apart in y
        if self.speed < 0.1:
            reward -= 0.5*abs(self.ob_dict['left_hip_y_pos'])
            reward -= 0.5*abs(self.ob_dict['right_hip_y_pos'])
            reward -= 0.5*abs(self.ob_dict['left_hip_x_pos'])
            reward -= 0.5*abs(self.ob_dict['right_hip_x_pos'])
            if self.ob_dict['left_knee_pos'] < -0.2:
                reward -= 0.1*abs(self.ob_dict['left_knee_pos'])
            if self.ob_dict['right_knee_pos'] < -0.2:
                reward -= 0.1*abs(self.ob_dict['right_knee_pos'])
            if self.ob_dict['ready_to_walk']:
                reward -= 0.05*(1 - self.ob_dict['right_heel1'])
                reward -= 0.05*(1 - self.ob_dict['right_heel2'])
                reward -= 0.05*(1 - self.ob_dict['right_toe'])
                reward -= 0.05*(1 - self.ob_dict['left_heel1'])
                reward -= 0.05*(1 - self.ob_dict['left_heel2'])
                reward -= 0.05*(1 - self.ob_dict['left_toe'])
        elif self.speed < 0.5:
            # reward -= 0.5*abs(self.ob_dict['left_hip_y_pos'])
            # reward -= 0.5*abs(self.ob_dict['right_hip_y_pos'])
            reward -= 0.5*abs(self.ob_dict['left_hip_x_pos'])
            reward -= 0.5*abs(self.ob_dict['right_hip_x_pos'])
            if abs(self.ob_dict['left_hip_y_pos'] - self.ob_dict['right_hip_y_pos']) < 0.2:
                if self.ob_dict['right_foot_swing'] and self.ob_dict['left_knee_pos'] < -0.3:
                    reward -= 0.1*abs(self.ob_dict['left_knee_pos'])
                elif self.ob_dict['left_foot_swing'] and self.ob_dict['right_knee_pos'] < -0.3:
                    reward -= 0.1*abs(self.ob_dict['right_knee_pos'])
        else:
            if self.box_num > 0 and self.box_num < (len(self.box_info[0])-1):
                prev_box_x = self.box_info[1][self.box_num-1][0]
                cur_box_x = self.box_info[1][self.box_num][0] 
                next_box_x = self.box_info[1][self.box_num+1][0]
                if not self.ob_dict['right_foot_left_ground']:   
                    dist_to_box = min(abs(prev_box_x - right_x), abs(cur_box_x - right_x), abs(next_box_x - right_x))
                    reward -= 0.0 if dist_to_box < 0.05 else dist_to_box*1.25
                    # reward -= dist_to_box 
                if not self.ob_dict['left_foot_left_ground']:    
                    dist_to_box = min(abs(prev_box_x - left_x), abs(cur_box_x - left_x), abs(next_box_x - left_x))
                    reward -= 0.0 if dist_to_box < 0.05 else dist_to_box*1.25     
                    # reward -= dist_to_box

            if abs(self.ob_dict['left_hip_y_pos'] - self.ob_dict['right_hip_y_pos']) < 0.2:
                if self.ob_dict['right_foot_swing'] and self.ob_dict['left_knee_pos'] < -0.3:
                    reward -= 0.1*abs(self.ob_dict['left_knee_pos'])
                elif self.ob_dict['left_foot_swing'] and self.ob_dict['right_knee_pos'] < -0.3:
                    reward -= 0.1*abs(self.ob_dict['right_knee_pos'])
                # if not self.ob_dict['right_foot_left_ground']:
                #     reward -= 0.05*(1 - self.ob_dict['right_heel1'])
                #     reward -= 0.05*(1 - self.ob_dict['right_heel2'])
                #     reward -= 0.05*(1 - self.ob_dict['right_toe'])
                # if not self.ob_dict['left_foot_left_ground']:
                #     reward -= 0.05*(1 - self.ob_dict['left_heel1'])
                #     reward -= 0.05*(1 - self.ob_dict['left_heel2'])
                #     reward -= 0.05*(1 - self.ob_dict['left_toe'])

        # Penalize ankle roll
        reward -= 0.08*abs(self.ob_dict['left_ankle_x_pos'])
        reward -= 0.08*abs(self.ob_dict['right_ankle_x_pos'])
        # print(0.1*abs(self.ob_dict['left_ankle_x_pos']), 0.1*abs(self.ob_dict['right_ankle_x_pos']))

        pre_sym = reward
        if self.args.urdf:
            # sym_fact = 0.05   
            sym_fact = 0.075  
        else:
            sym_fact = 0.05
        # if self.time_of_step != 0 and len(self.action_store) == (self.time_of_step):
        #     prev_z = z = next_z = 0
        #     if self.e2e:
        #         if self.box_info is not None and (self.box_num+1) < len(self.box_info[0]) and self.box_num > 0:
        #             prev_z = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
        #             z = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        #             next_z = self.box_info[1][self.box_num+1][2] + self.box_info[2][self.box_num+1][2]
        #         if round(z - prev_z,2) == round(next_z - z,2):
        #             reward -= sym_fact*abs(self.ob_dict['right_hip_y_pos'] - self.action_store[0][0])
        #             reward -= sym_fact*abs(self.ob_dict['right_knee_pos'] - self.action_store[0][1])
        #             reward -= sym_fact*abs(self.ob_dict['left_hip_y_pos'] - self.action_store[0][2])
        #             reward -= sym_fact*abs(self.ob_dict['left_knee_pos'] - self.action_store[0][3])
        #     else:
        #         reward -= sym_fact*abs(self.ob_dict['right_hip_y_pos'] - self.action_store[0][0])
        #         reward -= sym_fact*abs(self.ob_dict['right_knee_pos'] - self.action_store[0][1])
        #         reward -= sym_fact*abs(self.ob_dict['left_hip_y_pos'] - self.action_store[0][2])
        #         reward -= sym_fact*abs(self.ob_dict['left_knee_pos'] - self.action_store[0][3])
        #                     # reward -= (sym_fact/2)*abs(self.ob_dict['right_hip_y_vel'] - self.action_store[0][4])
                            # reward -= (sym_fact/2)*abs(self.ob_dict['right_knee_vel'] - self.action_store[0][5])
                            # reward -= (sym_fact/2)*abs(self.ob_dict['left_hip_y_vel'] - self.action_store[0][6])
                            # reward -= (sym_fact/2)*abs(self.ob_dict['left_knee_vel'] - self.action_store[0][7])

        data = [self.ob_dict['left_hip_y_pos'],self.ob_dict['left_knee_pos'],self.ob_dict['right_hip_y_pos'],self.ob_dict['right_knee_pos']]
        # data = [self.ob_dict['left_hip_y_pos'],self.ob_dict['left_knee_pos'],self.ob_dict['right_hip_y_pos'],self.ob_dict['right_knee_pos'], self.ob_dict['left_hip_y_vel'],self.ob_dict['left_knee_vel'],self.ob_dict['right_hip_y_vel'],self.ob_dict['right_knee_vel']]
        self.action_store.append(data)
        self.reward_breakdown['sym'].append(pre_sym - reward)

        return reward, done

    def get_observation(self):

        jointStates = p.getJointStates(self.Id,self.motors)
        self.ob_dict.update({self.state[i]:j[0] for i,j in enumerate(jointStates)})
        self.ob_dict.update({self.state[i+self.ac_size]:j[1] for i,j in enumerate(jointStates)})

        # Get relative com
        self.body_xyz, (self.qx, self.qy, self.qz, self.qw) = p.getBasePositionAndOrientation(self.Id)
        self.ob_dict['body_xyz'] = self.body_xyz
        # Get angles and velocities
        roll, pitch, yaw = p.getEulerFromQuaternion([self.qx, self.qy, self.qz, self.qw])
        prev_r, prev_p, prev_y = self.ob_dict['roll'], self.ob_dict['pitch'], self.ob_dict['yaw']
        self.body_vxyz, self.base_rot_vel = p.getBaseVelocity(self.Id)
        self.ob_dict.update({'roll':roll,'pitch':pitch,'yaw':yaw})
        self.ob_dict.update({'roll_vel':self.base_rot_vel[0],'pitch_vel':self.base_rot_vel[1],'yaw_vel':self.base_rot_vel[2]})

        rot_speed = np.array(
        	[[np.cos(-yaw), -np.sin(-yaw), 0],
        	 [np.sin(-yaw), np.cos(-yaw), 0],
        	 [		0,			 0, 1]]
        )

        vx, vy, vz = np.dot(rot_speed, (self.body_vxyz[0],self.body_vxyz[1],self.body_vxyz[2]))  # rotate speed back to body point of view
                          
        self.ob_dict.update({'vx':vx,'vy':vy,'vz':vz})
            
        try:
            msg = 'rotated: ' + str(round(vx, 2)) + ' original: ' + str(round(self.body_vxyz[0], 2))
            self.screen.addstr(5, 0, msg)   
        except:
            pass
        # Get feet contacts
        self.ob_dict['right_heel1']= len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel1'], -1))>0
        self.ob_dict['right_heel2']= len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel2'], -1))>0
        self.ob_dict['right_toe']= len(p.getContactPoints(self.Id, -1, self.feet_dict['right_toe'], -1))>0
        self.ob_dict['left_heel1']= len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel1'], -1))>0
        self.ob_dict['left_heel2']= len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel2'], -1))>0
        self.ob_dict['left_toe']= len(p.getContactPoints(self.Id, -1, self.feet_dict['left_toe'], -1))>0

        # Update feet that have left the ground.
        self.ob_dict['prev_right_foot_left_ground'] = self.ob_dict['right_foot_left_ground']
        self.ob_dict['prev_left_foot_left_ground'] = self.ob_dict['left_foot_left_ground']
        self.ob_dict['right_foot_left_ground'] = not self.ob_dict['right_heel1'] and not self.ob_dict['right_heel2'] and not self.ob_dict['right_toe']
        self.ob_dict['left_foot_left_ground'] = not self.ob_dict['left_heel1'] and not self.ob_dict['left_heel2'] and not self.ob_dict['left_toe']

        if not self.ob_dict['ready_to_walk'] and (not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground']):
            self.ob_dict['ready_to_walk'] = True
            self.ob_dict['prev_left_foot_left_ground'] = False
            self.ob_dict['prev_right_foot_left_ground'] = False

        if self.ob_dict['right_foot_swing']:
            swing = 'right'
        else: 
            swing = 'left'
        # Check if we should change swing and stance feet.
        if self.ob_dict['ready_to_walk'] and self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']:
            self.ob_dict['right_foot_swing'] = not self.ob_dict['right_foot_swing']
            self.ob_dict['left_foot_swing'] = not self.ob_dict['left_foot_swing']
            if self.speed < 0.3:
                self.ob_dict['right_foot_swing'] = 0
                self.ob_dict['left_foot_swing'] = 0
            self.ob_dict['impact'] = True
        else: self.ob_dict['impact'] = False

        self.ob_dict['left_shin']= len(p.getContactPoints(self.Id, -1, self.shin_dict['left_shin'], -1))>0
        self.ob_dict['right_shin']= len(p.getContactPoints(self.Id, -1, self.shin_dict['right_shin'], -1))>0

        self.ob_dict.update({'com_z': self.body_xyz[2] - self.z_offset})

    def update_speed(self, speed, yaw_v=None, strafe_speed=None):
        self.speed = speed
        if yaw_v is not None:
            self.yaw_v = yaw_v  
            if strafe_speed is not None:
                self.y_v = strafe_speed
                return np.array([self.ob_dict[s] for s in self.state] + [self.speed] + [self.yaw_v] + [self.y_v])
            else:
                return np.array([self.ob_dict[s] for s in self.state] + [self.speed] + [self.yaw_v])
        else:
            return np.array([self.ob_dict[s] for s in self.state] + [self.speed])

    def obs_and_stuff(self, stuff):
        return np.concatenate([np.array([self.ob_dict[s] for s in self.state]), stuff])

    def update_speed_and_opt(self, speed, opt_buffer):
        self.speed = speed
        return np.array([self.ob_dict[s] for s in self.state] + [self.speed] + list(opt_buffer))

    def just_obs(self):
        return np.array([self.ob_dict[s] for s in self.state])

    def dict_from_obs(self, obs):
        # print(obs.shape)
        return {s:o for s, o in zip(self.state, obs)}

    def add_disturbance(self):
        force_x = (random.random() - 0.5)*self.max_disturbance
        force_y = (random.random() - 0.5)*self.max_disturbance
        force_z = (random.random() - 0.5)*(self.max_disturbance/4)
        # print("applying forces", force_x, force_y, force_z)
        p.applyExternalForce(self.Id,-1,[force_x,force_y,force_z],[0,0,0],p.LINK_FRAME)
        force_roll = (random.random() - 0.5)*self.max_disturbance/2
        force_pitch = (random.random() - 0.5)*self.max_disturbance/2
        force_yaw = (random.random() - 0.5)*(self.max_disturbance/2)
        p.applyExternalTorque(self.Id,-1,[force_roll,force_pitch,force_yaw],p.LINK_FRAME)

    def apply_forces(self, actions=None):

        roll, pitch, yaw = self.ob_dict['roll'], self.ob_dict['pitch'], self.ob_dict['yaw']
        vx, vy, vz = self.ob_dict['vx'], self.ob_dict['vy'], self.ob_dict['vz']

        if self.speed < 0.3:
            z_force = 1.5*(self.Kp * ((1+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
            x_force = 2*self.Kd_x * (self.speed - vx)
            y_force = 2*self.Kd_x * (self.y_v - vy)

        elif self.args.urdf:
            z_force = 1.5*(self.Kp * ((1+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
            # z_force = 3.5*(self.Kp * ((1+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
            x_force = 2*self.Kd_x * (self.speed - vx)
            if self.strafe:
                y_force = 2*self.Kd_x * (self.y_v - vy)
            else:
                y_force = - 2*(0.1 * self.Kp * self.body_xyz[1] + self.Kd * vy)        
            # z_force = 0
        else:
            z_force = 1.5*(self.Kp * ((1+self.z_offset)-self.body_xyz[2]) + self.Kd * vz * -1)
            x_force = self.Kd_x * (self.speed - vx)
            if self.strafe:
                y_force = self.Kd_x * (self.y_v - vy)
            else:
                y_force = - (0.1 * self.Kp * self.body_xyz[1] + self.Kd * vy)

        p.applyExternalForce(self.Id,-1,[x_force,y_force,z_force],[0,0,0],p.LINK_FRAME)

        if self.speed < 0.3:
            yaw_force = -0.2*self.Kd*(self.ob_dict['yaw_vel'] - self.yaw_v)
            roll_force = -2*self.Kp * roll    
            pitch_force = -2*self.Kp * pitch
        elif self.args.urdf:
            yaw_force = -0.2*self.Kd*(self.ob_dict['yaw_vel'] - self.yaw_v)
            roll_force = -2*self.Kp * roll    
            pitch_force = -2*self.Kp * pitch
        else:
            yaw_force = -0.1*self.Kd*(self.ob_dict['yaw_vel'] - self.yaw_v)
            roll_force = -self.Kp * roll
            pitch_force = -self.Kp * pitch

        p.applyExternalTorque(self.Id,-1,[roll_force,pitch_force,yaw_force],p.LINK_FRAME)

        if self.speed >= 0.3:
            swing_hip_torque = 0
            stance_hip_torque = 0
            swing_knee_torque = 0
            stance_knee_torque = 0
            swing = self.cur_foot
            if swing == 'left':
                stance = 'right'
                swing_hip_num = 8
                swing_knee_num = 9
                stance_hip_num = 2
                stance_knee_num = 3
            else:
                stance = 'left'
                swing_hip_num = 2
                swing_knee_num = 3
                stance_hip_num = 8
                stance_knee_num = 9
            # print(self.speed, self.time_of_step)
            # Trial..
            cur_time = (self.steps-50) % self.time_of_step
            if self.args.urdf:
                # dx = 0.3
                dx = 0.16
                # dx = 0.0
            else:
                dx = 0.12
            if self.ob_dict[swing + '_hip_y_pos'] > -0.2:
                swing_hip_torque = 10*(self.Kp/400)*(-dx)
            if self.ob_dict[stance + '_hip_y_pos'] < 0.1:
                stance_hip_torque = 2*(self.Kp/400)*(dx)
            if self.ob_dict[swing + '_knee_pos'] > -1.0 and cur_time < 10:
                swing_knee_torque = 4 *(self.Kp/400)*(-dx)
            if self.ob_dict[swing + '_knee_pos'] < -0.5 and cur_time > 10:
                swing_knee_torque = 4*(self.Kp/400)*(dx)
            if self.ob_dict[stance + '_knee_pos'] < -0.5:
               stance_knee_torque = 8  *(self.Kp/400)*(dx)
            actions[swing_knee_num] += swing_knee_torque
            actions[swing_hip_num] += swing_hip_torque
            actions[stance_knee_num] += stance_knee_torque
            actions[stance_hip_num] += stance_hip_torque
        
            if self.steps != 0 and (self.steps % self.time_of_step) == 0:
                if self.cur_foot == 'left': self.cur_foot = 'right'
                else: self.cur_foot = 'left'
       
        else:
            left_hip_torque = 0
            right_hip_torque = 0
            right_knee_torque = 0
            left_knee_torque = 0
            dx = 0.12
            if self.ob_dict['left_hip_y_pos'] < 0.25:
                left_hip_torque = 2*(self.Kp/400)*(dx)
            if self.ob_dict['left_knee_pos'] < -0.5:
               left_knee_torque = 8  *(self.Kp/400)*(dx)
            if self.ob_dict['right_hip_y_pos'] < 0.25:
                right_hip_torque = 2*(self.Kp/400)*(dx)
            if self.ob_dict['right_knee_pos'] < -0.5:
               right_knee_torque = 8  *(self.Kp/400)*(dx)
            # Hips
            actions[2] += right_hip_torque
            actions[8] += left_hip_torque
            # Knees
            actions[3] += right_knee_torque
            actions[9] += left_knee_torque

        return actions

    def save_sim_data(self, best=False, worst=False, trans=False):
        comm = MPI.COMM_WORLD
        if comm.Get_rank() == 0:
            if trans: path = self.PATH + 'trans_'
            elif best: path = self.PATH + 'best_'
            elif worst: path = self.PATH + 'worst_'
            else: path = self.PATH
            try:
                np.save(path + 'sim_data.npy', np.array(self.sim_data))
                if path != self.PATH:
                    np.save(self.PATH + 'sim_data.npy', np.array(self.sim_data))
                # if (self.up_stairs or self.down_stairs or self.up_down_flat or self.mh) and self.box_info is not None:
                if self.box_info is not None:
                    np.save(path + 'box_info_pos.npy', np.array(self.box_info[1]))
                    np.save(path + 'box_info_size.npy', np.array(self.box_info[2]))
                    np.save(path + 'box_info_frict.npy', np.array(self.box_info[3]))
                    np.save(path + 'box_info_colour.npy', np.array(self.box_info[4]))
                    if path != self.PATH:
                        np.save(self.PATH + 'box_info_pos.npy', np.array(self.box_info[1]))
                        np.save(self.PATH + 'box_info_size.npy', np.array(self.box_info[2]))
                        np.save(self.PATH + 'box_info_frict.npy', np.array(self.box_info[3]))
                        np.save(self.PATH + 'box_info_colour.npy', np.array(self.box_info[4]))
            except Exception as e:
                print("Save sim data error:")
                print(e)

    def record_sim_data(self):

    	if len(self.sim_data) > 100000: return
    	data = [self.body_xyz, [self.qx, self.qy, self.qz, self.qw]]
    	joints = p.getJointStates(self.Id, self.motors)
    	data.append([i[0] for i in joints])
    	self.sim_data.append(data)

    def set_position(self, pos, orn, joints=None, velocities=None, joint_vel=None):
        pos = [pos[0], pos[1]+self.y_offset, pos[2]]
        p.resetBasePositionAndOrientation(self.Id, pos, orn)
        if joints is not None:
            if joint_vel is not None:
                for j, jv, m in zip(joints, joint_vel, self.motors):
                    p.resetJointState(self.Id, m, targetValue=j, targetVelocity=jv)
            else:
                for j, m in zip(joints, self.motors):
                	p.resetJointState(self.Id, m, targetValue=j)
        if velocities is not None:
            p.resetBaseVelocity(self.Id, velocities[0], velocities[1])

    def transform_no_rot(self, pos, point):
        trans = np.array([[1,0,0,pos[0]],[0,1,0,pos[1]],[0,0,1,pos[2]],[0,0,0,1]])
        return np.dot(trans,[point[0],point[1],point[2], 1])
    
    def transform_rot(self, yaw, pos, point):
        # rot_mat = np.array(
        # 	[[np.cos(-yaw), -np.sin(-yaw), 0],
        # 	 [np.sin(-yaw), np.cos(-yaw), 0],
        # 	 [		0,			 0, 1]] )
        rot_mat = np.array(
        	[[np.cos(-yaw), np.sin(-yaw), 0],
        	 [-np.sin(-yaw), np.cos(-yaw), 0],
        	 [		0,			 0, 1]] )
        x,y,z = np.dot(rot_mat,[point[0],point[1],point[2]])
        return pos[0] + x, pos[1] + y, pos[2] + z

    def get_image(self, cam_pos='lower'):
        camTargetPos = [0.,0.,0.]
        cameraUp = [0,0,1]
        cameraPos = [1,1,1]
        yaw = 40
        pitch = 10.0

        roll=0
        upAxisIndex = 2
        camDistance = 4
        # new ===================
        pixelWidth = self.im_size[0]
        pixelHeight = self.im_size[1]
        
        # nearPlane = 0.01
        # farPlane = 1000
        
        # fov = 60
        pos, orn = p.getBasePositionAndOrientation(self.Id)
        roll, pitch, yaw = p.getEulerFromQuaternion(orn)
        if cam_pos == 'lower':
            dx = 0.6
            dy = 0
            dz = -1
            # cam_dx = 0.2
            cam_dx = 0.1
            cam_dy = 0
            cam_dz = 0.2
            fov = 60

        elif cam_pos == 'upper':
            dx = 3.0
            dy = 0
            dz = -1
            cam_dx = 0.0
            cam_dy = 0
            cam_dz = 0.4
            fov = 80


        # lightDirection = [0,1,1]
        # shadow = 0

        # lightDirection = [pos[0],pos[1],pos[2]]

        lightDirection = [pos[0]+cam_dx,pos[1],pos[2]]
        shadow = 1
        
        lightColor = [1,1,1]#optional argument
        
        # p_x, p_y, p_z, _ = self.transform_no_rot(pos, [dx,dy,dz])
        # cam_x, cam_y, cam_z, _ = self.transform_no_rot(pos, [cam_dx, cam_dy, cam_dz])
        
        p_x, p_y, p_z = self.transform_rot(yaw, pos, [dx,dy,dz])
        cam_x, cam_y, cam_z = self.transform_rot(yaw, pos, [cam_dx, cam_dy, cam_dz])
        nearPlane = 0.01
        farPlane = 1000
        aspect = 1
        projectionMatrix = p.computeProjectionMatrixFOV(fov, aspect, nearPlane, farPlane);

        viewMatrix = p.computeViewMatrix([cam_x,cam_y,cam_z], [p_x, p_y, p_z], cameraUp)
        try:
            image = p.getCameraImage(pixelWidth, pixelHeight, viewMatrix, projectionMatrix=projectionMatrix, lightDirection=lightDirection,lightColor=lightColor, shadow=shadow, renderer = p.ER_TINY_RENDERER)
            
            # image = p.getCameraImage(pixelWidth, pixelHeight, viewMatrix=viewMatrix, renderer = p.ER_TINY_RENDERER)
            # rgb = image[2]/255.0
            
            # rgb = image[2][:,:,:3]/255.0
            # depth = np.expand_dims(image[3], axis=2)
            
            rgb = np.array(copy.deepcopy(image[2])).reshape([pixelHeight, pixelWidth, 4])[:,:,:3]/255.0
            depth = np.array(copy.deepcopy(image[3])).reshape([pixelHeight, pixelWidth, 1])
            # print(rgb.shape, depth.shape)
            # if down_stairs, block off 75% of the image. It can see too far, so if any changes far in the image, the policy is affected
            if self.down_stairs:
                if random.random() < 0.2:
                    rgb[:int(48*0.3),:,:] = 0
                    depth[:int(48*0.3),:] = 0
            # rgb[:,:,:] = 0
            # depth[:,:,:] = 0
            # if cam_pos == 'upper':
            #     re_im = rgb*255.0
            #     cv2.imshow('robot_vision',re_im.astype(dtype=np.uint8)[...,::-1])
            #     cv2.waitKey(1)
            self.im = np.nan_to_num(np.concatenate((rgb,depth), axis=2))
            return self.im

        except Exception as e:
            print(e)
            return np.zeros([pixelWidth, pixelHeight, 4])

    def invert(self):
        inverted_ob = []
        for s in self.inverted_state:
            if s in ['vy','roll','yaw','roll_vel','yaw_vel']:
                inverted_ob.append(-self.ob_dict[s])
            else:
                inverted_ob.append(self.ob_dict[s])
        inverted_ob = np.array(inverted_ob + [self.speed])
        inverted_im = np.flip(self.im, 1)
        # im = (self.im[:,:,:3]*255.0).astype(dtype=np.uint8)
        # inv_im = (inverted_im[:,:,:3]*255.0).astype(dtype=np.uint8)
        # concat_im = np.hstack([im, inv_im])
        # cv2.imshow('robot_vision',concat_im)
        # cv2.waitKey(1)
        return inverted_ob, inverted_im


if __name__ == "__main__":
    from mpi4py import MPI
    import argparse
    import cv2
    parser = argparse.ArgumentParser()
    parser.add_argument('--render',default=False,action='store_true')
    parser.add_argument('--cur',default=False,action='store_true')
    parser.add_argument('--up_stairs',default=False,action='store_true')
    parser.add_argument('--down_stairs',default=False,action='store_true')
    args = parser.parse_args()
    env = Env()

    PATH = '/home/brendan/results/thing/'
    env.arguments(RENDER=args.render, PATH=PATH, cur=args.cur, record_step=False, up_stairs=args.up_stairs, down_stairs=args.down_stairs)
    env.height_coeff = 0.1

    ob = env.reset()
    i = 0
    while True:
        # print(boxes)
        actions = (np.random.rand(12)-0.5)*2

        ob, reward, done, ob_dict = env.step(actions)
        env.get_image()
        time.sleep(0.05)
        # cv2.imshow("frame", cv2.cvtColor(image[2], cv2.COLOR_BGR2RGB))
        # cv2.imshow("frame", image)
        # cv2.waitKey(1)
        if done:
            # print(done)
            env.reset()
            # env.obstacles.create_rand_floor()
        i += 1
