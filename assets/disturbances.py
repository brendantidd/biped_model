# From pybullet
import numpy as np
import pybullet as p
import copy

class BodyPart:

  def __init__(self, bullet_client, body_name, bodies, bodyIndex, bodyPartIndex):
    self.bodies = bodies
    self._p = bullet_client
    self.bodyIndex = bodyIndex
    self.bodyPartIndex = bodyPartIndex
    self.initialPosition = self.current_position()
    self.initialOrientation = self.current_orientation()
    self.bp_pose = Pose_Helper(self)

  def state_fields_of_pose_of(
      self, body_id,
      link_id=-1):  # a method you will most probably need a lot to get pose and orientation
    if link_id == -1:
      (x, y, z), (a, b, c, d) = self._p.getBasePositionAndOrientation(body_id)
    else:
      (x, y, z), (a, b, c, d), _, _, _, _ = self._p.getLinkState(body_id, link_id)
    return np.array([x, y, z, a, b, c, d])

  def get_position(self):
    return self.current_position()

  def get_pose(self):
    return self.state_fields_of_pose_of(self.bodies[self.bodyIndex], self.bodyPartIndex)

  def speed(self):
    if self.bodyPartIndex == -1:
      (vx, vy, vz), _ = self._p.getBaseVelocity(self.bodies[self.bodyIndex])
    else:
      (x, y, z), (a, b, c, d), _, _, _, _, (vx, vy, vz), (vr, vp, vy) = self._p.getLinkState(
          self.bodies[self.bodyIndex], self.bodyPartIndex, computeLinkVelocity=1)
    return np.array([vx, vy, vz])

  def current_position(self):
    return self.get_pose()[:3]

  def current_orientation(self):
    return self.get_pose()[3:]

  def get_orientation(self):
    return self.current_orientation()

  def reset_position(self, position):
    self._p.resetBasePositionAndOrientation(self.bodies[self.bodyIndex], position,
                                            self.get_orientation())

  def reset_orientation(self, orientation):
    self._p.resetBasePositionAndOrientation(self.bodies[self.bodyIndex], self.get_position(),
                                            orientation)

  def reset_velocity(self, linearVelocity=[0, 0, 0], angularVelocity=[0, 0, 0]):
    self._p.resetBaseVelocity(self.bodies[self.bodyIndex], linearVelocity, angularVelocity)

  def reset_pose(self, position, orientation):
    self._p.resetBasePositionAndOrientation(self.bodies[self.bodyIndex], position, orientation)

  def pose(self):
    return self.bp_pose

  def contact_list(self):
    return self._p.getContactPoints(self.bodies[self.bodyIndex], -1, self.bodyPartIndex, -1)


class Disturbances():
  def __init__ (self):
    self.box_id = []
    self.positions = []
    self.sizes = []
    self.colours = []
    
    self.circle_id = []
    self.circle_sizes = []
    self.circle_colours = []

  def remove_obstacles(self):
    if self.box_id:
      for b in self.box_id:
        p.removeBody(b)
      self.box_id = []
      self.positions = []
      self.sizes = []
      self.colours = []

    if self.circle_id:
      for b in self.circle_id:
        p.removeBody(b)
      self.circle_id = []
      self.circle_sizes = []
      self.circle_colours = []

    if (self.aggressive_cube):
      self._p.resetBasePositionAndOrientation(self.aggressive_cube.bodies[0], [-1.5, 0, 0.05],
                                              [0, 0, 0, 1])
    else:
      self.aggressive_cube = get_cube(self._p, -1.5, 0, 0.05)

  def get_cube(_p, x, y, z):
    body = p.loadURDF(os.path.join(pybullet_data.getDataPath(), "cube_small.urdf"), [x, y, z])
    _p.changeDynamics(body, -1, mass=1.2)  #match Roboschool
    part_name, _ = p.getBodyInfo(body)
    part_name = part_name.decode("utf8")
    bodies = [body]
    return BodyPart(_p, part_name, bodies, 0, -1)


    self.aggressive_cube = get_cube(self._p, -1.5, 0, 0.05)

    if self.frame % 30 == 0 and self.frame > 100 and self.on_ground_frame_counter == 0:
      target_xyz = np.array(self.body_xyz)
      robot_speed = np.array(self.robot_body.speed())
      angle = self.np_random.uniform(low=-3.14, high=3.14)
      from_dist = 4.0
      attack_speed = self.np_random.uniform(
          low=20.0, high=30.0)  # speed 20..30 (* mass in cube.urdf = impulse)
      time_to_travel = from_dist / attack_speed
      target_xyz += robot_speed * time_to_travel  # predict future position at the moment the cube hits the robot
      position = [
          target_xyz[0] + from_dist * np.cos(angle), target_xyz[1] + from_dist * np.sin(angle),
          target_xyz[2] + 1.0
      ]
      attack_speed_vector = target_xyz - np.array(position)
      attack_speed_vector *= attack_speed / np.linalg.norm(attack_speed_vector)
      attack_speed_vector += self.np_random.uniform(low=-1.0, high=+1.0, size=(3,))
      self.aggressive_cube.reset_position(position)
      self.aggressive_cube.reset_velocity(linearVelocity=attack_speed_vector)

  def add_steps(self, difficulty=10, height_coeff=0.07, terrain_type='mix', base=False, base_before=False, initial_pos=[0,0,0], baseline=False, two=False, rand_flat=False, foot=True):
    initial_size = [0.18, 0, 0.5]
    # initial_size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    # initial_size = [0.2, 0.7, 0.5]
    # print("fixed size", initial_size)
    size = copy.copy(initial_size)
    # pos = [-initial_size[0]-0.5+initial_pos[0],0+initial_pos[1],initial_size[2]+initial_pos[2]]
    pos = [-initial_size[0] + 0.35 +initial_pos[0],0+initial_pos[1],initial_size[2]+initial_pos[2]]
    prev_size = copy.copy(initial_size)
    prev_yaw = 0
    order = ['steps' for _ in range(15)]
    colours = []
    for pol in order:
      colours.append([0.6,0.4,0.7,1])

    yaws = [0.0]*len(order)

    count = 0
    lowest = 0
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = prev_height
    size[2] = prev_height
    prev_size = copy.copy(size)
    yaw = 0
    # height_coeff += 0.03
    prev_o = 'flat'
    # step_width = np.random.uniform(-0.04*difficulty + 0.5, -0.04*difficulty + 0.7)
    # step_pos = np.random.choice([-1,1])*np.random.uniform(0,0.2)
    step_width = 0.1
    if foot:
      step_pos = -1*0.15
    else:
      step_pos = 1*0.15
    pos = [pos[0], step_pos, prev_height]
    size = [size[0], step_width, prev_height]
    
      #     elif pol == 'zero':
      #   colours.append([0.8,0.5,0.3,1])
      # elif pol == 'flat':
      #   colours.append([0.3,0.3,0.8,1])
    # size_box = [0.45, 0.55, 0.5]
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size_box, yaw=yaw, colour=[0.8,0.5,0.3,1], centre=False)
    
    
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw, colour=colours[0], centre=True)
    # pos, prev_yaw = self.add_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw, colour=colours[0], centre=True)
    
        
    self.add_box(pos, orn=[0,0,0], size=size, colour=colours[0])
    
    prev_size = copy.copy(initial_size)
    step_pos = -step_pos
    pos = [pos[0], step_pos, prev_height]
    size = [size[0], step_width, prev_height]
    self.add_box(pos, orn=[0,0,0], size=size, colour=colours[0])
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw, colour=colours[0], centre=True)
    for i,o in enumerate(order):        
      step_pos = -1*step_pos
      pos[1] = step_pos
      # height = prev_height + np.random.uniform(-difficulty/100, difficulty/100)
      height = prev_height 
      pos = [pos[0], pos[1], height]
      size = [size[0], step_width, height]
      prev_o = o
      prev_height = height
      yaw = 0.0
      pos2 = [pos[0], -pos[1], height]
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw, colour=colours[i], centre=True)
      
      if difficulty < 9:
        size2 = [size[0]*((10-difficulty)/10), step_width, prev_height]
        pos2, prev_yaw = self.add_curved_box(pos2, prev_yaw=prev_yaw, prev_size=prev_size, size=size2, yaw=yaw, colour=colours[i], centre=True)

      prev_size = copy.copy(size)

  def add_straight_world(self, difficulty=10, height_coeff=0.07, terrain_type='mix', base=False, base_before=False, initial_pos=[0,0,0], baseline=False, num_artifacts=1, rand_flat=False):
    # difficulty = 10
    # if difficulty == 10:
    #   height_coeff = 0.07
    # else:
    #   # height_coeff = np.random.uniform((0.04/10)*difficulty + 0.02, (0.05/10)*difficulty + 0.02)
    #   height_coeff = (0.05/10)*difficulty + 0.02
    initial_size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    # initial_size = [0.2, 0.7, 0.5]
    # initial_size = [np.random.uniform(0.18,0.22), 0.55, 0.5]
    size = copy.copy(initial_size)
    pos = [-initial_size[0]-0.5+initial_pos[0],0+initial_pos[1],initial_size[2]+initial_pos[2]]
    prev_size = copy.copy(initial_size)
    prev_yaw = 0
    if terrain_type == 'mix':
      # Meke sure not same terrain type twice in a row
      rand_order = []
      # all_options = ['jumps','gaps','stairs', 'steps', 'high_jumps']
      # option = np.random.choice(['jumps','gaps','stairs', 'steps', 'high_jumps'])
      all_options = ['jumps','gaps','stairs']
      option = np.random.choice(['jumps','gaps','stairs'])
      for i in range(7):
        rand_order.append(option)
        options = copy.copy(all_options)
        options.remove(option)
        option = np.random.choice(options)

    elif terrain_type == 'dqn':
      rand_order = [np.random.choice(['jumps','gaps','stairs']) for _ in range(15)]
    elif terrain_type == 'multi':
      rand_order = ['jumps','gaps','stairs']
    elif terrain_type in ['flat','hop']:
      rand_order = ['flat']
    elif terrain_type == 'zero':
      rand_order = []
    else:
      if baseline:
        rand_order = [terrain_type for _ in range(7)]
      else:
        rand_order = [terrain_type for _ in range(num_artifacts)]
    order = ['flat','flat','flat','flat','flat']
    pol_order = ['flat','flat','flat','flat','flat']
    pre_artifact_box = []
    if base:
      order = ['flat']*(3*2 + 7*7)
      pol_order = ['flat']*(3*2 + 7*7)
    else:
      for r in rand_order:
        if r == 'stairs':
          up_down = np.random.choice(['up', 'down'])
        # if terrain_type == 'mix':        
        #   artifact_size = np.random.randint(4,7)
        #   # artifact_size = np.random.randint(5,9)
        #   if r == 'stairs':
        #     placement = np.random.randint(0,3)
        #     stair = np.random.choice(['up', 'down'])

        #   else:
        #     if artifact_size == 4:
        #       placement = 2
        #     else:
        #       placement = np.random.randint(2,artifact_size-2)
        #       # placement = np.random.randint(1,artifact_size-3)
        # else:
        # if r in ['stairs', 'steps', 'flat']:
        #   artifact_size = np.random.randint(7,8)
        # else:
        if r == 'high_jumps':
          artifact_size = 2
        elif r in ['flat','hop']:
          artifact_size = 40
        elif r in ['steps', 'stairs']:
          artifact_size = np.random.randint(6,8)
        else:
          artifact_size = np.random.randint(4,8)
        placement = 1
        pre_artifact_box.append(len(order)) 
        for i in range(artifact_size):
          if r in ['flat','hop']:
            order.append('flat')
          elif r in ['steps']:
            order.append('steps')
            # pol_order.append('base')
          elif r in ['jumps', 'gaps', 'high_jumps', 'steps']:
            # if i == 3:
            if (i == placement) or r == 'steps':
              order.append(r)
            else:
              order.append('flat')
          else:
            if terrain_type == 'mix':
              if i >= placement:
                order.append(np.random.choice(['up', 'down']))
              else:
                order.append('flat')
            else:
              if i >= placement and i != artifact_size - 1:
                order.append(np.random.choice(['up', 'down']))
              else:
                order.append('flat')
          pol_order.append(r)
      order.extend(['flat'])
      pol_order.extend(['zero'])

    colours = []
    for pol in pol_order:
      if pol == 'stairs':
        colours.append([0.8,0.5,0.8,1])
      elif pol == 'jumps':
        colours.append([0.8,0.3,0.3,1])
      elif pol == 'high_jumps':
        colours.append([0.8,0.4,0.3,1])
      elif pol == 'steps':
        colours.append([0.6,0.4,0.7,1])
      elif pol == 'gaps':
        colours.append([0.3,0.8,0.3,1])
      elif pol == 'zero':
        colours.append([0.8,0.5,0.3,1])
      elif pol in ['flat','hop']:
        colours.append([0.3,0.3,0.8,1])
      else:
        print("in obstacle", pol)

    yaws = [0.0]*len(order)

    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = prev_height
    size[2] = prev_height
    # self.add_box(pos, [0,0,0], size)
    prev_size = copy.copy(size)
    yaw_count = 0
    prev_o = 'flat'
    # print(pol_order, order, difficulty)
    # print(pol_order)
    # print(pre_artifact_box)
    for i,o in enumerate(order):        
      # initial_size = [np.random.uniform(0.18,0.22), initial_size[1], 0.5]
      if pol_order[i] == 'zero':
        initial_size = [0.8, initial_size[1], 0.5]
      # elif (i > 0 and pol_order[i-1] != pol_order[i]):
      elif i in pre_artifact_box:
      # if i > 1 and ((pol_order[i-2] != pol_order[i]) or (pol_order[i-1] != pol_order[i])):
        initial_size = [0.45, initial_size[1], 0.5]
        # initial_size = [0.225, initial_size[1], 0.5]
        # order_count += 1
        # print("art", order_count)
      else:
        initial_size = [np.random.uniform(0.18,0.22), initial_size[1], 0.5]
        # initial_size = [initial_size[0], initial_size[1], 0.5]

      if o == 'down':
        height = prev_height - height_coeff
        pos[2] = height
        size = [initial_size[0], initial_size[1], height]
      elif o == 'up':
        height = prev_height + height_coeff
        pos[2] = height
        size = [initial_size[0], initial_size[1], height]
      elif o == 'jumps':
        # pos = [pos[0], pos[1], pos[2] + difficulty*0.015]
        if difficulty == 10:
          pos = [pos[0], pos[1], pos[2] + 0.15]
        else:
          # pos = [pos[0], pos[1], pos[2] + 0.05 + np.random.uniform((difficulty-1)*0.01, difficulty*0.01)]
          pos = [pos[0], pos[1], pos[2] + 0.05 + difficulty*0.01]
        size = [0.05+1*0.03, size[1], pos[2]]
      elif o == 'high_jumps':
        if difficulty == 10:
          pos = [pos[0], pos[1], pos[2] + 0.25]
        else:
          # pos = [pos[0], pos[1], pos[2] + np.random.uniform(0.1 + (difficulty-1)*0.015, 0.1 + (difficulty)*0.015)]
          pos = [pos[0], pos[1], pos[2] + 0.1 + (difficulty)*0.015]
        size = [0.03, initial_size[1], pos[2]]
      elif o == 'steps':
        # step_width = 0.1 + (10 - difficulty)
        if difficulty == 10:
          step_width = 0.1
        else:
          # step_width = np.random.uniform(-0.022*(difficulty-1) + 0.322, -0.022*difficulty + 0.322)
          step_width = -0.022*difficulty + 0.322

        # step_width = -0.044*difficulty + 0.544
        # step_width = 0.3
        # print(step_width)
        # step_width = 0.12
        # if prev_o != 'steps':
        if i in pre_artifact_box:
          # step_pos = np.random.choice([-1,1])*(0.15 + np.random.uniform(-difficulty/200, difficulty/200))
          # step_pos = np.random.choice([-1,1])*(0.15 + np.random.uniform(-10/200, 10/200))
          step_pos = np.random.choice([-1,1])*(0.1)
          # step_pos = np.random.choice([-1,1])*(0.15)
          # step_pos = np.random.choice([-1,1])*(0.22)
          size = [initial_size[0], initial_size[1], height]
          pos2 = copy.copy(pos)
        else:
          step_pos = -1*step_pos
          pos[1] = step_pos
          height = prev_height 
          pos = [pos[0], pos[1], height]
          size = [initial_size[0], step_width, height]
          # if difficulty < 8:
          #   pos2 = [pos[0], -pos[1], height]
          #   size2 = [size[0]*((7-difficulty)/10), size[1], size[2]]
          #   pos2, prev_yaw = self.add_curved_box(pos2, prev_yaw=prev_yaw, prev_size=prev_size, size=size2, yaw=yaw, colour=colours[i], centre=True)
      elif o == 'gaps':
        pos = [pos[0], pos[1], 0.01]
        size = [difficulty*0.035, size[1], pos[2]]
        # size = [0.1 + difficulty*0.025, size[1], pos[2]]
        # if difficulty == 10:
          # size = [0.1 + difficulty*0.025, size[1], pos[2]]
        # else:
          # size = [0.1 + np.random.uniform((difficulty-1)*0.025, difficulty*0.025), size[1], pos[2]]
      else:
        height = prev_height
        # pos[1] = 0
        pos[2] = height
        size = [initial_size[0], initial_size[1], height]
      prev_o = o
      prev_height = height
      yaw = 0.0
      # print(i, len(colours))
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw, colour=colours[i], centre=True)
      prev_size = copy.copy(size)
    

    return pol_order

  def add_curved_box(self, pos=[0,0,0], prev_yaw=0.0, prev_size=[1,0.25,0.5], size=[1,0.25,0.5], yaw=-0.25, colour=[0.8,0.3,0.3,1], centre=False):
    if centre:
      length, width, _ = prev_size
      diag = np.sqrt(length**2 + width**2)
      if yaw >= 0:
        yaw = min(yaw, np.arctan2(length*2, width*2)) 
        psi = np.arctan2(-width, length)
      else:
        yaw = max(yaw, -np.arctan2(length*2, width*2)) 
        psi = np.arctan2(width, length) 
      cnr = [pos[0] + diag*np.cos(psi + prev_yaw), pos[1] + diag*np.sin(psi + prev_yaw), pos[2]]
      length, width, _ = size
      diag = np.sqrt(length**2 + width**2)
      if yaw >= 0:
        phi = np.arctan2(width, length) 
      else:
        phi = np.arctan2(-width, length)
      yaw = yaw + prev_yaw
      x = diag*np.cos(yaw + phi)
      y = diag*np.sin(yaw + phi)
      # pos = [cnr[0] + x, cnr[1] + y, cnr[2]]
      pos = [cnr[0] + x, pos[1], cnr[2]]
    else:
      length, width, _ = prev_size
      diag = np.sqrt(length**2 + width**2)
      if yaw >= 0:
        yaw = min(yaw, np.arctan2(length*2, width*2)) 
        psi = np.arctan2(-width, length)
      else:
        yaw = max(yaw, -np.arctan2(length*2, width*2)) 
        psi = np.arctan2(width, length) 
      cnr = [pos[0] + diag*np.cos(psi + prev_yaw), pos[1] + diag*np.sin(psi + prev_yaw), pos[2]]
      length, width, _ = size
      diag = np.sqrt(length**2 + width**2)
      if yaw >= 0:
        phi = np.arctan2(width, length) 
      else:
        phi = np.arctan2(-width, length)
      yaw = yaw + prev_yaw
      x = diag*np.cos(yaw + phi)
      y = diag*np.sin(yaw + phi)
      pos = [cnr[0] + x, cnr[1] + y, cnr[2]]
    self.add_box(pos=pos, orn=[0,0,yaw], size=list(size), colour=colour)
    return pos, yaw

  def add_circle(self, pos, size=[0.1,0.01], colour=[0,0,1,1]):
    visBoxId = p.createVisualShape(p.GEOM_CYLINDER, radius=size[0], length=size[1], rgbaColor=colour)
    circle_id = p.createMultiBody(baseMass=0, baseVisualShapeIndex=visBoxId, basePosition=pos)
    self.circle_id.append(circle_id)
    self.circle_sizes.append(size)
    self.circle_colours.append(colour)

  def add_box(self, pos, orn, size, colour=[0.8,0.3,0.3,1]):
    size = copy.copy(size)
    colBoxId = p.createCollisionShape(p.GEOM_BOX, halfExtents=size)
    visBoxId = p.createVisualShape(p.GEOM_BOX, halfExtents=size, rgbaColor=colour)
    q = p.getQuaternionFromEuler(orn)
    box_id = p.createMultiBody(baseMass=0, baseCollisionShapeIndex=colBoxId, baseVisualShapeIndex=visBoxId, basePosition=pos, baseOrientation=q)
    self.box_id.append(box_id)
    self.positions.append(pos+orn)
    self.sizes.append(size)
    self.colours.append(colour)
    # print(pos, size)

  def get_box_info(self):
    # print(self.sizes)
    return [self.box_id, self.positions, self.sizes, self.colours]

if __name__=="__main__":
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--use_env', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  args = parser.parse_args()

  if args.use_env:
    from env import Env
    env = Env(render=args.render, display_im=args.display_im, obstacle_type='path')
    # env = Env(render=False, display_im=True)
    # physicsClientId = p.connect(p.GUI)
    # obstacles = Obstacles()
    # obstacles.stairs(['down','up','down','up','down'])
    # obstacles.stairs(['up' for _ in range(10)])
    # obstacles.doughnut(size=0.5,width=0.5)
    # obstacles.path(path=[[1,1,0.5,1],[1.1,1,0.5,1],[1,1,0.5,1]])
    # obstacles.path(difficulty=4)
    # obstacles.adversarial_world()
    # obstacles.path(difficulty=10)
    # obstacles.stairs(['up','up','up','up','up','up','up','up','up','down','down','down','down','down','down'], height_coeff=0.07)

    env.reset()
    env.obstacles.remove_obstacles()
    # env.obstacles.test_world()
    env.obstacles.test_world()
    # env.obstacles.path(difficulty=1)
    env.world_map = env.get_world_map(env.obstacles.get_box_info())

    while True:
      env.step(np.random.random(env.ac_size))
      env.get_im()
  else:
    if args.render:
      physicsClientId = p.connect(p.GUI)
    else:
      physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot
    obstacles = Obstacles()
    p.loadMJCF("ground.xml")
    obstacles.add_show1()
    # obstacles.add_curved_box()
    # obstacles.add_curves(difficulty=7)
    # obstacles.add_curvey_stairs(order=['flat']*3 + [np.random.choice(['up','down','flat'],p=[0.3,0.3,0.4]) for _ in range(30)], height_coeff=0.07, difficulty=7)
    # obstacles.add_curvey_stairs(order=['flat']*3 + [np.random.choice(['up','down'],p=[0.5,0.5]) for _ in range(40)], height_coeff=0.07, difficulty=1)
    # obstacles.add_domain_training_world()
    # obstacles.large_world()
    # obstacles.add_domain_training_world_no_stairs()
    while True:
      pass
