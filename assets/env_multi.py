import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import numpy as np
import pybullet as p
from collections import deque
from pathlib import Path
home = str(Path.home())
from mpi4py import MPI
comm = MPI.COMM_WORLD
import time
import cv2
import random
import copy

class Env():
  rank = comm.Get_rank()
  # Sim preferes 240Hz
  # simStep = 1/240
  simStep = 1/120
  timeStep = 1/120
  # Pos and vel, com, contacts + swing_feet, vel commands
  ob_size = (12*2+8 + 7 + 8 + 2 + 4)
  ac_size = 12
  total_steps = 0
  steps = 0
  Kp = 400
  initial_Kp = Kp
  Kd = 0.1
  height_coeff = 0.011
  difficulty = 1
  max_yaw = 0.0
  episodes = 0
  iters_so_far = 0
  max_disturbance = 100
  # max_disturbance = 250
  # max_disturbance = 1000
  rew_Kp = 1
  max_v = 1.5
  min_v = 0.5
  max_yaw = 1.25
  # Strafe max velocity
  # max_vy = 0.75
  max_vy = 0.4
  pause_time = 50
  replace_Id = None
  grid_size = 0.025  


  # def __init__(self, render=False, PATH=None,  args=None, record_step=True, display_im=False, cur=False, obstacle_type='flat', control_type=None, vis=False, disturbances=True, speed_cur=False, doa=False, multi=False, num_robots=10):
    
  def __init__(self, render=False, PATH=None,  args=None, record_step=True, display_im=False, cur=False, obstacle_type='None', control_type=None, vis=False, disturbances=True, speed_cur=False, doa=False, multi=False, dqn=False, comparison=None, initial_disturbance=100, final_disturbance=1200, vis_type='depth', difficulty=1, dist_difficulty=0, num_artifacts=1, MASTER=True, early_stop=False):


    if not args.single_pol:
      self.map_length = 10
      self.robot_resolution = 0.5
      # self.num_robots = int(self.map_length/self.robot_resolution)
      self.num_robots = 10
      self.robots = {i:{} for i in range(self.num_robots)}
      self.robots['next'] = {}
    else:
      self.num_robots = 0
      self.robots = {}
      self.num_positives = 0
      self.num_negatives = 0

    
    self.render = render
    self.PATH = PATH
    self.args = args
    self.record_step = record_step
    self.display_im = display_im
    self.cur = cur
    self.speed_cur = speed_cur
    self.obstacle_type = obstacle_type
    self.doa = doa
    self.multi = multi

    self.control_type = control_type
    self.vis = vis
    self.stopped_start = True
    # self.stopped_start = False
    if self.doa:
      self.disturbances = False
    else:
      self.disturbances = disturbances

    # self.box_nums = [-1,0,1,2]
    self.box_nums = [-1,0,1,2,3]
    # self.box_nums = [0,1]
    # self.num_boxes = 5
    self.num_boxes = len(self.box_nums)
    
    self.box_dim = 5
    if self.vis:
      self.im_size = [60,40,1]
    else:  
      self.im_size = [self.num_boxes*self.box_dim]

    self.reward_breakdown = {'goal':deque(maxlen=100), 'pos':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100), 'com':deque(maxlen=100), 'sym':deque(maxlen=100), 'act':deque(maxlen=100)}
  
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot


    self.sim_data = []
    self.load()

    self.box_info = None
    self.world_map = None
    self.z_offset = 0
    try:
      from assets.obstacles import Obstacles
    except:
      from obstacles import Obstacles
    self.obstacles = Obstacles()
    self.still_pos = np.array([0.0,0.0,-0.1,-0.2,0.0,-0.1,0.0,0.0,-0.1,-0.2,0.0,-0.1])
    self.exp_pos = self.still_pos

  def load(self):

    p.loadMJCF(currentdir + "/ground.xml")
    # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
    if not self.args.single_pol:
      for i in range(self.num_robots):
        self.robots[i]['Id'] = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
      # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION )
      self.Id = self.robots[0]['Id']
    else:
      objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
      # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION )
      self.Id = objs

    p.setTimeStep(self.simStep)
    p.setGravity(0,0,-9.8)
    # ======================================================================

    numJoints = p.getNumJoints(self.Id)
    # Camera following robot:
    # body_xyz, (qx, qy, qz, qw) = p.getBasePositionAndOrientation(self.Id)
    # p.resetDebugVisualizerCamera(2.0, self.camera_rotation, -10.0, body_xyz)
    # p.resetDebugVisualizerCamera(2.0, 50, -10.0, body_xyz)

    self.jdict = {}
    self.feet_dict = {}
    self.leg_dict = {}
    self.body_dict = {}
    # self.feet = ["left_heel1", "left_heel1", "right_heel1", "right_heel1", "left_toe", "right_toe"]
    self.feet = ["left_heel1", "left_heel2", "left_toe1", "left_toe2", "right_heel1", "right_heel2", "right_toe1", "right_toe2"]
    self.feet_contact = {f:True for f in self.feet}
    self.ordered_joints = []
    self.ordered_joint_indices = []

    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name=="left_heel1": self.feet_dict["left_heel1"] = j
      if link_name=="right_heel1": self.feet_dict["right_heel1"] = j
      if link_name=="left_heel2": self.feet_dict["left_heel2"] = j
      if link_name=="right_heel2": self.feet_dict["right_heel2"] = j
      if link_name=="left_toe1": self.feet_dict["left_toe1"] = j
      if link_name=="right_toe1": self.feet_dict["right_toe1"] = j
      if link_name=="left_toe2": self.feet_dict["left_toe2"] = j
      if link_name=="right_toe2": self.feet_dict["right_toe2"] = j
      # print(link_name)
      if link_name=="base_link": self.body_dict["body_link"] = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
    
    self.motor_names = ["right_hip_z", "right_hip_x", "right_hip_y", "right_knee"]
    self.motor_names += ["right_ankle_x", "right_ankle_y"]
    self.motor_names += ["left_hip_z", "left_hip_x", "left_hip_y", "left_knee"]
    self.motor_names += ["left_ankle_x", "left_ankle_y"]
    self.motor_power = [300, 300, 900, 600]
    self.motor_power += [150, 300]
    self.motor_power += [300, 300, 900, 600]
    self.motor_power += [150, 300]
       
    self.motors = [self.jdict[n] for n in self.motor_names]
    
    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}

    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

    for key in self.feet_dict:
      p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9)

    self.ep_lens = deque(maxlen=5)

    self.ep_speeds = deque(maxlen=5)
    self.speeds = [0]

    self.ep_rewards = deque(maxlen=5)
    self.total_reward = 0

    self.pos_error = deque(maxlen=5)
    self.pos_errors = [0.0]
    self.step_time = {'right':deque(maxlen=100), 'left':deque(maxlen=100)}
    self.box_num = 0
    self.replace_Id = None


  def reset(self, params=None, base_before=False, robot=None):
    
    if self.replace_Id is not None:
      p.removeUserDebugItem(self.replace_Id)

    if self.episodes > 0 and self.record_step:
      self.save_sim_data()
    else:
      self.sim_data = []
    
    self.foot_on_box = {'left':0,'right':0}   
    self.desired_box = {'left':0,'right':0}
    # if self.episodes != 0 and self.episodes % 20 == 0:
    #   print("Average step time: ", "right: ", np.mean(self.step_time['right']), "left: ", np.mean(self.step_time['left']))
    # self.prev_step_time = {'right':0, 'left':0}
    self.touchdown_pens = {0:[0], 1:[0]}
    self.touchdown_dists = {'left':0, 'right':0}
    self.touchdown_box = {'left':set(), 'right':set()}

    self.foot_box = {'right':{0}, 'left':{0}}
    self.current_foot_box = {'right':0, 'left':0}
    self.desired_box = {'right':0, 'left':0}
    self.step_sign = 0
    self.swing_on_ground_time = 0
    self.prev_thing = 0

    self.right_boxes = []
    self.left_boxes = []

    self.ep_lens.append(self.steps)
    self.ep_rewards.append(self.total_reward)

    self.ep_speeds.append(np.mean(self.speeds))
    self.speeds = [0]

    self.pos_error.append(np.mean(self.pos_errors))
    self.pos_errors = []

    # self.height_coeff = 0.07
    # self.difficulty = 10

    # if self.disturbances and self.max_disturbance < 500 and (self.height_coeff >= 0.07 or self.difficulty >= 10) and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > self.args.cur_len).all():
    if self.disturbances and self.max_disturbance < 1000 and (self.height_coeff >= 0.07 or self.difficulty >= 10) and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > self.args.cur_len).all():
      self.ep_rewards = deque(maxlen=5)
      self.max_disturbance += 50


    # if self.obstacle_type in ['up_stairs', 'down_stairs', 'stairs', 'gaps', 'jumps', 'base', 'turn']:  
    if self.obstacle_type != 'flat':  
      # if not self.cur and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > 800).all():
      # if not self.cur and len(self.ep_rewards) == 5 and np.mean(self.ep_rewards) > 1500:
      # if not self.cur and self.obstacle_type in ['gaps', 'jumps', 'base', 'turn'] and self.difficulty < 10 and len(self.ep_rewards) == 5 and np.mean(self.ep_rewards) > self.args.cur_len:  
      if not self.cur and self.obstacle_type in ['gaps', 'jumps', 'base', 'turn'] and self.difficulty < 10 and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > self.args.cur_len).all():  
    # if not self.cur and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > 500).all():
    # if not self.cur and len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 12).all() and self.steps != 0 and np.mean(self.ep_speeds) > 0.5 and len(self.ep_speeds) == 5:
      # self.ep_lens = deque(maxlen=5)
      # self.ep_speeds = deque(maxlen=5)
        self.ep_rewards = deque(maxlen=5)
        self.difficulty += 1*self.args.inc
      # elif not self.cur and ((self.obstacle_type in ['up_stairs', 'down_stairs', 'stairs'] and self.height_coeff < 0.07 and len(self.ep_rewards) == 5 and np.mean(self.ep_rewards) > self.args.cur_len) or (self.obstacle_type == 'stairs' and self.vis and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > 500).all())): 
      elif not self.cur and (self.obstacle_type in ['up_stairs', 'down_stairs', 'stairs'] and self.height_coeff < 0.07 and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > self.args.cur_len).all()) : 
        self.ep_rewards = deque(maxlen=5)
        self.height_coeff += 0.005*self.args.inc
        # self.height_coeff += 0.01

      if self.args.single_pol:
        if not robot:
          self.order = self.obstacles.add_straight_world(difficulty=self.difficulty, height_coeff=self.height_coeff, base_before=base_before, terrain_type=self.obstacle_type)
      else:
        self.obstacles.remove_obstacles()

        if self.obstacle_type == 'stairs':
          self.obstacles.add_curvey_stairs(order=['flat']*3 + [np.random.choice(['up','down'],p=[0.5,0.5]) for _ in range(47)], height_coeff=self.height_coeff, difficulty=0.0)
        elif self.obstacle_type == 'up_stairs':
          self.obstacles.add_curvey_stairs(order=['flat']*3 + ['up' for _ in range(47)], height_coeff=self.height_coeff, difficulty=self.difficulty)
        elif self.obstacle_type == 'down_stairs':
          self.obstacles.add_curvey_stairs(order=['flat']*3 + ['down' for _ in range(47)], height_coeff=self.height_coeff, difficulty=self.difficulty)
        elif self.obstacle_type == 'gaps':
          self.obstacles.add_gaps(difficulty=self.difficulty)
        elif self.obstacle_type == 'turn':
          self.obstacles.add_curvey_stairs(difficulty=0, height_coeff=0, order=['flat']*15)
        elif self.obstacle_type == 'jumps':
          self.obstacles.add_jumps(difficulty=self.difficulty)
        elif self.obstacle_type == 'base':
          self.obstacles.add_curvey_stairs(difficulty=0.0, order=['flat']*65)
        elif self.obstacle_type == 'run':
          self.obstacles.add_curvey_stairs(difficulty=0.0, order=['flat']*150)
        elif self.obstacle_type == 'mix':
          self.order = self.obstacles.add_straight_world(difficulty=self.difficulty, height_coeff=self.height_coeff, terrain_type='multi')
          # self.order = self.obstacles.add_straight_world(difficulty=self.difficulty, height_coeff=self.height_coeff)
          # self.order = self.obstacles.add_show1(difficulty=self.difficulty, height_coeff=self.height_coeff)
          # self.order = self.obstacles.add_show2(difficulty=self.difficulty, height_coeff=self.height_coeff)
          # self.order = self.obstacles.add_straight_world(difficulty=self.difficulty, height_coeff=self.height_coeff)
          # self.order = self.obstacles.add_straight_world(difficulty=self.difficulty, height_coeff=self.height_coeff, base=True)

        elif self.obstacle_type == 'domain_train':
          self.obstacles.add_domain_training_world()
        elif self.obstacle_type == 'domain_train_no_stairs':
          self.obstacles.add_domain_training_world_no_stairs()

      if not robot:
        print("this should only happen once")
        self.box_info = self.obstacles.get_box_info()
        self.world_map = self.get_world_map(self.box_info)    
      # print(len(self.box_info[0]), len(self.order))
      # exit()

      # cv2.imshow('world_map_10', self.world_map)
      # cv2.waitKey(0)
    # print(self.ep_rewards1)

    if self.cur:
      # if len(self.ep_rewards) == 5 and np.mean(self.ep_rewards) > self.args.cur_len or (self.obstacle_type == 'turn' and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > 300).all()) or (self.obstacle_type == 'stairs' and self.vis and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > 500).all()):
      if len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > self.args.cur_len).all() or (self.obstacle_type == 'turn' and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > 300).all()) or (self.obstacle_type == 'stairs' and self.vis and len(self.ep_rewards) == 5 and (np.array(self.ep_rewards) > 500).all()):
        self.ep_lens = deque(maxlen=5)
        self.pos_error = deque(maxlen=5)
        self.ep_rewards = deque(maxlen=5)
        self.Kp = self.Kp*0.75
        self.Kd = self.Kd*0.75
        if self.Kp < 10:
          self.Kp = self.Kd = 0
          self.cur = False
    

    start_rot = [0.0,0.0,0.0]
    start_orn = p.getQuaternionFromEuler(start_rot)
    
    self.body_xyz, self.yaw = [0.0, 0.0, 0.0 + 0.95], start_rot[2]
    self.boxes = np.zeros(self.im_size)
    self.get_hm()
    
    self.ob_dict = {'prev_right_foot_left_ground':False,'prev_left_foot_left_ground':False,'left_foot_left_ground':False,'right_foot_left_ground':False}
    
    self.ob_dict['prev_right_heel1'] = self.ob_dict['right_heel1'] = False
    self.ob_dict['prev_right_heel2'] = self.ob_dict['right_heel2'] = False
    self.ob_dict['prev_right_toe2']   = self.ob_dict['right_toe2']   = False
    self.ob_dict['prev_right_toe1']   = self.ob_dict['right_toe1']   = False
    self.ob_dict['prev_left_heel1']  = self.ob_dict['left_heel1']  = False
    self.ob_dict['prev_left_heel2']  = self.ob_dict['left_heel2']  = False
    self.ob_dict['prev_left_toe2']  = self.ob_dict['left_toe2']  = False
    self.ob_dict['prev_left_toe1']    = self.ob_dict['left_toe1']    = False
    
    # self.ob_dict['ready_to_walk'] = False

    self.initial_swing_foot = np.random.randint(2)
    if self.initial_swing_foot:
      self.ob_dict['swing_foot'] = True
      self.ob_dict['right_foot_swing'] = False
      self.ob_dict['left_foot_swing'] = False
    else: 
      self.ob_dict['swing_foot'] = False
      self.ob_dict['right_foot_swing'] = False
      self.ob_dict['left_foot_swing'] = False
    self.first_step = True
    self.step_count = 0
    self.ob_dict['cur_swing_foot'] = self.ob_dict['swing_foot']

    if self.args.single_pol:
      if self.obstacle_type == 'stairs':
        # rand_box = np.random.randint(0,5)
        # rand_box = np.random.randint(0,5)
        rand_box = np.random.randint(0,3)
      else:
        rand_box = np.random.randint(0,3)
      
      # if self.args.goal_set:
      #   pos = [self.box_info[1][rand_box][0] + self.box_info[2][rand_box][0] + np.random.uniform(-0.15,0.15), np.random.uniform(-0.3,0.3), 0.95+self.z_offset] 
      #   orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.75,0.75)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
      # else:
      pos = [self.box_info[1][rand_box][0] + self.box_info[2][rand_box][0] + np.random.uniform(-0.15,0.15), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
      orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.3,0.3)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)

      # pos = [self.box_info[1][1][0], 0.0, 0.95+1.0] 
      # orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,0])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
      
    elif self.doa or self.multi or self.obstacle_type in ['turn','mix']:
      if self.multi:
        self.box_num = 1
      else:
        self.box_num = np.random.randint(0, len(self.box_info[0])-1)
      box_x, box_y, box_yaw = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][5]
      self.body_xyz, self.yaw = [box_x, box_y, 0.0], box_yaw 
      
      if self.obstacle_type == 'turn':
        # Choices are 5 degres from the 90 degree positions for north, south east west (8 possibilities)
        if self.box_num < 3:
          self.initial_yaw = np.random.choice([1.48, -1.48,  1.65, -1.65, 3.05, -3.05])
        elif self.box_num >= len(self.box_info[0]) - 2:
          self.initial_yaw = np.random.choice([1.48, -1.48,  1.65, -1.65, 0.08, -0.08])
        else:
          self.initial_yaw = np.random.choice([1.48, -1.48,  1.65, -1.65])
        self.initial_x, self.initial_y = self.body_xyz[0], self.body_xyz[1]
        # self.initial_yaw = np.pi
      else:
        self.initial_yaw = 0
        self.initial_x, self.initial_y = self.body_xyz[0], self.body_xyz[1]

      # self.body_xyz, self.yaw = [box_x, box_y + np.random.uniform(-0.2, 0.2), 0.0], np.random.uniform(-np.pi, np.pi)
      # self.body_xyz, self.yaw = [0.0, 0.0, 0.0], 0.0
      
      
      self.get_hm()
      pos, orn, joints, base_vel, joint_vel = [self.body_xyz[0],self.body_xyz[1],0.95 + self.z_offset], list(p.getQuaternionFromEuler([0,0,self.initial_yaw])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)

    else:
      # if self.obstacle_type == 'turn':
      #   left_or_right = np.random.choice([-1,1],p=[0.5,0.5])
      #   pos = [0,left_or_right*np.random.uniform(0.2,0.4),0.95 + self.z_offset]
      #   if left_or_right > 0:
      #     orn = list(p.getQuaternionFromEuler([0,0,np.random.uniform(np.pi/4, 3*np.pi/4)]))
      #   else:
      #     orn = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-3*np.pi/4, -np.pi/4)]))
      #   base_vel = [[(np.random.random()-0.5)/10,(np.random.random()-0.5)/10,0],[0,0,0]]    
      #   # base_vel = [[0.0,0.0,0],[0,0,0]]    
      #   joints, joint_vel = [0.]*len(self.motor_names), [0.]*len(self.motor_names)
      # else:

      # pos = [0,0,0.95 + self.z_offset]
      # orn = [0,0,0,1]
      
      pos = [np.random.uniform(-0.05,0.1), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
      orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.3,0.3)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
      
      base_vel = [[0,0,0],[0,0,0]]    
      joints, joint_vel = [0.]*len(self.motor_names), [0.]*len(self.motor_names)

    if self.disturbances:
      c = [0.01, 0.01, 0.01, 0.012, 0.1, 0.01, 0.01]
      pos[0] += np.random.uniform(low=0.2, high=c[0])
      pos[1] += np.random.uniform(low=-c[0], high=c[0])
      # No noise to z, can get stuck in the floor
      orn[0] += np.random.uniform(low=-c[2], high=c[2])
      orn[1] += np.random.uniform(low=-c[2], high=c[2])
      orn[2] += np.random.uniform(low=-c[2], high=c[2])
      # Noise on w term does funky things
      for i in range(len(base_vel[0])):
        base_vel[0][i] += np.random.uniform(low=-c[3], high=c[3])
        base_vel[1][i] += np.random.uniform(low=-c[4], high=c[4])  


    self.set_position(pos, orn, joints, base_vel, joint_vel)

    self.prev_speed = self.speed = self.original_speed = self.prev_yaw_speed = self.yaw_speed = self.original_yaw_speed = self.prev_y_speed = self.y_speed = self.original_y_speed = 0.0
    self.prev_time_of_step = self.time_of_step = 0

    self.total_reward = 0
    self.steps = 0
    self.step_count = 0
    self.episodes += 1
    self.prev_artifact = None
    self.get_observation()
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel

    if not self.args.single_pol:
      self.robot_num = 1
      self.robots['next']['pos'] = self.body_xyz
      self.robots['next']['orn'] = [self.qx, self.qy, self.qz, self.qw]
      self.robots['next']['joints'] = self.joints
      for i in range(self.robot_num, self.num_robots):
        self.robots[i]['pos'] = [pos[0], pos[1], pos[2]-1.5*i]
        self.robots[i]['orn'] = orn
        self.robots[i]['joints'] = joints
        self.set_position(self.robots[i]['pos'], self.robots[i]['orn'], self.robots[i]['joints'], robot_id=self.robots[i]['Id'])
    else:
      if robot:
        try:
          robot['pos'] = [robot['pos'][0], robot['pos'][1], robot['pos'][2]]
          if robot['success']:
            self.num_positives += 1
          else:
            self.num_negatives += 1

          # print(robot)
          self.num_robots += 1
          # self.robots[self.robot_num] = {'pos':robot['pos'], 'orn': robot['orn'], 'joints': robot['joints'], 'success':robot['success']}
          self.robots[self.num_robots] = robot
          # self.set_position(robot['pos'], robot['orn'], joints=robot['joints'], robot_id=Id)
        except:
          pass
      else:
        self.obstacles.add_straight_world(difficulty=self.difficulty, height_coeff=self.height_coeff, base_before=base_before, terrain_type=self.obstacle_type, initial_pos=[10,0,0])
        self.obstacles.add_straight_world(difficulty=self.difficulty, height_coeff=self.height_coeff, base_before=base_before, terrain_type=self.obstacle_type, initial_pos=[20,0,0])
        

    # self.action_store = deque(maxlen=self.time_of_step)
    self.action_store = np.zeros([self.time_of_step, 4])
    
    # Work out hip angle needed to evenly walk over blocks
    if self.box_info is None:
      self.x_dist = 0.45
    else:
      self.x_dist = self.box_info[2][0][0]*2
    self.thigh_length = 0.29
    self.shin_length = 0.31
    standing_leg_length = np.sqrt(self.thigh_length**2 + self.shin_length**2 - 2*self.thigh_length*self.shin_length*np.cos(np.pi-0.2))
    angle = np.arccos((self.x_dist/2)/standing_leg_length)
    self.hip_y_total_angle = np.pi - 2*angle 
    self.hip_x_total_angle = 0.05
    # self.hip_z_total_angle = 0.5
    self.hip_z_total_angle = 1.49

    self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.original_speed, self.original_yaw_speed]
    return np.array(self.state)

  def step(self, actions, freeze_robot=False):

    if not self.multi and (((self.steps*self.timeStep) % 3) == 0 and self.steps != 0) or self.steps == self.pause_time:
      # if random.random() < 0.05 and not self.doa and self.obstacle_type != 'run':
      if random.random() < 0.2 and self.obstacle_type == 'base':
        self.original_speed = 0
      else:
        if not self.speed_cur:        
          if self.obstacle_type == 'run':
            self.original_speed = 4.0
          # elif self.obstacle_type in ['stairs','jumps','gaps']:
          #   self.original_speed = 0.75 
          else:
            self.original_speed = 1.0 
          # self.original_speed = 2.0
          # self.original_speed = 0.5
          # print(self.steps, self.original_speed)
        else:
          self.original_speed = np.random.uniform(low=self.min_v,high=self.max_v)           
      
      if random.random() < 0.05 or not self.speed_cur:
        self.yaw_speed = 0
      else:
        self.yaw_speed = np.random.uniform(low=-self.max_yaw,high=self.max_yaw)           
      # if self.strafe:
      #     self.y_v = np.random.uniform(low=-self.max_vy,high=self.max_vy)          
    
      if abs(self.original_speed) >= 0.2:
        self.time_of_step = int(60/abs(self.original_speed))
        self.dsp_time = int(20/abs(self.original_speed))
        self.action_store = np.zeros([self.time_of_step, 4])
        # self.action_store = deque(maxlen=self.time_of_step)
      else:
        self.time_of_step = 0

    # if self.steps > 200:
    #   self.original_speed = 0.0

    # if not self.speed_cur and self.steps == self.pause_time:
    #   if abs(self.original_speed) >= 0.2:
    #     self.time_of_step = int(60/abs(self.original_speed))
    #     self.dsp_time = int(20/abs(self.original_speed))
    #     self.action_store = deque(maxlen=self.time_of_step)
    #   else:
    #     self.time_of_step = 0

    # if not self.speed_cur and self.steps > self.pause_time:
    #   self.yaw_speed = 0
    #   if self.obstacle_type == 'run':
    #     self.original_speed = self.max_v
    #   else:
    #     self.original_speed = 1

    if self.obstacle_type == 'turn':
      if self.steps - self.pause_time < 0:
        self.target_yaw = self.initial_yaw
        self.original_yaw_speed = 0
      else:
        # If on an edge, turn shortest path to correct facing
        if self.box_num < 3:
          self.target_yaw = 0
        elif self.box_num >= ( len(self.box_info[0]) - 2 ):
          self.target_yaw = np.pi
        else:
          if self.initial_yaw < np.pi/2 and self.initial_yaw >= -np.pi/2:
            self.target_yaw = 0
          else:
            self.target_yaw = np.pi
        if self.target_yaw == np.pi and self.initial_yaw <= -np.pi/2:
          self.target_yaw = -np.pi

        yaw_error = self.target_yaw - self.yaw
        
        if abs(yaw_error) < 0.1:
          self.original_yaw_speed = 0
          # self.box_num = np.random.randint(0, len(self.box_info[0])-1)
          # box_x, box_y, box_yaw = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][5]
          # self.body_xyz, self.yaw = [box_x, box_y, 0.0], box_yaw 
          # self.ini tial_yaw = np.random.choice([1.48, -1.48,  1.65, -1.65, 0.08, -0.08, 3.05, -3.05])
          # self.get_hm()
          # pos, orn, joints, base_vel, joint_vel = [self.body_xyz[0],self.body_xyz[1],0.95 + self.z_offset], list(p.getQuaternionFromEuler([0,0,self.initial_yaw])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
          # self.set_position(pos, orn, joints, base_vel, joint_vel)
        else:
          self.original_yaw_speed = np.sign(yaw_error)*1.5
          self.time_of_step = int(60/abs(self.original_yaw_speed))
          # self.action_store = np.zeros([self.time_of_step, 2])
        self.original_speed = 0


    speeds = np.array([self.original_speed, self.original_y_speed, self.original_yaw_speed])
    if (speeds != 0).any():
      self.time_of_step = int(60/np.max(abs(speeds)))
    #   self.action_store = np.zeros([self.time_of_step, 2])

    # Limit acceleration
    # if self.original_speed > 0.5:
    self.speed = np.clip(self.original_speed, self.prev_speed - 0.01, self.prev_speed + 0.01)
    self.yaw_speed = np.clip(self.original_yaw_speed, self.prev_yaw_speed - 0.05, self.prev_yaw_speed + 0.05)
    self.y_speed = 0.0
    # print(self.steps, self.speed)
    # else:
    #   self.speed = self.original_speed

    if self.time_of_step != 0 and (np.array([self.original_speed, self.original_y_speed, self.original_yaw_speed]) == 0).all() and (np.abs([self.speed, self.y_speed, self.yaw_speed]) < 0.01).all():
      self.time_of_step = 0

    # if self.yaw > np.pi/4 or self.yaw < -np.pi/4:
    #   self.yaw_speed = 0
# 
    # self.speed = 0
    if self.disturbances and random.random() < 0.02:
      self.add_disturbance(self.max_disturbance)
        
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel
    
    self.actions = actions
    

    # Calculate desired positions
    self.get_expert()

    if self.cur:
      exp_forces = self.apply_forces(actions)
    else:
      exp_forces = np.zeros(self.ac_size)
    
    forces = np.array(self.motor_power)*np.array(self.actions)*0.082 + exp_forces
    # forces = exp_forces
    forces = np.clip(forces, -150, 150)
    if self.args.expert:
      # x = self.speed*(self.steps - self.pause_time) * self.timeStep
      # self.set_position(pos=[0,0,1.2+self.z_offset], orn=[0,0,0,1], joints=self.exp_pos)
      p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=self.exp_pos)
    else:
      p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=forces)



    if not self.args.single_pol:
      # if not self.robots['next'] and self.body_xyz[0] > self.robot_resolution and self.body_xyz[0] % self.robot_resolution < 0.1:
        # print(self.body_xyz[0] % self.robot_resolution)
      # self.robot_freez_num = [1,2,3,8,9,1]
      # if not self.robots['next'] and ((self.box_num + 1 < len(self.box_info[0]) and self.order[self.box_num + 1] != self.order[self.box_num] and (self.body_xyz[0] - self.box_info[1][self.box_num][0]) > 0.11) or (self.box_num -4 > 0 and self.order[self.box_num - 4] != self.order[self.box_num-3] and (self.box_info[1][self.box_num][0] - self.body_xyz[0]) < 0.12)):

      if not self.robots['next'] and ((self.box_num - 4 > 0 and self.order[self.box_num - 4] != self.order[self.box_num-3] and (self.box_info[1][self.box_num][0] - self.body_xyz[0]) < 0.12) or (self.order[self.box_num - 1] != self.order[self.box_num] and (self.box_info[1][self.box_num][0] - self.body_xyz[0]) < 0.11)):
      # if not self.robots['next'] and ((self.box_num + 1 < len(self.box_info[0]) and self.order[self.box_num + 1] != self.order[self.box_num] and (self.body_xyz[0] - self.box_info[1][self.box_num][0]) > 0.11) or (self.order[self.box_num - 1] != self.order[self.box_num] and (self.box_info[1][self.box_num][0] - self.body_xyz[0]) < 0.11)):
      # if freeze_robot:
        self.robots['next']['pos'] = self.body_xyz
        self.robots['next']['orn'] = [self.qx, self.qy, self.qz, self.qw]
        self.robots['next']['joints'] = self.joints
      if self.robots['next'] and self.body_xyz[0] - self.robots['next']['pos'][0] > 0.75:
      # if self.robots['next'] and self.body_xyz[0] - self.robots['next']['pos'][0] > self.robot_resolution:
        self.robots[self.robot_num]['pos'] = self.robots['next']['pos']
        self.robots[self.robot_num]['orn'] = self.robots['next']['orn']
        self.robots[self.robot_num]['joints'] = self.robots['next']['joints']
        self.robot_num += 1
        self.robots['next'] = {}
      for i in range(1,self.num_robots):
        self.set_position(self.robots[i]['pos'], self.robots[i]['orn'], self.robots[i]['joints'], robot_id=self.robots[i]['Id'])

      # if not (self.box_info[1][-1][0] - self.body_xyz[0]) < 0.1:
      if self.order[self.box_num] != 'zero':
        for _ in range(int(self.timeStep/self.simStep)):
          p.stepSimulation()
      else:
        while True:
          pass
    else:
      # if self.num_robots > 2:
      # print(self.order[self.box_num])
      if False:
        if self.order[self.box_num] == 'zero':
          for i in range(self.num_robots):
          # if (self.robots[i]['pos'][0]) < self.body_xyz[0] - 0.3:
            print("ploting", self.robots[i])
            self.set_position(self.robots[i]['pos'], self.robots[i]['orn'], joints=self.robots[i]['joints'], robot_id=self.robots[i]['id'])   
          # print(self.robots)
          while True:
            pass

        elif freeze_robot:
          self.robots[self.num_robots] = {}
          self.robots[self.num_robots]['pos'] = self.body_xyz
          self.robots[self.num_robots]['orn'] = [self.qx, self.qy, self.qz, self.qw]
          self.robots[self.num_robots]['joints'] = self.joints

          # self.robots[i]['pos'] = [self.robots[i]['pos'][0], self.robots[i]['pos'][1], self.robots[i]['pos'][2]]
          Id = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
          self.robots[self.num_robots]['id'] = Id
          self.num_robots += 1
          print(self.num_robots)

        for i in range(self.num_robots-1):
          if (self.robots[i]['pos'][0]) < self.body_xyz[0] - 0.3:
            self.set_position(self.robots[i]['pos'], self.robots[i]['orn'], joints=self.robots[i]['joints'], robot_id=self.robots[i]['id'])      
        # print(self.num_robots)
      else:
        # if self.num_robots < 50:
        num = 10
        # num = 4
        if not ( self.num_positives > num and self.num_negatives > num):
        # if self.num_positives < num or self.num_negatives < num:
          for _ in range(int(self.timeStep/self.simStep)):
            p.stepSimulation()
        else:
          num_positives = 0
          num_negatives = 0
          for i in self.robots:
            # if 
            if self.robots[i]["success"]:
              if num_positives < num:
                print("pos",i, self.robots[i]['pos'][0]) 

                # if i < 10:
              # if True:
                # self.robots[i]['pos'] = [self.robots[i]['pos'][0] + 10, self.robots[i]['pos'][1], self.robots[i]['pos'][2]]
                self.robots[i]['pos'] = [self.robots[i]['pos'][0] + 10, self.robots[i]['pos'][1], self.robots[i]['pos'][2]]
                Id = p.loadURDF(currentdir + "/robot_green.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
                self.set_position(self.robots[i]['pos'], self.robots[i]['orn'], joints=self.robots[i]['joints'], robot_id=Id)
                num_positives += 1
            else:
              if num_negatives < num:
                print("neg",i, self.robots[i]['pos'][0]) 
                # if num_negatives == 8:
                # if i == 17:
              # if True:
                self.robots[i]['pos'] = [self.robots[i]['pos'][0] + 10, self.robots[i]['pos'][1], self.robots[i]['pos'][2]]
                Id = p.loadURDF(currentdir + "/robot_red.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
                self.set_position(self.robots[i]['pos'], self.robots[i]['orn'], joints=self.robots[i]['joints'], robot_id=Id)
                num_negatives += 1
          while True:
            pass

    if self.render:
      if self.args.use_roa:
        # print("roa")
        # time.sleep(0.01)
        time.sleep(0.000001)
      else:
        time.sleep(0.0000001)
    self.get_observation()
    self.prev_speed = self.speed
    self.prev_yaw_speed = self.yaw_speed
    self.prev_time_of_step = self.time_of_step
    self.prev_box_num = self.box_num

    if self.record_step: 
      self.record_sim_data()
    reward, done = self.get_reward()
    self.prev_actions = self.actions
    self.total_reward += reward
    self.steps += 1
    self.total_steps += 1
    
    self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.original_speed, self.original_yaw_speed]
    return np.array(self.state), reward, done, None

  def get_reward(self):

    reward = 0
    done = False
    if self.args.single_pol and self.box_num > 13 or (self.obstacle_type == 'zero' and self.steps > 400):
      done = True
    goal, pos, neg, vel, com, sym, tip = 0, 0, 0, 0, 0, 0, 0
    
    act = -0.0001*np.sum(self.actions**2)

    if self.obstacle_type == 'turn':
      # x = abs(self.vx - self.speed)
      # goal = 0.75*(2.0/(np.e**(3*x) + np.e**(-3*x)))
      # print(self.box_num, self.target_yaw, self.original_yaw_speed, self.steps - self.pause_time > 0)
      # if self.original_yaw_speed == 0 and self.steps - self.pause_time > 0:
      # if self.steps > self.pause_time and (self.steps - self.pause_time) > (abs(self.initial_yaw - self.target_yaw)/1.5)*(1/self.timeStep):
      if self.steps > 400:
        done = True
        # print(self.ep_goal)
        # reward += 10
        # print(len(self.box_info[0]) - 3, "box info len:", len(self.box_info[0]), "box:", self.box_num, self.target_yaw, self.yaw, "close to goal")
      x = abs(self.yaw_vel - self.yaw_speed)
      goal = 0.75*(2.0/(np.e**(3*x) + np.e**(-3*x)))
      pos = 0.65*np.exp(-2.0*np.sum((np.array(self.joints) - self.exp_pos)**2))
      com = 0.1*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2], self.body_xyz[0], self.body_xyz[1]]) - np.array([0.0, 0.0, 0.95+self.z_offset, self.initial_x, self.initial_y]))**2))
      if self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground']:
        neg -= 0.05 
      if self.step_count > 1 and self.time_of_step > 0:
        neg -= min(self.touchdown_pen/100, 1.0)
        if min(self.touchdown_pen/100, 1.0) > 0:
          self.touchdown_pens[self.ob_dict['cur_swing_foot']].append(min(self.touchdown_pen/100, 1.0))
    else:
      x = abs(self.vx - self.speed)
      goal = 0.75*(2.0/(np.e**(3*x) + np.e**(-3*x)))
      if self.speed < 0.1:
        goal -= abs(self.vx)
      x = abs(self.yaw_vel - self.yaw_speed)
      goal += 0.25*(2.0/(np.e**(3*x) + np.e**(-3*x)))

      if self.speed > 0.5:
        self.speeds.append(self.vx)

      pos = 0.65*np.exp(-2.0*np.sum((np.array(self.exp_pos) - np.array(self.joints))**2))      
      vel = 0.05*np.exp(-0.1*np.sum((np.zeros(self.ac_size) - np.array(self.joint_vel))**2))

      com = 0.2*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2], self.body_xyz[1], self.yaw]) - np.array([0.0, 0.0, 0.95+self.z_offset, 0.0, 0.0]))**2))

      if self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground'] and (self.obstacle_type in ['base','stairs'] or (self.obstacle_type in ['gaps', 'jumps'] and self.cur)):
        neg -= 0.05      

      # if self.box_info is not None and self.box_num > 1:
      
      if self.step_count > 2 and self.time_of_step > 0:
        if min(self.touchdown_pen/50, 5.0) > 0:
          neg -= min(self.touchdown_pen/50, 5.0)  
          self.touchdown_pens[self.ob_dict['cur_swing_foot']].append(min(self.touchdown_pen/50, 5.0))
          # if self.ob_dict['cur_swing_foot']:
          #   swing = 'right'
          #   stance = 'left'
          # else:
          #   stance = 'right'
          #   swing = 'left'
        if self.obstacle_type in ['base','stairs'] or (self.obstacle_type in ['gaps', 'jumps'] and self.cur):
          neg -= (self.local_foot_pos['left'][0] - self.touchdown_dists['right'])**2
          if self.double_feet:
            neg -= 0.05              
          if self.single_step:
            neg -= 0.05      

      if self.step_count > 2:
        if self.obstacle_type in ['base','stairs'] or (self.obstacle_type in ['gaps', 'jumps'] and self.cur):
          try:
            box_x, box_y = self.box_info[1][self.desired_box['right']][0], self.box_info[1][self.desired_box['right']][1]
            # yaw1 = self.box_info[1][self.desired_box['right']][5] + np.pi/2
            # yaw2 = self.box_info[1][self.desired_box['right']][5] + np.pi/2 + np.pi
            # l = self.box_info[2][self.desired_box['right']][1]
            # x1,y1 = [l*np.cos(yaw1)+box_x, l*np.sin(yaw1)+box_y]
            # x2,y2 = [l*np.cos(yaw2)+box_x, l*np.sin(yaw2)+box_y]
            # d_right = self.dist_to_segment([right_foot[0], right_foot[1]], [x1,y1], [x2,y2])
            d_right = (self.foot_pos['right'][0] - box_x)**2

            box_x, box_y = self.box_info[1][self.desired_box['left']][0], self.box_info[1][self.desired_box['left']][1]
            # yaw1 = self.box_info[1][self.desired_box['left']][5] + np.pi/2
            # yaw2 = self.box_info[1][self.desired_box['left']][5] + np.pi/2 + np.pi
            # l = self.box_info[2][self.desired_box['left']][1]
            # x1,y1 = [l*np.cos(yaw1)+box_x, l*np.sin(yaw1)+box_y]
            # x2,y2 = [l*np.cos(yaw2)+box_x, l*np.sin(yaw2)+box_y]
            # d_left = self.dist_to_segment([left_foot[0], left_foot[1]], [x1,y1], [x2,y2])
            d_left = (self.foot_pos['left'][0] - box_x)**2

            # print(d_right, d_left)
            # print(0.2*np.exp(-5.0*(d_right + d_left)))
            # neg -= 0.2*(d_right + d_left)
            # print(d_right + d_left)
            neg += 0.2*np.exp(-5.0*(d_right + d_left))
            # neg += 0.3*np.exp(-5.0*(d_right + d_left))
            # print(0.25*np.exp(-10*(d_right + d_left)))
          except Exception as e:
            print("something wrong", e)
            print(self.box_info[1])
            print(self.desired_box)

    sym_fact = 0.0
    if self.time_of_step != 0 and self.step_count > 2:
      if self.cur_time < self.time_of_step:
        if self.ob_dict['cur_swing_foot']:
          swing = 'right'
          stance = 'left'
        else:
          swing = 'left'
          stance = 'right'
        # current_pos_torque = np.array([self.ob_dict[a + '_pos'] for a in self.motor_names] + list(self.actions))
        # sym = sym_fact*np.exp(-0.1*np.sum((self.action_store[self.cur_time,:] - current_pos_torque)**2))
        # self.action_store[self.cur_time, :] = current_pos_torque

        current_hip_y = np.array([self.ob_dict[swing + '_hip_y_pos'], self.ob_dict[swing + '_knee_pos'], self.ob_dict[stance + '_hip_y_pos'], self.ob_dict[stance + '_knee_pos']])
        sym = sym_fact*np.exp(-5*np.sum((self.action_store[self.cur_time,:] - current_hip_y)**2))      
        # print(self.cur_time, swing, stance, current_hip_y, self.action_store[self.cur_time, :], np.sum((self.action_store[self.cur_time,:] - current_hip_y)**2))
        # print(sym)
        self.action_store[self.cur_time, :] = current_hip_y


    reward = goal + pos + com + neg + sym + act + vel + tip
    self.pos_errors.append(pos)

    self.reward_breakdown['goal'].append(goal)
    self.reward_breakdown['pos'].append(pos)
    self.reward_breakdown['vel'].append(vel)
    self.reward_breakdown['neg'].append(neg)
    self.reward_breakdown['tip'].append(tip)
    self.reward_breakdown['com'].append(com)
    self.reward_breakdown['sym'].append(sym)
    self.reward_breakdown['act'].append(act)
    
    # if self.obstacle_type == 'jump':
    #   if ((self.steps + 1) % 16 == 0 and self.steps != 0) and (self.steps > 8*256-1 or self.body_xyz[2] < (1.7)):   
    #     done = True
    # else:
    if self.doa:
      if self.body_xyz[2] < (0.7 + self.z_min) or self.steps > 2700:   
        done = True
    else:
      # if ((self.steps + 1) % 16 == 0 and self.steps != 0) and (self.steps > 8*256-1 or self.body_xyz[2] < (0.7 + self.z_min)):   
      if (self.steps > 2048 - 1) or self.body_xyz[2] < (0.7 + self.z_min):   
        done = True
    return reward, done

  def get_observation(self):
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.uniform(-0.5,0.5,self.ac_size))
    
    self.ob_dict.update({n + '_pos':j for n,j in zip(self.motor_names, self.joints)})


    self.body_xyz, (self.qx, self.qy, self.qz, self.qw) = p.getBasePositionAndOrientation(self.Id)
    self.roll, self.pitch, self.yaw = p.getEulerFromQuaternion([self.qx, self.qy, self.qz, self.qw])

    self.body_vxyz, self.base_rot_vel = p.getBaseVelocity(self.Id)
    
    self.roll_vel = self.base_rot_vel[0]
    self.pitch_vel = self.base_rot_vel[1]
    self.yaw_vel = self.base_rot_vel[2]

    rot_speed = np.array(
      [[np.cos(-self.yaw), -np.sin(-self.yaw), 0],
        [np.sin(-self.yaw), np.cos(-self.yaw), 0],
        [		0,			 0, 1]]
    )

    self.vx, self.vy, self.vz = np.dot(rot_speed, (self.body_vxyz[0],self.body_vxyz[1],self.body_vxyz[2]))
    
    # Policy shouldn't know yaw
    self.body = [self.vx, self.vy, self.vz, self.roll, self.pitch, self.roll_vel, self.pitch_vel, self.yaw_vel, self.body_xyz[2] - self.z_offset]

    self.ob_dict['prev_right_heel1']  = self.ob_dict['right_heel1'] 
    self.ob_dict['prev_right_heel2']  = self.ob_dict['right_heel2'] 
    self.ob_dict['prev_right_toe1']   = self.ob_dict['right_toe1']   
    self.ob_dict['prev_right_toe2']   = self.ob_dict['right_toe2']   
    self.ob_dict['prev_left_heel1']   = self.ob_dict['left_heel1'] 
    self.ob_dict['prev_left_heel2']   = self.ob_dict['left_heel2'] 
    self.ob_dict['prev_left_toe1']    = self.ob_dict['left_toe1']   
    self.ob_dict['prev_left_toe2']    = self.ob_dict['left_toe2']   

    self.ob_dict['right_heel1'] = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel1'], -1))>0
    self.ob_dict['right_heel2'] = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel2'], -1))>0
    self.ob_dict['right_toe1']  = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_toe1'], -1))>0
    self.ob_dict['right_toe2']  = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_toe2'], -1))>0
    self.ob_dict['left_heel1']  = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel1'], -1))>0
    self.ob_dict['left_heel2']  = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel2'], -1))>0
    self.ob_dict['left_toe1']   = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_toe1'], -1))>0
    self.ob_dict['left_toe2']   = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_toe2'], -1))>0

    # Update feet that have left the ground.
    self.ob_dict['prev_right_foot_left_ground'] = self.ob_dict['right_foot_left_ground']
    self.ob_dict['prev_left_foot_left_ground'] = self.ob_dict['left_foot_left_ground']
    self.ob_dict['right_foot_left_ground'] = not self.ob_dict['right_heel1'] and not self.ob_dict['right_heel2'] and not self.ob_dict['right_toe2'] and not self.ob_dict['right_toe1']
    self.ob_dict['left_foot_left_ground'] = not self.ob_dict['left_heel1'] and not self.ob_dict['left_heel2'] and not self.ob_dict['left_toe2'] and not self.ob_dict['left_toe1']
    
    self.ob_dict['right_foot_on_ground'] = self.ob_dict['right_heel1'] and self.ob_dict['right_heel2'] and self.ob_dict['right_toe2'] and self.ob_dict['right_toe1']    
    self.ob_dict['left_foot_on_ground'] = self.ob_dict['left_heel1'] and self.ob_dict['left_heel2'] and self.ob_dict['left_toe2'] and self.ob_dict['left_toe1']    

    right_contacts = [self.ob_dict['right_heel1'],self.ob_dict['right_heel2'],self.ob_dict['right_toe2'],self.ob_dict['right_toe1']]
    left_contacts = [self.ob_dict['left_heel1'],self.ob_dict['left_heel2'],self.ob_dict['left_toe2'],self.ob_dict['left_toe1']]
    prev_right_contacts = [self.ob_dict['prev_right_heel1'],self.ob_dict['prev_right_heel2'],self.ob_dict['prev_right_toe2'],self.ob_dict['prev_right_toe1']]
    prev_left_contacts = [self.ob_dict['prev_left_heel1'],self.ob_dict['prev_left_heel2'],self.ob_dict['prev_left_toe2'],self.ob_dict['prev_left_toe1']]

    self.foot_pos = {}
    self.foot_pos['left'] = p.getLinkState(self.Id, self.feet_dict['left_heel1'])[0]
    self.foot_pos['right'] = p.getLinkState(self.Id, self.feet_dict['right_heel1'])[0]

    self.local_foot_pos = {}
    self.local_foot_pos['left'] = self.global_to_local_2d(self.body_xyz, self.yaw, self.foot_pos['left'])
    self.local_foot_pos['right'] = self.global_to_local_2d(self.body_xyz, self.yaw, self.foot_pos['right'])

    # Was previously stopped, now need to select a swing foot
    if self.prev_time_of_step == 0 and self.time_of_step != 0:
      self.ob_dict['swing_foot'] = self.ob_dict['cur_swing_foot'] = np.random.randint(2)
      self.ob_dict['right_foot_swing'] = self.ob_dict['swing_foot']
      self.ob_dict['left_foot_swing'] = not self.ob_dict['right_foot_swing']

    if self.ob_dict['right_foot_swing']:
      swing = 'right'
      stance = 'left'
    else: 
      swing = 'left'
      stance = 'right'

    if self.time_of_step == 0:
      self.ob_dict['right_foot_swing'] = 0
      self.ob_dict['left_foot_swing'] = 0
      if self.box_info is not None:
        for foot in ['right', 'left']:
          for i in range(self.box_num-2, self.box_num+2):
            if i < 0 or i > (len(self.box_info[0]) - 1): continue
            x, y = self.foot_pos[foot][0], self.foot_pos[foot][1]
            length, width = self.box_info[2][i][0], self.box_info[2][i][1]
            box_yaw = self.box_info[1][i][5] 
            diag = np.sqrt(length**2 + width**2)
            x1 = self.box_info[1][i][0] + diag*np.cos(box_yaw+np.arctan2( width,  length))
            y1 = self.box_info[1][i][1] + diag*np.sin(box_yaw+np.arctan2( width,  length))
            x2 = self.box_info[1][i][0] + diag*np.cos(box_yaw+np.arctan2(-width, -length))
            y2 = self.box_info[1][i][1] + diag*np.sin(box_yaw+np.arctan2(-width, -length))
            if x1 > x2:
              x_temp = x1
              x1 = x2
              x2 = x_temp
            if y1 > y2:
              y_temp = y1
              y1 = y2
              y2 = y_temp
            if x1 < x < x2 and y1 < y < y2:
              self.desired_box[foot] = i
              break
    else:

      if self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']:
        self.swing_on_ground_time = self.steps

    self.swing_stance = [self.ob_dict['right_foot_swing'], self.ob_dict['left_foot_swing']]

    self.contacts = right_contacts + left_contacts + prev_right_contacts + prev_left_contacts + self.swing_stance
    # self.contacts = right_contacts + left_contacts + prev_right_contacts + prev_left_contacts + [self.initial_swing_foot]

    # Find current box number (under com)
    if self.box_info is not None:
      if self.box_num == None:
        self.box_num = 0
      i = self.box_num
      total_i = 0
      while True:
        x, y = self.body_xyz[0], self.body_xyz[1]
        x1, y1 = self.box_info[1][i][0] - self.box_info[2][i][0], self.box_info[1][i][1] - self.box_info[2][i][1]
        x2, y2 = self.box_info[1][i][0] + self.box_info[2][i][0], self.box_info[1][i][1] + self.box_info[2][i][1]
        if x1 > x2:
          x_temp = x1
          x1 = x2
          x2 = x_temp
        elif y1 > y2:
          y_temp = y1
          y1 = y2
          y2 = y_temp
        if x1 < x < x2 and y1 < y < y2:
          self.box_num = i
          break
        i += 1
        total_i += 1
        if i > (len(self.box_info[0]) - 1):
          i = 0
        if total_i > (len(self.box_info[0]) - 1):
          # ("com not over box, box number frozen"), box number will remain the last box number
          break
      
      for foot in ['left','right']:
        # foot_pos = p.getLinkState(self.Id, self.feet_dict[foot +'_heel1'])[0]
        for i in range(self.box_num-1, self.box_num+2):
          if i < 0 or i > (len(self.box_info[0]) - 1): continue
          x, y = self.foot_pos[foot][0], self.foot_pos[foot][1]
          x1, y1 = self.box_info[1][i][0] - self.box_info[2][i][0], self.box_info[1][i][1] - self.box_info[2][i][1]
          x2, y2 = self.box_info[1][i][0] + self.box_info[2][i][0], self.box_info[1][i][1] + self.box_info[2][i][1]
          if x1 > x2:
            x_temp = x1
            x1 = x2
            x2 = x_temp
          elif y1 > y2:
            y_temp = y1
            y1 = y2
            y2 = y_temp
          if x1 < x < x2 and y1 < y < y2:
            self.foot_on_box[foot] = i
            break

      # Need to play with this
      self.boxes = []
      self.null_box = [0]*self.box_dim
      for box in self.box_nums:
        if (self.box_num + box < 1) or (self.box_num + box > len(self.box_info[0]) - 1):
          self.boxes.extend(self.null_box)
        else:
        
          length, width = self.box_info[2][self.box_num+box][0], self.box_info[2][self.box_num+box][1]
          # box_yaw = np.arctan2(length, width) + self.box_info[1][self.box_num+box][5]
          box_yaw = self.box_info[1][self.box_num+box][5] 
          
          diag = np.sqrt(length**2 + width**2)
          self.world_box_x1 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2( width,  length))
          self.world_box_y1 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2( width,  length))
          self.world_box_x2 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2( width, -length))
          self.world_box_y2 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2( width, -length))
          self.world_box_x3 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2(-width,  length))
          self.world_box_y3 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2(-width,  length))
          self.world_box_x4 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2(-width, -length))
          self.world_box_y4 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2(-width, -length))

          box_x1, box_y1 = self.global_to_local_2d(self.body_xyz, self.yaw, [self.world_box_x1, self.world_box_y1])
          box_x2, box_y2 = self.global_to_local_2d(self.body_xyz, self.yaw, [self.world_box_x4, self.world_box_y4])

          z  = self.body_xyz[2] - (self.box_info[1][self.box_num+box][2] + self.box_info[2][self.box_num+box][2])
          # z  = self.body_xyz[2] - (self.box_info[1][self.box_num+box][2]*2)
          self.boxes.extend([box_x1,box_y1,box_x2,box_y2,z])
    else:
      self.boxes = [0.0]*self.box_dim*self.num_boxes
      
    if self.box_num is None or self.box_info is None:
      self.z_min = 0
    else:
      zs = []
      zs.append(self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2])
      if self.box_num - 1 > 0:
        zs.append(self.box_info[1][self.box_num - 1][2] + self.box_info[2][self.box_num - 1][2])
      if self.box_num + 1 < len(self.box_info[0]) - 1:
        zs.append(self.box_info[1][self.box_num + 1][2] + self.box_info[2][self.box_num + 1][2])
      self.z_min = min(zs)
      if self.z_min < 0.1:
        zs.remove(self.z_min)
        self.z_min = min(zs)
  
    if self.multi:
      self.artifact_detection()

  def artifact_detection(self):
    '''
    Look at upcoming terrain. Identify: Jumps, gaps, stairs, base, zero, and xyyaw position of region of attraction. Put in the map if render.
    '''
    # self.switch = False
    x,y,z = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
    self.artifact = 'base'
    if self.box_num < 3:
      x,y,z = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
    if (self.box_num > len(self.box_info[0]) - 5):
      self.artifact = 'zero'
      x,y,z = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
    else:
      prev_z = self.box_info[1][self.box_num - 1][2]
      for box in self.box_nums:
        if (self.box_num + box > len(self.box_info[0]) - 1):
          continue
        if self.box_info[1][self.box_num+box][2] < 0.2:
          self.artifact = 'gaps'
          x,y,z = self.box_info[1][self.box_num+box-2][0], self.box_info[1][self.box_num+box-2][1], self.box_info[1][self.box_num+box-2][2] + self.box_info[2][self.box_num+box-2][2]
          break
        elif abs(self.box_info[1][self.box_num+box][2] - prev_z) > 0.10:
          self.artifact = 'jumps'
          x,y,z = self.box_info[1][self.box_num+box-2][0], self.box_info[1][self.box_num+box-2][1], self.box_info[1][self.box_num+box-2][2] + self.box_info[2][self.box_num+box-2][2]
          break
        elif abs(self.box_info[1][self.box_num+box][2] - prev_z) > 0.05:
          self.artifact = 'stairs'
          x,y,z = self.box_info[1][self.box_num+box-2][0], self.box_info[1][self.box_num+box-2][1], self.box_info[1][self.box_num+box-2][2] + self.box_info[2][self.box_num+box-2][2]
          break
        prev_z = self.box_info[1][self.box_num + box][2]
      if self.artifact == 'base':
        x,y,z = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        self.switch = True

    self.prev_artifact = self.artifact


  def get_expert(self):
    # if self.time_of_step == 0:
    if (np.abs([self.speed, self.y_speed, self.yaw_speed]) < 0.1).all():
      self.exp_pos = copy.copy(self.still_pos)
      self.cur_time = 0
      self.ob_dict['right_foot_swing'] = 0
      self.ob_dict['left_foot_swing'] = 0
    else:
      # print(self.speed, self.time_of_step)
      # Was previously stopped, now need to select a swing foot
      if self.prev_time_of_step == 0 and self.time_of_step != 0:
        self.ob_dict['swing_foot'] = self.ob_dict['cur_swing_foot'] = np.random.randint(2)
        self.ob_dict['right_foot_swing'] = self.ob_dict['swing_foot']
        self.ob_dict['left_foot_swing'] = not self.ob_dict['right_foot_swing']

      if self.ob_dict['cur_swing_foot']:
        swing = 'right'
        stance = 'left'
      else:
        swing = 'left'
        stance = 'right'
      self.touchdown_pen = 0
      if (self.args.expert and (self.steps-self.pause_time) % self.time_of_step == 0) or (self.time_of_step > 0.0 and self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']):
      
        self.touchdown_pen = abs(self.cur_time - self.time_of_step)
        self.touchdown_dists[swing] = (self.local_foot_pos[swing][0])
        # self.touchdown_box[swing] = self.foot_on_box[swing]
        
        if len(self.touchdown_box[swing]) > 0:
          max_box = max(self.touchdown_box[swing])
          if (max_box - self.foot_on_box[swing]) != 2:
            self.single_step = True
          else:
            self.single_step = False

        if self.foot_on_box[swing] not in self.touchdown_box[stance]:
          if len(self.touchdown_box[swing]) > 0 and abs(max_box - self.foot_on_box[swing]) > 1:
            self.touchdown_box[swing].add(self.foot_on_box[swing])
          elif len(self.touchdown_box[swing]) < 1:
            self.touchdown_box[swing].add(self.foot_on_box[swing])
          self.double_feet = False
        else:
          self.double_feet = True

        self.cur_time = 0
        self.ob_dict['cur_swing_foot'] = not self.ob_dict['cur_swing_foot']
        self.step_count += 1
        
        if self.box_info is not None:
          if self.ob_dict['cur_swing_foot']:
            swing = 'right'
            stance = 'left'
          else:
            swing = 'left'
            stance = 'right'

          if self.foot_on_box[stance] + 1 < len(self.box_info[1]):
            self.desired_box[swing] = self.foot_on_box[stance] + 1
     
      self.ob_dict['swing_foot'] = self.ob_dict['cur_swing_foot']
      if self.ob_dict['swing_foot']:
        self.ob_dict['right_foot_swing'] = True
      else:
        self.ob_dict['right_foot_swing'] = False
      self.ob_dict['left_foot_swing'] = not self.ob_dict['right_foot_swing']
    
      # if self.args.expert and self.box_info is not None:
      #   self.box_info[2][self.box_num][2] = 0.01
      #   self.box_info[2][self.box_num+1][2] = 0.0

      step_type = 'flat'
      step_fact = 0
      if self.steps - self.pause_time < self.time_of_step:
        stance_hip = self.hip_y_total_angle/4
        swing_hip  = -self.hip_y_total_angle/4
        if self.cur_time < 2*self.time_of_step/4:
          swing_knee = -0.4
        else:
          swing_knee = -0.2
        stance_knee = -0.2
      else:
        if step_type == 'flat':
          stance_hip = 2*self.hip_y_total_angle/5 
          swing_hip  = -3*self.hip_y_total_angle/5 
          if self.cur_time < 2*self.time_of_step/4:
            swing_knee = -1.3
          else:
            swing_knee = -0.2
          stance_knee = -0.2
          # print(stance_hip, swing_hip, swing_knee)
        elif step_type == 'up':
          stance_hip = 2*self.hip_y_total_angle/5 
          swing_hip  = -3*self.hip_y_total_angle/5 - step_fact*8
          if self.cur_time < 2*self.time_of_step/4:
            swing_knee = -1.3 - step_fact*2*7.5
          else:
            swing_knee = -0.2 - step_fact*2*7.5
          stance_knee = -0.2
        elif step_type == 'down':
          stance_hip = 2*self.hip_y_total_angle/5 
          swing_hip  = -3*self.hip_y_total_angle/5 
          if self.cur_time < 2*self.time_of_step/4:
            swing_knee = -1.3
            stance_knee = -0.2
          else:
            swing_knee = -0.2
            stance_knee = -0.2 - step_fact*2*7.5
      
      delta = 0.015*(1.0*abs(self.speed)) + 0.1*step_fact
      knee_delta = 0.04*(1.0* abs(self.speed))+ 0.1*step_fact
      
      # print(self.speed)

      stance_hip_x = 0.05
      take_off_ankle_y = 0.3
      if self.speed < 0:
        if swing == 'right':
          # Swing
          self.exp_pos[2] = min(self.exp_pos[2] + delta, -swing_hip)
          if self.cur_time < 2*self.time_of_step/4:
            self.exp_pos[3] = max(self.exp_pos[3] - knee_delta, swing_knee)
          else:
            self.exp_pos[3] = min(self.exp_pos[3] + knee_delta, swing_knee)
          # Stance
          self.exp_pos[8] = max(self.exp_pos[8] - delta, -stance_hip)
          self.exp_pos[9] = min(self.exp_pos[9] + knee_delta, stance_knee)
        else:
          # Swing
          self.exp_pos[8] = min(self.exp_pos[8] + delta, -swing_hip)
          if self.cur_time < 2*self.time_of_step/4:
            self.exp_pos[9] = max(self.exp_pos[9] - knee_delta, swing_knee)
          else:
            self.exp_pos[9] = min(self.exp_pos[9] + knee_delta, swing_knee)
          # Stance
          self.exp_pos[2] = max(self.exp_pos[2] - delta, -stance_hip)
          self.exp_pos[3] = min(self.exp_pos[3] + knee_delta, stance_knee)
      elif self.speed == 0 and (self.yaw_speed or self.y_speed):
        y_dt = 40*0.02/self.time_of_step
        if swing == 'right':
          if self.cur_time < 2*self.time_of_step/4:
            self.exp_pos[2] = max(self.exp_pos[2] - y_dt, -0.3)
            self.exp_pos[3] = max(self.exp_pos[3] - y_dt, -0.7)
          else:
            self.exp_pos[2] = min(self.exp_pos[2] + y_dt, -0.1)
            self.exp_pos[3] = min(self.exp_pos[3] + y_dt, -0.2)
        else:
          if self.cur_time < 2*self.time_of_step/4:
            self.exp_pos[8] = max(self.exp_pos[8] - y_dt, -0.3)
            self.exp_pos[9] = max(self.exp_pos[9] - y_dt, -0.6)
          else:
            self.exp_pos[8] = min(self.exp_pos[8] + y_dt, -0.1)
            self.exp_pos[9] = min(self.exp_pos[9] + y_dt, -0.2)

      else:
        if swing == 'right':
          # Swing
          self.exp_pos[2] = max(self.exp_pos[2] - delta, swing_hip)
          if self.cur_time < 2*self.time_of_step/4:
            self.exp_pos[3] = max(self.exp_pos[3] - knee_delta, swing_knee)
          else:
            self.exp_pos[3] = min(self.exp_pos[3] + knee_delta, swing_knee)
          # Stance
          self.exp_pos[8] = min(self.exp_pos[8] + delta, stance_hip)
          self.exp_pos[9] = min(self.exp_pos[9] + knee_delta, stance_knee)
        else:
          # Swing
          self.exp_pos[8] = max(self.exp_pos[8] - delta, swing_hip)
          if self.cur_time < 2*self.time_of_step/4:
            self.exp_pos[9] = max(self.exp_pos[9] - knee_delta, swing_knee)
          else:
            self.exp_pos[9] = min(self.exp_pos[9] + knee_delta, swing_knee)
          # Stance
          self.exp_pos[2] = min(self.exp_pos[2] + delta, stance_hip)
          self.exp_pos[3] = min(self.exp_pos[3] + knee_delta, stance_knee)

      # right z
      if self.yaw_speed == 0:
        self.exp_pos[0] = self.still_pos[0]
        self.exp_pos[6] = self.still_pos[6]
      else:
        z_dt = 40*0.02/self.time_of_step
        if swing == 'right':
          if self.yaw_speed < 0:
            self.exp_pos[0] = max(self.exp_pos[0] - z_dt, -self.hip_z_total_angle)
            self.exp_pos[6] = max(self.exp_pos[6] - z_dt, -self.hip_z_total_angle)
          else:
            self.exp_pos[0] = min(self.exp_pos[0] + z_dt, self.hip_z_total_angle)
            self.exp_pos[6] = min(self.exp_pos[6] + z_dt, self.hip_z_total_angle)
        else:
          if self.yaw_speed < 0:
            self.exp_pos[0] = min(self.exp_pos[0] + z_dt, self.hip_z_total_angle)
            self.exp_pos[6] = min(self.exp_pos[6] + z_dt, self.hip_z_total_angle)
          else:
            self.exp_pos[0] = max(self.exp_pos[0] - z_dt, -self.hip_z_total_angle)
            self.exp_pos[6] = max(self.exp_pos[6] - z_dt, -self.hip_z_total_angle)
          
      if self.y_speed == 0:
        self.exp_pos[1] = self.still_pos[1]
        self.exp_pos[7] = self.still_pos[7]
      else:
        x_dt = 120*0.002/self.time_of_step
        if swing == 'right':
          if self.y_speed < 0:            
            self.exp_pos[1] = max(self.exp_pos[1] - x_dt, -self.hip_x_total_angle*4)
            self.exp_pos[7] = max(self.exp_pos[7] - x_dt, -self.hip_x_total_angle*4)
          else:            
            self.exp_pos[1] = min(self.exp_pos[1] + x_dt, self.hip_x_total_angle)
            self.exp_pos[7] = min(self.exp_pos[7] + x_dt, self.hip_x_total_angle)
        else:
          if self.y_speed < 0:            
            self.exp_pos[1] = min(self.exp_pos[1] + x_dt, self.hip_x_total_angle)
            self.exp_pos[7] = min(self.exp_pos[7] + x_dt, self.hip_x_total_angle)
          else:            
            self.exp_pos[1] = max(self.exp_pos[1] - x_dt, -self.hip_x_total_angle*4)
            self.exp_pos[7] = max(self.exp_pos[7] - x_dt, -self.hip_x_total_angle*4)
            
      # Ankles
      self.exp_pos[4] = self.exp_pos[1]
      self.exp_pos[5] = self.still_pos[5] - self.exp_pos[2]/2

      self.exp_pos[10] = self.exp_pos[7]
      self.exp_pos[11] = self.still_pos[11] - self.exp_pos[8]/2

      self.cur_time += 1
      # print(self.speed, self.exp_pos[2], self.exp_pos[8])
 

  def apply_forces(self, actions=None):
    if self.obstacle_type == 'turn':
      z_force = 1.5*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
      # z_force = 0
      x_force = 1.0*(self.Kp *(self.initial_x - self.body_xyz[0]) + self.Kp*(0.0 - self.vx))
      y_force = 1.0*(self.Kp *(self.initial_y - self.body_xyz[1]) + self.Kp*(0.0 - self.vy))
      p.applyExternalForce(self.Id,-1,[x_force,y_force,z_force],[0,0,0],p.LINK_FRAME)
      # print(self.yaw_speed, self.target_yaw, self.time_of_step)
      # yaw_force = 0.1*self.Kp*(self.yaw_speed - self.yaw_vel) + 0.1*self.Kp*(self.target_yaw - self.yaw)
      yaw_force = 0.1*self.Kp*(self.yaw_speed - self.yaw_vel) 
      roll_force = -0.1*self.Kp * self.roll    - 0.1*self.Kp*self.roll_vel
      pitch_force = -0.1*self.Kp * self.pitch  - 0.1*self.Kp*self.pitch_vel
      # roll_force = 0.1*self.Kp * self.roll    
      # pitch_force =-0.1*self.Kp * self.pitch  
      roll_force = pitch_force = 0
      # print(self.yaw_vel, roll_force, pitch_force, yaw_force)
      # print(self.roll, self.pitch)
      p.applyExternalTorque(self.Id,-1,[roll_force,pitch_force,yaw_force],p.LINK_FRAME)
    else:
      if self.speed < 0.3:
        z_force = 1.5*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
        x_force = 1.0*self.Kp * (self.speed - self.vx)
        y_force = 1.0*self.Kp*(0.0 - self.body_xyz[1]) + self.Kp*(0.0 - self.vy)
      else:
        z_force = 1.5*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
        x_force = 1.0*self.Kp * (self.speed - self.vx)
        # z_force = 1.0*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
        # x_force = 0.05*self.Kp * (self.speed - self.vx)
        y_force = 1.0*(self.Kp *(0.0 - self.body_xyz[1]) + self.Kp*(0.0 - self.vy))
      # print(x_force)
      p.applyExternalForce(self.Id,-1,[x_force,y_force,z_force],[0,0,0],p.LINK_FRAME)

      if self.speed < 0.3:
        yaw_force = 0.1*self.Kp*(self.yaw_speed - self.yaw_vel)
        roll_force = -0.5*self.Kp * self.roll    - 0.1*self.Kp*self.roll_vel
        pitch_force = -1.0*self.Kp * self.pitch  - 0.1*self.Kp*self.pitch_vel
      else:
        yaw_force = 1.0*self.Kp*(0.0 - self.yaw) - 0.1*self.Kp*self.yaw_vel
        roll_force = -1.0*self.Kp * self.roll    - 0.1*self.Kp*self.roll_vel
        pitch_force = -2.0*self.Kp * self.pitch  - 0.1*self.Kp*self.pitch_vel
      p.applyExternalTorque(self.Id,-1,[roll_force,pitch_force,yaw_force],p.LINK_FRAME)
    
    # if self.obstacle_type == 'jumps':
    #   if self.box_num + 1 < len(self.box_info[0]):
    #     if self.box_info[self.box]
    return (self.Kp/self.initial_Kp)*(80*(self.exp_pos - np.array(self.joints)) + 0.5*(np.zeros(self.ac_size) - np.array(self.joint_vel)))
    # return (self.Kp/self.initial_Kp)*(100*(self.exp_pos - np.array(self.joints)) + 0.0*(np.zeros(self.ac_size) - np.array(self.joint_vel)))


  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self, PATH=None):
    if self.rank == 0:
      if PATH is not None:
        path = PATH
      else:
        path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))     
        try:
          if self.box_info is not None:
            np.save(path + 'box_info.npy', np.array(self.box_info))
        except Exception as e:
          print("trying to save box info", e)
        self.sim_data = []
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)

  def to_grid(self, y, x):
    x = int(x/self.grid_size +(-self.min_x+1)/self.grid_size)
    y = int(y/self.grid_size + (-self.min_y+1)/self.grid_size)
    return (y, x) 

  def project_to_segment(self, p, v, w):
    l2 = (v[0] - w[0])**2 + (v[1] - w[1])**2
    if l2 == 0: 
      return v[0], v[1]
    t = ((p[0] - v[0]) * (w[0] - v[0]) + (p[1] - v[1]) * (w[1] - v[1])) / l2
    t = max(0, min(1, t))
    return [v[0] + t * (w[0] - v[0]), v[1] + t * (w[1] - v[1])] 

  def dist_to_segment(self, p, v, w):
    p_on_line = self.project_to_segment(p, v, w)
    return np.sqrt((p_on_line[0] - p[0])**2 + (p_on_line[1] - p[1])**2)

  def global_to_local_2d(self, robot_pos, robot_yaw, global_point):
    x1 = global_point[0] - robot_pos[0]
    y1 = global_point[1] - robot_pos[1]
    new_x_point = np.cos(-robot_yaw) * x1 - np.sin(-robot_yaw) * y1
    new_y_point = np.cos(-robot_yaw) * y1 + np.sin(-robot_yaw) * x1
    return new_x_point, new_y_point

  def transform_rot(self, yaw, pos, point):
    # rot_mat = np.array(
    # 	[[np.cos(-yaw), -np.sin(-yaw), 0],
    # 	 [np.sin(-yaw), np.cos(-yaw), 0],
    # 	 [		0,			 0, 1]] )
    rot_mat = np.array(
      [[np.cos(-yaw), np.sin(-yaw), 0],
        [-np.sin(-yaw), np.cos(-yaw), 0],
        [		0,			 0, 1]] )
    x,y,z = np.dot(rot_mat,[point[0],point[1],point[2]])
    return pos[0] + x, pos[1] + y, pos[2] + z
  
  def get_z_offset(self):
    # if self.world_map is not None:
    x,y,yaw = np.nan_to_num(self.body_xyz[0]/self.grid_size), np.nan_to_num(self.body_xyz[1]/self.grid_size), np.nan_to_num(self.yaw)
    # # Calculating z offset from box numbers now
    try:
      x_pix = int(x+(-self.min_x+1)/self.grid_size)
      y_pix = int(y+(-self.min_y+1)/self.grid_size)
      ordered_pix = np.sort(self.world_map[x_pix-10:x_pix+10, y_pix-10:y_pix+10], axis=None)
      self.z_offset = max(ordered_pix[-1], 0.5)
    except Exception as e:
      # print("get_z_offset exception", e)
      self.z_offset = 0.0

  def get_hm(self, im_size=[60,40,1]):
    x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.yaw
    if self.world_map is not None:

      # t1 = time.time()
      hm_pts = self.transform_rot_and_add(1*yaw, [x+(-self.min_x+1)/self.grid_size, y+(-self.min_y+1)/self.grid_size], self.ij)
      hm_pts[:,0] = np.clip(hm_pts[:,0], 0, self.world_map.shape[0]-1)
      hm_pts[:,1] = np.clip(hm_pts[:,1], 0, self.world_map.shape[1]-1)
      self.hm[self.hm_ij[:,0],self.hm_ij[:,1]] = self.world_map[hm_pts[:,0],hm_pts[:,1]]
      # Clip to -1, 1 meters
      # print(self.z_offset)
      
      # Constant height
      # hm = np.clip(self.hm - self.z_offset, -1, 1).reshape(im_size)
      # hm = (hm+1)/2

      # White is lowest
      # hm = np.clip(self.body_xyz[2] - self.hm, 0, 2).reshape(im_size)
      # hm = (hm)/2

      # Black is lowest
      hm = np.clip(self.hm-self.body_xyz[2], -2, 0).reshape(im_size)
      hm = (hm+2)/2
    
    else:
      print("No world map")
      hm = np.zeros(im_size)
    return hm

  def get_rgbd(self, cam_pos='lower'):
    camTargetPos = [0.,0.,0.]
    cameraUp = [0,0,1]
    cameraPos = [1,1,1]
    yaw = 40
    pitch = 10.0

    roll=0
    upAxisIndex = 2
    camDistance = 4
    # new ===================
    pixelWidth = self.im_size[0]
    pixelHeight = self.im_size[1]
    pos = self.body_xyz
    roll, pitch, yaw = self.roll, self.pitch, self.yaw
    # pos, orn = p.getBasePositionAndOrientation(self.Id)
    # roll, pitch, yaw = p.getEulerFromQuaternion(orn)
    if cam_pos == 'lower':
      dx = 0.6
      dy = 0
      dz = -1
      # cam_dx = 0.2
      cam_dx = 0.1
      cam_dy = 0
      cam_dz = 0.2
      fov = 60

    elif cam_pos == 'upper':
      dx = 3.0
      dy = 0
      dz = -1
      cam_dx = 0.0
      cam_dy = 0
      cam_dz = 0.4
      fov = 80

    lightDirection = [pos[0]+cam_dx,pos[1],pos[2]]
    shadow = 1
    
    lightColor = [1,1,1]#optional argument

    p_x, p_y, p_z = self.transform_rot(yaw, pos, [dx,dy,dz])
    cam_x, cam_y, cam_z = self.transform_rot(yaw, pos, [cam_dx, cam_dy, cam_dz])
    nearPlane = 0.25
    farPlane = 2
    aspect = 1
    
    projectionMatrix = p.computeProjectionMatrixFOV(fov, aspect, nearPlane, farPlane)
    viewMatrix = p.computeViewMatrix([cam_x,cam_y,cam_z], [p_x, p_y, p_z], cameraUp)
    try:
      image = p.getCameraImage(pixelWidth, pixelHeight, viewMatrix, projectionMatrix=projectionMatrix, lightDirection=lightDirection,lightColor=lightColor, shadow=shadow, renderer = p.ER_TINY_RENDERER)
      
      depth = copy.deepcopy(image[3]).reshape([pixelHeight, pixelWidth, 1])
      if self.vis_type == 'depth':
        # return np.nan_to_num(depth)
        # print(depth[:100,:100,:])
        # cv2.imshow("depth", depth)
        # cv2.waitKey(1)
        return depth

      rgb = np.array(copy.deepcopy(image[2])).reshape([pixelHeight, pixelWidth, 4])[:,:,:3]/255.0
      # print(rgb.shape, depth.shape)
      # if down_stairs, block off 75% of the image. It can see too far, so if any changes far in the image, the policy is affected
      # if self.down_stairs:
      #     if random.random() < 0.2:
      #         rgb[:int(48*0.3),:,:] = 0
      #         depth[:int(48*0.3),:] = 0
      # rgb[:,:,:] = 0
      # depth[:,:,:] = 0
      # if cam_pos == 'upper':
      # re_im = rgb*255.0
      # cv2.imshow('robot_vision2',re_im.astype(dtype=np.uint8)[...,::-1])
      # cv2.imshow('robot_vision2',re_im.astype(dtype=np.uint8)[...,::-1])
      # cv2.waitKey(1)
      return np.nan_to_num(np.concatenate((rgb,depth), axis=2))

    except Exception as e:
        print("get_rgb exception", e)
        return np.zeros([pixelWidth, pixelHeight, 4])


  def get_im(self):
    self.get_z_offset()
    # print(self.vis_type)
    if self.vis_type == 'hm':
      self.im = self.get_hm()
    elif self.vis_type == 'state':
      self.im = np.array(self.boxes).reshape(self.im_size)
    elif self.vis_type in ['rgbd','depth']:
      if self.steps % self.args.camera_rate == 0:
        self.im = self.get_rgbd()
    if self.display_im:
      self.display()        
    return self.im

  def display(self):
    cv2.imshow("image", self.im)
    cv2.waitKey(1)

  def get_world_map(self, box_info=None):
    gz = False
    if gz:
      self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0]/2)
      self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1]/2)
      self.max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0]/2)
      self.max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1]/2)
    else:
      self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0])
      self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1])
      self.max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0])
      self.max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1])
    world_shape = [int((self.max_x - self.min_x + 2)/self.grid_size), int((self.max_y - self.min_y + 2)/self.grid_size)]

    hm = np.ones([world_shape[0], world_shape[1]]).astype(np.float32)*-1
    if box_info is not None:
      positions = box_info[1]
      sizes = box_info[2]
      for pos, size in zip(positions, sizes):
        x, y, z = pos[0]/self.grid_size, pos[1]/self.grid_size, pos[2]
        if gz:
          x_size, y_size, z_size = (size[0]/2)/self.grid_size, (size[1]/2)/self.grid_size, size[2]/2
        else:
          x_size, y_size, z_size = (size[0])/self.grid_size, (size[1])/self.grid_size, size[2]
        # print(x_size, y_size)
        ij = np.array([[i,j,1] for i in range(int(0-x_size), int(0+x_size)) for j in range(int(0-y_size), int(0+y_size))])

        hm_pts = self.transform_rot_and_add(1*pos[5], [x+(-self.min_x+1)/self.grid_size, y+(-self.min_y+1)/self.grid_size], ij)
        for pt in hm_pts:
          try:
            # Not sure why this doesn't work for larger z heights? Should be box z pos + box z size (half extent)
            # if hm[pt[0], pt[1]] < (z + z_size):
            #   hm[pt[0], pt[1]] = (z + z_size)
            if hm[pt[0], pt[1]] < (z * 2):
              hm[pt[0], pt[1]] = (z * 2)
          except Exception as e:
            print(e)
            print(pt)
    
            
    self.dx_forward, self.dx_back, self.dy = 36, 24, 20        
    self.ij = np.array([[i,j,1] for i in range(-self.dx_back, self.dx_forward) for j in range(-self.dy, self.dy)])
    self.hm_ij = np.array([[i,j] for i in range(0, self.dx_back + self.dx_forward) for j in range(0, self.dy + self.dy)])
    self.hm = np.ones([self.dx_back+self.dx_forward, 2*self.dy], dtype=np.float32)*-1
    # print("in world map", self.hm.shape)
    return hm

  def transform_rot_and_add(self, yaw, pos, points):
    rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw), pos[0]],
            [np.sin(yaw), np.cos(yaw), pos[1]],
            [0, 0, 1]])
    return np.dot(rot_mat,points.T).T.astype(np.int32)

  def add_disturbance(self, max_dist=None):
    force_x = (random.random() - 0.5)*max_dist
    force_y = (random.random() - 0.5)*max_dist
    force_z = (random.random() - 0.5)*(max_dist/4)
    # print("applying forces", force_x, force_y, force_z)
    p.applyExternalForce(self.Id,-1,[force_x,force_y,force_z],[0,0,0],p.LINK_FRAME)
    force_roll = (random.random() - 0.5)*max_dist/2
    force_pitch = (random.random() - 0.5)*max_dist/2
    force_yaw = (random.random() - 0.5)*(max_dist/2)
    p.applyExternalTorque(self.Id,-1,[force_roll,force_pitch,force_yaw],p.LINK_FRAME)

  def log_stuff(self, logger, writer, iters_so_far):
    self.iters_so_far = iters_so_far
    writer.add_scalar("breakdown/goal", np.mean(self.reward_breakdown['goal']), self.iters_so_far)        
    writer.add_scalar("breakdown/pos", np.mean(self.reward_breakdown['pos']), self.iters_so_far)        
    writer.add_scalar("breakdown/vel", np.mean(self.reward_breakdown['vel']), self.iters_so_far)   
    writer.add_scalar("breakdown/tip", np.mean(self.reward_breakdown['tip']), self.iters_so_far)   
    writer.add_scalar("breakdown/com", np.mean(self.reward_breakdown['com']), self.iters_so_far)   
    writer.add_scalar("breakdown/neg", np.mean(self.reward_breakdown['neg']), self.iters_so_far)   
    writer.add_scalar("breakdown/sym", np.mean(self.reward_breakdown['sym']), self.iters_so_far)   
    writer.add_scalar("breakdown/act", np.mean(self.reward_breakdown['act']), self.iters_so_far)   
    writer.add_scalar("difficulty", self.difficulty, self.iters_so_far)   
    writer.add_scalar("height", self.height_coeff, self.iters_so_far)   

    writer.add_scalar("Kp", self.Kp, self.iters_so_far)
    logger.record_tabular("Kp", self.Kp)
    logger.record_tabular("max yaw", self.max_yaw)
    logger.record_tabular("pos error", np.mean(self.pos_error))
    # if self.obstacle_type == 'path':
    logger.record_tabular("difficulty", self.difficulty)
    # elif self.obstacle_type == 'stairs':
    logger.record_tabular("height_coeff", self.height_coeff)
    
    writer.add_scalar("touchdown/left", np.mean(self.touchdown_pens[0]), self.iters_so_far)   
    writer.add_scalar("touchdown/right", np.mean(self.touchdown_pens[1]), self.iters_so_far)   

    writer.add_scalar("touchdown_dists/left", np.mean(self.touchdown_dists['left']), self.iters_so_far)   
    writer.add_scalar("touchdown_dists/right", np.mean(self.touchdown_dists['right']), self.iters_so_far)   

    logger.record_tabular("touchdown_left", np.mean(self.touchdown_pens[0]))
    logger.record_tabular("touchdown_right", np.mean(self.touchdown_pens[1]))
    
    logger.record_tabular("touchdown_dists_left", np.mean(self.touchdown_dists['left']))
    logger.record_tabular("touchdown_dists_right", np.mean(self.touchdown_dists['right']))
    
    writer.add_scalar("max_disturbance", self.max_disturbance, self.iters_so_far)   
    logger.record_tabular("max_disturbance", self.max_disturbance)
