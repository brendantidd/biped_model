import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import numpy as np
import pybullet as p
from collections import deque
from pathlib import Path
home = str(Path.home())
from mpi4py import MPI
comm = MPI.COMM_WORLD
import time
import cv2
import random

class Env():
  rank = comm.Get_rank()
  # Sim preferes 240Hz
  # simStep = 1/240
  simStep = 1/120
  # Mocap rate is 120Hz
  timeStep = 1/120
  ob_size = (12*2+8 + 6 + 6 + 2 + 2 + 1)
  im_size = [60,40,1]
  ac_size = 12
  total_steps = 0
  steps = 0
  Kp = 400
  initial_Kp = Kp
  Kd = 0.1
  height_coeff = 0.01
  difficulty = 1
  max_yaw = 0.0
  episodes = 0
  iters_so_far = 0
  max_disturbance = 500
  rew_Kp = 1
  max_v = 1.5
  min_v = 0.5
  max_yaw = 1.25
  # Strafe max velocity
  # max_vy = 0.75
  max_vy = 0.4

  def __init__(self, render=False, PATH=None,  args=None, record_step=True, display_im=False, cur=False, obstacle_type='flat', control_type=None, vis=False, disturbances=True, speed_cur=False):

    self.render = render
    self.PATH = PATH
    self.args = args
    self.record_step = record_step
    self.display_im = display_im
    self.cur = cur
    self.speed_cur = speed_cur
    self.obstacle_type = obstacle_type

    self.control_type = control_type
    self.vis = vis
    self.stopped_start = True
    # self.stopped_start = False
    self.disturbances = disturbances

    if not self.vis:
      # self.ob_size += 48
      self.ob_size += 5*5


    self.reward_breakdown = {'goal':deque(maxlen=100), 'pos':deque(maxlen=100), 'vel':deque(maxlen=100),  'neg':deque(maxlen=100), 'tip':deque(maxlen=100), 'com':deque(maxlen=100), 'sym':deque(maxlen=100), 'act':deque(maxlen=100)}
  
    if self.render:
      self.physicsClientId = p.connect(p.GUI)
    else:
      self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot


    self.sim_data = []
    self.exp_sim_data = []
    self.load()
    self.prev_exp_pos = 0.0
    self.prev_my_pos = 0.0

    self.world_map = None
    self.z_offset = 0
    try:
      from assets.obstacles import Obstacles
    except:
      from obstacles import Obstacles
    self.obstacles = Obstacles()
    if self.doa:
      self.obstacles.add_domain_training_world()
      self.box_info = self.obstacles.get_box_info()
      self.world_map = self.get_world_map(self.box_info) 
      self.full_buffer = []

  def load(self):
    
    p.loadMJCF(currentdir + "/ground.xml")
    # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
    objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
    # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION )
    self.Id = objs

    p.setTimeStep(self.simStep)
    p.setGravity(0,0,-9.8)
    # ======================================================================

    numJoints = p.getNumJoints(self.Id)
    # Camera following robot:
    # body_xyz, (qx, qy, qz, qw) = p.getBasePositionAndOrientation(self.Id)
    # p.resetDebugVisualizerCamera(2.0, self.camera_rotation, -10.0, body_xyz)
    # p.resetDebugVisualizerCamera(2.0, 50, -10.0, body_xyz)

    self.jdict = {}
    self.feet_dict = {}
    self.leg_dict = {}
    self.body_dict = {}
    self.feet = ["left_heel1", "left_heel2", "right_heel1", "right_heel2", "left_toe", "right_toe"]
    self.feet_contact = {f:True for f in self.feet}
    self.ordered_joints = []
    self.ordered_joint_indices = []

    for j in range( p.getNumJoints(self.Id) ):
      info = p.getJointInfo(self.Id, j)
      link_name = info[12].decode("ascii")
      if link_name=="left_heel1": self.feet_dict["left_heel1"] = j
      if link_name=="left_heel2": self.feet_dict["left_heel2"] = j
      if link_name=="right_heel1": self.feet_dict["right_heel1"] = j
      if link_name=="right_heel2": self.feet_dict["right_heel2"] = j
      if link_name=="left_toe": self.feet_dict["left_toe"] = j
      if link_name=="right_toe": self.feet_dict["right_toe"] = j
      # print(link_name)
      if link_name=="base_link": self.body_dict["body_link"] = j
      self.ordered_joint_indices.append(j)
      if info[2] != p.JOINT_REVOLUTE: continue
      jname = info[1].decode("ascii")
      lower, upper = (info[8], info[9])
      self.ordered_joints.append( (j, lower, upper) )
      self.jdict[jname] = j
    
    self.motor_names = ["right_hip_z", "right_hip_x", "right_hip_y", "right_knee"]
    self.motor_names += ["right_ankle_x", "right_ankle_y"]
    self.motor_names += ["left_hip_z", "left_hip_x", "left_hip_y", "left_knee"]
    self.motor_names += ["left_ankle_x", "left_ankle_y"]
    self.motor_power = [300, 300, 900, 600]
    self.motor_power += [150, 300]
    self.motor_power += [300, 300, 900, 600]
    self.motor_power += [150, 300]
       
    self.motors = [self.jdict[n] for n in self.motor_names]
    
    forces = np.ones(len(self.motors))*240
    self.actions = {key:0.0 for key in self.motor_names}

    p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

    for key in self.feet_dict:
      p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9)

    self.ep_lens = deque(maxlen=5)
    self.pos_error = deque(maxlen=5)
    self.pos_errors = [0.0]

  def reset(self, params=None):
    if self.episodes > 0 and self.record_step and not self.doa:
      self.save_sim_data()
    else:
      self.sim_data = []
      self.exp_sim_data = []
    
    self.foot_box = {'right':1, 'left':1}

    if self.episodes > 0 and self.args.display_doa and self.rank == 0:
    # if self.episodes > 0 and self.rank == 0:
      # if not self.args.test_pol:
      #   self.episode_buffer = np.array(self.episode_buffer)
      #   self.episode_buffer[-30:,-1] = 0.5
      #   self.episode_buffer[-1,-1] = 1
      # print(self.episode_buffer)
      self.full_buffer = list(self.episode_buffer)
      # self.full_buffer.extend(list(self.episode_buffer))
      # print(len(self.full_buffer))
      # self.update_display()
      t1 = time.time()
      for status in self.full_buffer:
        
        y, x = int(status[1]/self.grid_size + (-self.min_y+1)/self.grid_size), int(status[0]/self.grid_size +(-self.min_x+1)/self.grid_size)

        # y2, x2 = 0.25*np.sin(status[2]) + status[1], 0.25*np.cos(status[2]) + status[0]
        # y2, x2 = int(y2/self.grid_size + (-self.min_y+1)/self.grid_size), int(x2/self.grid_size +(-self.min_x+1)/self.grid_size)
        # if not self.args.test_pol:
        if status[3] > 0.8:
          # cv2.arrowedLine(self.doa_wm_col, (y,x),(y2,x2), (0,0,255), 1)
          cv2.circle(self.doa_wm_col,(y, x), 3, (0,0,255), -1)
        else:
          # if status[0]:
          # print(int(255-status[4]*20))
          print(status[4], y, x)
          if status[4] == 0:
            # cv2.circle(self.doa_wm_col,(y, x), 1, (44, 160, 44), -1)
            # cv2.circle(self.doa_wm_col,(10, 10), 1, (255,0,0), -1)
            cv2.circle(self.doa_wm_col,(y, x), 1, (255,0,0), -1)
          elif status[4] == 1:
            # cv2.circle(self.doa_wm_col,(y, x), 1, (31, 119, 180), -1)
            cv2.circle(self.doa_wm_col,(y, x), 1, (0,255,0), -1)
          elif status[4] == 2:
            # cv2.circle(self.doa_wm_col,(y, x), 1, (174, 199, 232), -1)
            cv2.circle(self.doa_wm_col,(y, x), 1, (150, 150, 0), -1)
          elif status[4] == 3:
            # cv2.circle(self.doa_wm_col,(y, x), 1, (255, 127, 14), -1)
            # cv2.circle(self.doa_wm_col,(40, 40), 1, (100, 0, 255), -1)
            cv2.circle(self.doa_wm_col,(y, x), 1, (100, 0, 255), -1)
          # else:
            # cv2.circle(self.doa_wm_col,(y, x), 1, (0,255,0), -1)
        
        # elif status[3] == 0.5:
        #   # cv2.arrowedLine(self.doa_wm_col, (y,x),(y2,x2), (255,0,0), 1)
        #   cv2.circle(self.doa_wm_col,(y, x), 1, (255,0,0), -1)
        # elif status[3] == 0:
        #   # cv2.arrowedLine(self.doa_wm_col, (y,x),(y2,x2), (0,255,0), 1)
        #   cv2.circle(self.doa_wm_col,(y, x), 1, (0,255,0), -1)
        # else:
        #   # print(status[3])
        #   if status[3] > 0.3:
        #     cv2.circle(self.doa_wm_col,(y, x), 1, (0,0,int(255*status[3])), -1)
        #   # elif status[3] > 0.1:
        #     # cv2.circle(self.doa_wm_col,(y, x), 1, (int(255*status[3]),0,0), -1)
        #   else:
        #     cv2.circle(self.doa_wm_col,(y, x), 1, (0,int(255*status[3]),0), -1)
      # print(time.time()-t1)
      if self.args.display_doa:
        cv2.imshow('doa_map', self.doa_wm_col)
        cv2.waitKey(1)
      else:
        cv2.imwrite(self.PATH + 'doa_map.png', self.doa_wm_col)

    self.episode_buffer = []

    self.right_boxes = []
    self.left_boxes = []

    self.vel_buffer = deque(maxlen=100)
    self.initial_xyz = self.exp_body_xyz = np.array([0.0,0.0,0.0])
  
    self.ep_lens.append(self.steps)
    # print(np.mean(self.pos_errors))
    self.pos_error.append(np.mean(self.pos_errors))
    self.pos_errors = []
    self.box_num = 0

    if self.args.test_pol:
      self.height_coeff = 0.07
      self.difficulty = 7

    if not self.doa:
      if self.obstacle_type in ['up_stairs', 'down_stairs', 'stairs']:
        # print("thing")
        # self.height_coeff = 0.07; self.difficulty = 7
        if not self.cur and len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 8).all() and self.steps != 0:
          self.ep_lens = deque(maxlen=5)
          if self.height_coeff < 0.07:
            self.height_coeff += 0.01
          if self.difficulty < 7:
            self.difficulty += 1
        self.obstacles.remove_obstacles()
        # self.difficulty = 7
        if self.obstacle_type == 'stairs':
          self.obstacles.add_curvey_stairs(order=['flat']*3 + [np.random.choice(['up','down'],p=[0.5,0.5]) for _ in range(47)], height_coeff=self.height_coeff, 
          difficulty=self.difficulty)
        elif self.obstacle_type == 'up_stairs':
          self.obstacles.add_curvey_stairs(order=['flat']*3 + ['up' for _ in range(47)], height_coeff=self.height_coeff, 
          difficulty=self.difficulty)
        elif self.obstacle_type == 'down_stairs':
          self.obstacles.add_curvey_stairs(order=['flat']*3 + ['down' for _ in range(47)], height_coeff=self.height_coeff, 
          difficulty=self.difficulty)
        self.box_info = self.obstacles.get_box_info()
        self.world_map = self.get_world_map(self.box_info)    
      
      elif self.obstacle_type == 'path':
        if self.difficulty < 7 and not self.cur and len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 8).all() and self.steps != 0:
          self.ep_lens = deque(maxlen=5)
          self.difficulty += 1
        self.obstacles.remove_obstacles()
        self.obstacles.add_curves(difficulty=self.difficulty)
        self.box_info = self.obstacles.get_box_info()
        self.world_map = self.get_world_map(self.box_info)    
      
      elif self.obstacle_type == 'gaps':
        if self.difficulty < 7 and not self.cur and len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 8).all() and self.steps != 0:
          self.ep_lens = deque(maxlen=5)
          self.difficulty += 1
        self.obstacles.remove_obstacles()
        self.obstacles.add_gaps(difficulty=self.difficulty)
        self.box_info = self.obstacles.get_box_info()
        self.world_map = self.get_world_map(self.box_info)    
      
      elif self.obstacle_type == 'stop_turn':
        if self.difficulty < 7 and not self.cur and len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 8).all() and self.steps != 0:
          self.ep_lens = deque(maxlen=5)
          self.difficulty += 1
          if self.height_coeff < 0.07:
            self.height_coeff += 0.01
        self.obstacles.remove_obstacles()
        # if not self.cur:
        self.obstacles.add_random(difficulty=self.difficulty, height_coeff=self.height_coeff)
        # else:
          # self.obstacles.add_base()
        self.box_info = self.obstacles.get_box_info()
        self.world_map = self.get_world_map(self.box_info)  

      elif self.obstacle_type == 'jumps':
        if self.difficulty < 7 and not self.cur and len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 8).all() and self.steps != 0:
          self.ep_lens = deque(maxlen=5)
          self.difficulty += 1
        # self.difficulty = 7
        self.obstacles.remove_obstacles()
        self.obstacles.add_jumps(difficulty=self.difficulty)
        self.box_info = self.obstacles.get_box_info()
        self.world_map = self.get_world_map(self.box_info)  

      elif self.obstacle_type == 'base' or self.obstacle_type == 'run':
        self.obstacles.remove_obstacles()
        self.obstacles.add_base(self.obstacle_type)
        self.box_info = self.obstacles.get_box_info()
        self.world_map = self.get_world_map(self.box_info)  

      elif self.obstacle_type == 'domain_train' and self.episodes == 0:
          self.obstacles.add_domain_training_world()
          self.box_info = self.obstacles.get_box_info()
          self.world_map = self.get_world_map(self.box_info) 
    
    # print(np.mean(self.pos_error))
    if self.cur:
      if len(self.ep_lens) == 5 and (np.mean(self.ep_lens)*self.timeStep) > 2 and self.steps != 0:
      # if len(self.ep_lens) == 5 and (np.mean(self.ep_lens)*self.timeStep) > 3 and self.steps != 0:
      # if len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 3).all() and self.steps != 0 and (np.array(self.pos_error) > 0.4).all():
      # if len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 3).all() and self.steps != 0 and np.mean(self.pos_error) > 0.6:
      # if len(self.ep_lens) == 5 and self.steps != 0 and np.mean(self.pos_error) > 0.6:
      # if len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 3).all() and self.steps != 0 and np.mean(self.pos_error) > 0.4:
      # if len(self.ep_lens) == 5 and ((np.array(self.ep_lens)*self.timeStep) > 3).all() and self.steps != 0 and np.mean(self.pos_error) > 0.3:
        self.ep_lens = deque(maxlen=5)
        self.pos_error = deque(maxlen=5)
        self.Kp = self.Kp*0.75
        self.Kd = self.Kd*0.75
        if self.Kp < 10:
          self.Kp = self.Kd = 0
          self.cur = False
          # self.obstacle_type = self.original_obstacle_type
          
    self.initial_xyz = self.exp_body_xyz
    # temp = self.samples[self.sample_pointer,:]
    # self.exp_body_xyz, self.exp_body_rot, self.exp_joints, self.exp_joint_vel = temp[:3], p.getQuaternionFromEuler(temp[3:6]), temp[6:self.ac_size+6], temp[self.ac_size+6:]
    # start_rot = [0.0,0.0,np.random.uniform(-np.pi, np.pi)]
    start_rot = [0.0,0.0,0.0]
    start_orn = p.getQuaternionFromEuler(start_rot)
    # self.body_xyz, self.yaw = [self.exp_body_xyz[0], self.exp_body_xyz[1], self.exp_body_xyz[2] + 0.95 ], start_rot[2]
    self.body_xyz, self.yaw = [0.0, 0.0, 0.0 + 0.95], start_rot[2]
    self.get_hm()
    
    self.ob_dict = {'prev_right_foot_left_ground':False,'prev_left_foot_left_ground':False,'left_foot_left_ground':False,'right_foot_left_ground':False}
    self.ob_dict.update({'exp_prev_right_foot_left_ground':False,'exp_prev_left_foot_left_ground':False,'exp_left_foot_left_ground':False,'exp_right_foot_left_ground':False})
    
    self.ob_dict['prev_right_heel1'] = self.ob_dict['right_heel1'] = False
    self.ob_dict['prev_right_heel2'] = self.ob_dict['right_heel2'] = False
    self.ob_dict['prev_right_toe']   = self.ob_dict['right_toe']   = False
    self.ob_dict['prev_left_heel1']  = self.ob_dict['left_heel1']  = False
    self.ob_dict['prev_left_heel2']  = self.ob_dict['left_heel2']  = False
    self.ob_dict['prev_left_toe']    = self.ob_dict['left_toe']    = False
    
    # self.ob_dict['ready_to_walk'] = False

    self.ob_dict['right_foot_swing'] = False
    self.ob_dict['left_foot_swing'] = False
    self.first_step = True

    # if self.ob_dict['right_foot_swing']:
    #   self.cur_foot = 'right'
    #   # self.set_position([0,-0.05,0.95 + self.z_offset], start_orn, [0.0]*self.ac_size, joint_vel=[0.0]*self.ac_size)
    # else:
    #   self.cur_foot = 'left'
    #   # self.set_position([0,0.05,0.95 + self.z_offset], start_orn, [0.0]*self.ac_size, joint_vel=[0.0]*self.ac_size)
    if self.stopped_start:
      # if self.ob_dict['right_foot_swing']:
        # self.sample_pointer = 40
      # else:
        # self.sample_pointer = 110
      self.sample_pointer = np.random.choice([40,110], p=[0.5,0.5])
      # self.sample_pointer = np.random.randint(0,self.sample_size)
    # else:
    #   self.sample_pointer = np.random.randint(0,self.sample_size)

      if self.sample_pointer == 40:
        # Right swing
        self.ob_dict['swing_foot'] = True
      else:
        # Left swing
        self.ob_dict['swing_foot'] = False


    temp = self.samples[self.sample_pointer,:]
    self.exp_body_xyz, self.exp_joints, self.exp_joint_vel = temp[:3], temp[6:self.ac_size+6], temp[self.ac_size+6:]

    self.exp_body_rot =  p.getQuaternionFromEuler([temp[3], temp[4], temp[5]+np.random.uniform(-self.max_yaw, self.max_yaw)])
    # print(self.exp_body_rot)
    if self.mocap:
      self.set_position([self.exp_body_xyz[0], self.exp_body_xyz[1] + 1, self.exp_body_xyz[2] + 0.95 + self.z_offset], self.exp_body_rot, self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.exp_Id)
    # self.set_position([self.exp_body_xyz[0], self.exp_body_xyz[1], self.exp_body_xyz[2] + 0.95 + self.z_offset], self.exp_body_rot, self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.Id)
    # self.z_offset = self.box_info[1][0][2]*2
    # print(self.z_offset, self.box_info[1][0][2]+self.box_info[2][0][2], self.box_info[1][0][2],self.box_info[2][0][2])
    # pos, orn, joints, base_vel, joint_vel = [0,0,0.95 + self.z_offset],[0,0,0,1], [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
  
    if self.doa:
      self.box_num = np.random.randint(0, len(self.box_info[0]))
      box_x, box_y = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1]
      # self.body_xyz, self.yaw = [box_x, box_y + np.random.uniform(-0.2, 0.2), 0.0], self.box_info[1][self.box_num][5]
      self.body_xyz, self.yaw = [box_x, box_y, 0.0], self.box_info[1][self.box_num][5]
      # self.body_xyz, self.yaw = [box_x, box_y + np.random.uniform(-0.2, 0.2), 0.0], np.random.uniform(-np.pi, np.pi)
      self.get_hm()
      while self.z_offset < 0.75:
        self.body_xyz = [np.random.uniform(self.min_x, self.max_x), np.random.uniform(self.min_y, self.max_y), 0.0]
        self.get_hm()
      pos, orn, joints, base_vel, joint_vel = [self.body_xyz[0],self.body_xyz[1],0.95 + self.z_offset],p.getQuaternionFromEuler([0,0,self.yaw]), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
    else:
      if self.obstacle_type == 'stop_turn':
        left_or_right = np.random.choice([-1,1],p=[0.5,0.5])
        pos = [0,left_or_right*np.random.uniform(0.2,0.4),0.95 + self.z_offset]
        if left_or_right > 0:
          orn = list(p.getQuaternionFromEuler([0,0,np.random.uniform(np.pi/4, 3*np.pi/4)]))
        else:
          orn = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-3*np.pi/4, -np.pi/4)]))
        base_vel = [[(np.random.random()-0.5)/10,(np.random.random()-0.5)/10,0],[0,0,0]]    
        # base_vel = [[0.0,0.0,0],[0,0,0]]    
        joints, joint_vel = [0.]*len(self.motor_names), [0.]*len(self.motor_names)

      elif self.obstacle_type == 'gaps':
        pos = [0,0,0.95 + self.z_offset]
        orn = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-np.pi/4,np.pi/4)]))
        base_vel = [[0,0,0],[0,0,0]]    
        joints, joint_vel = [0.]*len(self.motor_names), [0.]*len(self.motor_names)

      else:
        pos = [0,0,0.95 + self.z_offset]
        orn = [0,0,0,1]
        base_vel = [[0,0,0],[0,0,0]]    
        joints, joint_vel = [0.]*len(self.motor_names), [0.]*len(self.motor_names)
      
      if self.disturbances:
        c = [0.01, 0.01, 0.01, 0.012, 0.1, 0.01, 0.01]
        pos[0] += np.random.uniform(low=0.2, high=c[0])
        pos[1] += np.random.uniform(low=-c[0], high=c[0])
        # No noise to z, can get stuck in the floor
        # pos[2] += np.random.uniform(low=-c[0], high=c[0])
        orn[0] += np.random.uniform(low=-c[2], high=c[2])
        orn[1] += np.random.uniform(low=-c[2], high=c[2])
        orn[2] += np.random.uniform(low=-c[2], high=c[2])
        # Noise on w term does funky things
        # orn[3] += np.random.uniform(low=-c[2], high=c[2])
        for i in range(len(base_vel[0])):
          base_vel[0][i] += np.random.uniform(low=-c[3], high=c[3])
          base_vel[1][i] += np.random.uniform(low=-c[4], high=c[4])

        # for i, (j, m) in enumerate(zip(self.ordered_joints, self.motor_names)):
        #   if 'ankle_y' not in m: #Don't want to at noise to ankle y, foot can end up stuck in the floor
        #     joints[i] += np.clip(np.random.uniform(low=-c[5], high=c[5]), j[1], j[2])
        #     joint_vel[i] += np.random.uniform(low=-c[6], high=c[6])

    self.set_position(pos, orn, joints, base_vel, joint_vel)

    self.prev_speed = self.speed = self.yaw_speed = 0.0
    self.total_reward = 0
    self.steps = 0
    self.episodes += 1
    self.get_observation()
    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel
    self.prev_exp_joints = [0.0]*self.ac_size

    # self.time_of_step = int(60/abs(1.0))
    self.time_of_step = int(0.0)
    self.action_store = deque(maxlen=self.time_of_step)
    

    self.pause_time = int(np.random.uniform(40,80))
    # if self.iters_so_far > 200:
    #   self.phase = 0.0
    # else:
    #   self.phase = self.sample_pointer / self.sample_size
    # self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.phase]
    # self.state = self.joints + self.joint_vel + self.body + self.contacts + self.swing_stance
    if not self.vis:
      self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.speed, 0.0] + self.boxes
    else:
      self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.speed, 0.0]
    return np.array(self.state)

  def step(self, actions):
    
    if (self.speed_cur and ((self.steps*self.timeStep) % 3) == 0 and self.steps != 0) or self.steps == self.pause_time:
      if self.obstacle_type == 'run':      
        self.speed = np.random.uniform(low=self.min_v+0.5,high=self.max_v+0.5)           
      else:
        self.speed = np.random.uniform(low=self.min_v,high=self.max_v)           

      self.yaw_speed = np.random.uniform(low=-self.max_yaw,high=self.max_yaw)           
      # if self.strafe:
      #     self.y_v = np.random.uniform(low=-self.max_vy,high=self.max_vy)          
      if abs(self.speed) >= 0.25:
        self.time_of_step = int(67/abs(self.speed))
      else:
        self.time_of_step = 0

    
    if self.obstacle_type == 'stop_turn':
      if abs(self.yaw) < 0.1:
        self.yaw_speed = 0
      elif (self.yaw < np.pi/2 and self.yaw > 0) or self.yaw <= -np.pi/2:
        self.yaw_speed = -1.5
      elif self.yaw >= np.pi/2 or (self.yaw >= -np.pi/2 and self.yaw < 0):
        self.yaw_speed = 1.5
      else:
        print("haven't accounted for a yaw", self.yaw)
        self.yaw_speed = 0
      self.speed = 0
    else:
      if self.yaw > np.pi/4 or self.yaw < -np.pi/4:
        self.yaw_speed = 0

    if self.disturbances and random.random() < 0.02:
      self.add_disturbance(self.max_disturbance)
        
    # self.phase = self.sample_pointer/self.sample_size
    
    # if (self.steps*self.timeStep) % 8 == 0:
    #   self.yaw_sign = np.random.choice([-1,1],p=[0.5,0.5])
    # if self.steps > self.pause_time or not self.stopped_start:
    # # if self.steps > 100 or not self.stopped_start:
    #   # if not self.cur and self.obstacle_type == 'stop_turn':
    #   if self.obstacle_type == 'stop_turn':
    #       if abs(self.yaw) < 0.1:
    #         self.yaw_speed = 0
    #       elif (self.yaw < np.pi/2 and self.yaw > 0) or self.yaw <= -np.pi/2:
    #         self.yaw_speed = -1.5
    #       elif self.yaw >= np.pi/2 or (self.yaw >= -np.pi/2 and self.yaw < 0):
    #         self.yaw_speed = 1.5
    #       else:
    #         print("haven't accounted for a yaw", self.yaw)
    #         self.yaw_speed = 0
    #       self.speed = 0
    #   else:
    #     if self.obstacle_type == 'run':
    #       self.speed = 1.75
    #     else:
    #       self.speed = 1.0
    #   self.time_of_step = int(self.sample_size/2.0)
    #   # self.time_of_step = int(60/abs(1.0))
    # else: 
    #   self.speed = 0.0
    #   self.time_of_step = 0
    # print(self.speed)
    # print(self.steps,self.yaw_speed)

    self.prev_state = self.joints + self.joint_vel
    self.prev_joints = self.joints 
    self.prev_joint_vel = self.joint_vel
    
    self.actions = actions
      
    if self.cur:
      exp_forces = self.apply_forces(actions)
      if self.speed > 0.3:
        exp_forces = (self.Kp/self.initial_Kp)*(80*(np.array(self.exp_joints) - np.array(self.joints)) + 0.5*(np.zeros(self.ac_size) - np.array(self.joint_vel)))
      else:
        exp_forces = (self.Kp/self.initial_Kp)*(80*(np.zeros(self.ac_size) - np.array(self.joints)) + 0.5*(np.zeros(self.ac_size) - np.array(self.joint_vel)))
    else:
      exp_forces = np.zeros(self.ac_size)

    forces = np.array(self.motor_power)*np.array(self.actions)*0.082 + exp_forces
    forces = np.clip(forces, -150, 150)

    # p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=self.exp_joints)

    self.exp_joint_vel = (np.array(self.exp_joints) - np.array(self.prev_exp_joints))/self.timeStep
    self.prev_exp_joints = self.exp_joints

    if self.mocap:
      self.set_position([self.exp_body_xyz[0], self.exp_body_xyz[1] + 1, self.exp_body_xyz[2] + 0.90 + self.z_offset], self.exp_body_rot, self.exp_joints, joint_vel=self.exp_joint_vel, robot_id=self.exp_Id)

    # self.set_position([0.0, 0.0, self.exp_body_xyz[2] + 0.90 + self.z_offset], self.exp_body_rot)

    p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=forces)

    for _ in range(int(self.timeStep/self.simStep)):
      p.stepSimulation()

    if self.render:
      time.sleep(0.01)
    self.get_observation()
    self.prev_speed = self.speed

    if self.record_step: 
      self.record_sim_data()
    reward, done = self.get_reward()
    self.prev_actions = self.actions
    self.total_reward += reward
    self.steps += 1
    self.total_steps += 1
    
    # if self.iters_so_far > 200:
    #   self.phase = 0.0
    # else:
    #   self.phase = self.sample_pointer / self.sample_size

    if self.speed > 0.3:
      # if self.obstacle_type == 'run':
      #   self.sample_pointer += 2
      # else:
      self.sample_pointer += self.speed
      # print(self.speed, self.sample_pointer)
      if self.sample_pointer > self.sample_size-1:
        self.sample_pointer = 0
        self.initial_xyz = [self.exp_body_xyz[0], 0.0, 0.0]
      # self.sample_pointer = int(self.sample_pointer)
      temp = self.samples[int(self.sample_pointer),:]
      self.exp_body_xyz, self.exp_body_rot, self.exp_joints, _ = temp[:3], p.getQuaternionFromEuler(temp[3:6]), temp[6:self.ac_size+6], temp[self.ac_size+6:]
      self.exp_body_xyz = np.array(self.exp_body_xyz) + np.array(self.initial_xyz)
    
    # if self.doa:
    #   if self.args.test_pol:
    #     self.episode_buffer.append([self.body_xyz[0], self.body_xyz[1], self.yaw, doa])
    #   else:
    #     self.episode_buffer.append([self.body_xyz[0], self.body_xyz[1], self.yaw, 0])

    # self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.phase]
    # self.state = self.joints + self.joint_vel + self.body + self.contacts + self.swing_stance
    if not self.vis:
      self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.speed, 0.0] + self.boxes
    else:
      self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.speed, 0.0]
    return np.array(self.state), reward, done, None

  def get_reward(self):


    reward = 0
    done = False

    if self.doa:
      if self.body_xyz[2] < (0.8 + self.z_min) or (abs(self.pitch) > np.pi/4 or abs(self.roll) > np.pi/4):   
        done = True
      return reward, done

    if self.mocap:
      exp = p.getBasePositionAndOrientation(self.exp_Id)
      exp_pos = exp[0]
      exp_rot = p.getEulerFromQuaternion(exp[1])
      exp_vel = (exp_pos[0] - self.prev_exp_pos)/self.timeStep

    my_pos = self.body_xyz[0]
    my_vel = (my_pos - self.prev_my_pos)/self.timeStep
      
    still_pos = np.array([0.0,0.0,-0.1,-0.2,0.0,-0.1,0.0,0.0,-0.1,-0.2,0.0,-0.1])
    # self.vel_buffer.append(self.vx)
    
    goal, pos, neg, vel, com, sym, tip = 0, 0, 0, 0, 0, 0, 0
    
    act = -0.0001*np.sum(self.actions**2)

    # if self.obstacle_type == 'gaps' and not self.cur:
    #   x = abs(self.vx - self.speed)
    #   goal = (2.0/(np.e**(3*x) + np.e**(-3*x)))
    #   reward = goal
    # elif self.obstacle_type == 'jumps' and not self.cur:
    #   x = abs(self.vx - self.speed)
    #   goal = (2.0/(np.e**(3*x) + np.e**(-3*x)))
    #   reward = goal
    if self.obstacle_type == 'stop_turn' and not self.cur:
      # w_goal, w_pos, w_neg, w_vel, w_com, w_sym, w_tip, w_act = 1.0, 1.0, 1.0, 0, 0, 0, 0, 0
      x = abs(self.yaw_vel - self.yaw_speed)
      goal = (2.0/(np.e**(3*x) + np.e**(-3*x)))
      pos = 0.1*np.exp(-2.0*np.sum((np.array(self.joints) - still_pos)**2))
      com = 0.1*np.exp(-5.0*np.sum((np.array([self.roll, self.pitch, self.body_xyz[0], self.body_xyz[1]]) - np.array([0.0,0.0,0.0,0.0]))**2))
      if self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground']:
        neg -= 0.05 
      reward = goal + pos + com + neg + act
    else:
      x = abs(self.vx - self.speed)
      goal = 0.75*(2.0/(np.e**(3*x) + np.e**(-3*x)))
      x = abs(self.yaw_vel - self.yaw_speed)
      goal += 0.25*(2.0/(np.e**(3*x) + np.e**(-3*x)))
      # goal = 0.25*(3.0/(np.e**(3*x) + np.e**(-3*x)))
      # goal = np.exp(-10*np.sum(abs(np.array(exp_vel) - np.array(my_vel))))
      self.prev_exp_pos = exp_pos[0]
      self.prev_my_pos = my_pos

      # print("fix foot")
      left_foot = p.getLinkState(self.Id, self.feet_dict['left_heel1'])[0]
      right_foot = p.getLinkState(self.Id, self.feet_dict['right_heel1'])[0]
      avg_foot = (np.array(left_foot) + np.array(right_foot))/2
      # com = list(np.array(self.body_xyz) - avg_foot)

      if self.mocap:
        exp_left_foot =  p.getLinkState(self.exp_Id, self.feet_dict['left_heel1'])[0]
        exp_right_foot = p.getLinkState(self.exp_Id, self.feet_dict['right_heel1'])[0]
        avg_exp_foot = (np.array(exp_left_foot) + np.array(exp_right_foot))/2
        exp_com = list(np.array(exp_pos) - avg_exp_foot)
        exp_foot = avg_exp_foot - np.array(exp_pos)
        self.exp_feet_pos = list(np.array(exp_left_foot) - np.array(exp_pos)) + list(np.array(exp_right_foot) - np.array(exp_pos))

      foot = avg_foot - np.array(self.body_xyz)
      
      self.feet_pos = list(np.array(left_foot) - np.array(self.body_xyz)) + list(np.array(right_foot) - np.array(self.body_xyz))
      
      # Only care about hip_z, hip_y, knee (it get's too much for policy to discern with the full state)
      idx = [0,2,3,6,8,9]

      if self.speed < 0.1:
      #   pos = np.exp(-1.0*np.sum(abs(np.zeros(self.ac_size) - np.array(self.joints))))
      #   vel = 0.5*np.exp(-0.1*np.sum(abs(np.zeros(self.ac_size) - np.array(self.joint_vel))))
        # pos = np.exp(-3.0*np.sum((np.zeros(self.ac_size) - np.array(self.joints))**2))
        # pos = -0.1*np.sum((np.zeros(self.ac_size) - np.array(self.joints))**2)
        # pos = np.exp(-4.0*np.sum((np.array(self.exp_joints)[idx] - np.array(self.joints)[idx])**2))
        # pos = np.exp(-2.0*np.sum((still_pos - np.array(self.joints))**2))
        pos = 0.75*np.exp(-2.0*np.sum((np.array(self.exp_joints) - np.array(self.joints))**2))
        # pos = np.exp(-2.0*np.sum((np.zeros(self.ac_size) - np.array(self.joints))**2))
        vel = 0.1*np.exp(-0.1*np.sum((np.zeros(self.ac_size) - np.array(self.joint_vel))**2))

      else:
        # pos = np.exp(-2*np.sum(abs(np.array(self.exp_joints) - np.array(self.joints))))
        # pos = np.exp(-1.0*np.sum(abs(np.array(self.exp_joints) - np.array(self.joints))))
        # pos = np.exp(-2.0*np.sum((np.array(self.exp_joints) - np.array(self.joints))**2))
        # pos = np.exp(-4.0*np.sum((np.array(self.exp_joints)[idx] - np.array(self.joints)[idx])**2))
        pos = 0.75*np.exp(-2.0*np.sum((np.array(self.exp_joints) - np.array(self.joints))**2))
        # pos = np.exp(-3.0*np.sum((np.array(self.exp_joints) - np.array(self.joints))**2))
        # pos = -0.1*np.sum((np.array(self.exp_joints)[idx] - np.array(self.joints)[idx])**2)
        # pos = np.exp(-0.5*np.sum(abs(np.array(self.exp_joints) - np.array(self.joints))))
      # pos = -1.0*np.sum(abs(np.array(self.exp_joints) - np.array(self.joints)))
        vel = 0.1*np.exp(-0.1*np.sum((np.array(self.exp_joint_vel) - np.array(self.joint_vel))**2))
      # tip = 0.1*np.exp(-20*np.sum(abs(foot - exp_foot)))
      tip = 0.1*np.exp(-20*np.sum((np.array(self.feet_pos) - np.array(self.exp_feet_pos))**2))
      # tip = 0.0
      # print([self.qx, self.qy, self.qz, self.qw, self.body_xyz[2]])
      # print([list(exp_rot)+[exp_pos[2]]])
      # com = np.exp(-10*np.sum(abs(np.array([self.qx, self.qy, self.qz, self.qw,self.body_xyz[2]]) - np.array(list(exp_rot)+[exp_pos[2]]))))
      # com = 0.5*np.exp(-10*np.sum(abs(np.array([self.roll, self.pitch] + com) - np.array([0.0, 0.0] + exp_com))))
      # com = 0.5*np.exp(-10*np.sum((np.array([self.roll, self.pitch] + com) - np.array([0.0, 0.0] + exp_com))**2))
      # com = 0.1*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2]]) - np.array([0.0, 0.0, 0.95+self.z_offset]))**2))
      # print(exp_pos[2]-self.z_offset)
      des_y = self.box_info[1][self.box_num][1]
      # des_yaw = self.box_info[1][self.box_num][5]
      # print(self.yaw, des_yaw)
      com = 0.2*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2]]) - np.array([0.0, 0.0, 0.95+self.z_offset]))**2))
      
      # com = -1*np.sum(abs(np.array([self.qx, self.qy, self.qz, self.qw,self.body_xyz[2]]) - np.array(list([0,0,0,1])+[0.95])))

      # tip = 0.04

      # pos = 0.65*np.exp(-2.0*np.sum((np.array(self.exp_joints) - np.array(self.joints))**2))
      # vel = 0.1*np.exp(-0.1*np.sum((np.array(self.exp_joint_vel) - np.array(self.joint_vel))**2))
      # tip = 0.15*np.exp(-40*np.sum((np.array(self.exp_feet_pos) - np.array(self.feet_pos))**2))
      # com = 0.1*np.exp(-10*np.sum((np.array([exp_pos[0],0.0,exp_pos[2]]) - np.array(self.body_xyz))**2))

      # neg = -0.005*(sum(abs(np.array(self.prev_actions)-self.actions))) 
      # neg = 0.0
      # if self.speed > 0.3 and self.steps > 100:
      #   if self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground']:
      #     neg += 0.2
      #   if not self.ob_dict['right_foot_left_ground'] or not self.ob_dict['left_foot_left_ground']:
      #     neg += 0.2

      neg = 0

      if self.speed > 0.3 and self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground'] and self.obstacle_type not in ['gaps', 
    'jumps', 'run']:
        neg -= 0.1       
      
    #   if self.speed > 0.3 and not self.ob_dict['right_foot_left_ground'] and not self.ob_dict['left_foot_left_ground'] and self.obstacle_type not in ['gaps', 
    # 'jumps', 'run']:
    #     neg -= 0.02       


      # if self.obstacle_type not in ['flat']:
      if False:
        # Need to account for yaw..
        # print(self.right_boxes, self.left_boxes)
        
        if self.speed > 0.3:
          if (self.steps - self.pause_time)%self.time_of_step == 0:
            if self.first_step:
              self.step_size = 1
              self.first_step = False
            else:
              self.step_size = 2
            if self.ob_dict['swing_foot']:
              self.foot_box['right'] += self.step_size
            else:
              self.foot_box['left'] += self.step_size
            self.ob_dict['swing_foot'] = not self.ob_dict['swing_foot']
          # print(self.foot_box['right'], self.foot_box['left'], self.time_of_step, self.steps, self.pause_time)

          # box_x, box_y = self.box_info[1][self.foot_box['right']][0], self.box_info[1][self.foot_box['right']][1]
          # box_yaw = self.box_info[1][self.foot_box['right']][5] + np.pi/2 + np.pi
          # right_error = np.sum((np.array(right_foot[:2]) - np.array([0.08*np.cos(box_yaw)+box_x, 0.08*np.sin(box_yaw)+box_y]))**2)
          
          # box_x, box_y = self.box_info[1][self.foot_box['left']][0], self.box_info[1][self.foot_box['left']][1]
          # box_yaw = self.box_info[1][self.foot_box['left']][5] + np.pi/2
          # left_error = np.sum((np.array(left_foot[:2]) - np.array([0.08*np.cos(box_yaw)+box_x, 0.08*np.sin(box_yaw)+box_y]))**2)

          # neg += 0.2*np.exp(-10*(right_error + left_error))


          box_x, box_y = self.box_info[1][self.foot_box['right']][0], self.box_info[1][self.foot_box['right']][1]
          yaw1 = self.box_info[1][self.foot_box['right']][5] + np.pi/2
          yaw2 = self.box_info[1][self.foot_box['right']][5] + np.pi/2 + np.pi
          l = self.box_info[2][self.foot_box['right']][1]
          x1,y1 = [l*np.cos(yaw1)+box_x, l*np.sin(yaw1)+box_y]
          x2,y2 = [l*np.cos(yaw2)+box_x, l*np.sin(yaw2)+box_y]
          d_right = self.dist_to_segment([right_foot[0], right_foot[1]], [x1,y1], [x2,y2])

          box_x, box_y = self.box_info[1][self.foot_box['left']][0], self.box_info[1][self.foot_box['left']][1]
          yaw1 = self.box_info[1][self.foot_box['left']][5] + np.pi/2
          yaw2 = self.box_info[1][self.foot_box['left']][5] + np.pi/2 + np.pi
          l = self.box_info[2][self.foot_box['left']][1]
          x1,y1 = [l*np.cos(yaw1)+box_x, l*np.sin(yaw1)+box_y]
          x2,y2 = [l*np.cos(yaw2)+box_x, l*np.sin(yaw2)+box_y]
          d_left = self.dist_to_segment([left_foot[0], left_foot[1]], [x1,y1], [x2,y2])

          # x1, y1 = self.box_info[1][self.foot_box['right']][0], self.box_info[1][self.foot_box['right']][1]
          # box_yaw = self.box_info[1][self.foot_box['right']][5] + np.pi/2 + np.pi
          # x2, y2 = x1 + np.cos(box_yaw), y1 + np.sin(box_yaw)
          # a = (y1 - y2)
          # b = (x2 - x1)
          # c = (x1*y2 - x2*y1)
          # d_right = abs(a*right_foot[0] + b*right_foot[1] + c)/np.sqrt(a**2 + b**2)
          

      
          # x1, y1 = self.box_info[1][self.foot_box['left']][0], self.box_info[1][self.foot_box['left']][1]
          # box_yaw = self.box_info[1][self.foot_box['left']][5] + np.pi/2
          # x2, y2 = x1 + np.cos(box_yaw), y1 + np.sin(box_yaw)
          # a = (y1 - y2)
          # b = (x2 - x1)
          # c = (x1*y2 - x2*y1)
          # d_left = abs(a*left_foot[0] + b*left_foot[1] + c)/np.sqrt(a**2 + b**2)
          
          neg += 0.25*np.exp(-10*(d_right + d_left))

        if False:
          if 'right' in self.foot_on_box and self.foot_on_box['right'] > 1:
            # If foot on the ground, past the first step, other foot hasn't stepped on it already, and this is the first time stepping on it, reward close to box x position
            if self.foot_on_box['right'] not in self.left_boxes and self.right_boxes.count(self.foot_on_box['right']) == 1 and (self.foot_on_box['right'] + self.right_boxes[0] - 2) % 2 == 0:

              x1, y1 = self.box_info[1][self.foot_on_box['right']][0], self.box_info[1][self.foot_on_box['right']][1]
              box_yaw = self.box_info[1][self.foot_on_box['right']][5] + np.pi/2
              x2, y2 = x1 + np.cos(box_yaw), y1 + np.sin(box_yaw)
              # m = (y1-y2)/(x1-x2)
              # b = y1 - m*x1
              # d1 = np.sqrt(((b+m*x1-y1)/(m**2+1)))

              # m1 = (y1-y2)/(x1-x2)
              # b1 = y1 - m1*x1
              # # d1 = np.sqrt(((b1+m1*x1-y1)/(m1**2+1)))

              # m2 = -1/m1
              # b2 = right_foot[1] - m2*right_foot[0]

              # x3 = -(b1-b2)/(m1-m2)
              # y3 = m1*x3 + b1
              # d1 = np.sqrt((right_foot[0] - x3)**2 + (right_foot[1] - y3)**2)
              # d1 = np.sqrt(((b+m*x1-y1)/(m**2+1)))

              a = (y1 - y2)
              b = (x2 - x1)
              c = (x1*y2 - x2*y1)
              d = abs(a*right_foot[0] + b*right_foot[1] + c)/np.sqrt(a**2 + b**2)
              # print('right', d, d)
              neg += 0.1*np.exp(-5*d)
              # if self.rank == 0:
              #   print('right', self.foot_on_box['right'], self.right_boxes, d)

              # box_x, box_y = self.box_info[1][self.foot_on_box['right']][0], self.box_info[1][self.foot_on_box['right']][1]
              # box_yaw = self.box_info[1][self.foot_on_box['right']][5] + np.pi/2 + np.pi
              # neg += 0.2*np.exp(-10*np.sum((np.array(right_foot[:2]) - np.array([0.1*np.cos(box_yaw)+box_x, 0.1*np.sin(box_yaw)+box_y]))**2))
              # neg += 0.1*np.exp(-10*np.sum((right_foot[0] - self.box_info[1][self.foot_on_box['right']][0])**2))
            # if other foot has already stepped on this box penalize
            if self.foot_on_box['right'] in self.left_boxes:
              neg -= 0.05
            # if foot stepped on box multiple times penalize
            if self.right_boxes.count(self.foot_on_box['right']) > 1:
              neg -= 0.02*self.right_boxes.count(self.foot_on_box['right'])
          
          if 'left' in self.foot_on_box and self.foot_on_box['left'] > 1:
            # If foot on the ground, past the first step, other foot hasn't stepped on it already, and this is the first time stepping on it, reward close to box x position
            if self.foot_on_box['left'] not in self.right_boxes and self.left_boxes.count(self.foot_on_box['left']) == 1 and (self.foot_on_box['left'] + self.left_boxes[0] - 2) % 2 == 0:
    
              x1, y1 = self.box_info[1][self.foot_on_box['left']][0], self.box_info[1][self.foot_on_box['left']][1]
              box_yaw = self.box_info[1][self.foot_on_box['left']][5] + np.pi/2
              x2, y2 = x1 + np.cos(box_yaw), y1 + np.sin(box_yaw)
              a = (y1 - y2)
              b = (x2 - x1)
              c = (x1*y2 - x2*y1)
              d = abs(a*left_foot[0] + b*left_foot[1] + c)/np.sqrt(a**2 + b**2)
              # print('left', d)
              neg += 0.1*np.exp(-5*d)
              # if self.rank == 0:
              #   print('left', self.foot_on_box['left'], self.left_boxes, d)
    
              # box_x, box_y = self.box_info[1][self.foot_on_box['left']][0], self.box_info[1][self.foot_on_box['left']][1]
              # box_yaw = self.box_info[1][self.foot_on_box['left']][5] + np.pi/2
              # neg += 0.2*np.exp(-10*np.sum((np.array(left_foot[:2]) - np.array([0.1*np.cos(box_yaw)+box_x, 0.1*np.sin(box_yaw)+box_y]))**2))
              # neg += 0.1*np.exp(-10*np.sum(left_foot[0] - self.box_info[1][self.foot_on_box['left']][0])**2))
            # if other foot has already stepped on this box penalize
            if self.foot_on_box['left'] in self.right_boxes:
              neg -= 0.05
            # if foot stepped on box multiple times penalize
            if self.left_boxes.count(self.foot_on_box['left']) > 1:
              neg -= 0.02*self.left_boxes.count(self.foot_on_box['left'])
                
      # sym = 0
      # sym_fact = 0.0
      sym_fact = 0.02
      terms = 0
      if self.time_of_step != 0:
        if len(self.action_store) == (self.time_of_step):
        #   # prev_z = z = next_z = 0
        #   # if self.e2e:
        #   # if self.box_info is not None and (self.box_num+1) < len(self.box_info[0]) and self.box_num > 0:
        #   #   prev_z = self.box_info[1][self.box_num-1][2] + self.box_info[2][self.box_num-1][2]
        #   #   z = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        #   #   next_z = self.box_info[1][self.box_num+1][2] + self.box_info[2][self.box_num+1][2]
        #   # if round(z - prev_z,2) == round(next_z - z,2):
          terms += (self.ob_dict['right_hip_y_pos'] - self.action_store[0][0])**2
          terms += (self.ob_dict['right_knee_pos'] - self.action_store[0][1])**2
          terms += (self.ob_dict['left_hip_y_pos'] - self.action_store[0][2])**2
          terms += (self.ob_dict['left_knee_pos'] - self.action_store[0][3])**2
        sym = sym_fact*np.exp(-10*terms)
        self.action_store.append([self.ob_dict['left_hip_y_pos'],self.ob_dict['left_knee_pos'],self.ob_dict['right_hip_y_pos'],self.ob_dict['right_knee_pos']])
        # print(len(self.action_store))
      # print(sym)
      # reward = goal + pos + com - neg + tip + vel
      # print(sym, neg)
      reward = goal + pos + com + neg + sym + act
      self.pos_errors.append(pos)

    # self.reward_breakdown['pos'] = pos
    # self.reward_breakdown['vel'] = vel
    # self.reward_breakdown['neg'] = neg
    # self.reward_breakdown['tip'] = tip
    # self.reward_breakdown['com'] = com
    # self.reward_breakdown['sym'] = sym

    self.reward_breakdown['goal'].append(goal)
    self.reward_breakdown['pos'].append(pos)
    self.reward_breakdown['vel'].append(vel)
    self.reward_breakdown['neg'].append(neg)
    self.reward_breakdown['tip'].append(tip)
    self.reward_breakdown['com'].append(com)
    self.reward_breakdown['sym'].append(sym)
    self.reward_breakdown['act'].append(act)
    
    # print(0.7+self.z_offset, 0.7+self.z_min)
    if self.obstacle_type == 'jump':
      if ((self.steps + 1) % 16 == 0 and self.steps != 0) and (self.steps > 8*256-1 or self.body_xyz[2] < (1.7)):   
        done = True
    else:
      if ((self.steps + 1) % 16 == 0 and self.steps != 0) and (self.steps > 8*256-1 or self.body_xyz[2] < (0.7 + self.z_min)):   
        done = True
    return reward, done

  def get_observation(self):
    jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
    self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
    self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.uniform(-0.5,0.5,self.ac_size))
    
    self.ob_dict.update({n + '_pos':j for n,j in zip(self.motor_names, self.joints)})


    self.body_xyz, (self.qx, self.qy, self.qz, self.qw) = p.getBasePositionAndOrientation(self.Id)
    self.roll, self.pitch, self.yaw = p.getEulerFromQuaternion([self.qx, self.qy, self.qz, self.qw])

    self.body_vxyz, self.base_rot_vel = p.getBaseVelocity(self.Id)
    
    self.roll_vel = self.base_rot_vel[0]
    self.pitch_vel = self.base_rot_vel[1]
    self.yaw_vel = self.base_rot_vel[2]

    rot_speed = np.array(
      [[np.cos(-self.yaw), -np.sin(-self.yaw), 0],
        [np.sin(-self.yaw), np.cos(-self.yaw), 0],
        [		0,			 0, 1]]
    )

    self.vx, self.vy, self.vz = np.dot(rot_speed, (self.body_vxyz[0],self.body_vxyz[1],self.body_vxyz[2]))
    
    # Policy shouldn't know yaw
    self.body = [self.vx, self.vy, self.vz, self.roll, self.pitch, self.roll_vel, self.pitch_vel, self.yaw_vel, self.body_xyz[2] - self.z_offset]

    self.ob_dict['prev_right_heel1'] = self.ob_dict['right_heel1'] 
    self.ob_dict['prev_right_heel2'] = self.ob_dict['right_heel2'] 
    self.ob_dict['prev_right_toe']   = self.ob_dict['right_toe']   
    self.ob_dict['prev_left_heel1']  = self.ob_dict['left_heel1']  
    self.ob_dict['prev_left_heel2']  = self.ob_dict['left_heel2']  
    self.ob_dict['prev_left_toe']    = self.ob_dict['left_toe']    

    self.ob_dict['right_heel1'] = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel1'], -1))>0
    self.ob_dict['right_heel2'] = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel2'], -1))>0
    self.ob_dict['right_toe']   = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_toe'], -1))>0
    self.ob_dict['left_heel1']  = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel1'], -1))>0
    self.ob_dict['left_heel2']  = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel2'], -1))>0
    self.ob_dict['left_toe']    = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_toe'], -1))>0

    # Update feet that have left the ground.
    self.ob_dict['prev_right_foot_left_ground'] = self.ob_dict['right_foot_left_ground']
    self.ob_dict['prev_left_foot_left_ground'] = self.ob_dict['left_foot_left_ground']
    self.ob_dict['right_foot_left_ground'] = not self.ob_dict['right_heel1'] and not self.ob_dict['right_heel2'] and not self.ob_dict['right_toe']
    self.ob_dict['left_foot_left_ground'] = not self.ob_dict['left_heel1'] and not self.ob_dict['left_heel2'] and not self.ob_dict['left_toe']

    right_contacts = [self.ob_dict['right_heel1'],self.ob_dict['right_heel2'],self.ob_dict['right_toe']]
    left_contacts = [self.ob_dict['left_heel1'],self.ob_dict['left_heel2'],self.ob_dict['left_toe']]
    prev_right_contacts = [self.ob_dict['prev_right_heel1'],self.ob_dict['prev_right_heel2'],self.ob_dict['prev_right_toe']]
    prev_left_contacts = [self.ob_dict['prev_left_heel1'],self.ob_dict['prev_left_heel2'],self.ob_dict['prev_left_toe']]
    

    if self.mocap:
      self.ob_dict['exp_right_heel1'] = len(p.getContactPoints(self.exp_Id, -1, self.feet_dict['right_heel1'], -1))>0
      self.ob_dict['exp_right_heel2'] = len(p.getContactPoints(self.exp_Id, -1, self.feet_dict['right_heel2'], -1))>0
      self.ob_dict['exp_right_toe']   = len(p.getContactPoints(self.exp_Id, -1, self.feet_dict['right_toe'], -1))>0
      self.ob_dict['exp_left_heel1']  = len(p.getContactPoints(self.exp_Id, -1, self.feet_dict['left_heel1'], -1))>0
      self.ob_dict['exp_left_heel2']  = len(p.getContactPoints(self.exp_Id, -1, self.feet_dict['left_heel2'], -1))>0
      self.ob_dict['exp_left_toe']    = len(p.getContactPoints(self.exp_Id, -1, self.feet_dict['left_toe'], -1))>0

      # Update feet that have left the ground.
      self.ob_dict['exp_prev_right_foot_left_ground'] = self.ob_dict['exp_right_foot_left_ground']
      self.ob_dict['exp_prev_left_foot_left_ground'] = self.ob_dict['exp_left_foot_left_ground']
      self.ob_dict['exp_right_foot_left_ground'] = not self.ob_dict['exp_right_heel1'] and not self.ob_dict['exp_right_heel2'] and not self.ob_dict['exp_right_toe']
      self.ob_dict['exp_left_foot_left_ground'] = not self.ob_dict['exp_left_heel1'] and not self.ob_dict['exp_left_heel2'] and not self.ob_dict['exp_left_toe']

    # exp_left_foot =  p.getLinkState(self.Id, self.feet_dict['left_heel1'])[0]
    # exp_right_foot = p.getLinkState(self.Id, self.feet_dict['right_heel1'])[0]
    # print(exp_left_foot[0] - exp_right_foot[0])

    # print(self.ob_dict['exp_right_foot_left_ground'], self.ob_dict['exp_left_foot_left_ground'])
    # if not self.ob_dict['ready_to_walk'] and (not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground']):
    #   self.ob_dict['ready_to_walk'] = True
    #   self.ob_dict['prev_left_foot_left_ground'] = False
    #   self.ob_dict['prev_right_foot_left_ground'] = False
    #   if self.sample_pointer > 20 and self.sample_pointer < 90:
    #     self.ob_dict['left_foot_swing'] = False
    #   else:
    #     self.ob_dict['left_foot_swing'] = True
    #   self.ob_dict['right_foot_swing'] = not self.ob_dict['left_foot_swing']
      
    # if self.prev_speed < 0.3 and self.speed >= 0.3:
    #   if self.sample_pointer > 20 and self.sample_pointer < 90:
    #     self.ob_dict['left_foot_swing'] = False
    #   else:
    #     self.ob_dict['left_foot_swing'] = True
    #   self.ob_dict['right_foot_swing'] = not self.ob_dict['left_foot_swing']


    # thing = 200


    if self.prev_speed < 0.3 and self.speed >= 0.3:
      self.ob_dict['right_foot_swing'] = self.ob_dict['swing_foot']
      self.ob_dict['left_foot_swing'] = not self.ob_dict['right_foot_swing']

    if self.ob_dict['right_foot_swing']:
      swing = 'right'
    else: 
      swing = 'left'

    if self.speed < 0.3:
      self.ob_dict['right_foot_swing'] = 0
      self.ob_dict['left_foot_swing'] = 0
    else:
      # if self.iters_so_far < 200:
      #   if self.ob_dict['exp_prev_' + swing + '_foot_left_ground'] and not self.ob_dict['exp_' + swing + '_foot_left_ground']:
      #     self.ob_dict['right_foot_swing'] = not self.ob_dict['right_foot_swing']
      #     self.ob_dict['left_foot_swing'] = not self.ob_dict['left_foot_swing']
      # else:
      if self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']:
        self.ob_dict['right_foot_swing'] = not self.ob_dict['right_foot_swing']
        self.ob_dict['left_foot_swing'] = not self.ob_dict['left_foot_swing']

    self.swing_stance = [self.ob_dict['right_foot_swing'], self.ob_dict['left_foot_swing']]


    self.contacts = right_contacts + left_contacts + prev_right_contacts + prev_left_contacts + self.swing_stance

    # Check if we should change swing and stance feet.
    # if self.ob_dict['ready_to_walk'] and self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']:
    #   self.ob_dict['right_foot_swing'] = not self.ob_dict['right_foot_swing']
    #   self.ob_dict['left_foot_swing'] = not self.ob_dict['left_foot_swing']
    #   if self.speed < 0.3:
    #     self.ob_dict['right_foot_swing'] = 0
    #     self.ob_dict['left_foot_swing'] = 0
    #   self.ob_dict['impact'] = True
    # else: self.ob_dict['impact'] = False

    # if self.speed > 0.3 and self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']:
    #   self.ob_dict['right_foot_swing'] = not self.ob_dict['right_foot_swing']
    #   self.ob_dict['left_foot_swing'] = not self.ob_dict['left_foot_swing']
    # elif self.speed < 0.3:
      # self.ob_dict['right_foot_swing'] = 0
      # self.ob_dict['left_foot_swing'] = 0
    
    # self.ob_dict['impact'] = True
    # else: self.ob_dict['impact'] = False


    if self.obstacle_type != 'flat':
      # for i in range(self.box_num, len(self.box_info[0])):
      i = self.box_num
      total_i = 0
      while True:
        x, y = self.body_xyz[0], self.body_xyz[1]
        x1, y1 = self.box_info[1][i][0] - self.box_info[2][i][0], self.box_info[1][i][1] - self.box_info[2][i][1]
        x2, y2 = self.box_info[1][i][0] + self.box_info[2][i][0], self.box_info[1][i][1] + self.box_info[2][i][1]
        if x1 > x2:
          x_temp = x1
          x1 = x2
          x2 = x_temp
        elif y1 > y2:
          y_temp = y1
          y1 = y2
          y2 = y_temp
        if x1 < x < x2 and y1 < y < y2:
          self.box_num = i
          # "robot is over box number", self.box_num)
          break
        i += 1
        total_i += 1
        if i > (len(self.box_info[0]) - 1):
          i = 0
        if total_i > (len(self.box_info[0]) - 1):
          # ("com not over box, box number frozen")
          break
      
      # box = 0
      self.boxes = []
      self.null_box = [0,0,0,0,0]
      for box in [-2,-1,0,1,2]:
        if (self.box_num + box < 1) or (self.box_num + box > len(self.box_info[0]) - 1):
          self.boxes.extend(self.null_box)
        else:
          length, width = self.box_info[2][self.box_num+box][0], self.box_info[2][self.box_num+box][1]
          # box_yaw = np.arctan2(length, width) + self.box_info[1][self.box_num+box][5]
          box_yaw = self.box_info[1][self.box_num+box][5] 
          
          
          diag = np.sqrt(length**2 + width**2)
          self.world_box_x1 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2( width,  length))
          self.world_box_y1 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2( width,  length))
          # self.world_box_x2 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2( width, -length))
          # self.world_box_y2 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2( width, -length))
          # self.world_box_x3 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2(-width,  length))
          # self.world_box_y3 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2(-width,  length))
          self.world_box_x4 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2(-width, -length))
          self.world_box_y4 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2(-width, -length))

          # p_on_line = self.project_to_segment(p, v, w)0


          d1 = np.sqrt((self.body_xyz[0] - self.world_box_x1)**2 + (self.body_xyz[1] - self.world_box_y1)**2)
          # d2 = np.sqrt((self.body_xyz[0] - self.world_box_x2)**2 + (self.body_xyz[1] - self.world_box_y2)**2)
          # d3 = np.sqrt((self.body_xyz[0] - self.world_box_x3)**2 + (self.body_xyz[1] - self.world_box_y3)**2)
          d4 = np.sqrt((self.body_xyz[0] - self.world_box_x4)**2 + (self.body_xyz[1] - self.world_box_y4)**2)
          # min_box = np.argmin([d1,d2,d3,d4])
          # max_box = np.argmax([d1,d2,d3,d4])

          # cnrs = [[self.world_box_x1, self.world_box_y1],[self.world_box_x2, self.world_box_y2],[self.world_box_x3, self.world_box_y3],[self.world_box_x4, self.world_box_y4]]
          # min_x, min_y = cnrs[min_box]
          # max_x, max_y = cnrs[max_box]
          # min_dist = [d1,d2,d3,d4][min_box]
          # max_dist = [d1,d2,d3,d4][max_box]

          # x1 = min_x - self.body_xyz[0]
          # y1 = min_y - self.body_xyz[1]
          # x2 = max_x - self.body_xyz[0]
          # y2 = max_y - self.body_xyz[1]
          # box_x1 = min_dist*np.cos(np.arctan2(y1,x1) - self.yaw)
          # box_y1 = min_dist*np.sin(np.arctan2(y1,x1) - self.yaw)
          # box_x2 = max_dist*np.cos(np.arctan2(y2,x2) - self.yaw)
          # box_y2 = max_dist*np.sin(np.arctan2(y2,x2) - self.yaw)

          x1 = self.world_box_x1 - self.body_xyz[0]
          y1 = self.world_box_y1 - self.body_xyz[1]
          x2 = self.world_box_x4 - self.body_xyz[0]
          y2 = self.world_box_y4 - self.body_xyz[1]
          box_x1 = d1*np.cos(np.arctan2(y1,x1) - self.yaw)
          box_y1 = d1*np.sin(np.arctan2(y1,x1) - self.yaw)
          box_x2 = d4*np.cos(np.arctan2(y2,x2) - self.yaw)
          box_y2 = d4*np.sin(np.arctan2(y2,x2) - self.yaw)

          z  = self.body_xyz[2] - (self.box_info[1][self.box_num+box][2] + self.box_info[2][self.box_num+box][2])
          # z  = self.body_xyz[2] - (self.box_info[1][self.box_num+box][2]*2)
          self.boxes.extend([box_x1,box_y1,box_x2,box_y2,z, self.yaw-box_yaw])
      
      # if self.box_info[1][self.box_num+box][2]*2 != self.box_info[1][self.box_num+box][2] + self.box_info[2][self.box_num+box][2]:
        # print("height wrong ", self.box_info[1][self.box_num+box][2]*2, self.box_info[1][self.box_num+box][2] + self.box_info[2][self.box_num+box][2])
 
    #   # Box info: x,y,z (relative to body_xyz), yaw, width, length
    #   self.boxes = []
    #   self.null_box = [0,0,0,0,0,0,0,0,0,0,0,0]
    #   for box in [-1,0,1,2]:
    #     if (self.box_num + box < 1) or (self.box_num + box > len(self.box_info[0]) - 1):
    #       self.boxes += self.null_box
    #     else:
    #       x = self.body_xyz[0] - self.box_info[1][self.box_num+box][0]
    #       y = self.body_xyz[1] - self.box_info[1][self.box_num+box][1]
    #       z = self.body_xyz[2] - (self.box_info[1][self.box_num+box][2] + self.box_info[2][self.box_num+box][2])
    #       yaw = self.yaw - self.box_info[1][self.box_num+box] [5]
    #       width = self.box_info[2][self.box_num+box][0]
    #       length = self.box_info[2][self.box_num+box][1]
    #       diag = np.sqrt(length**2 + width**2)
    #       x1, y1 = x + diag*np.cos(np.arctan2( width,  length)), y + diag*np.sin(np.arctan2( width,  length))
    #       x2, y2 = x + diag*np.cos(np.arctan2(-width,  length)), y + diag*np.sin(np.arctan2(-width,  length))
    #       x3, y3 = x + diag*np.cos(np.arctan2( width, -length)), y + diag*np.sin(np.arctan2( width, -length))
    #       x4, y4 = x + diag*np.cos(np.arctan2(-width, -length)), y + diag*np.sin(np.arctan2(-width, -length))
    #       # self.boxes += [x, y, z, yaw, width, length]
    #       self.boxes += [x, y, z, yaw, x1, y1, x2, y2, x3, y3, x4, y4]
    # else:
    #   self.boxes = [0.0]*12*4
    # print(self.boxes)
    if self.obstacle_type != 'flat' or self.doa:
      zs = []
      zs.append(self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2])
      if self.box_num - 1 > 0:
        zs.append(self.box_info[1][self.box_num - 1][2] + self.box_info[2][self.box_num - 1][2])
      if self.box_num + 1 < len(self.box_info[0]):
        zs.append(self.box_info[1][self.box_num + 1][2] + self.box_info[2][self.box_num + 1][2])
      self.z_min = min(np.clip(zs, 0.5,None))
      if self.z_min < 0.5:
        print("zmin less than 0.5?")
      self.z_max = max(zs)

    # if self.obstacle_type != 'flat':
    #   feet_in_contact = []
    #   self.foot_on_box = {}
    #   if not self.ob_dict['left_foot_left_ground']:
    #     feet_in_contact.append('left')
    #   if not self.ob_dict['right_foot_left_ground']:
    #     feet_in_contact.append('right')
    #   for foot in feet_in_contact:
    #     foot_pos = p.getLinkState(self.Id, self.feet_dict[foot +'_heel1'])[0]
    #     for i in range(self.box_num-1, self.box_num+2):
    #       if i < 0 or i > (len(self.box_info[0]) - 1): continue
    #       x, y = foot_pos[0], foot_pos[1]
    #       x1, y1 = self.box_info[1][i][0] - self.box_info[2][i][0], self.box_info[1][i][1] - self.box_info[2][i][1]
    #       x2, y2 = self.box_info[1][i][0] + self.box_info[2][i][0], self.box_info[1][i][1] + self.box_info[2][i][1]
    #       if x1 > x2:
    #         x_temp = x1
    #         x1 = x2
    #         x2 = x_temp
    #       elif y1 > y2:
    #         y_temp = y1
    #         y1 = y2
    #         y2 = y_temp
    #       # print(foot, x1,x,x2,y1,y,y2)
    #       if x1 < x < x2 and y1 < y < y2:
    #         self.foot_on_box[foot] = i
    #         if self.ob_dict['prev_right_foot_left_ground'] and foot == 'right' and i not in self.left_boxes:
    #           self.right_boxes.append(i)
    #         if self.ob_dict['prev_left_foot_left_ground'] and foot == 'left' and i not in self.right_boxes:
    #           self.left_boxes.append(i)
    #         # print("left", self.ob_dict['left_foot_left_ground'], "right", self.ob_dict['right_foot_left_ground'], foot)
    #         # print(foot, " is on box", foot_on_box)
    #         break
          
    #     # print(foot,self.foot_on_box)
    #     self.foot_on_box['com'] = self.box_num
    #   # Box info: x,y,z (relative to body_xyz), yaw, width, length
    #   # self.boxes = []
    #   # self.null_box = [0,0,0,0,0,0]
    #   # for box in [-1,0,1,2]:
    #   #   if (self.box_num + box < 1) or (self.box_num + box > len(self.box_info[0]) + 1):
    #   #     self.boxes += self.null_box
    #   #   else:
    #   #     x = self.body_xyz[0] - self.box_info[1][self.box_num+box][0]
    #   #     y = self.body_xyz[1] - self.box_info[1][self.box_num+box][1]
    #   #     z = self.body_xyz[2] - (self.box_info[1][self.box_num+box][2] + self.box_info[2][self.box_num+box][2])
    #   #     yaw = self.yaw - self.box_info[1][self.box_num+box][5]
    #   #     width = self.box_info[2][self.box_num+box][0]
    #   #     length = self.box_info[2][self.box_num+box][1]
    #   #     self.boxes += [x, y, z, yaw, width, length]

  def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
    if robot_id is None:
      robot_id = self.Id
    pos = [pos[0], pos[1], pos[2]]
    p.resetBasePositionAndOrientation(robot_id, pos, orn)
    if joints is not None:
      if joint_vel is not None:
        for j, jv, m in zip(joints, joint_vel, self.motors):
          p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
      else:
        for j, m in zip(joints, self.motors):
          p.resetJointState(robot_id, m, targetValue=j)
    if velocities is not None:
      p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

  def save_sim_data(self):
    if self.rank == 0:
      path = self.PATH
      try:
        np.save(path + 'sim_data.npy', np.array(self.sim_data))     
        if self.mocap:  
          np.save(path + 'exp_sim_data.npy', np.array(self.exp_sim_data))       
        try:
          if self.obstacle_type != 'flat' or self.doa:
            np.save(path + 'box_info.npy', np.array(self.box_info))
        except Exception as e:
          print("trying to save box info", e)
        self.sim_data = []
        if self.mocap:
          self.exp_sim_data = []
      except Exception as e:
        print("Save sim data error:")
        print(e)

  def record_sim_data(self):
    if len(self.sim_data) > 100000: return
    pos, orn = p.getBasePositionAndOrientation(self.Id)
    data = [pos, orn]
    joints = p.getJointStates(self.Id, self.motors)
    data.append([i[0] for i in joints])
    self.sim_data.append(data)
    
    if len(self.sim_data) > 100000: return
    data = [[self.exp_body_xyz[0], self.exp_body_xyz[1]+1, self.exp_body_xyz[2] + 0.95 + self.z_offset], self.exp_body_rot]
    data.append(self.exp_joints)
    self.exp_sim_data.append(data)

  def apply_forces(self, actions=None):
    if self.speed < 0.3:
      z_force = 1.5*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
      # z_force = 1.5*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
      x_force = 1.0*self.Kp * (self.speed - self.vx)
      y_force = 1.0*self.Kp*(0.0 - self.body_xyz[1]) + self.Kp*(0.0 - self.vy)
    else:
      # z_force = 10*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
      # z_force = 1.5*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
      # x_force = 1.0*self.Kp * (self.speed - self.vx)
      # y_force = 1.0*(self.Kp *(0.0 - self.body_xyz[1]) + self.Kp*(0.0 - self.vy))
      z_force = 1.5*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
      x_force = 1.0*self.Kp * (self.speed - self.vx)
      y_force = 1.0*(self.Kp *(0.0 - self.body_xyz[1]) + self.Kp*(0.0 - self.vy))
      # print(self.speed)
    # x_force, y_force, z_force = 0.0, 0.0, 0.0
    # print(x_force, y_force, z_force)
    p.applyExternalForce(self.Id,-1,[x_force,y_force,z_force],[0,0,0],p.LINK_FRAME)

    # if self.speed < 0.3:
    #   yaw_force = 0.001*self.Kp*(0.0 - self.yaw)
    #   roll_force = -0.1*self.Kp * self.roll    
    #   pitch_force = -0.1*self.Kp * self.pitch
    # else:
    if self.obstacle_type == 'stop_turn':
      yaw_force = 0.1*self.Kp*(self.yaw_speed - self.yaw_vel)
      # print(yaw_force)
      roll_force = -0.5*self.Kp * self.roll    - 0.1*self.Kp*self.roll_vel
      pitch_force = -1.0*self.Kp * self.pitch  - 0.1*self.Kp*self.pitch_vel
    else:
      yaw_force = 1.0*self.Kp*(0.0 - self.yaw) - 0.1*self.Kp*self.yaw_vel
      roll_force = -1.0*self.Kp * self.roll    - 0.1*self.Kp*self.roll_vel
      pitch_force = -2.0*self.Kp * self.pitch  - 0.1*self.Kp*self.pitch_vel
    # roll_force, pitch_force, yaw_force = 0.0, 0.0, 0.0
    p.applyExternalTorque(self.Id,-1,[roll_force,pitch_force,yaw_force],p.LINK_FRAME)
    
    actions = np.zeros(self.ac_size)

    if self.speed >= 0.3:
      if self.ob_dict['swing_foot']:
        swing = 'right'
      else:
        swing = 'left'

      swing_hip_torque = 0
      stance_hip_torque = 0
      swing_knee_torque = 0
      stance_knee_torque = 0
      if swing == 'left':
          stance = 'right'
          swing_hip_num = 8
          swing_knee_num = 9
          stance_hip_num = 2
          stance_knee_num = 3
      else:
          stance = 'left'
          swing_hip_num = 2
          swing_knee_num = 3
          stance_hip_num = 8
          stance_knee_num = 9
      cur_time = (self.steps-50) % self.time_of_step
      dx = 0.16
      if self.ob_dict[swing + '_hip_y_pos'] > -0.2:
          swing_hip_torque = 10*(self.Kp/self.initial_Kp)*(-dx)
      if self.ob_dict[stance + '_hip_y_pos'] < 0.1:
          stance_hip_torque = 2*(self.Kp/self.initial_Kp)*(dx)
      if self.ob_dict[swing + '_knee_pos'] > -1.0 and cur_time < 10:
          swing_knee_torque = 4 *(self.Kp/self.initial_Kp)*(-dx)
      if self.ob_dict[swing + '_knee_pos'] < -0.5 and cur_time > 10:
          swing_knee_torque = 4*(self.Kp/self.initial_Kp)*(dx)
      if self.ob_dict[stance + '_knee_pos'] < -0.5:
          stance_knee_torque = 8  *(self.Kp/self.initial_Kp)*(dx)
      actions[swing_knee_num] += swing_knee_torque
      actions[swing_hip_num] += swing_hip_torque
      actions[stance_knee_num] += stance_knee_torque
      actions[stance_hip_num] += stance_hip_torque

      # if self.steps != 0 and (self.steps % self.time_of_step) == 0:
      #     if self.cur_foot == 'left': self.cur_foot = 'right'
      #     else: self.cur_foot = 'left'
    # forces = np.array(self.motor_power)*np.array(self.actions)*0.082 + exp_forces
      return actions*self.motor_power*0.082
    
    return (self.Kp/self.initial_Kp)*(80*(np.zeros(self.ac_size) - np.array(self.joints)) + 0.5*(np.zeros(self.ac_size) - np.array(self.joint_vel)))


  def get_hm(self):
    if self.world_map is not None:
      x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.yaw
      
      # # Calculating z offset from box numbers now
      try:
        x_pix = int(x+(-self.min_x+1)/self.grid_size)
        y_pix = int(y+(-self.min_y+1)/self.grid_size)
        ordered_pix = np.sort(self.world_map[x_pix-5:x_pix+5, y_pix-5:y_pix+5], axis=None)
        self.z_offset = max(ordered_pix[-1], 0.5)
      except Exception as e:
        # pass
        # print(e)
        # print("We're off the map harry!")
        self.z_offset = 0.5

      self.hm = np.ones([self.dx_back+self.dx_forward, 2*self.dy], dtype=np.float32)*-1
      # t1 = time.time()
      hm_pts = self.transform_rot_and_add(1*yaw, [x+(-self.min_x+1)/self.grid_size, y+(-self.min_y+1)/self.grid_size], self.ij)
      hm_pts[:,0] = np.clip(hm_pts[:,0], 0, self.world_map.shape[0]-1)
      hm_pts[:,1] = np.clip(hm_pts[:,1], 0, self.world_map.shape[1]-1)
      
      self.hm[self.hm_ij[:,0],self.hm_ij[:,1]] = self.world_map[hm_pts[:,0],hm_pts[:,1]]
      # for ij_pts, pts in zip(self.hm_ij, hm_pts[:,:2]):
      #   try:
      #     self.hm[ij_pts[0], ij_pts[1]] = self.world_map[pts[0], pts[1]]
      #   except Exception as e:
      #     print(e)
      #     print(ij_pts, pts, self.hm.shape, self.world_map.shape)
      self.hm = np.clip(self.hm - self.z_offset, -1, 1).reshape(self.im_size)
      # Normalise
      self.hm = (self.hm+1)/2
    else:
      self.hm = np.zeros(self.im_size, dtype=np.float32)
      # self.z_offset = 0.5
    if self.display_im:
      self.display()        
    return self.hm
  
  def to_grid(self, y, x):
    x = int(x/self.grid_size +(-self.min_x+1)/self.grid_size)
    y = int(y/self.grid_size + (-self.min_y+1)/self.grid_size)
    return (y, x) 

  def project_to_segment(self, p, v, w):
    l2 = (v[0] - w[0])**2 + (v[1] - w[1])**2
    if l2 == 0: 
      return v[0], v[1]
    t = ((p[0] - v[0]) * (w[0] - v[0]) + (p[1] - v[1]) * (w[1] - v[1])) / l2
    t = max(0, min(1, t))
    return [v[0] + t * (w[0] - v[0]), v[1] + t * (w[1] - v[1])] 

  def dist_to_segment(self, p, v, w):
    p_on_line = self.project_to_segment(p, v, w)
    return np.sqrt((p_on_line[0] - p[0])**2 + (p_on_line[1] - p[1])**2)

  def display(self):
    
    # dx_forward, dx_back, dy = 40, 20, 20
    x1, y1, x2, y2, x3, y3, x4, y4 = self.dx_forward, self.dy, -self.dx_back, self.dy, -self.dx_back, -self.dy, self.dx_forward, -self.dy
    
    dx_forward_col, dx_back_col, dy_col = 30, 12, 12
    x1_col, y1_col, x2_col, y2_col, x3_col, y3_col, x4_col, y4_col = dx_forward_col, dy_col, -dx_back_col, dy_col, -dx_back_col, -dy_col, dx_forward_col, -dy_col
    if self.world_map is not None:
      norm_wm = (np.min(self.world_map) - self.world_map)/(np.max(self.world_map) - np.min(self.world_map))
    else:
      norm_wm = np.zeros([100,100])
    
    wm_col = cv2.cvtColor(norm_wm,cv2.COLOR_GRAY2RGB)

    x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.yaw
    rect_pts = np.array([[y1,x1,1],[y2,x2,1],[y3,x3,1],[y4,x4,1]])
    pts = self.transform_rot_and_add(-1*yaw, [y+ (-self.min_y+1)/self.grid_size, x+ (-self.min_x+1)/self.grid_size], rect_pts)        
    pts = pts[:, :2]
    cv2.polylines(wm_col, [pts], True, (0,255,0), 1)
    cv2.circle(wm_col,(int(y + (-self.min_y+1)/self.grid_size), int(x +(-self.min_x+1)/self.grid_size)), 3, (0,0,255), -1)
    
    if True:
      # self.steps > 0:
      try:  
        cv2.circle(wm_col, self.to_grid(self.world_box_y1, self.world_box_x1), 3, (255,0,0), -1)
        # cv2.circle(wm_col, self.to_grid(self.world_box_y2, self.world_box_x2), 3, (255,0,0), -1)
        # cv2.circle(wm_col, self.to_grid(self.world_box_y3, self.world_box_x3), 3, (255,0,0), -1)
        cv2.circle(wm_col, self.to_grid(self.world_box_y4, self.world_box_x4), 3, (255,0,0), -1)
    
        # for key in self.foot_on_box:
        for key in ['right', 'left']:
          x1 = x4 =  self.box_info[2][self.foot_box[key]][0]/self.grid_size
          x2 = x3 = -self.box_info[2][self.foot_box[key]][0]/self.grid_size
          y1 = y2 =  self.box_info[2][self.foot_box[key]][1]/self.grid_size
          y3 = y4 = -self.box_info[2][self.foot_box[key]][1]/self.grid_size
          x,y,yaw =  self.box_info[1][self.foot_box[key]][0]/self.grid_size, self.box_info[1][self.foot_box[key]][1]/self.grid_size, self.box_info[1][self.foot_box[key]][5]
          rect_pts = np.array([[y1,x1,1],[y2,x2,1],[y3,x3,1],[y4,x4,1]])
          pts = self.transform_rot_and_add(-1*yaw, [y+ (-self.min_y+1)/self.grid_size, x+ (-self.min_x+1)/self.grid_size], rect_pts)        
          pts = pts[:, :2]
          if key == 'right':
            cv2.polylines(wm_col, [pts], True, (0,0,255), 2)
          elif key == 'left':
            cv2.polylines(wm_col, [pts], True, (255,0,0), 2)
          else:
            cv2.polylines(wm_col, [pts], True, (0,255,0), 1)
          if key != 'com':
            # # Circles:
            # box_x, box_y = self.box_info[1][self.foot_box[key]][0], self.box_info[1][self.foot_box[key]][1]
            # if key == 'left':
            #   box_yaw = self.box_info[1][self.foot_box[key]][5] + np.pi/2
            # else:
            #   box_yaw = self.box_info[1][self.foot_box[key]][5] + np.pi/2 + np.pi
            # # print(box_yaw)
            # dims = np.array([0.1*np.cos(box_yaw)+box_x, 0.1*np.sin(box_yaw)+box_y])/self.grid_size
            # if key == 'left':
            #   cv2.circle(wm_col,(int(dims[1] + (-self.min_y+1)/self.grid_size), int(dims[0] +(-self.min_x+1)/self.grid_size)), 4, (255,0,0), -1)
            # else:
            #   cv2.circle(wm_col,(int(dims[1] + (-self.min_y+1)/self.grid_size), int(dims[0] +(-self.min_x+1)/self.grid_size)), 4, (0,0,255), -1)
            
            # Lines
            box_x, box_y = self.box_info[1][self.foot_box[key]][0], self.box_info[1][self.foot_box[key]][1]
            yaw1 = self.box_info[1][self.foot_box[key]][5] + np.pi/2
            yaw2 = self.box_info[1][self.foot_box[key]][5] + np.pi/2 + np.pi
            l = self.box_info[2][self.foot_box[key]][1]
            x1,y1 = [l*np.cos(yaw1)+box_x, l*np.sin(yaw1)+box_y]
            x2,y2 = [l*np.cos(yaw2)+box_x, l*np.sin(yaw2)+box_y]
            if key == 'right':
              wm_col = cv2.line(wm_col, self.to_grid(y1,x1), self.to_grid(y2,x2), (0,0,255), 3) 
            else:
              wm_col = cv2.line(wm_col, self.to_grid(y1,x1), self.to_grid (y2,x2), (255,0,0), 3) 

          # x1, y1 = self.box_info[1][self.foot_box['left']][0], self.box_info[1][self.foot_box['left']][1]
          # box_yaw = self.box_info[1][self.foot_box['left']][5] + np.pi/2
          # x2, y2 = x1 + np.cos(box_yaw), y1 + np.sin(box_yaw)

      except Exception as e:
        print("exception when trying to display")
        print(e)

    # if self.world == 'subway':
    #   for wp in self.trajectory:  
    #     # color = (np.random.randint(0,255),np.random.randint(0,255),np.random.randint(0,255))
    #     color = (255,0,0)
    #     # cv2.circle(wm_col, wp, 5, color, thickness=1, lineType=8, shift=0)
    #     row, col = int(wp[1]/self.self.grid_size), int(wp[0]/self.self.grid_size)
    #     cv2.circle(wm_col,(row, col), 10, color, -1)
    #   wp = self.waypoint    
    #   row, col = int(wp[1]/self.self.grid_size), int(wp[0]/self.self.grid_size)
    #   cv2.circle(wm_col,(row, col), 10, (0,255,0), -1)
    #   wp = self.world_state['global_waypoint'] 
    #   row, col = int(wp[1]/self.self.grid_size), int(wp[0]/self.self.grid_size)
    #   cv2.circle(wm_col,(row, col), 10, (0,0,255), -1)
  
    # if not self.multi and self.rank == 0:
    #   for num in range(1, self.total_rank):  
    #     # print(self.other_robots)
    #     try:
    #       x,y= self.other_robots["_r" + str(num+1)]['pose'].x/self.grid_size, self.other_robots["_r" + str(num+1)]['pose'].y/self.grid_size
    #       q = self.other_robots["_r" + str(num+1)]['orn']
    #       yaw = np.arctan2(2.0 * (q.z*q.w + q.x*q.y) , - 1.0 + 2.0 * (q.w*q.w + q.x*q.x))
    #       pts = self.transform_rot_and_add(-1*yaw, [y+ (-self.min_y+1)/self.grid_size, x+(-self.min_x+1)/self.grid_size], rect_pts)        
    #       pts = pts[:, :2]
    #       cv2.polylines(wm_col, [pts], True, (0,255,0), 1)     
    #       cv2.circle(wm_col,(int(y + (-self.min_y+1)/self.grid_size), int(x +(-self.min_x+1)/self.grid_size)), 3, (0,0,255), -1)
    #     except Exception as e:
    #       print(e)
    #       print("No data from other robots")
    
    
    norm_hm = np.array(self.hm*255.0, dtype=np.uint8)
    norm_hm = cv2.rotate(norm_hm, cv2.ROTATE_180)
    px,py = self.hm.shape[0], self.hm.shape[1]

    # norm_hm = cv2.inRange(norm_hm, 20, 230)
    # norm_hm = cv2.dilate(norm_hm, np.ones((5,5), np.uint8) , iterations=1) 
    # death_square_count = np.sum(norm_hm[8:(px-28),8:(py-8)])/255
    # slight_death_square_count = np.sum(norm_hm[5:(px-20),4:(py-4)])/255 - np.sum(norm_hm[8:(px-28),8:(py-8)])/255 
    
    # rect_pts = np.array([[py-4,px-20],[4,px-20],[4,5],[py-4,5]])
    # cv2.polylines(norm_hm, [rect_pts], True, (0,255,0), 1)
    # rect_pts = np.array([[py-8,px-28],[8,px-28],[8,8],[py-8,8]])
    # cv2.polylines(norm_hm, [rect_pts], True, (0,0,255), 1)

    if self.rank == 0:
      cv2.imshow('world_map', wm_col)
      cv2.waitKey(1)
      # cv2.imshow('local_map', norm_hm)
      # cv2.waitKey(1)

  def get_world_map(self, box_info=None, grid_size=0.025):
    gz = False
    if gz:
      self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0]/2)
      self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1]/2)
      self.max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0]/2)
      self.max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1]/2)
    else:
      self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0])
      self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1])
      self.max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0])
      self.max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1])
    world_shape = [int((self.max_x - self.min_x + 2)/grid_size), int((self.max_y - self.min_y + 2)/grid_size)]

    hm = np.ones([world_shape[0], world_shape[1]]).astype(np.float32)*-1
    if box_info is not None:
      positions = box_info[1]
      sizes = box_info[2]
      for pos, size in zip(positions, sizes):
        x, y, z = pos[0]/grid_size, pos[1]/grid_size, pos[2]
        if gz:
          x_size, y_size, z_size = (size[0]/2)/grid_size, (size[1]/2)/grid_size, size[2]/2
        else:
          x_size, y_size, z_size = (size[0])/grid_size, (size[1])/grid_size, size[2]
        # print(x_size, y_size)
        ij = np.array([[i,j,1] for i in range(int(0-x_size), int(0+x_size)) for j in range(int(0-y_size), int(0+y_size))])

        hm_pts = self.transform_rot_and_add(1*pos[5], [x+(-self.min_x+1)/grid_size, y+(-self.min_y+1)/grid_size], ij)
        for pt in hm_pts:
          try:
            # Not sure why this doesn't work for larger z heights? Should be box z pos + box z size (half extent)
            # if hm[pt[0], pt[1]] < (z + z_size):
            #   hm[pt[0], pt[1]] = (z + z_size)
            if hm[pt[0], pt[1]] < (z * 2):
              hm[pt[0], pt[1]] = (z * 2)
          except Exception as e:
            print(e)
            print(pt)

    self.grid_size = grid_size              
    self.dx_forward, self.dx_back, self.dy = 36, 24, 20        
    self.ij = np.array([[i,j,1] for i in range(-self.dx_back, self.dx_forward) for j in range(-self.dy, self.dy)])
    self.hm_ij = np.array([[i,j] for i in range(0, self.dx_back + self.dx_forward) for j in range(0, self.dy + self.dy)])

    if self.doa:
      norm_wm = (np.min(hm) - hm)/(np.max(hm) - np.min(hm))
      self.doa_wm_col = cv2.cvtColor(norm_wm,cv2.COLOR_GRAY2RGB)
    return hm

  def transform_rot_and_add(self, yaw, pos, points):
    rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw), pos[0]],
            [np.sin(yaw), np.cos(yaw), pos[1]],
            [0, 0, 1]])
    return np.dot(rot_mat,points.T).T.astype(np.int32)

  def add_disturbance(self, max_dist=None):
    force_x = (random.random() - 0.5)*max_dist
    force_y = (random.random() - 0.5)*max_dist
    force_z = (random.random() - 0.5)*(max_dist/4)
    # print("applying forces", force_x, force_y, force_z)
    p.applyExternalForce(self.Id,-1,[force_x,force_y,force_z],[0,0,0],p.LINK_FRAME)
    force_roll = (random.random() - 0.5)*max_dist/2
    force_pitch = (random.random() - 0.5)*max_dist/2
    force_yaw = (random.random() - 0.5)*(max_dist/2)
    p.applyExternalTorque(self.Id,-1,[force_roll,force_pitch,force_yaw],p.LINK_FRAME)

  def log_stuff(self, logger, writer, iters_so_far):
    self.iters_so_far = iters_so_far
    writer.add_scalar("breakdown/goal", np.mean(self.reward_breakdown['goal']), self.iters_so_far)        
    writer.add_scalar("breakdown/pos", np.mean(self.reward_breakdown['pos']), self.iters_so_far)        
    writer.add_scalar("breakdown/vel", np.mean(self.reward_breakdown['vel']), self.iters_so_far)   
    writer.add_scalar("breakdown/tip", np.mean(self.reward_breakdown['tip']), self.iters_so_far)   
    writer.add_scalar("breakdown/com", np.mean(self.reward_breakdown['com']), self.iters_so_far)   
    writer.add_scalar("breakdown/neg", np.mean(self.reward_breakdown['neg']), self.iters_so_far)   
    writer.add_scalar("breakdown/sym", np.mean(self.reward_breakdown['sym']), self.iters_so_far)   
    writer.add_scalar("breakdown/act", np.mean(self.reward_breakdown['act']), self.iters_so_far)   
    writer.add_scalar("difficulty", self.difficulty, self.iters_so_far)   
    writer.add_scalar("height", self.height_coeff, self.iters_so_far)   
           
    # writer.add_scalar("breakdown/pos", self.reward_breakdown['pos'], self.iters_so_far)        
    # writer.add_scalar("breakdown/vel", self.reward_breakdown['vel'], self.iters_so_far)   
    # writer.add_scalar("breakdown/tip", self.reward_breakdown['tip'], self.iters_so_far)   
    # writer.add_scalar("breakdown/com", self.reward_breakdown['com'], self.iters_so_far)   
    # writer.add_scalar("breakdown/neg", self.reward_breakdown['neg'], self.iters_so_far)   
    # writer.add_scalar("breakdown/sym", self.reward_breakdown['sym'], self.iters_so_far)   
    writer.add_scalar("Kp", self.Kp, self.iters_so_far)
    logger.record_tabular("Kp", self.Kp)
    logger.record_tabular("max yaw", self.max_yaw)
    logger.record_tabular("pos error", np.mean(self.pos_error))
    if self.obstacle_type == 'path':
      logger.record_tabular("difficulty", self.difficulty)
    elif self.obstacle_type == 'stairs':
      logger.record_tabular("height_coeff", self.height_coeff)
    
