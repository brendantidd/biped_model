#!/bin/bash

# Terrains="flat jumps gaps stairs steps"


# mpirun -np 16 python3 run.py --folder eval_ --obstacle_type jumps --exp s3/jumps2 --difficulty 10 --stage3 --lr 1e-5 --initial_disturbance 1000 --final_disturbance 2000 --dist_difficulty 10 --max_ts 20000000 --eval_first
# wait

mpirun -np 16 python3 run.py --folder eval_ --obstacle_type jumps --exp s3/jumps3 --difficulty 10 --stage3 --lr 1e-5 --initial_disturbance 700 --final_disturbance 700 --dist_difficulty 10 --max_ts 20000000 --eval_first
wait

# Terrains="gaps stairs steps"

# declare -a Arguments=("_stage1_stage2")

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         mpirun -np 16 python3 run.py --folder eval_ --obstacle_type $terrain --exp "s12/$terrain" --difficulty 10 --stage3 --lr 1e-5 --initial_disturbance 500 --dist_difficulty 10 --max_ts 20000000 --eval_first
#         wait
#         mpirun -np 16 python3 run.py --folder eval_ --obstacle_type $terrain --exp "s3/$terrain" --difficulty 10 --stage3 --lr 1e-5 --initial_disturbance 2000 --dist_difficulty 10 --max_ts 20000000 --eval_first
#         wait
#     done
# done
