import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2
import json
import git
np.set_printoptions(precision=3, suppress=True)

def run(args):

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'
  DATA_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  # myseed = int(time.time() + 10000 * rank)
  myseed = int(args.seed + 10000 * rank)
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    with open(PATH + 'commandline_args.txt', 'w') as f:
      f.write('Hash:')
      f.write(str(sha) + "\n")
      json.dump(args.__dict__, f, indent=2)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.5)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
  
  
  # obstacle_types = ['gaps','base','jumps','stairs','zero']
  # obstacle_types = ['flat','high_jumps', 'gaps']
  obstacle_types = ['flat','high_jumps', 'gaps', 'jumps', 'stairs', 'steps']
  # obstacle_types = ['flat','high_jumps', 'gaps', 'jumps', 'stairs']
  # obstacle_types = ['flat','gaps', 'jumps', 'stairs', 'steps']
  # obstacle_types = ['flat','high_jumps']

  from assets.env import Env

  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=True, multi=args.multi, dqn=True, obstacle_types=obstacle_types, num_artifacts=args.num_artifacts, initial_disturbance=1000)
  # print(env.im_size)

  if not args.get_data:
    from models.classifier import Classifier
    classifier = Classifier(args, im_size=env.im_size, dense_size=128, sess=sess)
    initialize_uninitialized()

    classifier.load_data(obstacle_types, WEIGHTS_PATH= home + '/results/biped_model/latest/classifier/data/')
    classifier.run_train(writer, PATH)
    
  else:

    from models.ppo import Model

    horizon = 2048


    pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis) for name in obstacle_types if name != 'zero'}  

    if args.hpc:
      WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
    else:
      # WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
      WEIGHTS_PATH = './weights'
    
    for name in obstacle_types:
      pol[name].load_pol(base_name=name, WEIGHTS_PATH=WEIGHTS_PATH + '/' + name + '/')

    initialize_uninitialized()


    # Throw an error if the graph grows (shouldn't change once everything is initialised)
    # tf.get_default_graph().finalize()

    prev_done = True
    env.difficulty = args.difficulty
   
    data_size = 20000
    t2 = time.time()

    for ob_type in obstacle_types:
      t1 = time.time()
      if not os.path.exists(DATA_PATH + ob_type):
        os.makedirs(DATA_PATH + ob_type)
      env.obstacle_type = ob_type
      env.height_coeff = args.height_coeff
      prev_done = True
      ob = env.reset()
      im = env.get_im()
      ep_ret = 0
      ep_len = 0
      ep_rets = []
      ep_lens = []
      ep_steps = 0
      ep_count = 0

      flat_inputs = []
      flat_labels = []

      inputs = []
      labels = []
      count = 0

      if args.render and args.debug:
        replace_Id2 = p.addUserDebugText(ob_type,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)

      while True:
        act, _, _, _ = pol[ob_type].step(ob, im, stochastic=False)

        if args.render and args.debug:
          replace_Id2 = p.addUserDebugText(ob_type,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)

        torques = act
        
        next_ob, rew, done, _ = env.step(torques)
        next_im = env.get_im()
        
        # print(env.order)
        
        if count % 5000 == 0 and count != 0:
          print("count ", count, "of ", data_size, "for ", ob_type)


        if env.order[env.box_num] == 'zero':
          flat_inputs.append(next_im)
          flat_labels.append(obstacle_types.index('flat'))
        else:
          if env.order[env.box_num] == 'flat':
            flat_inputs.append(next_im)
            flat_labels.append(obstacle_types.index('flat'))
          else:
            inputs.append(next_im)
            labels.append(obstacle_types.index(env.order[env.box_num]))
          if env.order[env.box_num] == ob_type:
            count += 1
            if count > data_size:
              if env.order[env.box_num] != 'flat':
                print("saving weights for ", ob_type, "count ", count, "total ", len(labels), "time", time.time() - t1)
                np.save(DATA_PATH + ob_type + "/inputs.npy", inputs)
                np.save(DATA_PATH + ob_type + "/labels.npy", labels)
              else:
                print("pretending to save weights for ", ob_type, "count ", count, "total ", len(flat_labels))
              break

        ob = next_ob
        im = next_im
        ep_ret += rew
        ep_len += 1

        if done:         
          ob = env.reset()
          im = env.get_im()

          ep_rets.append(ep_ret)  
          ep_lens.append(ep_len)     
          ep_ret = 0
          ep_len = 0
          ep_count += 1  
    
    print("saving flat weights ", len(flat_labels), "time", time.time() - t2)
    np.save(DATA_PATH + "flat/inputs.npy", flat_inputs)
    np.save(DATA_PATH + "flat/labels.npy", flat_labels)

if __name__ == '__main__':
  
  # My hack for loading defaults, but still accepting run specific defaults
  import defaults
  from dotmap import DotMap
  args1 = defaults.get_defaults() 
  parser = argparse.ArgumentParser()
  # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
  parser.add_argument('--folder', default='classifier')
  parser.add_argument('--exp', default='test')
  parser.add_argument('--difficulty', default=10, type=int)
  parser.add_argument('--get_data', default=False, action='store_true')
  parser.add_argument('--num_artifacts', default=2, type=int)
  parser.add_argument('--camera_rate', default=1, type=int)
  args2, _ = parser.parse_known_args()
  args2 = vars(args2)
  for key in args2:
    args1[key] = args2[key]
  args = DotMap(args1)
  # os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)