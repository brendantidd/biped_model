import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2

def run(args):

  if args.vis:
    args.folder += "_vis"

  PATH = home + '/results/biped_model/latest/switch/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  # horizon = 2048
  # horizon = 64
  horizon = 32

  if args.multi_robots:
    from assets.env_multi import Env
  else:
    from assets.env import Env

  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False)

  from models.ppo_vf import Model
  from models.doa import RoANet
  from models import ppo_roa
  
  obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
  start_obstacle_types = ['gaps', 'base', 'jumps','stairs']
  if args.obstacle_type != 'zero':
    start_obstacle_types.remove(args.obstacle_type)

  pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}  

  switch_pol = ppo_roa.Model(args.obstacle_type + "_switch", env=env, ob_size=env.ob_size, ac_size=1, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=args.lr)

  if args.test_pol:
    roa = RoANet(args.obstacle_type, args=args, ob_size=env.ob_size, im_size=env.im_size[0])
  
  initialize()
  sync_from_root(sess, switch_pol.vars, comm=comm)

  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
  else:
    WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
  
  for name in obstacle_types:
    if name == 'zero': continue
    pol[name].load(WEIGHTS_PATH + '/' + name + '/')
  if args.test_pol:
    # roa.load(home + '/results/biped_model/latest/' + args.folder + '/' + args.obstacle_type + '18/best/')
    # roa.load(home + '/results/biped_model/latest/' + args.folder + '/' + args.obstacle_type + '/best/')
    roa.load(home + '/results/biped_model/latest/' + args.folder + '/' + args.obstacle_type + '_128/best/')
  
  env.difficulty = args.difficulty
  env.height_coeff = args.height_coeff

  if args.render:
    eval_length = 100
  else:
    eval_length = 10000

  # data_length = 5000
  data_length = 7500

  prev_done = True
  ob = env.reset()
  im = env.get_im()

  if args.test_pol:
    labels = []
    baseline_labels = []
  else:
    labels = np.zeros([data_length, 1])
  data_pointer = 0

  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  ep_steps = 0

  terrain_data = []
  switch_data = []

  episode_count = 0
  
  switch_step = None
  current_pol = np.random.choice(start_obstacle_types) 

  if args.render and args.debug:
    replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)

  obs = []
  imgs = []
  hms = []

  current_pol_data = []

  t1 = time.time()
  t2 = time.time()
  count = 0
  initial_pos = None
  failed_to_switch = False
  
  baseline_stats = {'zero':0.1800, 'base':0.9600, 'stairs':0.8700, 'jumps': 0.61 , 'gaps':0.5200}
  baseline_stat = baseline_stats[args.obstacle_type]
  if args.test_pol: 
    stochastic = False
  else:
    stochastic = True
  if args.multi_robots:
    robot = {}
  
  box_cross_step = switch_step = None
  prev_switched = switched = False
  # switch_success = []
  print("Getting goal set for ", current_pol, " in terrain type for ", current_pol, ", recording values for stable regions in velocity range. Collecting ", eval_length, " samples for each")
  vpred, nlogp = 0,0
  switch_thres = 0.5
  switch_history = deque(maxlen=120*horizon)
  max_disturbance = 750 

  while True:

    if not switch_step and ((args.obstacle_type == 'base' and env.box_num > 4 and args.obstacle_type == env.order[env.box_num]) or (args.obstacle_type in ['gaps','jumps','stairs','zero'] and args.obstacle_type == env.order[env.box_num])):
      if not box_cross_step:
        box_cross_step = env.steps
      if args.test_pol:
        switch_act = roa.step(ob,im)
        if switch_act > switch_thres:
          switch_step = env.steps
      elif args.alternate:
        if episode_count%2 == 0:    
          switch_step = np.random.randint(0,120) + env.steps
        else:
          switch_act = switch_pol.get_value(ob,im)[0][0]
          if switch_act > switch_thres:
            switch_step = env.steps
      else:
        switch_step = np.random.randint(0,120) + env.steps
      
    if not switched and ((switch_step and env.steps == switch_step ) or (box_cross_step and env.steps == (box_cross_step + 120))):
      current_pol = args.obstacle_type
      current_ob = ob
      current_im = im
      initial_pos = [env.body_xyz[0], env.body_xyz[1], env.z_offset]
      initial_heading = env.yaw
      switched = True

    if not args.test_pol and args.vis and np.random.random() < 0.2 and env.vx > 0.4 and (not box_cross_step or (box_cross_step and (switch_step - 10 > env.steps))):
      # print("disturbing")
      if args.debug:
        replace_Id2 = p.addUserDebugText("disturbing",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.7],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
      env.add_disturbance(max_disturbance, forward_only=True)
    if args.test_pol and args.vis and box_cross_step and not switched:
      env.add_disturbance(max_disturbance, forward_only=True)

    if current_pol == 'zero':
      zero_ob = copy.copy(ob)
      zero_ob[-2] = 0
      act, _, _, _ = pol['base'].step(zero_ob, im, stochastic=False)
    else:
      act, _, _, _ = pol[current_pol].step(ob, im, stochastic=False)
    
    terrain_data.append(env.z_offset)
    if box_cross_step and box_cross_step + 120 >= env.steps:
      if args.test_pol:
        switch_thing = roa.step(ob, im)
        switch_data.append(switch_thing)
      else:
        switch_thing = switch_pol.get_value(ob, im)[0][0]
        switch_data.append(switch_thing)
      switch_history.append(switch_thing)
    else:
      switch_data.append(0)

    torques = act
    
    next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_im()

    if (args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300):
      done = True

    rew = 0
    if done:
      if args.vis:
        # if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)) and (abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.2 and  (env.body_xyz[2] - env.z_offset) > 0.94 ):
        if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)):
          label = True  
          colour = [0.1,0.9,0.1,0.5]
          rew = 1
          # print(rew)
        else:
          label = False
          colour = [0.9,0.1,0.1,0.5]
      else:
        # print(abs(env.body_xyz[1]), abs(env.yaw), env.body_xyz[2] - env.z_offset)
        # if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)) and (abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.2 and (env.body_xyz[2] - env.z_offset) > 0.94 ):
        if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)):
          label = True  
          colour = [0.1,0.9,0.1,0.5]
          rew = 1
        else:
          label = False
          colour = [0.9,0.1,0.1,0.5]
    # print(done, not prev_switched, switched)
    # if not args.test_pol and not prev_switched and switched:
    # if not args.test_pol and switched and done:
    if switched and done:
    # if not args.test_pol and done:
      # switch_pol.add_to_buffer([ob, im, switch_act, rew, prev_done, vpred, nlogp])
      # if episode_count%2 == 0:    
      switch_pol.add_to_buffer([current_ob, current_im, label])
      ep_steps += 1
      # print(ep_steps)
    prev_done = done
    ob = next_ob
    im = next_im

    if done:
      if args.alternate:
        if not episode_count%2 == 0:
          ep_ret += rew
          ep_len += 1 
      else:
        ep_ret += rew
        ep_len += 1

    if not args.test_pol and not prev_switched and switched and ep_steps != 0 and ep_steps % horizon == 0:
    # if not args.test_pol and switched and ep_steps != 0 and ep_steps % horizon == 0:
      # print(ep_steps, np.array(switch_pol.data['ob']).shape)
      # print(ep_rets)
      # _, vpred, _, _ = switch_pol.step(next_ob, next_im, stochastic=True)
      # switch_pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens})  
      # if episode_count%2 == 0:  
      switch_pol.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []

    if args.render and args.debug:
      replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
    
    if args.test_pol and episode_count > 400:
      print("Data: ", len(labels), "of ", data_length,  "breakdown: ", round(np.mean(labels),2), "baseline: ", baseline_stat, "time: ", round(time.time() - t1,2), "total: ", round(time.time() - t2,2))
      break

    current_pol_data.append(obstacle_types.index(current_pol))
    prev_switched = switched


    if done:  
      
      if args.render and initial_pos is not None and args.debug:
        visBoxId = p.createVisualShape(p.GEOM_SPHERE, radius=0.1, rgbaColor=colour)
        box_id = p.createMultiBody(baseMass=0, baseVisualShapeIndex=visBoxId, basePosition=initial_pos, baseOrientation= p.getQuaternionFromEuler([0.0,1.5709,initial_heading]))

      if rank == 0 and episode_count % horizon == 0:
      # if rank == 0:
        print("new switch_thres", switch_thres, " returns ", rew)
        num_axes = 4
        fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
        axes[0].plot([_ for _ in range(len(switch_data))], switch_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[0].set_ylim([-0.1,1.1])

        sample_size = 128
        sample_obs, sample_imgs, sample_labels = switch_pol.sample(sample_size)
        if args.test_pol:
          sample_preds = roa.multi_step(sample_obs, sample_imgs)        
          # print("samples", sample_preds.shape)
        else:
          sample_preds = switch_pol.get_value(sample_obs, sample_imgs)        

        axes[1].plot([_ for _ in range(sample_obs.shape[0])], sample_preds, c=np.array([204, 102, 0])/255.0, alpha=1.0)  
        axes[1].plot([_ for _ in range(sample_obs.shape[0])], sample_labels, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[1].legend(['preds', 'labels'])
        axes[1].set_ylim([-0.1,1.1])

        axes[2].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([204, 102, 0])/255.0, alpha=1.0)  

        axes[3].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[3].set_title("terrain height", loc='left')
        fig.tight_layout(pad=4.0)
        if current_pol_data[0] == 0:
          plt.savefig(PATH + 'switch_gaps.png', bbox_inches='tight')
        elif current_pol_data[0] == 1:
          plt.savefig(PATH + 'switch_base.png', bbox_inches='tight')
        elif current_pol_data[0] == 2:          
          plt.savefig(PATH + 'switch_jumps.png', bbox_inches='tight')
        elif current_pol_data[0] == 3:          
          plt.savefig(PATH + 'switch_stairs.png', bbox_inches='tight')

      if args.multi_robots:
        robot['success'] = label

      current_pol = np.random.choice(start_obstacle_types) 

      terrain_data = []
      current_pol_data = []
      switch_data = []

      if args.multi_robots:
        ob = env.reset(robot=robot)
        robot = {}
      else:
        ob = env.reset()
      im = env.get_im()
      if args.alternate:
        if not episode_count%2 == 0: 
          ep_rets.append(ep_ret)  
          ep_lens.append(ep_len)     
      else:
        ep_rets.append(ep_ret)  
        ep_lens.append(ep_len)     
      
      ep_ret = 0
      ep_len = 0
      initial_pos = None
      episode_count += 1
      
      # switch_thres = 0.9*max(switch_history)
      
      prev_switched = switched = False
      box_cross_step = switch_step = None



  print("-------------------------------------------------------------------------------------------------------")
  print("Data: {0:4d} of {1:4d} time: {2:4.2f} total: {3:5.2f}".format(data_pointer,  data_length, round(time.time() - t1,2), round(time.time() - t2,2)))
  # print("{0:s} thres: {1:0.2f} breakdown: {2:0.2f}".format(args.obstacle_type, args.roa_thres, round(np.mean(labels[:data_pointer,0]),2)))
  print("{0:s} thres: {1:0.2f} breakdown: {2:3.2f} baseline: {3:3.2f}".format(args.obstacle_type, args.roa_thres, round(np.mean(labels),2), baseline_stat))
  print("-------------------------------------------------------------------------------------------------------")


if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--sub_folder', default='b')
  # parser.add_argument('--folder', default='b4')
  # parser.add_argument('--folder', default='b9')
  parser.add_argument('--folder', default='b10')
  # parser.add_argument('--folder', default='b10_vis')

  parser.add_argument('--difficulty', default=10, type=int)
  parser.add_argument('--height_coeff', default=0.07)
  # parser.add_argument('--roa_thres', default=0.8, type=float)
  parser.add_argument('--roa_thres', default=0.85, type=float)

  parser.add_argument('--cur_len', default=1000, type=int)
  parser.add_argument('--inc', default=2, type=int)

  parser.add_argument('--alternate', default=False, action='store_true')
  parser.add_argument('--debug', default=False, action='store_true')
  parser.add_argument('--multi_robots', default=False, action='store_true')
  parser.add_argument('--single_pol', default=True, action='store_false')
  parser.add_argument('--full', default=False, action='store_true')
  parser.add_argument('--push', default=False, action='store_true')
  parser.add_argument('--no_rand', default=False, action='store_true')
  parser.add_argument('--centre', default=False, action='store_true')
  parser.add_argument('--use_roa', default=False, action='store_true')
  parser.add_argument('--no_reg', default=False, action='store_true')
  parser.add_argument('--base_only', default=False, action='store_true')
  parser.add_argument('--baseline', default=False, action='store_true')
  parser.add_argument('--use_goal_set', default=False, action='store_true')

  parser.add_argument('--use_vf', default=False, action='store_true')
  parser.add_argument('--multi', default=True, action='store_false')
  parser.add_argument('--goal_set', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')
  parser.add_argument('--gae', default=False, action='store_true')
  parser.add_argument('--doa', default=False, action='store_true')

  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--vis', default=False, action='store_true')
  # parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="domain_train_no_stairs", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=1e5, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  # parser.add_argument('--lr', default=1e-3, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)