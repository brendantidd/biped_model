import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import pybullet as p


def run(args):

  PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from assets.env import Env
  env = Env(render=args.render, PATH=PATH, args=args, cur=args.cur, obstacle_type=args.obstacle_type, control_type=args.control_type, display_im=args.display_im, vis=args.vis, speed_cur=args.speed_cur, disturbances=False)

  from models.ppo_vf import Model


  pol = Model(args.obstacle_type, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  base_pol = Model('base', env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  
  initialize()
  sync_from_root(sess, pol.vars, comm=comm)
  if args.separate_vf:
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, vf_learning_rate=args.vf_lr, horizon=horizon)
  else:
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if args.test_pol:
    if args.hpc:
      pol.load(home + '/hpc-home/results/biped_model/latest/' + args.obstacle_type + '/')      
      # pol.load(home + '/hpc-home/results/biped_model/weights/w6/base_speed/')      
      # pol.load(home + '/hpc-home/results/biped_model/weights/w5/b/' + args.obstacle_type + '/')      
    else:
      if args.obstacle_type == 'zero':
        pol.load(home + '/results/biped_model/weights/' + args.folder + '/base/')      
        base_pol.load(home + '/results/biped_model/weights/' + args.folder + '/base/')      
      else:
        pol.load(home + '/results/biped_model/weights/' + args.folder + '/'+ args.obstacle_type +'/')      
        base_pol.load(home + '/results/biped_model/weights/' + args.folder + '/base/')      

      # pol.load(PATH)


  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()
  # print(env.ob_size)

  prev_done = True
  data_length = 200000

  ob = env.reset()
  im = env.get_im()
  # labels = []
  # input_ob = [ob]
  # input_im = [im]
  input_ob = np.zeros([data_length, env.ob_size])
  input_im = np.zeros([data_length] + env.im_size)
  labels = np.zeros([data_length, 1])
  data_pointer = 0

  if args.get_goal_set:
    goal_set = np.zeros([120, env.ob_size+6]) 
    goal_count = 0
    switch_time = 120
  else:
    switch_time = np.random.randint(250,400)

  obs = []
  imgs = []

  t1 = time.time()
  t2 = time.time()
  stochastic = False
  max_disturbance = 2000
  num_fell = 0
  count = 0
  initial_pos = None
  while True:
    
    if args.single_pol and ((env.obstacle_type == 'stairs' and env.box_num < 7) or (env.obstacle_type in ['gaps','jumps'] and env.box_num < 8)) or (env.obstacle_type == 'zero' and env.steps == switch_time) or env.obstacle_type == 'base':
      # if args.obstacle_type == 'zero' and env.box_num > 10:
      #   env.original_speed = 0
      #   obs.append(ob)
      #   imgs.append(im)
      # else:
      obs.append(ob)
      imgs.append(im)
    # if args.render and env.box_num >= 6 and initial_pos is None:
    if args.render and env.box_num == 7 and initial_pos is None:

      initial_pos = [env.body_xyz[0], env.body_xyz[1], 1.0]
      initial_heading = env.yaw
  
    if not args.single_pol and env.steps == switch_time:
      # print(switch_time, "switched")
      start_ob = ob
      start_im = im
      if args.render:
        initial_pos = [env.body_xyz[0], env.body_xyz[1], 1.0]
        initial_heading = env.yaw

    if args.get_goal_set and goal_count == 0 and env.steps < 120:
      goal_set[env.steps, :] = np.concatenate([ob, np.array([env.body_xyz[0], env.body_xyz[1],env.body_xyz[2], env.roll, env.pitch, env.yaw])])

    if args.get_goal_set or args.single_pol:
      act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
    else:
      if env.steps < switch_time:
        if switch_time - env.steps == 10:
          env.add_disturbance(max_disturbance)
        act, vpred, _, nlogp = base_pol.step(ob, im, stochastic=stochastic)
      else:
        act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
    
    if args.single_pol and ((env.obstacle_type == 'stairs' and env.box_num < 5) or (env.obstacle_type in ['gaps','jumps'] and env.box_num < 5)) or (env.obstacle_type == 'zero' and env.steps == switch_time) or env.obstacle_type == 'base':
      # torques = act + np.random.uniform(-2.5,2.5, size=env.ac_size) 
      torques = act
    else:
      torques = act
    next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_im()
    

    prev_done = done
    ob = next_ob
    im = next_im     

    if count % 1000 == 0:
      print("Data: ", data_pointer, "of ", data_length, "fell: " , num_fell,  "breakdown: ", round(np.mean(labels[:data_pointer,0]),2), "time: ", round(time.time() - t1,2), "total: ", round(time.time() - t2,2))
      # print(labels[:data_pointer,0])
      t1 = time.time()

      np.save(PATH + 'input_ob.npy', input_ob[:data_pointer,:])
      np.save(PATH + 'input_im.npy', input_im[:data_pointer,:])
      np.save(PATH + 'labels.npy', labels[:data_pointer,:])
    count += 1

    if done:
      if (env.box_num > 12 and abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.5)  or (args.obstacle_type == 'zero' and abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.5 and env.steps > switch_time):
        label = True  
        colour = [0.1,0.9,0.1,0.5]
      else:
        label = False
        colour = [0.9,0.1,0.1,0.5]

      if args.get_goal_set:
        if goal_count == 0:
          np.save(PATH + 'goal_set.npy', goal_set)
        ob = env.reset(goal_set=goal_set[goal_count,:])
        print(np.mean(labels[:goal_count,:]))
        goal_count += 1
        if goal_count >= 120:
          exit()
          goal_count = 0
      else:
        if env.steps < switch_time and not args.single_pol:

          num_fell += 1
          # print(num_fell)
          ob = env.reset()
          im = env.get_im()
        else:    
          switch_time = np.random.randint(250,400)
          # switch_time = 400
          # switch_time = np.random.randint(100,300)
          ob = env.reset()
          im = env.get_im()
          # labels.append(env.label)
          # if data_pointer % 50 == 0:
          # if data_pointer % 50 == 0:
          #   print("Data: ", data_pointer, "of ", data_length, "fell: " , num_fell,  "breakdown: ", np.mean(labels[:data_pointer,0]), "time: ", time.time() - t1, "total: ", time.time() - t2)
          #   # print(labels[:data_pointer,0])
          #   t1 = time.time()

          #   np.save(PATH + 'input_ob.npy', input_ob[:data_pointer,:])
          #   np.save(PATH + 'input_im.npy', input_im[:data_pointer,:])
          #   np.save(PATH + 'labels.npy', labels[:data_pointer,:])
          if data_pointer > data_length:
            np.save(PATH + 'input_ob.npy', input_ob)
            np.save(PATH + 'input_im.npy', input_im)
            np.save(PATH + 'labels.npy', labels)
            break
          # print(labels)

    
      if args.render and initial_pos is not None:
      # if args.render and not args.single_pol:
        visBoxId = p.createVisualShape(p.GEOM_SPHERE, radius=0.1, rgbaColor=colour)
        box_id = p.createMultiBody(baseMass=0, baseVisualShapeIndex=visBoxId, basePosition=initial_pos, baseOrientation= p.getQuaternionFromEuler([0.0,1.5709,initial_heading]))

      if not args.single_pol:
        input_ob[data_pointer,:] = start_ob
        input_im[data_pointer,:] = start_im
        labels[data_pointer,:] = label
        data_pointer += 1
      else:
        # print(len(obs))
        if len(obs) > 0:
          idx = min(data_pointer+len(obs), data_length) - data_pointer
          # print(idx, len(obs[:idx]), len(obs))
          input_ob[data_pointer:data_pointer+idx] = obs[:idx]
          input_im[data_pointer:data_pointer+idx] = imgs[:idx]
          labels[data_pointer:data_pointer+idx] = label
          data_pointer += len(obs)
        obs = []
        imgs = []
      initial_pos = None
      
      # print(input_im[data_pointer,:])




if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--arrows', default=True, action='store_false')
  parser.add_argument('--get_goal_set', default=False, action='store_true')
  parser.add_argument('--single_pol', default=False, action='store_true')

  parser.add_argument('--cur_len', default=1000, type=int)
  parser.add_argument('--inc', default=1, type=int)

  parser.add_argument('--folder', default='b9')

  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--vf_only', default=False, action='store_true')
  parser.add_argument('--speed_cur', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--use_base', default=False, action='store_true')
  parser.add_argument('--separate_vf', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  # parser.add_argument('--test_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=True, action='store_false')
  parser.add_argument('--vis', default=False, action='store_true')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="base", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=10e7, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  # parser.add_argument('--vf_lr', default=1e-2, type=float)
  parser.add_argument('--vf_lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)