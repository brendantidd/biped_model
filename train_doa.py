import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt

def run(args):

  PATH = home + '/results/biped_model/latest/d/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  from assets.env import Env
  # env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, doa=True)
  env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False)

  if args.lstm_pol:
    from models.ppo_lstm import Model
    # from models.ppo_lstm_old import Model
  else:
    from models.ppo import Model
    from models.ppo_vf import VFModel
  
  # obstacle_types = ['gaps', 'jumps', 'base']
  obstacle_types = ['gaps', 'base', 'jumps']

  if args.multi:
    pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types}
    
    vf = {name:VFModel(name + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types}
  else:
    pol = Model(args.exp, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
    
    vf = VFModel(args.exp + "_vf", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis)
  # from models.doa import DoANet
  # doa_net = DoANet(name="doa_" + args.obstacle_type, args=args, ob_size=env.ob_size, im_size=env.im_size)

  initialize()
  # sync_from_root(sess, doa_net.vars, comm=comm)
  # doa_net.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)
  if not args.multi:
    vf.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if args.hpc:
    WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
  else:
    WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
  
  if args.multi:
    for name in obstacle_types:
      pol[name].load(WEIGHTS_PATH + '/' + name + '/')
      vf[name].load_base(pol[name].name, WEIGHTS_PATH + '/' + name + '/') 
  else:
    pol.load(WEIGHTS_PATH + '/' + args.exp + '/')
    vf.load_base(pol.name, WEIGHTS_PATH + '/' + args.exp + '/') 


  # if args.test_pol:
  #   doa_net.load(PATH)

  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()

  prev_done = True

  # env.obstacles.train_world()

  # print(env.ob_size)
  if args.goal_set:
    obstacle_count = 0
    env.obstacle_type = obstacle_types[obstacle_count]
  env.difficulty = args.difficulty
  prev_done = True
  ob = env.reset()
  im = env.get_im()
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  ep_steps = 0
  timesteps_so_far = 0
  stochastic = False

  # iters_so_far = 0
  # t1 = time.time()
  # plot_data = []
  vpred_data = []
  probs_data = []
  stable_vpred = []
  terrain_data = []
  current_pol_data = []
  stable_count = 120
  # all_vpred_data = []
  # slope_data = []
  # prev_vpred = 0
  # prev_prob = 0
  # prob = 0
  # death = False
  # death_countdown = None
  # slope = deque(maxlen=5)
  # ep_count = 0
  
  # obstacle_types = ['gaps', 'jumps', 'base']
  # msv   = [121, 28.7, 62]
  # means = [147, 115, 165]
  # stds  = [15, 10.9, 14.7]

  # obstacle_types = ['gaps', 'base', 'jumps']
  msv   = [121, 62, 28.7]
  means = [147, 165, 115]
  stds  = [15, 14.7, 10.9]

  prev_pol = current_pol = 2
  # prev_pol = current_pol = 0
  while True:
    if timesteps_so_far > args.max_ts:
      break 
    if args.multi:
      act, _, _, nlogp = pol[obstacle_types[current_pol]].step(ob, im, stochastic=stochastic)
      vpreds = []
      probs = []
      for num, name in enumerate(obstacle_types):
        vpred = vf[name].get_value(ob, im)
        vpreds.append(vpred)
        probs.append(normpdf(vpred, means[num], stds[num]))
        

      max_prob_idx = np.argmax(probs)
      if probs[max_prob_idx] > 0.0001:
        current_pol = max_prob_idx
        # current_pol = 1
      # if vpreds[current_pol] < msv[current_pol]:
      #   current_pol = np.argmax(vpreds)
        # print("switching pols", vpreds, "new pol", obstacle_types[current_pol])
    else:
      act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
      test_vf = vf.get_value(ob, im)
      vpreds = vpred
    # print(test_vf, vpred)
    current_pol_data.append(current_pol)
    vpred_data.append(vpreds)
    if args.multi:
      probs_data.append(probs)  

    if not args.multi and env.steps > stable_count and vpred_data[-1] > vpred_data[env.steps - stable_count]:
      # if vpred > vpred_data[env.steps - stable_count]:
      stable_vpred.append(vpred_data[env.steps - stable_count])
    terrain_data.append(env.z_offset)

    torques = act



    # if args.test_pol:
    #   vpred_data.append(vpred)
    #   prob = calc_probs(vpred, all_vpred_data)
    #   if env.steps == 0:
    #     prev_vpred = vpred
    #     prev_prob = prob
    #   death = prob < -4 and prob < prev_prob
    #   plot_data.append(death)
    #   # slope.append(vpred - prev_vpred)
    #   # slope_data.append(max(slope))
    #   slope_data.append(vpred - prev_vpred)
    
    next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_im()


    # if death and death_countdown is None and env.steps > 10:
    #   death_countdown = env.steps
    #   print("gonna die!", env.steps)
    #   env.speed = 0.0
    # print(args.test_pol, args.multi, not args.test_pol or not args.multi)
    if not args.test_pol and not args.multi:
      vf.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])

    prev_done = done
    ob = next_ob
    im = next_im
    ep_ret += rew
    ep_len += 1
    ep_steps += 1
    
    # prev_vpred = vpred
    # prev_prob = prob
    prev_pol = current_pol

    if not args.test_pol and not args.multi and ep_steps % horizon == 0:
      vpred = vf.get_value(next_ob, next_im)
      vf.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
      vf.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []
    
    # if not args.test_pol and doa_net.buffer.size == doa_net.buffer.buffer_size and ep_steps % horizon == 0:
    # if not args.test_pol and ep_steps % horizon == 0:
    #   if args.gae:
    #     last_dpred = doa_net.step(ob, im)
    #     loss = doa_net.train(last_dpred, done)
    #   else:
    #     loss = doa_net.train()
    #   if rank == 0:
    #     print("Iterations {0:d} Loss {1:.6f} Time {2:.4f}".format(iters_so_far, loss, time.time() - t1))
    #     t1 = time.time()
    #     doa_net.save(PATH)
    #     writer.add_scalar("loss", loss, iters_so_far)   
    #   iters_so_far += 1
    # print(env.speed)
    if done:    
      # slope = deque(maxlen=5)
      # if death_countdown is None:
      #   print("didn't predict death")
      # else:
      #   print("predicted death", env.steps - death_countdown, "steps ago")
      # if args.test_pol:
      #   if len(vpred_data) > 150:
      #     all_vpred_data.extend(vpred_data[:-150])
      #   print(len(all_vpred_data))
      #   probs = calc_probs(vpred_data, all_vpred_data)
      #   if args.plot:
        # fig = plt.figure()
        # ax=fig.add_subplot(121)
      if not args.multi:
        num_axes = 3
      else:    
        num_axes = 6    
      fig, axes = plt.subplots(num_axes,  figsize=(10, 3*num_axes))
      # slope_data = reject_outliers(np.array(slope_data))
      axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)  
      if not args.multi:
        axes[1].plot([_ for _ in range(len(stable_vpred))], stable_vpred, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        if stable_vpred:
          print("min stable value", min(stable_vpred), np.mean(stable_vpred), np.std(stable_vpred))
        axes[2].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  

      else:
      #   axes[0].plot([_ for _ in range(len(vpred_data))], [msv[0]]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='--', alpha=1.0)  
      #   axes[0].plot([_ for _ in range(len(vpred_data))], [msv[1]]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='--', alpha=1.0)  
      #   axes[0].plot([_ for _ in range(len(vpred_data))], [msv[2]]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='--', alpha=1.0)  
        # axes[0].plot([_ for _ in range(len(vpred_data))], [means[0] - stds[0]]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='--', alpha=1.0)  
        # axes[0].plot([_ for _ in range(len(vpred_data))], [means[1] - stds[1]]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='--', alpha=1.0)  
        # axes[0].plot([_ for _ in range(len(vpred_data))], [means[2] - stds[2]]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='--', alpha=1.0)  
        axes[0].plot([_ for _ in range(len(vpred_data))], [means[0]]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
        axes[0].plot([_ for _ in range(len(vpred_data))], [means[1]]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
        axes[0].plot([_ for _ in range(len(vpred_data))], [means[2]]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
        axes[0].plot([_ for _ in range(len(vpred_data))], [means[0] - 2*stds[0]]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='--', linewidth=1.0, alpha=0.2)  
        axes[0].set_title("Values (cost to go)", loc='left')

        axes[0].plot([_ for _ in range(len(vpred_data))], [means[1] - 2*stds[1]]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='--', linewidth=1.0, alpha=0.2)  
        axes[0].plot([_ for _ in range(len(vpred_data))], [means[2] - 2*stds[2]]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='--', linewidth=1.0, alpha=0.2)  
        axes[0].legend(obstacle_types)  
        print(np.array(current_pol_data).shape, len(current_pol_data))
        axes[1].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[1].set_ylim([-0.9,2.1])
        for s, n in enumerate(obstacle_types):
          axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
        axes[1].set_title("Current policy", loc='left')
        
        # probs_data = -1*np.abs(probs_data)
        # print(probs_data)
        axes[2].plot([_ for _ in range(len(probs_data))], probs_data, alpha=1.0)  
        axes[2].legend(obstacle_types)  
        axes[2].set_title("Likelihood in goal set (higher is better)", loc='left')

        axes[3].plot([_ for _ in range(len(probs_data))], logp(np.array(vpred_data), np.array(means), np.array(stds)), alpha=1.0)  
        axes[3].legend(obstacle_types)  
        axes[3].set_title("Log Likelihood in goal set (higher is better)", loc='left')
        
        axes[4].plot([_ for _ in range(len(probs_data))], mahalanobois_distance(np.array(vpred_data), np.array(means), np.array(stds)), alpha=1.0)  
        axes[4].legend(obstacle_types)  
        axes[4].set_title("Mahalanobois distance from goal set (lower is better)", loc='left')

        # axes[2].plot([_ for _ in range(len(vpred_data))], [normpdf(msv[0], means[0], stds[0])]*len(vpred_data), c=np.array([0, 102, 204])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
        # axes[2].plot([_ for _ in range(len(vpred_data))], [normpdf(msv[1], means[1], stds[1])]*len(vpred_data), c=np.array([255,165,0])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
        # axes[2].plot([_ for _ in range(len(vpred_data))], [normpdf(msv[2], means[2], stds[2])]*len(vpred_data), c=np.array([51, 204, 51])/255.0, linestyle='-', linewidth=1.0, alpha=0.4)  
        axes[5].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)  
        axes[5].set_title("terrain height", loc='left')

      fig.tight_layout(pad=4.0)


      # axes[0].plot([_ for _ in range(len(plot_data))], plot_data, c='b', alpha=1.0)  
      # axes[0].set_ylim([0,1])
      # axes[1].plot([_ for _ in range(len(vpred_data))], vpred_data, c='b', alpha=1.0)  
      # axes[2].plot([_ for _ in range(len(slope_data))], slope_data, c='b', alpha=1.0)  
      
      # axes[3].plot([_ for _ in range(len(probs))], probs, c='b', alpha=1.0)  
      plt.show()
      #   plot_data = []
      vpred_data = []
      probs_data = []
      # stable_vpred = []
      terrain_data = []
      current_pol_data = []
      #   slope_data = []
      # ep_count += 1
      # death_countdown = None
      # prev_vpred = 0
      # if args.test_pol:
      #   obstacle_count += 1
      #   if obstacle_count > len(obstacle_types):
      #     exit()
      # else:
      #   obstacle_count = np.random.randint(0,3)
      # env.obstacle_type = obstacle_types[obstacle_count]
      
      ob = env.reset()
      im = env.get_im()
      ep_rets.append(ep_ret)  
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0

      # current_pol = 0
      current_pol = 2

      if args.lstm_pol: 
        pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]    


def logp(x, mean, std):
  return -1*(0.5 * ((x - mean) / std)**2 + 0.5 * np.log(2.0 * np.pi) + np.log(std))

def mahalanobois_distance(x, mean, std):
  return np.sqrt((x-mean)**2/std**2)

def normpdf(x, mean, std):
    var = std**2
    denom = (2*np.pi*var)**.5
    num = np.exp(-(x-mean)**2/(2*var))
    return num/denom

def calc_probs(data, all_data):
  data = np.array(data)
  all_data = np.array(all_data)
  # print(all_data.mean(), all_data.std())
  # print(all_data.shape)
  # return (data - all_data.mean())/all_data.std()
  return (data - 87.97)/1.97

# def reject_outliers(data, m=2.):
#   d = np.abs(data - np.median(data))
#   mdev = np.median(d)
#   s = d / (mdev if mdev else 1.)
#   return data[s < m]

if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  parser.add_argument('--sub_folder', default='b')
  parser.add_argument('--folder', default='b3')

  parser.add_argument('--difficulty', default=6, type=int)
  parser.add_argument('--multi', default=False, action='store_true')
  parser.add_argument('--goal_set', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--plot', default=False, action='store_true')
  parser.add_argument('--gae', default=False, action='store_true')
  parser.add_argument('--doa', default=False, action='store_true')

  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=False, action='store_true')
  # parser.add_argument('--vis', default=False, action='store_true')
  parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_im', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  # parser.add_argument('--const_std', default=True, action='store_false')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="domain_train_no_stairs", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  parser.add_argument('--max_ts', default=1e5, type=int)
  parser.add_argument('--lr', default=1e-5, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)